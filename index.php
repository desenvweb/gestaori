<?php
session_start();
require 'php/utils.php';

if(!empty($_SESSION['logado']) && !empty($_COOKIE['user'])){
	header('location:paginas/clientes/todosClientes.php');
}else{
	if(!empty($_SESSION['logado'])){
		unset($_SESSION['logado']);
	}

	if(!empty($_COOKIE['user'])){
		unset($_COOKIE['user']);
	}
}

?>
<html lang="pt-BR">

<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="keywords" content= />
	<title>Sistema de Gestão</title>

	<link rel="stylesheet" href="../libs/bootstrap/css/bootstrap.min.css?<?php echo VERSION; ?>" />
	<link rel="stylesheet" href="../css/index.css?<?php echo VERSION; ?>" />
</head>

<body>
	<div class="box flex-box">
		<main class="box--content">
			<section class="form--div">
				<div class="form--title">
                    <h3>Login</h3>
                    <span class="text-muted" style="font-size: 15px;">Sistema de gestão</span>
				</div>

				<form class="form--inputs" id="form-login">
					<input type="email" name="email" class="form--input" placeholder="E-mail" required>
					<input type="password" name="senha" class="form--input" placeholder="Senha" style="margin-top: 10px;" required>
					<button class="input--submit" style="margin-top: 20px;">Entrar</button>
				</form>
				<div class="form--forgotPass">
					<!-- <a href="" data-bs-toggle="modal" data-bs-target="#exampleModal" class="textToClick">Esqueceu a senha?</a> -->
				</div>
			</section>
		</main>

		<div class="copyright">
			<img src="images/redeIndustrial.png" width="40">
			<span>© 2021 Rede Industrial. Todos direitos reservados. <?php echo VERSION; ?></span>
		</div>
	</div>

	<script src="libs/jquery/jquery.js?<?php echo VERSION; ?>"></script>
	<script src="js/functions.js?<?php echo VERSION; ?>"></script>
	<script src="js/login.js?<?php echo VERSION; ?>"></script>
</body>

</html>