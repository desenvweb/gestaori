<?php
session_start();
require '../../php/utils.php';


?>
<!DOCTYPE html>
<html lang="pt-BR">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link href="../../libs/bootstrap/css/bootstrap.min.css?<?php echo VERSION; ?>" rel="stylesheet">
    <link rel="stylesheet" href="../../css/formBase.css?<?php echo VERSION; ?>">
    <link rel="stylesheet" href="../../css/base.css?<?php echo VERSION; ?>">
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/select2-bootstrap-5-theme@1.2.0/dist/select2-bootstrap-5-theme.min.css" />
</head>

<body>
    <div class="box flex-box">
        <div class="content">
            <div class="body">
                    <section class="list--header">
                        <div>
                            <h3 class="header--title">Avaliação de Ticket</h3>
                        </div>
                        <div>
                            <div class="card mb-3 mt-5 mx-5" style="max-width: 540px;">
                                <div class="row g-0">
                                    <div class="col-md-4">
                                    <img src="../../images/redeIndustrial.png" width="100"" class="img-fluid rounded-start m-2">
                                    </div>
                                    <div class="col-md-8">
                                        <div class="card-body">
                                            <h5 class="card-title show--ticket">Ticket</h5>
                                            <p class="card-text show--descricao">Avaliação registrada com sucesso.</p>
                                            <p class="card-text"><small class="text-muted">Agradecemos pela sua avaliação, ela é muito importante para nós.</small></p>
                                        </div>
                                    </div>
                                    <div class="card-footer text-muted">
                                    <span>© 2021 Rede Industrial. Todos direitos reservados. <?php echo VERSION; ?></span>
                                        </div>

                                </div>
                            </div>
                        </div>
                    </section>
            </div>
        </div>
    </div>

    
    <div class="spinner-border text-primary" role="status">
        <span class="visually-hidden">Loading...</span>
    </div>

    <script src="../../js/jquery.js?<?php echo VERSION; ?>"></script>
    <script src="../../libs/jqueryMask/jqueryMask.js?<?php echo VERSION; ?>"></script>
    <script src="../../libs/bootstrap/js/bootstrap.min.js?<?php echo VERSION; ?>"></script>
    <script src="../../js/functions.js?<?php echo VERSION; ?>"></script>
    <script src="../../js/avaliacao.js?<?php echo VERSION; ?>"></script>

</body>

</html>