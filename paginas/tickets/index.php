<?php
session_start();
require '../../php/utils.php';

validaLogin();
logout();


?>
<!DOCTYPE html>
<html lang="pt-BR">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link href="../../libs/bootstrap/css/bootstrap.min.css?<?php echo VERSION; ?>" rel="stylesheet">
    <link rel="stylesheet" href="../../css/formBase.css?<?php echo VERSION; ?>">
    <link rel="stylesheet" href="../../css/base.css?<?php echo VERSION; ?>">
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/select2-bootstrap-5-theme@1.2.0/dist/select2-bootstrap-5-theme.min.css" />
</head>

<body>
    <div class="box flex-box">
        <div class="content">
            <?php
            require '../componentes/header.html';
            require '../componentes/sidebar.html';
            ?>


            <div class="body">
                <div class="list--div">
                    <section class="list--header">
                        <div>
                            <h3 class="header--title">Tickets</h3>
                            <button class="btn--padrao btn-add" data-bs-toggle="modal" data-bs-target="#modalInserirTicket">
                                Criar Ticket
                            </button>
                            <button class="btn--padrao btn-warning" id="gerar_pdf">
                                Gerar PDF
                            </button>
                            <div class="form-check form-check-inline mx-2">
                                <input class="form-check-input" type="checkbox" id="CbGroupbyCliente" value="nome_fantasia">
                                <label class="form-check-label" for="inlineCheckbox1">Agrupar por Cliente</label>
                            </div>
                            <div class="form-check form-check-inline mx-2">
                                <input class="form-check-input" type="checkbox" id="CbGroupbyGut" value="gut">
                                <label class="form-check-label" for="inlineCheckbox1">Prioridade GUT</label>
                            </div>
                        </div>
                        <div>
                            <div class="search--div mt-3" style="display: inline-block;">
                                <input type="text" id="searchInput" class="form-control" style="font-size: 14px; width: 300px;" placeholder="Pesquise pelo Número do Ticket">
                            </div>
                            <button class="btn--padrao btn-warning" data-bs-toggle="offcanvas" data-bs-target="#canvasFiltros" style="color: white; padding: 9px 20px;">
                                Filtros
                            </button>

                            <!--Botão Filtro  

                            <div class="offcanvas offcanvas-end" tabindex="-1" id="canvasFiltros">
                                <div class="offcanvas-header" style="padding-bottom: 0;">
                                    <h5 id="offcanvasTopLabel">Filtros </h5>
                                    <button type="button" class="btn-close text-reset" data-bs-dismiss="offcanvas" aria-label="Close"></button>
                                </div>
                                <div class="offcanvas-body">
                                    <form id="filtros">
                                        <div class="filters--row mt-3">
                                            <h3>Empresa</h3>
                                            <div class="input-rows mt-1">
                                                <div>
                                                    <input type="text" id="empresa" class="form-control">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="filters--row mt-3">
                                            <div>
                                                <h3>Responsável</h3>
                                                <input type="text" id="responsavel" class="form-control">
                                            </div>
                                        </div>
                                        <div class="mt-3">
                                            <button class="btn--padrao btn-warning" id="btn--filtros">
                                                Filtrar
                                            </button>
                                            <button type="reset" class="btn--padrao btn-add" id="btn--limparFiltros">
                                                Limpar
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>

                                <div class="spinner-border text-primary" role="status">
                                    <span class="visually-hidden">Loading...</span>
                                </div>

                            <!-- fim do botao filtro !-->
                        </div>
                    </section>
                    <section class="list--table">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th scope="col">Ticket</th>
                                        <th scope="col">Cliente</th>
                                        <th scope="col">Título</th>
                                        <th scope="col">Responsável</th>
                                        <th scope="col">GUT</th>
                                        <th scope="col">Previsão de Término</th>
                                        <th scope="col">Status</th>
                                        <!-- <th scope='col'>Responsável</th> -->
                                    </tr>
                                </thead>
                                <tbody id="tbody">
                                </tbody>
                                <tbody id="tbodysearch">
                                </tbody>
                            </table>
                        </div>
                    </section>
                </div>

                <div class="offcanvas offcanvas-end" tabindex="-1" id="canvasFiltros">
                    <div class="offcanvas-header" style="padding-bottom: 0;">
                        <h5 id="offcanvasTopLabel">Filtros </h5>
                        <button type="button" class="btn-close text-reset" data-bs-dismiss="offcanvas" aria-label="Close"></button>
                    </div>
                    <div class="offcanvas-body">
                        <form id="filtros">
                            <div class="filters--row mt-3">
                                <h3>Empresa</h3>
                                <div class="input-rows mt-1">
                                    <div>
                                        <input type="text" id="empresa" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="filters--row mt-3">
                                <div>
                                    <h3>Usuário</h3>
                                    <input type="text" id="usuario_cli" class="form-control">
                                </div>
                            </div>
                            <div class="filters--row mt-3">
                                <div>
                                    <h3>Responsável</h3>
                                    <input type="text" id="responsavel" class="form-control">
                                </div>
                            </div>
                            <div class="filters--row mt-3">
                                <h3>Selecione o Status do Ticket</h3>
                                <div>
                                    <input type="radio" id="todos_tkt" name="tkt" value="todos_tkt" checked>
                                    <label for="todos_tkt">Todos</label>
                                </div>

                                <div>
                                    <input type="radio" id="finalizados_tkt" name="tkt" value="finalizados_tkt">
                                    <label for="finalizados_tkt">Finalizados</label>
                                </div>

                                <div>
                                    <input type="radio" id="pendentes_tkt" name="tkt" value="pendentes_tkt">
                                    <label for="pendentes_tkt">Pendentes</label>
                                </div>
                            </div>
                            <div class="mt-3">
                                <button class="btn--padrao btn-warning" id="btn--filtros">
                                    Filtrar
                                </button>
                                <button type="reset" class="btn--padrao btn-add" id="btn--limparFiltros">
                                    Limpar
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <!-- Modal Inserir Ticket -->
    <div class="modal fade" id="modalInserirTicket" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Cadastro de Ticket</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <form method="post" id="form#inserir">
                    <div class="modal-body">
                        <div class="form--area">
                            <div class="form--row">
                                <div>
                                    <label>Título</label>
                                    <input type="text" name="titulo" class="form-control" required>
                                </div>
                                <div>
                                    <label>Setor</label>
                                    <select name="id_setor" class="form-control setor--select" required>
                                        <option value="" selected disabled>Selecione um Setor</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form--row">
                                <div>
                                    <label>Categoria</label>
                                    <select name="id_categoria" class="form-control categoria--select" required>
                                        <option value="" selected disabled>Selecione uma Categoria</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form--row">
                                <div>
                                    <label>Selecione o Cliente</label>
                                    <select name="id_cliente" class="form-control" id='mySelect3' style='width: 100%; height: 50px' required>
                                        <option value="">Selecione a Empresa</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form--row">
                                <div>
                                    <label>Selecione um Usuário</label>
                                    <select name="id_usucliente" class="form-control" id='mySelect2' style='width: 100%; height: 50px' required>
                                        <option value="">Selecione um Usuário</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form--row">
                                <div>
                                    <label>Prazo de Finalização do Ticket</label>
                                    <input type="date" class="form-control" name="dt_prevista">
                                    </div>
                            </div>
                            <div class="form--row">
                                <div>
                                    <label id="lgut_gut">Matriz GUT (para priorizar problemas a serem tratados) - G x U x T</label>
                                    <input type="hidden" name="gut" id="gut_gut">
                                </div>
                            </div>
                            <div class="container">
                                <div>
                                    <label><ABBR title="Impacto do probleam sobre operações e pessoas da empresa. Efeitos que surgirão a logno prazo em caso de não resolução">Gravidade</ABBR></label>
                                    <select name="gravidade" id="gut_gravidade" class="form-control gutGravidade--select" required>
                                        <option value="" selected disabled>Selecione a Gravidade</option>
                                    </select>
                                </div>
                                <div>
                                    <label><ABBR title="O tempo disponível e necessário para resolver o problema">Urgência</ABBR></label>
                                    <select name="urgencia" id="gut_urgencia" class="form-control gutUrgencia--select" required>
                                        <option value="" selected disabled>Selecione a Urgência</option>
                                    </select>
                                </div>
                                <div>
                                    <label><ABBR title="Potencial de crescimento (piora) do problema">Tendência</ABBR></label>
                                    <select name="tendencia" id="gut_tendencia" class="form-control gutTendencia--select" required>
                                        <option value="" selected disabled>Selecione a Tendência</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form--row">
                                <div>
                                    <label>Informações Gerais</label>
                                    <textarea name="informacoes_gerais" class="form-control"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="reset" class="btn btn-secondary" data-bs-dismiss="modal">Cancelar</button>
                        <button class="btn btn-primary">Cadastrar Ticket</button>
                        <button class="btn btn-primary loadingButton" type="button" disabled>
                            <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                            Salvando...
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Modal Editar Ticket -->
    <div class="modal fade" id="modalExibirDados" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title show--ticket" id="labelModalEdit">Ticket</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <form method="post" id="form#editar" enctype="multipart/form-data">
                    <input type="hidden" id="idTicket" name="id_ticket">
                    <div class="modal-body">
                        <button type="button" class="permiss btn--padrao btn-warning" data-bs-toggle="modal" data-bs-target="#modalExibirDialogo">Diálogo do Ticket</button>
                        <br>
                        <br>
                        <div class="formpermiss form--area">
                            <div class="form--row">
                                <div>
                                    <label>Título</label>
                                    <input type="text" name="titulo" id="titulo" class="form-control" required>
                                </div>
                                <div>
                                    <label>Setor</label>
                                    <select name="id_setor" id="id_setor" class="form-control setor--select" required>
                                        <option value="" selected disabled>Selecione um Setor</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form--row">
                                <div>
                                    <label>Categoria</label>
                                    <select name="id_categoria" id="id_categoria" class="form-control categoria--select" required>
                                        <option value="" selected disabled>Selecione uma Categoria</option>
                                    </select>
                                </div>
                                <div>
                                    <label>Responsável</label>
                                    <select name="id_responsavel" id="id_responsavel" class="form-control colaborador--select" required>
                                        <option value="" selected disabled>Selecione um Responsável</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form--row">
                                <div>
                                    <label>Cliente</label>
                                    <input type="text" id="nome_fantasia" class="form-control" disabled>
                                </div>
                                <div>
                                    <label>Usuário</label>
                                    <input type="text" id="usuario" class="form-control" disabled>
                                </div>
                            </div>
                            <div class="form--row">
                                <div>
                                    <label>Prazo de Finalização do Ticket</label>
                                    <input type="date" class="form-control" name="dt_prevista" id="dt_prevista" required>
                                </div>
                                <div>
                                    <label>Status</label>
                                    <select name="id_status" id="id_status" class="form-control statusTicket--select" required>
                                        <option value="" selected disabled>Selecione um Status</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form--row">
                                <div>
                                    <label id="l_gut">Matriz GUT (para priorizar problemas a serem tratados) - G x U x T</label>
                                    <input type="hidden" name="gut" id="gut">
                                </div>
                            </div>
                            <div class="container">
                                <div>
                                    <label><ABBR title="Impacto do problema sobre operações e pessoas da empresa. Efeitos que surgirão a logno prazo em caso de não resolução">Gravidade</ABBR></label>
                                    <select name="gravidade" id="gravidade" class="form-control gutGravidade--select" required>
                                    <option value="" selected disabled>Selecione a Gravidade</option>
                                    </select>
                                </div>
                                <div>
                                    <label><ABBR title="O tempo disponível e necessário para resolver o problema">Urgência</ABBR></label>
                                    <select name="urgencia" id="urgencia" class="form-control gutUrgencia--select" required>
                                        <option value="" selected disabled>Selecione a Urgência</option>
                                    </select>
                                </div>
                                <div>
                                    <label><ABBR title="Potencial de crescimento (piora) do problema">Tendência</ABBR></label>
                                    <select name="tendencia" id="tendencia" class="form-control gutTendencia--select" required>
                                        <option value="" selected disabled>Selecione a Tendência</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form--row">
                                <div>
                                    <label>Informações Gerais</label>
                                    <textarea name="informacoes_gerais" id="informacoes_gerais" class="form-control"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="reset" class="btn btn-secondary" data-bs-dismiss="modal">Cancelar</button>
                        <button class="btn btn-primary">Editar Ticket</button>
                        <button class="btn btn-primary loadingButton" type="button" disabled>
                            <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                            Salvando...
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <?php
    include_once '../componentes/trabalhando_agora.html';
    ?>

    <div class="spinner-border text-primary" role="status">
        <span class="visually-hidden">Loading...</span>
    </div>

    <script src="../../js/jquery.js?<?php echo VERSION; ?>"></script>
    <script src="../../libs/jqueryMask/jqueryMask.js?<?php echo VERSION; ?>"></script>
    <script src="../../libs/bootstrap/js/bootstrap.min.js?<?php echo VERSION; ?>"></script>
    <script src="../../js/functions.js?<?php echo VERSION; ?>"></script>
    <script src="../../js/base.js?<?php echo VERSION; ?>"></script>
    <script src="../../libs/select2.min.js"></script>
    <script src="../../js/ticket.js?<?php echo VERSION; ?>"></script>

</body>

</html>