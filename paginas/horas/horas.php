<?php
session_start();
require '../../php/utils.php';

validaLogin();
logout();


?>
<!DOCTYPE html>
<html lang="pt-BR">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link href="../../libs/bootstrap/css/bootstrap.min.css?<?php echo VERSION; ?>" rel="stylesheet">
    <link rel="stylesheet" href="../../css/formBase.css?<?php echo VERSION; ?>">
    <link rel="stylesheet" href="../../css/base.css?<?php echo VERSION; ?>">
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/select2-bootstrap-5-theme@1.2.0/dist/select2-bootstrap-5-theme.min.css" />
</head>

<body>
    <div class="box flex-box">
        <div class="content">
            <?php
            require '../componentes/header.html';
            require '../componentes/sidebar.html';
            ?>


            <div class="body">
                <div class="list--div">
                    <section class="list--header">
                        <div>
                            <h3 class="header--title">Horas Lançadas</h3>
                            <button class="btn--padrao btn-add" data-bs-toggle="modal" data-bs-target="#modalInserirHoras" style="background-color: royalblue; display: none;">
                                Lançar Horas
                            </button>
                            <button class="btn--padrao btn-warning" data-bs-toggle="modal" data-bs-target="#modalIniciarLD" style="color: white;" id="iniciar_ld">
                                Iniciar Trabalho
                            </button>
                        </div>
                        <div>
                            <div class="search--div mt-3" style="display: inline-block;">
                                <!-- <select class="form-control cliente--select filtro--list" style="font-size: 14px; width: 250px; display: inline-block;">
                                    <option value="" selected disabled>Pesquise por um Cliente</option>
                                </select> -->
                                <select class="form-control colaborador--select filtro--list" style="font-size: 14px; width: 250px; display: inline-block; display: none;">
                                    <option value="" selected disabled>Pesquise por um Colaborador</option>
                                </select>
                                <button class="btn--padrao btn-warning" id="btn--limparFiltro" style="color: white; padding: 9px 20px;">
                                    Limpar
                                </button>
                                <!-- <button class="btn--padrao btn-add" data-bs-toggle="offcanvas" data-bs-target="#canvasFiltros" style="color: white; padding: 9px 20px;">
                                    Filtros
                                </button> -->
                            </div>
                        </div>
                    </section>
                    <section class="list--table">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th scope="col">Cliente</th>
                                        <th scope="col">Ticket</th>
                                        <th scope="col">Descricao</th>
                                        <th scope="col">Colaborador</th>
                                        <th scope="col">Início</th>
                                        <th scope="col">Fim</th>
                                        <th scope="col">Diferença</th>
                                    </tr>
                                </thead>
                                <tbody id="tbody">
                                </tbody>
                                <tbody id="tbodysearch">
                                </tbody>
                            </table>
                        </div>
                    </section>
                </div>
            </div>

        </div>
    </div>

    <!-- Modal LD -->
    <div class="modal fade" id="modalIniciarLD" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Lançamento Direto</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <form id="iniciarLD">
                    <div class="modal-body">
                        <div class="form--row">
                            <div>
                                <label>Empresa</label>
                                <select name="id_empresa" class="form-control selectEmpresa" style='width: 100%; height: 50px' required>
                                    <option value="">Selecione a Empresa</option>
                                </select>
                            </div>
                        </div>
                        <div class="form--row">
                            <div>
                                <label>Ticket</label>
                                <select name="id_ticket" class="form-control selectTicket" style='width: 100%; height: 50px' required>
                                    <option value="">Selecione um Ticket</option>
                                </select>
                            </div>
                        </div>
                        <div class="form--row">
                            <div>
                                <label for="atividade_ld">Atividade</label>
                                <select name="atividade" id="atividade_ld" class="form-control atividade--select" required>
                                    <option value="" selected="" disabled="">Selecione uma Atividade</option>
                                </select>
                            </div>
                        </div>
                        <div class="form--row">
                            <div>
                                <label for="descricao_ld">Descrição</label>
                                <textarea name="descricao" id="descricao_ld" class="form-control" required></textarea>
                            </div>
                        </div>
                        <div class="form--row mt-3">
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" value="" id="checkbox_projeto">
                                <label class="form-check-label" for="checkbox_projeto">
                                    Lançamento em algum projeto?
                                </label>
                            </div>
                            <div>
                                <select name="id_projeto" id="projeto_ld" class="projetos--select form-control" required disabled>
                                    <option value="" selected="" disabled="">Selecione um Projeto</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="reset" class="btn btn-secondary defaultButton" data-bs-dismiss="modal">Cancelar</button>
                        <button class="btn btn-primary defaultButton">Iniciar Trabalho</button>
                        <button class="btn btn-primary loadingButton" type="button" disabled style="display: none;">
                            <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                            Salvando...
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Modal Lançar Horas -->
    <div class="modal fade" id="modalInserirHoras" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Iniciar Trabalho</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <form method="post" id="form#inserir">
                    <input type="hidden" name="tipo_contratado" value="PS">
                    <div class="modal-body">
                        <div class="form--area">
                            <div class="form--row">
                                <div>
                                    <label>Colaborador</label>
                                    <select name="id_usuario" class="colaborador--select form-control" required>
                                        <option selected disabled>Selecione um Colaborador</option>
                                    </select>
                                </div>
                                <div>
                                    <label>Empresa</label>
                                    <select name="id_empresa" class="form-control" id="selectEmpresa" style='width: 100%; height: 50px' required>
                                        <option value="">Selecione a Empresa</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form--row">
                                <div>
                                    <label>Data</label>
                                    <input type="date" name="dt_lancamento" class="form-control" required>
                                </div>
                                <div>
                                <label>Ticket</label>
                                <select name="id_ticket" class="form-control" id="selectTicket" style='width: 100%; height: 50px'>
                                    <option value="">Selecione um Ticket</option>
                                </select>
                            </div>
                            </div>
                            <div class="form--row">
                                <div>
                                    <label>Hora inicial</label>
                                    <input type="time" name="hora_ini" class="form-control" step="1" required>
                                </div>
                                <div>
                                    <label>Hora Final</label>
                                    <input type="time" name="hora_fim" class="form-control" step="1" required>
                                </div>
                            </div>
                            <div class="form--row">
                                <div>
                                    <label>Descrição</label>
                                    <textarea name="descricao" class="form-control" required></textarea>
                                </div>
                                <div>
                                    <label>Atividade</label>
                                    <select name="id_atividade" class="form-control atividade--select" required>
                                        <option value="" selected="" disabled="">Selecione uma Atividade</option>
                                    </select>
                                </div>
                            </div>
                            <!-- <div class="form--row mt-3">
                                <div>
                                    <select name="id_projeto" class="projetos--select form-control">
                                        <option value="" selected="" disabled="">Selecione um Projeto</option>
                                    </select>
                                </div>
                            </div> -->
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="reset" class="btn btn-secondary defaultButton" data-bs-dismiss="modal">Cancelar</button>
                        <button class="btn btn-primary defaultButton">Lançar Horas</button>
                        <button class="btn btn-primary loadingButton" type="button" disabled style="display: none;">
                            <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                            Salvando...
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Modal Exibir Dados -->
    <div class="modal fade" id="modalExibirDados" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Dados do Lançamento</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>

                <form class="formpermiss" method="post" id="form#editar">
                    <input type="hidden" name="id" id="id_lancamento">
                    <div class="modal-body">
                        <button class="btn--padrao defaultButton" id="excluirBtn" style="background-color: #ff2800; display: none;">
                            Excluir Lançamento
                        </button>
                        <div class="form--area mt-2">
                            <div class="form--row">
                                <div>
                                    <label>Colaborador</label>
                                    <select name="id_usuario" id="id_usuario" class="colaborador--select form-control" required>
                                        <option value="">Selecione o Colaborador</option>
                                    </select>
                                </div>
                                <div>
                                    <label>Empresa</label>
                                    <select name="id_empresa" class="form-control" id="selectEmpresa2" style='width: 100%; height: 50px' required>
                                        <option value="">Selecione a Empresa</option>
                                    </select>
                                </div>
                            </div>
                            <input type="hidden" id="id_projeto" name="id_projeto" class="form-control">
                            <div class="form--row">
                                <div>
                                    <label>Data</label>
                                    <input type="date" id="dt_lancamento" name="dt_lancamento" class="form-control" required>
                                </div>
                                <div>
                                <label>Ticket</label>
                                <select name="id_ticket" class="form-control" id="selectTicket2" style='width: 100%; height: 50px'>
                                    <option value="">Selecione um Ticket</option>
                                </select>
                            </div>
                            </div>

                            <div class="form--row">
                                <div>
                                    <label>Hora inicial</label>
                                    <input type="time" id="ini_lancamento" name="hora_ini" class="form-control" step="1" required>
                                </div>
                                <div>
                                    <label>Hora Final</label>
                                    <input type="time" id="fim_lancamento" name="hora_fim" class="form-control" step="1" required>
                                </div>
                            </div>

                            <div class="form--row">
                                <div>
                                    <label>Descrição</label>
                                    <textarea name="descricao" id="descricao" class="form-control" required></textarea>
                                </div>
                                <div>
                                    <label>Atividade</label>
                                    <select name="id_atividade" class="form-control atividade--select" id="id_atividade" required>
                                        <option value="" selected="" disabled="">Selecione uma Atividade</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form--row">
                                <div>
                                    <label>Projeto do Lançamento</label>
                                    <select name="id_projetos" id="id_projetos" class="projetos--select form-control">
                                        <option value="" selected="" disabled="">Projeto</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="reset" class="btn btn-secondary defaultButton" data-bs-dismiss="modal">Cancelar</button>
                        <button class="btn btn-primary defaultButton">Editar Horas</button>
                        <button class="btn btn-primary loadingButton" type="button" disabled style="display: none;">
                            <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                            Salvando...
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>


    <?php
    include_once '../componentes/trabalhando_agora.html';
    ?>

    <div class="spinner-border text-primary" role="status">
        <span class="visually-hidden">Loading...</span>
    </div>

    <script src="../../js/jquery.js?<?php echo VERSION; ?>"></script>
    <script src="../../libs/jqueryMask/jqueryMask.js?<?php echo VERSION; ?>"></script>
    <script src="../../libs/bootstrap/js/bootstrap.min.js?<?php echo VERSION; ?>"></script>
    <script src="../../js/functions.js?<?php echo VERSION; ?>"></script>
    <script src="../../js/base.js?<?php echo VERSION; ?>"></script>
    <script src="../../libs/select2.min.js"></script>
    <script src="../../js/horas.js?<?php echo VERSION; ?>"></script>

</body>

</html>