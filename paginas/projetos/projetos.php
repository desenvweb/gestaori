<?php
session_start();
require '../../php/utils.php';

validaLogin();
logout();


?>
<!DOCTYPE html>
<html lang="pt-BR">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link href="../../libs/bootstrap/css/bootstrap.min.css?<?php echo VERSION; ?>" rel="stylesheet">
    <link rel="stylesheet" href="../../css/formBase.css?<?php echo VERSION; ?>">
    <link rel="stylesheet" href="../../css/base.css?<?php echo VERSION; ?>">
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/select2-bootstrap-5-theme@1.2.0/dist/select2-bootstrap-5-theme.min.css" />

    <style>
        .saas-div {
            display: none;
        }
    </style>
    <title>Sistema de Gestão</title>
</head>

<body>
    <div class="box flex-box">
        <div class="content">
            <?php
            require '../componentes/header.html';
            require '../componentes/sidebar.html';
            ?>


            <div class="body">
                <div class="list--div">
                    <section class="list--header">
                        <div>
                            <h3 class="header--title">Projetos</h3>
                            <button class="btn--padrao btn-add" data-bs-toggle="modal" data-bs-target="#modalInserirProjeto" style="display: none;">
                                Adicionar Projeto
                            </button>
                        </div>
                        <div>
                            <div class="search--div mt-3" style="display: inline-block;">
                                <input type="text" id="searchInput" class="form-control" style="font-size: 14px; width: 300px;" placeholder="Pesquise pelo Nome do Projeto">
                            </div>
                        </div>
                    </section>
                    <section class="list--table">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th scope="col">Nome</th>
                                        <th scope="col">Responsável</th>
                                        <th scope="col">Prev. de Término</th>
                                        <th scope="col">Status</th>
                                    </tr>
                                </thead>
                                <tbody id="tbody">
                                </tbody>
                                <tbody id="tbodysearch">
                                </tbody>
                            </table>
                        </div>
                    </section>
                </div>
            </div>

        </div>
    </div>

    <!-- Modal Inserir Projeto -->
    <div class="modal fade" id="modalInserirProjeto" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Cadastro de Projetos</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <form method="post" id="form#inserir">
                    <div class="modal-body">
                        <div class="form--area">
                            <div class="form--row">
                                <div>
                                    <label>Nome do Projeto</label>
                                    <input type="text" name="nome" class="form-control" required>
                                </div>
                                <div>
                                    <label>Selecione o Cliente</label>
                                    <select name="id_empresa" class="form-control" id='mySelect3' style='width: 100%; height: 50px' required>
                                        <option value="">Selecione o Cliente</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form--row">
                                <div>
                                    <label>Responsável</label>
                                    <select name="id_responsavel" class="form-control colaborador--select" required>
                                        <option value="" selected disabled>Selecione um Responsável</option>
                                    </select>
                                </div>
                                <div>
                                    <label>Executante</label>
                                    <select name="id_executante" class="form-control colaborador--select">
                                        <option value="" selected disabled>Selecione um Executante</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form--row">
                                <div>
                                    <label>Comercial</label>
                                    <select name="id_comercial" id="inputComercial_insert" class="form-control colaborador--select">
                                        <option value="" selected disabled>Selecione um Comercial</option>
                                    </select>
                                </div>
                                <div>
                                    <label>Previsão de Término</label>
                                    <input type="date" name="prev_termino" class="form-control">
                                </div>
                            </div>
                            <div class="form--row">
                                <div>
                                    <label>Total de Horas</label>
                                    <input type="text" name="total_horas" class="form-control onlynumber" required>
                                </div>
                                <div>
                                    <label>Descrição do Projeto</label>
                                    <textarea class="form-control" name="descricao" maxlength="1100" required></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="reset" class="btn btn-secondary defaultButton" data-bs-dismiss="modal" style="display: none;">Cancelar</button>
                        <button class="btn btn-primary defaultButton" style="display: none;">Cadastrar Projeto</button>
                        <button class="btn btn-primary loadingButton" type="button" disabled>
                            <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                            Salvando...
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Modal Editar Projeto -->
    <div class="modal fade" id="modalExibirDados" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Cadastro de Projetos</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <form class="formpermiss" method="post" id="form#editar" enctype="multipart/form-data">
                    <input type="hidden" id="idProjeto" name="id">
                    <div class="modal-body">
                        <div class="form--area">
                            <div class="form--row mb-3">
                                <div>
                                    <input class="form-check-input" type="checkbox" value="S" id="desconta_horas" name="desconta_horas">
                                    <label class="form-check-label" for="desconta_horas" style="margin-left: 5px;">
                                        Descontar horas?
                                    </label>
                                </div>
                            </div>
                            <div class="form--row">
                                <div>
                                    <label>Nome do Projeto</label>
                                    <input type="text" name="nome" id="nome" class="form-control" required>
                                </div>
                                <div>
                                    <label>Cliente</label>
                                    <select name="id_empresa" id="id_empresa" class="form-control" style='width: 100%; height: 50px' required>
                                    </select>
                                </div>
                            </div>
                            <div class="form--row">
                                <div>
                                    <label>Responsável</label>
                                    <select name="id_responsavel" id="id_responsavel" class="form-control colaborador--select" required>
                                        <option value="" selected disabled>Selecione um Responsável</option>
                                    </select>
                                </div>
                                <div>
                                    <label>Executante</label>
                                    <select name="id_executante" id="id_executante" class="form-control colaborador--select">
                                        <option value="" selected disabled>Selecione um Executante</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form--row">
                                <div>
                                    <label>Comercial</label>
                                    <select name="id_comercial" id="id_comercial" class="form-control colaborador--select">
                                        <option value="" selected disabled>Selecione um Comercial</option>
                                    </select>
                                </div>
                                <div>
                                    <label>Previsão de Término</label>
                                    <input type="date" name="prev_termino" id="prev_termino" class="form-control">
                                </div>
                            </div>
                            <div class="form--row">
                                <div>
                                    <label>Total de Horas</label>
                                    <input type="text" name="total_horas" id="total_horas" class="form-control onlynumber" required>
                                </div>
                                <div>
                                    <label>Status</label>
                                    <select name="id_status" id="id_status" class="form-control status--select" required>
                                        <option value="" selected disabled>Selecione um Status</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form--row">
                                <div>
                                    <label>Descrição do Projeto</label>
                                    <textarea class="form-control" name="descricao" id="descricao" maxlength="1100" required></textarea>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="reset" class="btn btn-secondary defaultButton" data-bs-dismiss="modal">Cancelar</button>
                        <button class="btn btn-primary defaultButton">Editar Projeto</button>
                        <button class="btn btn-primary loadingButton" type="button" disabled>
                            <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                            Salvando...
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>


    <?php
    include_once '../componentes/trabalhando_agora.html';
    ?>

    <div class="spinner-border text-primary" role="status">
        <span class="visually-hidden">Loading...</span>
    </div>

    <script src="../../js/jquery.js?<?php echo VERSION; ?>"></script>
    <script src="../../libs/jqueryMask/jqueryMask.js?<?php echo VERSION; ?>"></script>
    <script src="../../libs/bootstrap/js/bootstrap.min.js?<?php echo VERSION; ?>"></script>
    <script src="../../js/functions.js?<?php echo VERSION; ?>"></script>
    <script src="../../js/base.js?<?php echo VERSION; ?>"></script>
    <script src="../../libs/select2.min.js"></script>
    <script src="../../js/projetos.js?<?php echo VERSION; ?>"></script>
</body>

</html>