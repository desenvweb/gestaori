<?php
session_start();
require '../../php/utils.php';

validaLogin();
logout();

?>
<!DOCTYPE html>
<html lang="pt-BR">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link href="../../libs/bootstrap/css/bootstrap.min.css?<?php echo VERSION; ?>" rel="stylesheet">
    <link rel="stylesheet" href="../../css/formBase.css?<?php echo VERSION; ?>">
    <link rel="stylesheet" href="../../css/base.css?<?php echo VERSION; ?>">
    <title>Sistema de Gestão</title>
</head>

<body>
    <div class="box flex-box">
        <div class="content">
            <?php
            require '../componentes/header.html';
            require '../componentes/sidebar.html';
            ?>


            <div class="body">
                <div class="list--div">
                    <section class="list--header">
                        <h3 class="header--title">Usuários/Colaboradores</h3>
                        <button class="btn--padrao btn-add" data-bs-toggle="modal" data-bs-target="#modalInserirCliente" style="display: none;">
                            Adicionar Usuário
                        </button>
                        <div class="search--div mt-3">
                            <input type="search" id="searchInput" class="form-control" style="font-size: 14px; width: 300px;" placeholder="Pesquise pelo nome">
                        </div>
                    </section>
                    <section class="list--table">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th scope="col">Nome</th>
                                        <th scope="col">Função</th>
                                        <th scope="col">Dias Trabalhados</th>
                                        <th class="hidepermiss" scope="col">Instrução</th>
                                        <th class="hidepermiss" scope="col">Avaliação</th>
                                        <th scope="col">Status</th>
                                        <th scope="col">Ações</th>
                                    </tr>
                                </thead>
                                <tbody id="tbody">
                                </tbody>
                                <tbody id="tbodysearch">
                                </tbody>
                            </table>
                        </div>
                    </section>
                </div>
            </div>

        </div>
    </div>


    <!-- Modal Inserir Cliente -->
    <div class="modal fade" id="modalInserirCliente" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Cadastro de Usuário</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <form method="post" id="form#inserir" enctype="multipart/form-data">
                    <div class="modal-body">
                        <div class="form--area">
                            <h3>Informações Básicas</h3>
                            <div class="form--row">
                                <!--<div>
                                    <label>Status</label>
                                    <select class="form-control" name="status" required>
                                        <option value="ativo">Ativo</option>
                                        <option value="ativo">Inativo</option>
                                    </select>
                                </div> -->
                                <div>
                                    <label>Nome</label>
                                    <input type="text" name="nome" class="form-control" required>
                                </div>
                            </div>
                            <div class="form--row">
                                <div>
                                    <label>Tipo de Colaborador</label>
                                    <select class="form-control" id="tipoColaborador_insert" name="tipo_colaborador">
                                        <option value="PJ">Pessoa Jurídica</option>
                                        <option value="CLT">CLT</option>
                                    </select>
                                </div>
                                <div>
                                    <label for="certidao_insert">CNPJ</label>
                                    <input type="text" name="cnpj" id="certidao_insert" class="form-control" required>
                                </div>
                            </div>
                            <div class="form--row">
                                <div>
                                    <label>Setor</label>
                                    <select class="form-control setor--select" name="id_setor">
                                        <option value="" selected disabled>Selecione o setor</option>
                                    </select>
                                </div>
                                <div>
                                    <label>Função</label>
                                    <input type="text" name="funcao" class="form-control" required>
                                </div>
                            </div>
                            <h3>Localização</h3>
                            <div class="form--row">
                                <div>
                                    <label>CEP</label>
                                    <input type="text" name="cep" class="form-control">
                                </div>
                                <div>
                                    <label>Logradouro</label>
                                    <input type="text" name="logradouro" class="form-control">
                                </div>
                            </div>
                            <div class="form--row">
                                <div>
                                    <label>Número</label>
                                    <input type="text" name="numero" maxlength="10" class="form-control">
                                </div>
                                <div>
                                    <label>Bairro</label>
                                    <input type="text" name="bairro" class="form-control">
                                </div>
                            </div>
                            <div class="form--row">
                                <div>
                                    <label>Cidade</label>
                                    <input type="text" name="cidade" class="form-control">
                                </div>
                                <div>
                                    <label>Estado</label>
                                    <input type="text" name="estado" class="form-control">
                                </div>
                            </div>
                            <h3>Contato</h3>
                            <div class="form--row">
                                <div>
                                    <label>Telefone</label>
                                    <input type="text" name="telefone" class="form-control" required>
                                </div>
                                <div>
                                    <label>Skype</label>
                                    <input type="text" name="skype" class="form-control" required>
                                </div>
                            </div>
                            <h3>Acesso</h3>
                            <div class="form--row">
                                <div>
                                    <label>E-mail</label>
                                    <input type="text" name="email" class="form-control" required>
                                </div>
                            </div>
                            <div class="form--row">
                                <div>
                                    <label>Senha</label>
                                    <input type="password" id="senhaInicial" name="senha" class="form-control" required>
                                </div>
                                <div>
                                    <label>Confirme a senha</label>
                                    <input type="password" id="confirmSenha" class="form-control" required>
                                </div>
                            </div>
                            <h3>Outros dados</h3>
                            <div class="form--row">
                                <div>
                                    <label>Tipo de Usuário</label>
                                    <select name="tipo_usuario" class="form-control" required>
                                        <option value="1">Administrador</option>
                                        <option value="2">Colaborador</option>
                                    </select>
                                </div>
                                <div>
                                    <label>Salário</label>
                                    <input type="text" name="salario" class="form-control money">
                                </div>
                            </div>
                            <div class="form--row">
                                <div>
                                    <label>Vencimento das Férias</label>
                                    <input type="date" class="form-control" name="vencimento_ferias">
                                </div>
                                <div>
                                    <label>Direito às Férias</label>
                                    <input type="date" class="form-control" name="direito_ferias">
                                </div>
                            </div>
                            <div class="form--row">
                                <div>
                                    <label>Data de Início</label>
                                    <input type="date" class="form-control" name="dt_inicio_trabalho" required>
                                </div>
                                <div>
                                    <label>Vencimento do Contrato para PJ</label>
                                    <input type="date" class="form-control" name="vencimento_contrato">
                                </div>
                            </div>
                            <div class="form--row divShow" style="display: none;">
                                <div>
                                    <label>Experiências</label>
                                    <textarea name="experiencia"class="form-control"></textarea>
                                </div>
                            </div>
                            <div class="form--row">
                                <div>
                                    <label>Instrução</label>
                                    <input type="number" name="instrucao" min="1" max="10" step="0.5" class="form-control">
                                </div>
                                <div>
                                    <label>Avaliação</label>
                                    <input type="text" name="avaliacao" class="form-control">
                                </div>
                            </div>
                            <div class="form--row">
                                <div>
                                    <input type="checkbox" name="super_admin" >
                                    <label for="super_admin">Super Admin</label>
                                </div>
                            </div>
                            <h3>Anexos</h3>
                            <div class="form--row">
                                <div>
                                    <label class="mt-2">Anexo de documentos</label>
                                    <input type="file" name="anexo[]" class="form-control" multiple="multiple">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="reset" class="btn btn-secondary defaultButton" data-bs-dismiss="modal">Cancelar</button>
                        <button class="btn btn-primary defaultButton">Inserir Usuário/Colaborador</button>
                        <button class="btn btn-primary loadingButton" type="button" disabled style="display: none;">
                            <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                            Salvando...
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Modal Exibir Dados -->
    <div class="modal fade" id="modalExibirDados" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Dados do Usuário</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>

                <form class="formpermiss" method="post" id="form#editar">
                    <input type="hidden" name="id" id="idUsuario">
                    <div class="modal-body">
                        <button type="button" class="btn--padrao" id="excluirBtn" style="background-color: #ff2800;">Excluir Usuário</button>
                        <!--<button type="button" class="btn--padrao btn-warning" data-bs-toggle="modal" data-bs-target="#modalRelatórios">Horas Lançadas</button>-->
                        <br>
                        <br>
                        <div class="form--area">
                            <h3>Informações Básicas</h3>
                            <div class="form--row">
                                <div>
                                    <label>Nome</label>
                                    <input type="text" name="nome" id="nome" class="form-control" required>
                                </div>
                            </div>
                            <div class="form--row divShow" style="display: none;">
                                <div>
                                    <label>Tipo de Colaborador</label>
                                    <select class="form-control" id="tipo_colaborador" name="tipo_colaborador">
                                        <option value="PJ">Pessoa Jurídica</option>
                                        <option value="CLT">CLT</option>
                                    </select>
                                </div>
                                <div>
                                    <label for="cnpj">CNPJ</label>
                                    <input type="text" name="cnpj" id="cnpj" id="cnpj" class="form-control">
                                </div>
                            </div>
                            <div class="form--row">
                                <div>
                                    <label>Setor</label>
                                    <select class="form-control setor--select" name="id_setor" id="id_setor" required>
                                        <option value="" selected disabled>Selecione o setor</option>
                                    </select>
                                </div>
                                <div>
                                    <label>Função</label>
                                    <input type="text" name="funcao" id="funcao" class="form-control" required>
                                </div>
                            </div>
                            <h3 style="display: none;">Localização</h3>
                            <div class="form--row divShow" style="display: none;">
                                <div class="form--row">
                                    <div>
                                        <label>CEP</label>
                                        <input type="text" name="cep" id="cep" class="form-control">
                                    </div>
                                    <div>
                                        <label>Logradouro</label>
                                        <input type="text" name="logradouro" id="logradouro" class="form-control">
                                    </div>
                                </div>
                                <div class="form--row">
                                    <div>
                                        <label>Número</label>
                                        <input type="text" name="numero" id="numero" maxlength="10" class="form-control">
                                    </div>
                                    <div>
                                        <label>Bairro</label>
                                        <input type="text" name="bairro" id="bairro" class="form-control">
                                    </div>
                                </div>
                                <div class="form--row">
                                    <div>
                                        <label>Cidade</label>
                                        <input type="text" name="cidade" id="cidade" class="form-control">
                                    </div>
                                    <div>
                                        <label>Estado</label>
                                        <input type="text" name="estado" id="estado" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <h3 style="display: none;">Contato</h3>
                            <div class="form--row divShow" style="display: none;">
                                <div>
                                    <label>Telefone</label>
                                    <input type="text" name="telefone" id="telefone" class="form-control" required>
                                </div>
                                <div>
                                    <label>Skype</label>
                                    <input type="text" name="skype" id="skype" class="form-control" required>
                                </div>
                            </div>
                            <h3 class="divShow" style="display: none;">Acesso</h3>
                            <div class="form--row divShow" style="display: none;">
                                    <div>
                                        <label>E-mail</label>
                                        <input type="text" name="email" id="email" class="form-control" required>
                                    </div>
                            </div>
                            <div class="form--row divShow" style="display: none;">
                                    <div>
                                        <label>Senha</label>
                                        <input type="password" name="senha" id="senhaInicial2" class="form-control">
                                    </div>
                                    <div>
                                        <label>Confirme a senha</label>
                                        <input type="password" id="confirmSenha2" class="form-control">
                                    </div>
                            </div>
                            <h3 class="divShow" style="display: none;">Outros dados</h3>
                            <div class="form--row divShow" style="display: none;">
                                <div>
                                    <label>Tipo de Usuário</label>
                                    <select name="tipo_usuario" id="tipo_usuario" class="form-control" required>
                                        <option value="1">Administrador</option>
                                        <option value="2">Colaborador</option>
                                    </select>
                                </div>
                                <div>
                                    <label>Salário</label>
                                    <input type="text" name="salario" id="salario" class="form-control money">
                                </div>
                            </div>
                            <div class="form--row divShow" style="display: none;">
                                <div>
                                    <label>Vencimento das Férias</label>
                                    <input type="date" class="form-control" name="vencimento_ferias" id="vencimento_ferias">
                                </div>
                                <div>
                                    <label>Direito às Férias</label>
                                    <input type="date" class="form-control" name="direito_ferias" id="direito_ferias">
                                </div>
                            </div>
                            <div class="form--row divShow" style="display: none;">
                                <div>
                                    <label>Data de Início</label>
                                    <input type="date" class="form-control" name="dt_inicio_trabalho" id="dt_inicio_trabalho" required>
                                </div>

                                <div>
                                    <label>Vencimento do Contrato para PJ</label>
                                    <input type="date" class="form-control" name="vencimento_contrato" id="vencimento_contrato">
                                </div>
                            </div>
                            <div class="form--row divShow" style="display: none;">
                                <div>
                                    <label>Experiências</label>
                                    <textarea name="experiencia" id="experiencia" class="form-control"></textarea>
                                </div>
                            </div>
                            <div class="form--row">
                                <div class="form--row divShow" style="display: none;">
                                    <div>
                                        <label>Instrução</label>
                                        <input type="number" id="instrucao" name="instrucao" min="1" max="10" step="0.5" class="form-control">
                                    </div>
                                    <div>
                                        <label>Avaliação</label>
                                        <input type="text" id="avaliacao" name="avaliacao" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="form--row">
                                <div class="form--row divShow" style="display: none;">
                                    <div>
                                        <input type="checkbox" name="super_admin" id="super_admin">
                                        <label for="super_admin">Super Admin</label>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <h3 class="hidepermiss" style="margin-bottom: 10px;">Anexos</h3>
                            <div class="form--row mb-2 hidepermiss">
                                <div>
                                    <label class="mt-2">Adicionar anexo de documentos</label>
                                    <input type="file" name="anexo[]" class="form-control" multiple="multiple">
                                </div>
                            </div>
                            <div class="form--row anexo--listItens hidepermiss">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="reset" class="btn btn-secondary defaultButton" data-bs-dismiss="modal">Cancelar</button>
                        <button class="btn btn-primary defaultButton">Editar Usuário</button>
                        <button class="btn btn-primary loadingButton" type="button" disabled style="display: none;">
                            <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                            Salvando...
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>


    <!-- Modal Trabalho -->
    <div class="modal fade" id="modalExibirTrabalho" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title show--ticket" id="labelModalEdit">Trabalho em Execução</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <form enctype="multipart/form-data">
                    <input type="hidden" id="idTicket" name="id_ticket">
                    <div class="modal-body">
                        <br>
                        <div class="formpermiss form--area">
                            <div class="form--row">
                                <div>
                                    <label>Ticket</label>
                                    <input type="text" name="ticketTrabalho" id="ticketTrabalho" class="form-control">
                                </div>
                                <div>
                                    <label>Titulo</label>
                                    <input type="text" name="tituloTrabalho" id="tituloTrabalho" class="form-control">
                                </div>
                            </div>
                            <div class="form--row">
                                <div>
                                    <label>Empresa</label>
                                    <input type="text" name="empresaTrabalho" id="empresaTrabalho" class="form-control">
                                </div>
                                <div>
                                    <label>Atividade</label>
                                    <input type="text" name="atividadeTrabalho" id="atividadeTrabalho" class="form-control">
                                </div>
                            </div>
                            <div class="form--row">
                                <div>
                                <label>Data de Início</label>
                                    <input type="datetime" class="form-control" name="dt_inicio" id="dt_inicio">
                                </div>
                                <div>
                                <label>Prazo de Finalização do Ticket</label>
                                    <input type="date" class="form-control" name="dt_previstaTrabalho" id="dt_previstaTrabalho">
                                </div>
                            </div>
                            <div class="form--row">
                                <div>
                                    <label>Informações do Trabalho</label>
                                    <textarea name="informacoesTrabalho" id="informacoesTrabalho" class="form-control"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="reset" class="btn btn-primary" data-bs-dismiss="modal">Fechar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <?php
    include_once '../componentes/trabalhando_agora.html';
    ?>

    <div class="spinner-border text-primary" role="status">
        <span class="visually-hidden">Loading...</span>
    </div>

    <div class="modal fade" id="modalRelatórios" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="titulo_relatorio">Relatório de Horas</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <form id="relatorio_form">
                    <div class="modal-body">
                        <div style="display: flex; align-items: center; gap: 10px;">
                            <div>
                                <label>Cliente:</label>
                                <select class="form-control cliente--select filtro--list" name="cliente">
                                    <option value="" selected disabled="">Selecione um Cliente</option>
                                </select>
                            </div>
                            <div>
                                <label>Início:</label>
                                <input type="date" name="dt_inicial" class="form-control">
                            </div>
                            <div>
                                <label>Fim:</label>
                                <input type="date" name="dt_final" class="form-control">
                            </div>
                        </div>
                        <br>
                        <button type="reset" class="btn btn-secondary defaultButton" id="limpar_relatorio">Limpar</button>
                        <button class="btn btn-primary defaultButton">Pesquisar</button>
                        <button type="button" class="btn btn-primary" id="gerar_pdf" style="">Gerar PDF</button>
                        <button class="btn btn-primary loadingButton" type="button" disabled style="display: none;">
                            <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                            Pesquisando...
                        </button>
                        <br>
                        <br>
                        <br>
                        <div class="dados--row">
                            <span class="text-muted">Tempo Trabalhado no Período</span>
                            <h4 id="tempo_periodo"></h4>
                        </div>
                        <div class="dados--row">
                            <span class="text-muted">Total de Serviços</span>
                            <h4 id="total_servicos"></h4>
                        </div>
                        <br>
                        <br>
                        <table class="table">
                            <thead>
                                <tr>
                                    <th scope="col">Ticket</th>
                                    <th scope="col">Cliente</th>
                                    <th scope="col">Início</th>
                                    <th scope="col">Fim</th>
                                    <th scope="col">Diferença</th>
                                </tr>
                            </thead>
                            <tbody id="tbodyRelatorio">
                            </tbody>
                        </table>
                    </div>

                </form>
            </div>
        </div>
    </div>


    <script src="../../libs/bootstrap/js/bootstrap.min.js?<?php echo VERSION; ?>"></script>
    <script src="../../js/jquery.js?<?php echo VERSION; ?>"></script>
    <script src="../../libs/jqueryMask/jqueryMask.js?<?php echo VERSION; ?>"></script>
    <script src="../../js/functions.js?<?php echo VERSION; ?>"></script>
    <script src="../../js/base.js?<?php echo VERSION; ?>"></script>
    <script src="../../js/usuario.js?<?php echo VERSION; ?>"></script>
</body>

</html>