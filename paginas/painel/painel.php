<?php
session_start();
require '../../php/utils.php';


validaLogin();

if (isset($_GET['sair'])) {
    unset($_SESSION['logado']);
    unset($_COOKIE['user']);
    header('location:../index.php');
}
?>
<!DOCTYPE html>
<html lang="pt-BR">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css">
    <link href="../../libs/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="../../css/base.css">
    <title>Sistema de gestão</title>
</head>

<body>
    <div class="box flex-box">
        <div class="content">
            <?php
            require '../componentes/header.html';
            require '../componentes/sidebar.html';
            ?>


            <div class="body">
                <?php
                if (isset($_GET['permissInsuf'])) { ?>
                    <div class="alert alert-danger" role="alert">
                        Permissão insuficiente.
                    </div>
                <?php } ?>
            </div>

        </div>
    </div>

    <?php
    include_once '../componentes/trabalhando_agora.html';
    ?>

    <script src="../../js/functions.js"></script>
    <script src="../../js/base.js"></script>
</body>

</html>