<?php
session_start();
require '../../php/utils.php';

validaLogin();
logout();

?>
<!DOCTYPE html>
<html lang="pt-BR">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link href="../../libs/bootstrap/css/bootstrap.min.css?<?php echo VERSION; ?>" rel="stylesheet">
    <link rel="stylesheet" href="../../css/formBase.css?<?php echo VERSION; ?>">
    <link rel="stylesheet" href="../../css/base.css?<?php echo VERSION; ?>">
    <style>
        .saas-div {
            display: none;
        }
    </style>
    <title>Sistema de Gestão</title>
</head>

<body>
    <div class="box flex-box">
        <div class="content">

            <?php
            require '../componentes/header.html';
            require '../componentes/sidebar.html';
            ?>

            <div class="body">
                <div class="list--div">
                    <section class="list--header">
                        <div>
                            <h3 class="header--title">Clientes</h3>
                            <button class="btn--padrao btn-add" data-bs-toggle="modal" data-bs-target="#modalInserirCliente" style="display: none;" onclick="viewSaas('')">Adicionar Cliente</button>
                        </div>
                        <div>
                            <div class="search--div mt-3" style="display: inline-block;">
                                <input type="text" id="searchInput" class="form-control" style="font-size: 14px; width: 300px;" placeholder="Pesquise pelo CNPJ/Nome Fantasia/Razão Social">
                            </div>
                            <button class="btn--padrao btn-warning" data-bs-toggle="offcanvas" data-bs-target="#canvasFiltros" style="color: white; padding: 9px 20px;">
                                Filtros
                            </button>
                        </div>
                    </section>
                    <section class="list--table">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th scope="col">Nome Fantasia</th>
                                        <!-- <th scope="col">Tempo Restante</th> -->
                                        <th scope="col">Vigência Final</th>
                                        <th scope="col">Contrato</th>
                                    </tr>
                                </thead>
                                <tbody id="tbody">
                                </tbody>
                                <tbody id="tbodysearch">
                                </tbody>
                            </table>
                        </div>
                    </section>
                </div>
            </div>

        </div>
    </div>


    <!-- Modal Inserir Cliente -->
    <div class="modal fade" id="modalInserirCliente" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Cadastro de clientes</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <form method="post" id="form#inserir" enctype="multipart/form-data">
                    <div class="modal-body">
                        <div class="form--area">
                            <h3>Informações Básicas</h3>
                            <div class="form--row">
                                <div>
                                    <label>Status</label>
                                    <select name="status" class="form-control">
                                        <option value="ATIVO">Ativo</option>
                                        <option value="CONTRATO VENCIDO">Contrato Vencido</option>
                                    </select>
                                </div>
                                <div>
                                    <label>Razão Social</label>
                                    <input type="text" name="razao_social" class="form-control">
                                </div>
                            </div>
                            <div class="form--row">
                                <div>
                                    <label>Nome Fantasia</label>
                                    <input type="text" name="nome_fantasia" class="form-control" required>
                                </div>
                                <div>
                                    <label>CNPJ</label>
                                    <input type="text" name="cnpj" class="form-control">
                                </div>
                            </div>
                            <div class="form--row">
                                <div>
                                    <label>Tipo da Licença</label>
                                    <select name="tipo_licenca" class="form-control">
                                        <option value="PCM">SIGMA STARTER</option>
                                        <option value="PROFESSIONAL">SIGMA PROFESSIONAL</option>
                                        <option value="ENTERPRISE">SIGMA ENTERPRISE</option>
                                        <option value="SAAS">SIGMA ENTERPRISE SAAS</option>
                                    </select>
                                </div>
                                <div>
                                    <label>Inscrição Estadual</label>
                                    <input type="text" name="inscricao_estadual" class="form-control">
                                </div>
                            </div>
                            <h3>Localização</h3>
                            <div class="form--row">
                                <div>
                                    <label>CEP</label>
                                    <input type="text" name="cep" class="form-control">
                                </div>
                                <div>
                                    <label>Logradouro</label>
                                    <input type="text" name="logradouro" class="form-control">
                                </div>
                            </div>
                            <div class="form--row">
                                <div>
                                    <label>Número</label>
                                    <input type="text" name="numero" maxlength="10" class="form-control">
                                </div>
                                <div>
                                    <label>Bairro</label>
                                    <input type="text" name="bairro" class="form-control">
                                </div>
                            </div>
                            <div class="form--row">
                                <div>
                                    <label>Cidade</label>
                                    <input type="text" name="cidade" class="form-control">
                                </div>
                                <div>
                                    <label>Estado</label>
                                    <input type="text" name="estado" class="form-control">
                                </div>
                            </div>
                            <h3>Contato</h3>
                            <div style="width: 100%;">
                                <label>E-mails</label>
                                <div class="input-group">
                                    <input type="email" name="email[]" placeholder="E-mail 1" class="form-control">
                                    <input type="email" name="email[]" placeholder="E-mail 2" class="form-control">
                                </div>
                            </div>
                            <div class="form--row">
                                <div>
                                    <label>Telefone</label>
                                    <input type="text" name="telefone" class="form-control">
                                </div>
                            </div>
                            <h3>Informações do Contrato</h3>
                            <div class="form--row">
                                <div>
                                    <label>Vigência Inicial</label>
                                    <input type="date" name="vigencia_inicial" class="form-control">
                                </div>
                                <div>
                                    <label>Vigência Final</label>
                                    <input type="date" name="vigencia_final" class="form-control">
                                </div>
                            </div>
                            <div class="form--row">
                                <div>
                                    <label>Vigência Inicial Adendo</label>
                                    <input type="date" name="vigencia_inicial_adendo" class="form-control">
                                </div>
                                <div>
                                    <label>Vigência Final Adendo</label>
                                    <input type="date" name="vigencia_final_adendo" class="form-control">
                                </div>
                            </div>
                            <div class="form--row">
                                <div>
                                    <label>Tempo de validade das horas</label>
                                    <select name="validade_horas" class="form-control">
                                        <option value="mes">Mês</option>
                                        <option value="anos">Ano</option>
                                    </select>
                                </div>
                                <div>
                                    <label>Total de horas</label>
                                    <input type="text" name="horas_contrato" class="form-control onlynumber">
                                </div>
                            </div>
                            <div class="form--row">
                                <div>
                                    <label>Valor do Contrato</label>
                                    <input type="text" name="valor_contrato" class="form-control">
                                </div>
                                <div>
                                    <label>Multa</label>
                                    <input type="text" name="valor_multa" class="form-control">
                                </div>
                            </div>
                            <div class="form--row">
                                <div>
                                    <label>Empresa Contratada</label>
                                    <select name="empresa_contratada" class="form-control">
                                        <option value="sigma">Sigma</option>
                                        <option value="manut">Manut</option>
                                        <option value="microcenter">Microcenter</option>
                                    </select>
                                </div>
                                <div>
                                    <label>Periodicidade do Pagamento</label>
                                    <select name="periodicidade_pagamento" class="form-control">
                                        <option value="mensal">Mensal</option>
                                        <option value="anual">Anual</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form--row">
                                <div>
                                    <label>índice do Contrato</label>
                                    <select name="indice_contrato" class="form-control">
                                        <option value="IPCA">IPCA</option>
                                        <option value="IGPM">IGPM</option>
                                    </select>
                                </div>
                                <div>
                                    <label>Observações</label>
                                    <textarea name="observacoes" class="form-control" maxlength="400"></textarea>
                                </div>
                            </div>
                            <h3 class="saas-div">SaaS</h3>
                            <div class="form--row saas-div">
                                <div>
                                    <label>Quantidade de Usuários</label>
                                    <input type="text" name="usuarios_saas" class="form-control onlynumber">
                                </div>
                            </div>
                            <br>
                            <div class="form--row">
                                <div>
                                    <label class="mt-2">Anexo de documentos</label>
                                    <input type="file" name="anexo[]" class="form-control" multiple="multiple">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="reset" class="btn btn-secondary defaultButton" data-bs-dismiss="modal" style="display: none;">Cancelar</button>
                        <button class="btn btn-primary defaultButton" style="display: none;">Inserir Cliente</button>
                        <button class="btn btn-primary loadingButton" type="button" disabled>
                            <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                            Salvando...
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Modal Exibir Dados -->
    <div class="modal fade" id="modalExibirDados" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Dados do Cliente</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>

                <form class="formpermiss" method="post" id="form#editar" enctype="multipart/form-data">
                    <input type="hidden" name="id" id="idCliente">
                    <div class="modal-body">
                        <button class="btn--padrao defaultButton" id="excluirBtn" style="background-color: #ff2800; display: none;">Excluir Cliente</button>
                        <button type="button" class="btn--padrao btn-warning" data-bs-toggle="modal" data-bs-target="#modalRelatórios">Horas Lançadas</button>
                        <br>
                        <div class="form--area">
                            <h3>Informações Básicas</h3>
                            <div class="form--row">
                                <div>
                                    <label>Status</label>
                                    <select name="status" id="status" class="form-control">
                                        <option value="ATIVO">Ativo</option>
                                        <option value="CONTRATO VENCIDO">Contrato Vencido</option>
                                    </select>
                                </div>
                                <div>
                                    <label>Razão Social</label>
                                    <input type="text" id="razao_social" name="razao_social" class="form-control">
                                </div>
                            </div>
                            <div class="form--row">
                                <div>
                                    <label>Nome Fantasia</label>
                                    <input type="text" name="nome_fantasia" id="nome_fantasia" class="form-control">
                                </div>
                                <div>
                                    <label>CNPJ</label>
                                    <input type="text" name="cnpj" id="cnpj" class="form-control">
                                </div>
                            </div>
                            <div class="form--row">
                                <div>
                                    <label>Tipo da Licença</label>
                                    <select name="tipo_licenca" id="tipo_licenca" class="form-control">
                                        <option value="PCM">SIGMA STARTER</option>
                                        <option value="PROFESSIONAL">SIGMA PROFESSIONAL</option>
                                        <option value="ENTERPRISE">SIGMA ENTERPRISE</option>
                                        <option value="SAAS">SIGMA ENTERPRISE SAAS</option>
                                    </select>
                                </div>
                                <div>
                                    <label>Inscrição Estadual</label>
                                    <input type="text" name="inscricao_estadual" id="inscricao_estadual" class="form-control">
                                </div>
                            </div>
                            <h3>Localização</h3>
                            <div class="form--row">
                                <div>
                                    <label>CEP</label>
                                    <input type="text" name="cep" id="cep" class="form-control">
                                </div>
                                <div>
                                    <label>Logradouro</label>
                                    <input type="text" name="logradouro" id="logradouro" class="form-control">
                                </div>
                            </div>
                            <div class="form--row">
                                <div>
                                    <label>Número</label>
                                    <input type="text" name="numero" id="numero" class="form-control">
                                </div>
                                <div>
                                    <label>Bairro</label>
                                    <input type="text" name="bairro" id="bairro" class="form-control">
                                </div>
                            </div>
                            <div class="form--row">
                                <div>
                                    <label>Cidade</label>
                                    <input type="text" name="cidade" id="cidade" class="form-control">
                                </div>
                                <div>
                                    <label>Estado</label>
                                    <input type="text" name="estado" id="estado" class="form-control">
                                </div>
                            </div>
                            <h3>Contato</h3>
                            <div class="form--row">
                                <div>
                                    <label>E-mails</label>
                                    <input type="email" id="email1" name="email" placeholder="E-mail" class="form-control">
                                </div>

                                <div>
                                    <label>Telefone</label>
                                    <input type="text" name="telefone" id="telefone" class="form-control">
                                </div>
                            </div>
                            <h3>Informações do Contrato</h3>
                            <div class="form--row">
                                <div>
                                    <label>Vigência Inicial</label>
                                    <input type="date" name="dt_vigenciainicial" id="dt_vigenciainicial" class="form-control">
                                </div>
                                <div>
                                    <label>Vigência Final</label>
                                    <input type="date" name="dt_vigenciafinal" id="dt_vigenciafinal" class="form-control">
                                </div>
                            </div>
                            <div class="form--row">
                                <div>
                                    <label>Data de Cadastro</label>
                                    <input type="datetime" name="dt_cadastro" id="dt_cadastro" class="form-control">
                                </div>
                                <div>
                                    <label>Data do Reajuste</label>
                                    <input type="date" name="dt_proximoReajuste" id="dt_proximoReajuste" class="form-control">
                                </div>
                            </div>

                            <div class="form--row">
                                <div>
                                    <label>Total de Horas</label>
                                    <input type="text" name="horas_totais" id="horas_totais" class="form-control onlynumber">
                                </div>
                                <div>
                                    <label>Horas Mês</label>
                                    <input type="text" name="horas_mes" id="horas_mes" class="form-control onlynumber">
                                </div>
                            </div>
                            <div class="form--row">
                                <div>
                                    <label>Valor do Contrato</label>
                                    <input type="text" name="valor_total" id="valor_total" class="form-control" required>
                                </div>
                                <div>
                                    <label>Multa</label>
                                    <input type="text" name="multa" id="multa" class="form-control">
                                </div>
                            </div>
                            <div class="form--row">
                                <div>
                                    <label>Observações</label>
                                    <textarea name="obs" id="obs" class="form-control"></textarea>
                                </div>
                            </div>
                            <div class="form--row">
                                <div>
                                    <label>Empresa Contratada</label>
                                    <select name="empresa_contratada" id="empresa_contratada" class="form-control">
                                        <option value="sigma">Sigma</option>
                                        <option value="manut">Manut</option>
                                        <option value="microcenter">Microcenter</option>
                                    </select>
                                </div>
                                <div>
                                    <label>Periodicidade do Pagamento</label>
                                    <select name="periodicidade_pagamento" id="periodicidade_pagamento" class="form-control">
                                        <option value="mensal">Mensal</option>
                                        <option value="anual">Anual</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form--row">
                                <div>
                                    <label>índice do Contrato</label>
                                    <select name="indice" id="indice" class="form-control">
                                        <option value="IPCA">IPCA</option>
                                        <option value="IGPM">IGPM</option>
                                    </select>
                                </div>
                            </div>

                            <h3 class="saas-div">SaaS</h3>
                            <div class="form--row saas-div">
                                <div>
                                    <label>Quantidade de Usuários</label>
                                    <input type="text" name="usuarios_saas" id="usuarios_saas" class="form-control onlynumber">
                                </div>
                            </div>
                            <br>
                            <h3 class="hidepermiss" style="margin-bottom: 10px;">Anexos</h3>
                            <div class="form--row mb-2 hidepermiss">
                                <div>
                                    <label class="mt-2">Adicionar anexo de documentos</label>
                                    <input type="file" name="anexo[]" class="form-control" multiple="multiple">
                                </div>
                            </div>
                            <div class="form--row anexo--listItens hidepermiss">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="reset" class="btn btn-secondary defaultButton" data-bs-dismiss="modal" style="display: none;">Cancelar</button>
                        <button class="btn btn-primary defaultButton" style="display: none;">Salvar Mudanças</button>
                        <button class="btn btn-primary loadingButton" type="button" disabled>
                            <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                            Salvando...
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Modal Exibir Dados Gerais -->
    <div class="modal fade" id="modalDadosGerais" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Dados Gerais</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div class="dados--row">
                        <span class="text-muted">Soma de Valor Mensal dos Contratos</span>
                        <h4 id="soma_valor_mensal_contratos"></h4>
                    </div>
                    <div class="dados--row">
                        <span class="text-muted">Soma de Valor Anual dos Contratos</span>
                        <h4 id="soma_valor_anual_contratos"></h4>
                    </div>
                    <div class="dados--row">
                        <span class="text-muted">Soma de Valor Geral dos Contratos</span>
                        <h4 id="soma_valor_geral_contratos"></h4>
                    </div>
                    <div class="dados--row">
                        <span class="text-muted">Total de Clientes</span>
                        <h4 id="total_clientes"></h4>
                    </div>
                    <div class="dados--row">
                        <span class="text-muted">Total de Usuários SaaS</span>
                        <h4 id="qntTotal_usuarios_saas"></h4>
                    </div>
                </div>
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalRelatórios" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="titulo_relatorio">Relatório de Horas</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <form id="relatorio_form">
                    <div class="modal-body">
                        <div style="display: flex; align-items: center; gap: 10px;">
                            <div>
                                <label>Início:</label>
                                <input type="date" name="dt_inicial" class="form-control">
                            </div>
                            <div>
                                <label>Fim:</label>
                                <input type="date" name="dt_final" class="form-control">
                            </div>
                        </div>
                        <br>
                        <button type="reset" class="btn btn-secondary defaultButton" id="limpar_relatorio">Limpar</button>
                        <button class="btn btn-primary defaultButton">Pesquisar</button>
                        <button type="button" class="btn btn-primary" id="gerar_pdf" style="">Gerar PDF</button>
                        <button class="btn btn-primary loadingButton" type="button" disabled style="display: none;">
                            <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                            Pesquisando...
                        </button>
                        <br>
                        <br>
                        <br>
                        <div class="dados--row">
                            <span class="text-muted">Tempo de Horas Lançadas</span>
                            <h4 id="tempo_periodo"></h4>
                        </div>
                        <div class="dados--row">
                            <span class="text-muted">Total de Horas Lançadas</span>
                            <h4 id="total_servicos"></h4>
                        </div>
                        <div class="dados--row">
                            <span class="text-muted">Projetos</span>
                            <h4 id="total_projetos"></h4>
                        </div>
                        <div class="dados--row">
                            <span class="text-muted">Horas de Projetos</span>
                            <h4 id="tempo_total_projetos"></h4>
                        </div>
                        <br>
                        <br>

                        <table class="table">
                            <thead>
                                <tr>
                                    <th scope="col">Colaborador</th>
                                    <th scope="col">Início</th>
                                    <th scope="col">Fim</th>
                                    <th scope="col">Diferença</th>
                                </tr>
                            </thead>
                            <tbody id="tbodyRelatorio">
                            </tbody>
                        </table>
                    </div>

                </form>
            </div>
        </div>
    </div>

    <?php
    include_once '..componentes/trabalhando_agora.html';
    ?>

    <div class="offcanvas offcanvas-end" tabindex="-1" id="canvasFiltros">
        <div class="offcanvas-header" style="padding-bottom: 0;">
            <h5 id="offcanvasTopLabel">Filtros</h5>
            <button type="button" class="btn-close text-reset" data-bs-dismiss="offcanvas" aria-label="Close"></button>
        </div>
        <div class="offcanvas-body">
        <form id="filtros">
            <div class="filters--row">
                <h3>Filtro de Vigência Final Adendo</h3>
                <div class="input-rows mt-1">
                    <div>
                        <label>Início:</label>
                        <input type="date" id="dtvgfinal_inicio" class="form-control">
                    </div>
                    <div>
                        <label>Fim:</label>
                        <input type="date" id="dtvgfinal_fim" class="form-control">
                    </div>
                </div>
            </div>

            <div class="filters--row mt-3">
                <h3>Empresa Contratada</h3>
                <div class="input-rows mt-1">
                    <div>
                        <select id="contratada_filtro" class="form-control">
                            <option value="" selected disabled>Selecione a empresa contratada</option>
                            <option value="sigma">Sigma</option>
                            <option value="manut">Manut</option>
                            <option value="microcenter">Microcenter</option>
                        </select>
                    </div>
                </div>
            </div>

            <div class="filters--row mt-3">
                <div>
                    <h3>Periodicidade do Pagamento</h3>
                    <select id="periodicidade_pagamento_filtro" class="form-control">
                        <option value="" selected disabled>Selecione a periodicidade</option>
                        <option value="mensal">Mensal</option>
                        <option value="anual">Anual</option>
                    </select>
                </div>
            </div>
            <div class="mt-3">
                <button class="btn--padrao btn-warning" id="btn--filtros">
                    Filtrar
                </button>
                <button type="reset" class="btn--padrao btn-add" id="btn--limparFiltros">
                    Limpar
                </button>
            </div>
        </form>
        </div>
    </div>

    <div class="spinner-border text-primary" role="status">
        <span class="visually-hidden">Loading...</span>
    </div>

    <script src="../../js/jquery.js?<?php echo VERSION; ?>"></script>
    <script src="../../libs/jqueryMask/jqueryMask.js?<?php echo VERSION; ?>"></script>
    <script src="../../libs/maskmoney.js?<?php echo VERSION; ?>"></script>
    <script src="../../libs/bootstrap/js/bootstrap.min.js?<?php echo VERSION; ?>"></script>
    <script src="../../js/functions.js?<?php echo VERSION; ?>"></script>
    <script src="../../js/base.js?<?php echo VERSION; ?>"></script>
    <script src="../../js/clientes.js?<?php echo VERSION; ?>"></script>
</body>

</html>