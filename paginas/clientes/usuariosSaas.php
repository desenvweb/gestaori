<?php
session_start();
require '../../php/utils.php';

validaLogin();
logout();


?>
<!DOCTYPE html>
<html lang="pt-BR">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link href="../../libs/bootstrap/css/bootstrap.min.css?<?php echo VERSION; ?>" rel="stylesheet">
    <link rel="stylesheet" href="../../css/formBase.css?<?php echo VERSION; ?>">
    <link rel="stylesheet" href="../../css/base.css?<?php echo VERSION; ?>">
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/select2-bootstrap-5-theme@1.2.0/dist/select2-bootstrap-5-theme.min.css" />
</head>

<body>
    <div class="box flex-box">
        <div class="content">
            <?php
            require '../componentes/header.html';
            require '../componentes/sidebar.html';
            ?>


            <div class="body">
                <div class="list--div">
                    <section class="list--header">
                        <div>
                            <h3 class="header--title">Usuários Saas</h3>
                            <button class="btn--padrao btn-add" data-bs-toggle="modal" data-bs-target="#modalInserirUsuario">
                                Adicionar Usuário
                            </button>
                        </div>
                        <div>
                            <div class="search--div mt-3" style="display: inline-block;">
                                <input type="text" id="searchInput" class="form-control" style="font-size: 14px; width: 300px;" placeholder="Pesquise pelo Nome, E-mail ou Empresa">
                            </div>
                        </div>
                    </section>
                    <section class="list--table">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th scope="col">Empresa</th>
                                        <th scope="col">Nome</th>
                                        <th scope="col">E-mail</th>
                                    </tr>
                                </thead>
                                <tbody id="tbody">
                                </tbody>
                                <tbody id="tbodysearch">
                                </tbody>
                            </table>
                        </div>
                    </section>
                </div>
            </div>

        </div>
    </div>

    <!-- Modal Inserir Usuário -->
    <div class="modal fade" id="modalInserirUsuario" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Cadastro de Usuários</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <form method="post" id="form#inserir">
                    <div class="modal-body">
                        <div class="form--area">
                            <div class="form--row">
                                <div>
                                    <label>Nome</label>
                                    <input type="text" name="nome" class="form-control" required>
                                </div>
                                <div>
                                    <label>E-mail</label>
                                    <input type="email" name="email1" class="form-control">
                                </div>
                            </div>
                            <div class="form--row">
                                <div>
                                    <label>Selecione a empresa vinculada a esse usuário</label>
                                    <select name="id_empresa" class="form-control" id='input1Empresa' style='width: 100%; height: 50px' required>
                                        <option value="">Selecione uma Empresa</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="reset" class="btn btn-secondary" data-bs-dismiss="modal">Cancelar</button>
                        <button class="btn btn-primary">Cadastrar Usário</button>
                        <button class="btn btn-primary loadingButton" type="button" disabled>
                            <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                            Salvando...
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Modal Editar Usuário -->
    <div class="modal fade" id="modalExibirDados" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="labelModalEdit">Usuário</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <form class="formpermiss" method="post" id="form#editar" enctype="multipart/form-data">
                    <input type="hidden" id="idUsuario" name="id">
                    <div class="modal-body">

                        <div class="form--area">
                            <div class="form--row">
                                <div>
                                    <label>Nome</label>
                                    <input type="text" name="nome" id="nome" class="form-control" required>
                                </div>
                                <div>
                                    <label>E-mail</label>
                                    <input type="email" name="email1" id="email1" class="form-control">
                                </div>
                            </div>
                            <!-- <div class="form--row">
                                <div>
                                    <label>Empresa</label>
                                    <input type="text" id="empresa" class="form-control" disabled>
                                </div>
                            </div> -->

                            <div class="form--row">
                                <div>
                                    <label>Selecione a empresa vinculada a esse usuário</label>
                                    <select name="id_empresa" class="form-control" id='input2Empresa' style='width: 100%; height: 50px' required>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="reset" class="btn btn-secondary" data-bs-dismiss="modal">Cancelar</button>
                        <button class="btn btn-primary">Editar Usuário</button>
                        <button class="btn btn-primary loadingButton" type="button" disabled>
                            <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                            Salvando...
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>


    <?php
    include_once '../componentes/trabalhando_agora.html';
    ?>

    <div class="spinner-border text-primary" role="status">
        <span class="visually-hidden">Loading...</span>
    </div>

    <script src="../../js/jquery.js?<?php echo VERSION; ?>"></script>
    <script src="../../libs/jqueryMask/jqueryMask.js?<?php echo VERSION; ?>"></script>
    <script src="../../libs/bootstrap/js/bootstrap.min.js?<?php echo VERSION; ?>"></script>
    <script src="../../js/functions.js?<?php echo VERSION; ?>"></script>
    <script src="../../js/base.js?<?php echo VERSION; ?>"></script>
    <script src="../../libs/select2.min.js"></script>
    <script src="../../js/usuariosSaas.js?<?php echo VERSION; ?>"></script>

</body>

</html>