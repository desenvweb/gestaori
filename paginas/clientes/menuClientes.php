<?php
session_start();
require '../../php/utils.php';

validaLogin();

if (isset($_GET['sair'])) {
    unset($_SESSION['logado']);
    unset($_COOKIE['user']);
    header('location:../../index.php');
}

?>
<!DOCTYPE html>
<html lang="pt-BR">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link href="../../libs/bootstrap/css/bootstrap.min.css?<?php echo VERSION; ?>" rel="stylesheet">
    <link rel="stylesheet" href="../../css/formBase.css?<?php echo VERSION; ?>">
    <link rel="stylesheet" href="../../css/base.css?<?php echo VERSION; ?>">
    <style>
        .saas-div {
            display: none;
        }
    </style>
    <title>Sistema de Gestão</title>
</head>

<body>
    <div class="box flex-box">
        <div class="content">
            <?php
            require '../componentes/header.html';
            require '../componentes/sidebar.html';
            ?>


            <div class="body">
                <div class="list--div">
                    <section class="list--header">
                        <div>
                            <h3 class="header--title">Menu Empresas</h3>
                        </div>
                    </section>
                    <section class="list--body">
                        <a href="contratos.php" class="item--list">
                            <svg xmlns="http://www.w3.org/2000/svg" class="svg--color" height="60px" class="bi bi-file-earmark-text" viewBox="0 0 18 18" width="50px" fill="#000000">
                                <path d="M5.5 7a.5.5 0 0 0 0 1h5a.5.5 0 0 0 0-1h-5zM5 9.5a.5.5 0 0 1 .5-.5h5a.5.5 0 0 1 0 1h-5a.5.5 0 0 1-.5-.5zm0 2a.5.5 0 0 1 .5-.5h2a.5.5 0 0 1 0 1h-2a.5.5 0 0 1-.5-.5z"/>
                                <path d="M9.5 0H4a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h8a2 2 0 0 0 2-2V4.5L9.5 0zm0 1v2A1.5 1.5 0 0 0 11 4.5h2V14a1 1 0 0 1-1 1H4a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1h5.5z"/>
                            </svg>

                            <span>Acompanhamento de Contratos</span>
                        </a>
                        
                        <?php
                        if ($_SESSION['tipo_usuario'] == 1) { ?>
                            <a href="usuariosSaas.php" class="item--list">
                                <svg xmlns="http://www.w3.org/2000/svg" width="40" height="40" fill="currentColor" class="bi bi-clouds" viewBox="0 0 16 16">
                                    <path d="M16 7.5a2.5 2.5 0 0 1-1.456 2.272 3.513 3.513 0 0 0-.65-.824 1.5 1.5 0 0 0-.789-2.896.5.5 0 0 1-.627-.421 3 3 0 0 0-5.22-1.625 5.587 5.587 0 0 0-1.276.088 4.002 4.002 0 0 1 7.392.91A2.5 2.5 0 0 1 16 7.5z"/>
                                    <path d="M7 5a4.5 4.5 0 0 1 4.473 4h.027a2.5 2.5 0 0 1 0 5H3a3 3 0 0 1-.247-5.99A4.502 4.502 0 0 1 7 5zm3.5 4.5a3.5 3.5 0 0 0-6.89-.873.5.5 0 0 1-.51.375A2 2 0 1 0 3 13h8.5a1.5 1.5 0 1 0-.376-2.953.5.5 0 0 1-.624-.492V9.5z"/>
                                </svg>
                            <span>Usuários Saas</span>
                        </a>
                        <?php } ?>
                    </section>
                </div>
            </div>

        </div>
    </div>

    <?php
    include_once '../componentes/trabalhando_agora.html';
    ?>

    <!-- <div class="spinner-border text-primary" role="status">
        <span class="visually-hidden">Loading...</span>
    </div> -->

    <script src="../../libs/bootstrap/js/bootstrap.min.js?<?php echo VERSION; ?>"></script>
    <script src="../../js/jquery.js?<?php echo VERSION; ?>"></script>
    <script src="../../js/functions.js?<?php echo VERSION; ?>"></script>
    <script src="../../js/base.js?<?php echo VERSION; ?>"></script>
</body>

</html>