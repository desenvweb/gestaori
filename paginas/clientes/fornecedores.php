<?php
session_start();
require '../../php/utils.php';

validaLogin();
logout();

?>
<!DOCTYPE html>
<html lang="pt-BR">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link href="../../libs/bootstrap/css/bootstrap.min.css?<?php echo VERSION; ?>" rel="stylesheet">
    <link rel="stylesheet" href="../../css/formBase.css?<?php echo VERSION; ?>">
    <link rel="stylesheet" href="../../css/base.css?<?php echo VERSION; ?>">
    <title>Sistema de Gestão</title>
</head>

<body>
    <div class="box flex-box">
        <div class="content">
            <?php
            require '../componentes/header.html';
            require '../componentes/sidebar.html';
            ?>


            <div class="body">
                <div class="list--div">
                    <section class="list--header">
                        <h3 class="header--title">Fornecedores</h3>
                        <button class="btn--padrao btn-add" data-bs-toggle="modal" data-bs-target="#modalInserirFornecedor" style="display: none;">
                            Adicionar Fornecedor
                        </button>
                        <div class="search--div mt-3">
                            <input type="text" id="searchInput" class="form-control" style="font-size: 14px; width: 300px;" placeholder="Pesquise pelo nome">
                        </div>
                    </section>
                    <section class="list--table">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th scope="col">Nome Fantasia</th>
                                        <th scope="col">Categoria</th>
                                        <th scope="col">Ações</th>
                                    </tr>
                                </thead>
                                <tbody id="tbody">
                                </tbody>
                                <tbody id="tbodysearch">
                                </tbody>
                            </table>
                        </div>
                    </section>
                </div>
            </div>

        </div>
    </div>


    <!-- Modal Inserir Fornecedor -->
    <div class="modal fade" id="modalInserirFornecedor" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Cadastro de Fornecedor</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <form method="post" id="form#inserir" enctype="multipart/form-data">
                <div class="modal-body">
                        <div class="form--area">
                            <h3>Informações Básicas</h3>
                            <div class="row" style="width: 100%;">
                                <!--
                                <div>
                                    <label>Status</label>
                                    <select name="status" class="form-control">
                                        <option value="ATIVO">Ativo</option>
                                        <option value="CONTRATO VENCIDO">Contrato Vencido</option>
                                    </select>
                                </div>-->
                                <div class="col-md-12">
                                    <label>Razão Social</label>
                                    <input type="text" name="razao_social" class="form-control" required>
                                </div>
                            </div>
                            <div class="row" style="width: 100%;">
                                <div class="col-md-6">
                                    <label>Nome Fantasia</label>
                                    <input type="text" name="nome_fantasia" class="form-control" required>
                                </div>
                                <div class="col-md-6">
                                    <label>Categoria</label>
                                    <input type="text" name="categoria" class="form-control" required>
                                </div>
                            </div>
                            <div class="row" style="width: 100%;">
                                <div class="col-md-6">
                                    <label>CNPJ</label>
                                    <input type="text" name="cnpj" class="form-control" required>
                                </div>
                                <div class="col-md-6">
                                    <label>Inscrição Estadual</label>
                                    <input type="text" name="inscricao_estadual" class="form-control" required>
                                </div>
                            </div>
                            <h3>Localização</h3>
                            <div class="row" style="width: 100%;">
                                <div class="col-md-3">
                                    <label>CEP</label>
                                    <input type="text" name="cep" class="form-control" required>
                                </div>
                                <div class="col-md-6">
                                    <label>Logradouro</label>
                                    <input type="text" name="logradouro" class="form-control" required>
                                </div>
                                <div class="col-md-3">
                                    <label>Número</label>
                                    <input type="text" name="numero" maxlength="10" class="form-control" required>
                                </div>
                            </div>
                            <div class="row" style="width: 100%;">
                                <div class="col-md-5">
                                    <label>Bairro</label>
                                    <input type="text" name="bairro" class="form-control" required>
                                </div>
                                <div class="col-md-5">
                                    <label>Cidade</label>
                                    <input type="text" name="cidade" class="form-control" required>
                                </div>
                                <div class="col-md-2">
                                    <label>UF</label>
                                    <input type="text" name="uf" class="form-control" required>
                                </div>
                            </div>
                            <h3>Contato</h3>
                            <div class="row" style="width: 100%;">
                                <div class="col-md-3">
                                    <label>Telefone</label>
                                    <input type="text" name="fone" class="form-control">
                                </div>
                                <div class="col-md-3">
                                    <label>Contato</label>
                                    <input type="text" name="contato" class="form-control">
                                </div>
                                <div class="col-md-6">
                                    <label>E-mail</label>
                                    <input type="email" name="emails" placeholder="E-mail" class="form-control">
                                </div>
                            </div>
                            <div class="row" style="width: 100%;">
                                <div>
                                    <label>Observações</label>
                                    <textarea name="observacoes" class="form-control" maxlength="400"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="reset" class="btn btn-secondary defaultButton" data-bs-dismiss="modal">Cancelar</button>
                        <button class="btn btn-primary defaultButton">Inserir Fornecedor</button>
                        <button class="btn btn-primary loadingButton" type="button" disabled style="display: none;">
                            <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                            Salvando...
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Modal Exibir Dados -->
    <div class="modal fade" id="modalExibirDados" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Dados do Fornecedor</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>

                <form class="formpermiss" method="post" id="form#editar">
                    <input type="hidden" name="id" id="idFornecedor">
                    <div class="modal-body">
                        <div class="form--area">
                            <h3>Informações Básicas</h3>
                            <div class="row" style="width: 100%;">
                                <!--
                                <div>
                                    <label>Status</label>
                                    <select name="status" class="form-control">
                                        <option value="ATIVO">Ativo</option>
                                        <option value="CONTRATO VENCIDO">Contrato Vencido</option>
                                    </select>
                                </div>-->
                                <div class="col-md-12">
                                    <label>Razão Social</label>
                                    <input type="text" name="razao_social" id="razao_social" class="form-control" required>
                                </div>
                            </div>
                            <div class="row" style="width: 100%;">
                                <div class="col-md-6">
                                    <label>Nome Fantasia</label>
                                    <input type="text" name="nome_fantasia" id="nome_fantasia" class="form-control" required>
                                </div>
                                <div class="col-md-6">
                                    <label>Categoria</label>
                                    <input type="text" name="categoria" id="categoria" class="form-control" required>
                                </div>

                            </div>
                            <div class="row" style="width: 100%;">
                                <div class="col-md-6">
                                    <label>CNPJ</label>
                                    <input type="text" name="cnpj" id="cnpj" class="form-control" required>
                                </div>
                                <div class="col-md-6">
                                    <label>Inscrição Estadual</label>
                                    <input type="text" name="inscricao_estadual" id="inscricao_estadual" class="form-control" required>
                                </div>
                            </div>
                            <h3>Localização</h3>
                            <div class="row" style="width: 100%;">
                                <div class="col-md-3">
                                    <label>CEP</label>
                                    <input type="text" name="cep" id="cep" class="form-control" required>
                                </div>
                                <div class="col-md-6">
                                    <label>Logradouro</label>
                                    <input type="text" name="logradouro" id="logradouro" class="form-control" required>
                                </div>
                                <div class="col-md-3">
                                    <label>Número</label>
                                    <input type="text" name="numero" id="numero" maxlength="10" class="form-control" required>
                                </div>
                            </div>
                            <div class="row" style="width: 100%;">
                                <div class="col-md-5">
                                    <label>Bairro</label>
                                    <input type="text" name="bairro" id="bairro" class="form-control" required>
                                </div>
                                <div class="col-md-5">
                                    <label>Cidade</label>
                                    <input type="text" name="cidade" id="cidade" class="form-control" required>
                                </div>
                                <div class="col-md-2">
                                    <label>UF</label>
                                    <input type="text" name="uf" id="uf" class="form-control" required>
                                </div>
                            </div>
                            <h3>Contato</h3>
                            <div class="row" style="width: 100%;">
                                <div class="col-md-3">
                                    <label>Telefone</label>
                                    <input type="text" name="fone" id="fone" class="form-control">
                                </div>
                                <div class="col-md-3">
                                    <label>Contato</label>
                                    <input type="text" name="contato" id="contato" class="form-control">
                                </div>
                                <div class="col-md-6">
                                    <label>E-mail</label>
                                    <input type="email" name="emails" id="emails" placeholder="E-mail" class="form-control">
                                </div>
                            </div>
                            <div class="row" style="width: 100%;">
                                <div>
                                    <label>Observações</label>
                                    <textarea name="observacoes" id="observacoes" class="form-control" maxlength="400"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="reset" class="btn btn-secondary defaultButton" data-bs-dismiss="modal">Cancelar</button>
                        <button class="btn btn-primary defaultButton">Editar Fornecedor</button>
                        <button class="btn btn-primary loadingButton" type="button" disabled style="display: none;">
                            <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                            Salvando...
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>


   
    <?php
    include_once '../componentes/trabalhando_agora.html';
    ?>

    <div class="spinner-border text-primary" role="status">
        <span class="visually-hidden">Loading...</span>
    </div>

    <div class="modal fade" id="modalRelatórios" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="titulo_relatorio">Relatório de Horas</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <form id="relatorio_form">
                    <div class="modal-body">
                        <div style="display: flex; align-items: center; gap: 10px;">
                            <div>
                                <label>Fornecedor:</label>
                                <select class="form-control fornecedor--select filtro--list" name="fornecedor">
                                    <option value="" selected disabled="">Selecione um Fornecedor</option>
                                </select>
                            </div>
                            <div>
                                <label>Início:</label>
                                <input type="date" name="dt_inicial" class="form-control">
                            </div>
                            <div>
                                <label>Fim:</label>
                                <input type="date" name="dt_final" class="form-control">
                            </div>
                        </div>
                        <br>
                        <button type="reset" class="btn btn-secondary defaultButton" id="limpar_relatorio">Limpar</button>
                        <button class="btn btn-primary defaultButton">Pesquisar</button>
                        <button type="button" class="btn btn-primary" id="gerar_pdf" style="">Gerar PDF</button>
                        <button class="btn btn-primary loadingButton" type="button" disabled style="display: none;">
                            <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                            Pesquisando...
                        </button>
                        <br>
                        <br>
                        <br>
                        <div class="dados--row">
                            <span class="text-muted">Tempo Trabalhado no Período</span>
                            <h4 id="tempo_periodo"></h4>
                        </div>
                        <div class="dados--row">
                            <span class="text-muted">Total de Serviços</span>
                            <h4 id="total_servicos"></h4>
                        </div>
                        <br>
                        <br>
                        <table class="table">
                            <thead>
                                <tr>
                                    <th scope="col">Ticket</th>
                                    <th scope="col">Fornecedor</th>
                                    <th scope="col">Início</th>
                                    <th scope="col">Fim</th>
                                    <th scope="col">Diferença</th>
                                </tr>
                            </thead>
                            <tbody id="tbodyRelatorio">
                            </tbody>
                        </table>
                    </div>

                </form>
            </div>
        </div>
    </div>


    <script src="../../libs/bootstrap/js/bootstrap.min.js?<?php echo VERSION; ?>"></script>
    <script src="../../js/jquery.js?<?php echo VERSION; ?>"></script>
    <script src="../../libs/jqueryMask/jqueryMask.js?<?php echo VERSION; ?>"></script>
    <script src="../../js/functions.js?<?php echo VERSION; ?>"></script>
    <script src="../../js/base.js?<?php echo VERSION; ?>"></script>
    <script src="../../js/fornecedores.js?<?php echo VERSION; ?>"></script>
</body>

</html>