<?php
session_start();
require '../../php/utils.php';

validaLogin();
logout();

?>
<!DOCTYPE html>
<html lang="pt-BR">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link href="../../libs/bootstrap/css/bootstrap.min.css?<?php echo VERSION; ?>" rel="stylesheet">
    <link rel="stylesheet" href="../../css/formBase.css?<?php echo VERSION; ?>">
    <link rel="stylesheet" href="../../css/base.css?<?php echo VERSION; ?>">
    <style>
        .saas-div {
            display: none;
        }
    </style>
    <title>Sistema de Gestão</title>
</head>

<body>
    <div class="box flex-box">
        <div class="content">

            <?php
            require '../componentes/header.html';
            require '../componentes/sidebar.html';
            ?>

            <div class="body2">
                <div class="list--div">
                    <section class="list--header">
                        <div class="container">
                            <div class="col col-md-8">
                                <h3>Manutenção de Contratos</h3>
                            </div>
                            <div class="col col-md-6">
                                <div class="btn-group" role="group" aria-label="Basic radio toggle button group">
                                    <input type="radio" class="btn-check" name="btnradio-tpcontratos" id="contratosTodos" onclick="handleClickTpContrato(this);" value="todos" checked>
                                    <label class="btn btn-outline-primary" for="contratosTodos">Todos</label>

                                    <input type="radio" class="btn-check" name="btnradio-tpcontratos" id="contratosManutencao" onclick="handleClickTpContrato(this);" value="manutencao">
                                    <label class="btn btn-outline-primary" for="contratosManutencao">Manutenção</label>

                                    <input type="radio" class="btn-check" name="btnradio-tpcontratos" id="contratosSaas" onclick="handleClickTpContrato(this);" value="saas">
                                    <label class="btn btn-outline-primary" for="contratosSaas">Saas</label>

                                    <input type="radio" class="btn-check" name="btnradio-tpcontratos" id="contratosManutencaoSaas" onclick="handleClickTpContrato(this);" value="manutencao_saas">
                                    <label class="btn btn-outline-primary" for="contratosManutencaoSaas">Manutencao + Saas</label>

                                </div>
                            </div>
                        </div>
                        <div class="container">
                            <div class="row">
                                <div class="col-md-9 mt-2">
                                <div class="search--div" style="display: inline-block;">
                                <input type="text" id="searchInput" class="form-control" style="font-size: 14px; width: 300px;" placeholder="Pesquise pelo CNPJ/Nome Fantasia/Razão Social">
                            </div>
                            <button class="btn--padrao btn-warning" data-bs-toggle="offcanvas" data-bs-target="#canvasFiltros" style="color: white; padding: 9px 20px;">
                                Filtros
                            </button>                                </div>
                                <div class="col-md-3 mt-2">
                                    <button type="button" class="btn btn-outline-success" data-bs-toggle="modal" data-bs-target="#modalInserirCliente">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-plus-square-fill" viewBox="0 0 16 16">
                                            <path d="M2 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2zm6.5 4.5v3h3a.5.5 0 0 1 0 1h-3v3a.5.5 0 0 1-1 0v-3h-3a.5.5 0 0 1 0-1h3v-3a.5.5 0 0 1 1 0z"></path>
                                        </svg>
                                        Incluir Empresa
                                    </button>
                                </div>

                            </div>
                        </div>
                        <div class="row mt-4">
                            <div class="col-sm-3">
                                <div class="card">
                                <div class="card-body">
                                    <h6 class="card-title">Contratos Mensais</h5>
                                    <h5 class="card-text" id="soma_valor_mensal_contratos">R$ 0,00</h5>
                                </div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="card">
                                <div class="card-body">
                                    <h6 class="card-title">Contratos Anuais</h5>
                                    <h5 class="card-text" id="soma_valor_anual_contratos">R$ 0,00</h5>
                                </div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="card">
                                <div class="card-body">
                                    <h6 class="card-title">Contratos Totais</h5>
                                    <h5 class="card-text" id="soma_valor_geral_contratos">R$ 0,00</h5>
                                </div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="card">
                                <div class="card-body">
                                    <h6 class="card-text" id="qntTotal_usuarios_saas">0 usuários Saas</h5>
                                    <h6 class="card-text" id="total_clientes">0 Clientes</h5>
                                </div>
                                </div>
                            </div>

                        </div>
                    </section>
                    <section class="list--table">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th scope="col">Nome Fantasia</th>
                                        <th scope="col">Vigência Final</th>
                                        <th scope="col">Saldo Horas</th>
                                        <th scope="col">Usuário Saas</th>
                                        <th class="hidepermiss" scope="col">Valor Contrato</th>
                                        <th class="hidepermiss" scope="col">Pagamento</th>
                                        <th scope="col">Status</th>
                                        <th scope="col">Ações</th>
                                    </tr>
                                </thead>
                                <tbody id="tbody">
                                </tbody>
                                <tbody id="tbodysearch">
                                </tbody>
                            </table>
                        </div>
                    </section>
                </div>
            </div>

        </div>
    </div>

    <!-- Modal Inserir Cliente -->
    <div class="modal fade" id="modalInserirCliente" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Cadastro de clientes</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <form method="post" id="form#inserir" enctype="multipart/form-data">
                    <div class="modal-body">
                        <div class="form--area">
                            <h3>Informações Básicas</h3>
                            <div class="row" style="width: 100%;">
                                <div class="col-md-12">
                                    <label>Razão Social</label>
                                    <input type="text" name="razao_social" class="form-control" required>
                                </div>
                            </div>
                            <div class="row" style="width: 100%;">
                                <div class="col-md-12">
                                    <label>Nome Fantasia</label>
                                    <input type="text" name="nome_fantasia" class="form-control" required>
                                </div>
                            </div>
                            <div class="row" style="width: 100%;">
                                <div class="col-md-6">
                                    <label>CNPJ</label>
                                    <input type="text" name="cnpj" class="form-control" required>
                                </div>
                                <div class="col-md-6">
                                    <label>Inscrição Estadual</label>
                                    <input type="text" name="inscricao_estadual" class="form-control">
                                </div>
                            </div>
                            <h3>Localização</h3>
                            <div class="row" style="width: 100%;">
                                <div class="col-md-3">
                                    <label>CEP</label>
                                    <input type="text" name="cep" class="form-control">
                                </div>
                                <div class="col-md-6">
                                    <label>Logradouro</label>
                                    <input type="text" name="logradouro" class="form-control">
                                </div>
                                <div class="col-md-3">
                                    <label>Número</label>
                                    <input type="text" name="numero" maxlength="10" class="form-control">
                                </div>
                            </div>
                            <div class="row" style="width: 100%;">
                                <div class="col-md-5">
                                    <label>Bairro</label>
                                    <input type="text" name="bairro" class="form-control">
                                </div>
                                <div class="col-md-5">
                                    <label>Cidade</label>
                                    <input type="text" name="cidade" class="form-control">
                                </div>
                                <div class="col-md-2">
                                    <label>UF</label>
                                    <input type="text" name="uf" class="form-control">
                                </div>
                            </div>
                            <h3>Contato</h3>
                            <div class="row" style="width: 100%;">
                                <div class="col-md-3">
                                    <label>Telefone</label>
                                    <input type="text" name="telefone" class="form-control">
                                </div>
                                <div class="col-md-3">
                                    <label>Contato</label>
                                    <input type="text" name="contato" class="form-control">
                                </div>
                                <div class="col-md-6">
                                    <label>E-mail</label>
                                    <input type="email" name="email1" placeholder="E-mail" class="form-control">
                                </div>
                            </div>
                            <div class="row" style="width: 100%;">
                                <div>
                                    <label>Observações</label>
                                    <textarea name="observacoes" class="form-control" maxlength="400"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="reset" class="btn btn-secondary defaultButton" data-bs-dismiss="modal" style="display: none;">Cancelar</button>
                        <button class="btn btn-primary defaultButton" style="display: none;">Inserir Cliente</button>
                        <button class="btn btn-primary loadingButton" type="button" disabled>
                            <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                            Salvando...
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Modal Exibir Dados -->
    <div class="modal fade" id="modalExibirDados" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Dados do Cliente</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>

                <form class="formpermiss" method="post" id="form#editar" enctype="multipart/form-data">
                    <input type="hidden" name="id" id="idCliente">
                    <div class="modal-body">
                    <button type="button" class="btn--padrao btn-success" data-bs-toggle="modal" data-bs-target="#modalCadastrarHoras">Cadastrar Horas Avulsas</button>
                        <div class="form--area">
                            <h3>Informações Básicas</h3>
                            <div class="row" style="width: 100%;">
                                <div class="col-md-12">
                                    <label>Razão Social</label>
                                    <input type="text" name="razao_social" id="razao_social" class="form-control" required>
                                </div>
                            </div>
                            <div class="row" style="width: 100%;">
                                <div class="col-md-12">
                                    <label>Nome Fantasia</label>
                                    <input type="text" name="nome_fantasia" id="nome_fantasia" class="form-control" required>
                                </div>
                            </div>
                            <div class="row" style="width: 100%;">
                                <div class="col-md-6">
                                    <label>CNPJ</label>
                                    <input type="text" name="cnpj" id="cnpj" class="form-control" required>
                                </div>
                                <div class="col-md-6">
                                    <label>Inscrição Estadual</label>
                                    <input type="text" name="inscricao_estadual" id="inscricao_estadual" class="form-control">
                                </div>
                            </div>
                            <h3>Localização</h3>
                            <div class="row" style="width: 100%;">
                                <div class="col-md-3">
                                    <label>CEP</label>
                                    <input type="text" name="cep" id="cep" class="form-control">
                                </div>
                                <div class="col-md-6">
                                    <label>Logradouro</label>
                                    <input type="text" name="logradouro" id="logradouro" class="form-control">
                                </div>
                                <div class="col-md-3">
                                    <label>Número</label>
                                    <input type="text" name="numero" id="numero" maxlength="10" class="form-control">
                                </div>
                            </div>
                            <div class="row" style="width: 100%;">
                                <div class="col-md-5">
                                    <label>Bairro</label>
                                    <input type="text" name="bairro" id="bairro" class="form-control">
                                </div>
                                <div class="col-md-5">
                                    <label>Cidade</label>
                                    <input type="text" name="cidade" id="cidade" class="form-control">
                                </div>
                                <div class="col-md-2">
                                    <label>UF</label>
                                    <input type="text" name="uf" id="uf" class="form-control">
                                </div>
                            </div>
                            <h3>Contato</h3>
                            <div class="row" style="width: 100%;">
                                <div class="col-md-3">
                                    <label>Telefone</label>
                                    <input type="text" name="telefone" id="telefone" class="form-control">
                                </div>
                                <div class="col-md-3">
                                    <label>Contato</label>
                                    <input type="text" name="contato" id="contato" class="form-control">
                                </div>
                                <div class="col-md-6">
                                    <label>E-mail</label>
                                    <input type="email" name="email1" id="email1" placeholder="E-mail" class="form-control">
                                </div>
                            </div>
                            <div class="row" style="width: 100%;">
                                <div>
                                    <label>Observações</label>
                                    <textarea name="observacoes" id="observacoes" class="form-control" maxlength="400"></textarea>
                                </div>
                            </div>
                            <div class="form--area mt-4" style="width: 100%;">
                                <h3 style="text-align: center;">Horas Avulsas</h3>
                                <div class="row" id="accordionHorasAvulsas" style="display: none;">
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="reset" class="btn btn-secondary defaultButton" data-bs-dismiss="modal" style="display: none;">Fechar</button>
                        <button class="btn btn-primary defaultButton" style="display: none;">Salvar Mudanças</button>
                        <button class="btn btn-primary loadingButton" type="button" disabled>
                            <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                            Salvando...
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalRelatorioHoras" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="titulo_relatorio">Relatório de Horas</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <form id="relatorio_form">
                    <div class="modal-body">
                        <div style="display: flex; align-items: center; gap: 10px;">
                            <div>
                                <label>Início:</label>
                                <input type="date" name="dt_inicial" class="form-control">
                            </div>
                            <div>
                                <label>Fim:</label>
                                <input type="date" name="dt_final" class="form-control">
                            </div>
                        </div>
                        <br>
                        <button type="reset" class="btn btn-secondary defaultButton" id="limpar_relatorio">Limpar</button>
                        <button class="btn btn-primary defaultButton">Pesquisar</button>
                        <button type="button" class="btn btn-primary" id="gerar_pdf">Gerar PDF</button>
                        <button class="btn btn-primary loadingButton" type="button" disabled style="display: none;">
                            <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                            Pesquisando...
                        </button>
                        <br>
                        <br>
                        <br>
                        <div class="dados--row">
                            <span class="text-muted">Tempo de Horas Lançadas</span>
                            <h4 id="tempo_periodo"></h4>
                        </div>
                        <div class="dados--row">
                            <span class="text-muted">Total de Horas Lançadas</span>
                            <h4 id="total_servicos"></h4>
                        </div>
                        <div class="dados--row">
                            <span class="text-muted">Projetos</span>
                            <h4 id="total_projetos"></h4>
                        </div>
                        <div class="dados--row">
                            <span class="text-muted">Horas de Projetos</span>
                            <h4 id="tempo_total_projetos"></h4>
                        </div>
                        <br>
                        <br>

                        <table class="table">
                            <thead>
                                <tr>
                                    <th scope="col">Ticket</th>
                                    <th scope="col">Colaborador</th>
                                    <th scope="col">Início</th>
                                    <th scope="col">Fim</th>
                                    <th scope="col">Diferença</th>
                                </tr>
                            </thead>
                            <tbody id="tbodyRelatorio">
                            </tbody>
                        </table>
                    </div>

                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalContratos" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Dados do Contrato</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>

                    <div class="modal-body">
                        <button type="button" class="btn--padrao btn-add defaultButton" style="display: none;" data-bs-toggle="modal" data-bs-target="#modalCadastrarContrato">Novo Contrato</button>
                        <button type="button" id="addAdendo" class="btn--padrao btn-warning defaultButton" style="display: none;" data-bs-toggle="modal" data-bs-target="#modalCadastrarAdendo">Novo Adendo</button>

                        <div class="form--area mt-4">
                            <div class="row"  id="accordionContrato">
                            </div>
                        </div>

                    </div>
            </div>
        </div>
    </div>

    <!-- Modal Cadastra Contrato -->
    <div class="modal fade" id="modalCadastrarContrato" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Cadastro de Contrato</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <form enctype="multipart/form-data" id="formCadastarContrato">
                    <div class="modal-body">
                        <b>* São campos que não podem ser editados após o cadastro</b>
                        <br><br>
                        <div class="form--row">
                            <label>Tipo do Contrato&nbsp;<b>*</b></label>
                            <select name="tipo_contrato" id="inputTipoContrato" class="form-control" required>
                                <option value="manutencao">Manutenção</option>
                                <option value="saas">SaaS</option>
                                <option value="manutencao_saas">Manutenção + SaaS</option>
                            </select>
                        </div>
                        <div class="form--row">
                            <div>
                                <label>Vigência Inicial&nbsp;<b>*</b></label>
                                <input type="date" name="dt_vigenciainicial" class="form-control" required>
                            </div>
                            <div>
                                <label>Vigência Final&nbsp;<b>*</b></label>
                                <input type="date" name="dt_vigenciafinal" class="form-control" required>
                            </div>
                        </div>
                        <div class="form--row">
                            <div>
                                <label>Data do Reajuste</label>
                                <input type="date" name="dt_proximoReajuste" class="form-control">
                            </div>
                        </div>
                        <div class="form--row" data-nameInput="horas_totais">
                            <label>Total de Horas&nbsp;<b>*</b></label>
                            <input type="text" name="horas_totais" class="form-control onlynumber" required>
                        </div>
                        <div class="form--row" data-nameInput="usuarios_saas" style="display: none;">
                            <label>Qntd. de Usuários SaaS</label>
                            <input type="text" name="usuarios_saas" class="form-control onlynumber">
                        </div>                        
                        <div class="form--row">
                            <label>Valor do Contrato</label>
                            <input type="text" name="valor_total" class="form-control money" required>
                        </div>
                        <div class="form--row">
                            <label>Multa</label>
                            <input type="text" name="multa" class="form-control" required>
                        </div>
                        <div class="form--row">
                            <label>Empresa Contratada</label>
                            <select name="empresa_contratada" class="form-control" required>
                                <option value="sigma">Sigma</option>
                                <option value="manut">Manut</option>
                                <option value="microcenter">Microcenter</option>
                            </select>
                        </div>
                        <div class="form--row">
                            <label>índice do Contrato</label>
                            <select name="indice" class="form-control" required>
                                <option value="IGPM">IGPM</option>
                                <option value="INPC">INPC</option>
                                <option value="IPCA">IPCA</option>
                            </select>
                        </div>
                        <div class="form--row">
                            <label>Periodicidade do Pagamento</label>
                            <select name="periodicidade_pagamento" class="form-control" required>
                                <option value="anual">Anual</option>
                                <option value="mensal">Mensal</option>
                            </select>
                        </div>
                        <div class="form--row">
                            <label>Observações</label>
                            <textarea name="obs" class="form-control"></textarea>
                        </div>
                        <div class="form--row">
                            <label>Adicionar anexo de documentos</label>
                            <input type="file" name="anexo[]" class="form-control" multiple="multiple">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="reset" class="btn btn-secondary defaultButton" data-bs-dismiss="modal" style="display: none;">Cancelar</button>
                        <button class="btn btn-primary defaultButton" style="display: none;">Cadastrar Contrato</button>
                        <button class="btn btn-primary loadingButton" type="button" disabled>
                            <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                            Salvando...
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Modal Cadastro de Horas Avulsas -->
    <div class="modal fade" id="modalCadastrarHoras" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Cadastro de Horas Avulsas</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <form action="" class="formpermiss" id="formCadastrarHoras">
                    <div class="modal-body">
                        <div class="form--row">
                            <label>Empresa</label>
                            <input class="form-control" name="empresa" type="text" disabled>
                        </div>
                        <div class="form--row">
                            <label>Total de Horas</label>
                            <input class="form-control onlynumber" type="text" name="total_horas" required>
                        </div>
                        <div class="form--row">
                            <label>Valor da Compra</label>
                            <input type="text" name="valor_total" class="form-control money" required>
                        </div>
                        <div class="form--row">
                            <label>Observações</label>
                            <textarea name="obs" maxlength="100" class="form-control"></textarea>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="reset" class="btn btn-secondary defaultButton" data-bs-dismiss="modal" style="display: none;">Cancelar</button>
                        <button class="btn btn-primary defaultButton" style="display: none;">Cadastrar Horas</button>
                        <button class="btn btn-primary loadingButton" type="button" disabled>
                            <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                            Salvando...
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Modal Cadastro de Adendos -->
    <div class="modal fade" id="modalCadastrarAdendo" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Cadastro de Adendo</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <form action="" class="formpermiss" id="formCadastrarAdendo">
                    <div class="modal-body">
                        <div class="form--row">
                            <div class="form--row">
                                <label>Vigência Final</label>
                                <input type="date" name="dt_vigenciafinal_nova" class="form-control" required>
                            </div>
                            <div class="form--row">
                                <label>Data de Reajuste</label>
                                <input type="date" name="dt_reajuste_nova" class="form-control" required>
                            </div>
                        </div>
                        <div class="form--row">
                            <div class="form--row" data-nameInput="horas_totais_nova">
                                <label>Total de Horas</label>
                                <input type="text" name="horas_totais_nova" class="form-control onlynumber" required>
                            </div>

                            <div class="form--row" data-nameInput="usuarios_saas_nova" style="display: none;">
                                <label>Qntd. de Usuários SaaS</label>
                                <input type="text" name="usuarios_saas_nova" class="form-control onlynumber">
                            </div>

                            <div class="form--row">
                                <label>Valor do Contrato</label>
                                <input type="text" name="valor_total_nova" class="form-control money" required>
                            </div>
                        </div>
                        <div class="form--row">
                            <label>Observações</label>
                            <textarea name="obs" maxlength="100" class="form-control"></textarea>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="reset" class="btn btn-secondary defaultButton" data-bs-dismiss="modal" style="display: none;">Cancelar</button>
                        <button class="btn btn-primary defaultButton" style="display: none;">Cadastrar Adendo</button>
                        <button class="btn btn-primary loadingButton" type="button" disabled>
                            <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                            Salvando...
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div> 
    <?php
    include_once '..componentes/trabalhando_agora.html';
    ?>

    <div class="offcanvas offcanvas-end" tabindex="-1" id="canvasFiltros">
        <div class="offcanvas-header" style="padding-bottom: 0;">
            <h5 id="offcanvasTopLabel">Filtros</h5>
            <button type="button" class="btn-close text-reset" data-bs-dismiss="offcanvas" aria-label="Close"></button>
        </div>
        <div class="offcanvas-body">
        <form id="filtros">
            <div class="filters--row">
                <h3>Filtro de Vigência Final Adendo</h3>
                <div class="input-rows mt-1">
                    <div>
                        <label>Início:</label>
                        <input type="date" id="dtvgfinal_inicio" class="form-control">
                    </div>
                    <div>
                        <label>Fim:</label>
                        <input type="date" id="dtvgfinal_fim" class="form-control">
                    </div>
                </div>
            </div>

            <div class="filters--row mt-3">
                <h3>Empresa Contratada</h3>
                <div class="input-rows mt-1">
                    <div>
                        <select id="contratada_filtro" class="form-control">
                            <option value="" selected disabled>Selecione a empresa contratada</option>
                            <option value="sigma">Sigma</option>
                            <option value="manut">Manut</option>
                            <option value="microcenter">Microcenter</option>
                        </select>
                    </div>
                </div>
            </div>

            <div class="filters--row mt-3">
                <div>
                    <h3>Periodicidade do Pagamento</h3>
                    <select id="periodicidade_pagamento_filtro" class="form-control">
                        <option value="" selected disabled>Selecione a periodicidade</option>
                        <option value="mensal">Mensal</option>
                        <option value="anual">Anual</option>
                    </select>
                </div>
            </div>
            <div class="mt-3">
                <button class="btn--padrao btn-warning" id="btn--filtros">
                    Filtrar
                </button>
                <button type="reset" class="btn--padrao btn-add" id="btn--limparFiltros">
                    Limpar
                </button>
            </div>
        </form>
        </div>
    </div>
    
    <div class="spinner-border text-primary" role="status">
        <span class="visually-hidden">Loading...</span>
    </div>

    <script src="../../js/jquery.js?<?php echo VERSION; ?>"></script>
    <script src="../../libs/jqueryMask/jqueryMask.js?<?php echo VERSION; ?>"></script>
    <script src="../../libs/maskmoney.js?<?php echo VERSION; ?>"></script>
    <script src="../../libs/bootstrap/js/bootstrap.min.js?<?php echo VERSION; ?>"></script>
    <script src="../../js/functions.js?<?php echo VERSION; ?>"></script>
    <script src="../../js/base.js?<?php echo VERSION; ?>"></script>
    <script src="../../js/contratos.js?<?php echo VERSION; ?>"></script>
</body>

</html>