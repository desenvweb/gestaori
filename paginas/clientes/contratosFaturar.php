<?php
session_start();
require '../../php/utils.php';

validaLogin();
logout();

?>
<!DOCTYPE html>
<html lang="pt-BR">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link href="../../libs/bootstrap/css/bootstrap.min.css?<?php echo VERSION; ?>" rel="stylesheet">
    <link rel="stylesheet" href="../../css/formBase.css?<?php echo VERSION; ?>">
    <link rel="stylesheet" href="../../css/base.css?<?php echo VERSION; ?>">
    <title>Sistema de Gestão</title>
</head>

<body>
    <div class="box flex-box">
        <div class="content">

            <?php
            require '../componentes/header.html';
            require '../componentes/sidebar.html';
            ?>

            <div class="body2">
                <div class="list--div">
                    <section class="list--header">
                        <div class="col col-md-8">
                            <h3>Contratos a Faturar</h3>
                        </div>
                        <button class="btn--padrao btn-add" data-bs-toggle="modal" data-bs-target="#modalInserir" style="display: none;">
                            Adicionar Cadastro
                        </button>

                    </section>
                    <section class="list--table mt-5">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th scope="col">Cliente</th>
                                        <th scope="col">Data Faturamento</th>
                                        <th scope="col">Valor Contrato</th>
                                        <th scope="col">Indice</th>
                                        <th scope="col">Tipo Contrato</th>
                                        <th scope="col">Peridiocidade</th>
                                        <th scope="col">Ações</th>
                                    </tr>
                                </thead>
                                <tbody id="tbody">
                                </tbody>
                                <tbody id="tbodysearch">
                                </tbody>
                            </table>
                        </div>
                    </section>
                </div>
            </div>

        </div>
    </div>

    <!-- Modal Inserir Cliente -->
    <div class="modal fade" id="modalInserir" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Cadastro de contratos a faturar</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <form method="post" id="forminserir" enctype="multipart/form-data">
                    <div class="modal-body">
                        <div class="form--area">
                            <h3>Informações Básicas do Cliente</h3>
                            <div class="row" style="width: 100%;">
                                <div class="col-md-6">
                                    <label>Razão Social</label>
                                    <input type="text" name="razao_social" class="form-control" required>
                                </div>
                                <div class="col-md-6">
                                    <label>Nome Fantasia</label>
                                    <input type="text" name="nome_fantasia" class="form-control" required>
                                </div>
                            </div>
                            <div class="row" style="width: 100%;">
                                <div class="col-md-6">
                                    <label>CNPJ</label>
                                    <input type="text" name="cnpj" class="form-control" required>
                                </div>
                                <div class="col-md-6">
                                    <label>Inscrição Estadual</label>
                                    <input type="text" name="inscricao_estadual" class="form-control">
                                </div>
                            </div>
                            <h3>Localização</h3>
                            <div class="row" style="width: 100%;">
                                <div class="col-md-3">
                                    <label>CEP</label>
                                    <input type="text" name="cep" class="form-control">
                                </div>
                                <div class="col-md-6">
                                    <label>Logradouro</label>
                                    <input type="text" name="logradouro" class="form-control">
                                </div>
                                <div class="col-md-3">
                                    <label>Número</label>
                                    <input type="text" name="numero" maxlength="10" class="form-control">
                                </div>
                            </div>
                            <div class="row" style="width: 100%;">
                                <div class="col-md-5">
                                    <label>Bairro</label>
                                    <input type="text" name="bairro" class="form-control">
                                </div>
                                <div class="col-md-5">
                                    <label>Cidade</label>
                                    <input type="text" name="cidade" class="form-control">
                                </div>
                                <div class="col-md-2">
                                    <label>UF</label>
                                    <input type="text" name="uf" class="form-control">
                                </div>
                            </div>
                            <h3>Contato</h3>
                            <div class="row" style="width: 100%;">
                                <div class="col-md-3">
                                    <label>Telefone</label>
                                    <input type="text" name="telefone" class="form-control">
                                </div>
                                <div class="col-md-3">
                                    <label>Contato</label>
                                    <input type="text" name="contato" class="form-control">
                                </div>
                                <div class="col-md-6">
                                    <label>E-mail</label>
                                    <input type="email" name="email" placeholder="E-mail" class="form-control">
                                </div>
                            </div>
                            <h3>Dados do Contrato</h3>
                            <div class="row" style="width: 100%;">
                                <div class="col-md-6">
                                    <label>Tipo do Contrato</label>
                                    <select name="tipo_contrato" id="inputTipoContrato" class="form-control" required>
                                        <option value="manutencao">Manutenção</option>
                                        <option value="saas">SaaS</option>
                                        <option value="manutencao_saas">Manutenção + SaaS</option>
                                    </select>
                                </div>
                                <div class="col-md-6">
                                    <label>Data Faturamento</label>
                                    <input type="date" name="dt_faturamento" class="form-control" required>
                                </div>
                            </div>
                            <div class="row" style="width: 100%;">
                                <div class="col-md-6" data-nameInput="horas_totais">
                                    <label>Total de Horas</label>
                                    <input type="text" name="horas_totais" class="form-control onlynumber" required>
                                </div>
                                <div class="col-md-6" data-nameInput="usuarios_saas" style="display: none;">
                                    <label>Qntd. de Usuários SaaS</label>
                                    <input type="text" name="usuarios_saas" class="form-control onlynumber">
                                </div>                        
                            </div>
                            <div class="row" style="width: 100%;">
                                <div class="col-md-4">
                                    <label>Valor do Contrato</label>
                                    <input type="text" name="valor_total" class="form-control money" required>
                                </div>
                                <div class="col-md-4">
                                    <label>Multa</label>
                                    <input type="text" name="multa" class="form-control" required>
                                </div>
                                <div class="col-md-4">
                                    <label>Empresa Contratada</label>
                                    <select name="empresa_contratada" class="form-control" required>
                                        <option value="sigma">Sigma</option>
                                        <option value="manut">Manut</option>
                                        <option value="microcenter">Microcenter</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row" style="width: 100%;">
                                <div class="col-md-6">
                                    <label>índice do Contrato</label>
                                    <select name="indice" class="form-control" required>
                                        <option value="IGPM">IGPM</option>
                                        <option value="INPC">INPC</option>
                                        <option value="IPCA">IPCA</option>
                                    </select>
                                </div>
                                <div class="col-md-6">
                                    <label>Periodicidade do Pagamento</label>
                                    <select name="periodicidade_pagamento" class="form-control" required>
                                        <option value="anual">Anual</option>
                                        <option value="mensal">Mensal</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row" style="width: 100%;">
                                <div>
                                    <label>Observações</label>
                                    <textarea name="observacoes" class="form-control" maxlength="400"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="reset" class="btn btn-secondary defaultButton" data-bs-dismiss="modal" style="display: none;">Cancelar</button>
                        <button class="btn btn-primary defaultButton" style="display: none;">Inserir</button>
                        <button class="btn btn-primary loadingButton" type="button" disabled>
                            <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                            Salvando...
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Modal Inserir Cliente -->
    <div class="modal fade" id="modalExibirDados" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Dados do contrato a faturar</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <form class="formpermiss" method="post" id="form#editar" enctype="multipart/form-data">
                    <input type="hidden" name="id" id="id">
                    <input type="hidden" name="id_cadastrante" id="id_cadastrante">
                    <input type="hidden" name="status" id="status">
                    <div class="modal-body">
                        <div class="form--area">
                            <h3>Informações Básicas do Cliente</h3>
                            <div class="row" style="width: 100%;">
                                <div class="col-md-6">
                                    <label>Razão Social</label>
                                    <input type="text" name="razao_social" id="razao_social" class="form-control" required>
                                </div>
                                <div class="col-md-6">
                                    <label>Nome Fantasia</label>
                                    <input type="text" name="nome_fantasia" id="nome_fantasia" class="form-control" required>
                                </div>
                            </div>
                            <div class="row" style="width: 100%;">
                                <div class="col-md-6">
                                    <label>CNPJ</label>
                                    <input type="text" name="cnpj" id="cnpj" class="form-control" required>
                                </div>
                                <div class="col-md-6">
                                    <label>Inscrição Estadual</label>
                                    <input type="text" name="inscricao_estadual" id="inscricao_estadual" class="form-control">
                                </div>
                            </div>
                            <h3>Localização</h3>
                            <div class="row" style="width: 100%;">
                                <div class="col-md-3">
                                    <label>CEP</label>
                                    <input type="text" name="cep" id="cep" class="form-control">
                                </div>
                                <div class="col-md-6">
                                    <label>Logradouro</label>
                                    <input type="text" name="logradouro" id="logradouro" class="form-control">
                                </div>
                                <div class="col-md-3">
                                    <label>Número</label>
                                    <input type="text" name="numero" id="numero" maxlength="10" class="form-control">
                                </div>
                            </div>
                            <div class="row" style="width: 100%;">
                                <div class="col-md-5">
                                    <label>Bairro</label>
                                    <input type="text" name="bairro" id="bairro" class="form-control">
                                </div>
                                <div class="col-md-5">
                                    <label>Cidade</label>
                                    <input type="text" name="cidade" id="cidade" class="form-control">
                                </div>
                                <div class="col-md-2">
                                    <label>UF</label>
                                    <input type="text" name="uf" id="uf" class="form-control">
                                </div>
                            </div>
                            <h3>Contato</h3>
                            <div class="row" style="width: 100%;">
                                <div class="col-md-3">
                                    <label>Telefone</label>
                                    <input type="text" name="telefone" id="telefone" class="form-control">
                                </div>
                                <div class="col-md-3">
                                    <label>Contato</label>
                                    <input type="text" name="contato" id="contato" class="form-control">
                                </div>
                                <div class="col-md-6">
                                    <label>E-mail</label>
                                    <input type="email" name="email" id="email" placeholder="E-mail" class="form-control">
                                </div>
                            </div>
                            <h3>Dados do Contrato</h3>
                            <div class="row" style="width: 100%;">
                                <div class="col-md-6">
                                    <label>Tipo do Contrato</label>
                                    <select name="tipo_contrato" id="inputTipoContratoEdit" class="form-control" onchange="changeTipoContrato(this.value)" required>
                                        <option value="manutencao">Manutenção</option>
                                        <option value="saas">SaaS</option>
                                        <option value="manutencao_saas">Manutenção + SaaS</option>
                                    </select>
                                </div>
                                <div class="col-md-6">
                                    <label>Data Faturamento</label>
                                    <input type="date" name="dt_faturamento" id="dt_faturamento" class="form-control" required>
                                </div>
                            </div>
                            <div class="row" style="width: 100%;">
                                <div class="col-md-6" data-nameInput="horas_totais_edit">
                                    <label>Total de Horas</label>
                                    <input type="text" name="horas_totais" id="horas_totais" class="form-control onlynumber" required>
                                </div>
                                <div class="col-md-6" data-nameInput="usuarios_saas_edit" style="display: none;">
                                    <label>Qntd. de Usuários SaaS</label>
                                    <input type="text" name="usuarios_saas" id="usuarios_saas" class="form-control onlynumber">
                                </div>                        
                            </div>
                            <div class="row" style="width: 100%;">
                                <div class="col-md-4">
                                    <label>Valor do Contrato</label>
                                    <input type="text" name="valor_total" id="valor_total" class="form-control money" required>
                                </div>
                                <div class="col-md-4">
                                    <label>Multa</label>
                                    <input type="text" name="multa" id="multa" class="form-control" required>
                                </div>
                                <div class="col-md-4">
                                    <label>Empresa Contratada</label>
                                    <select name="empresa_contratada" id="empresa_contratada" class="form-control" required>
                                        <option value="sigma">Sigma</option>
                                        <option value="manut">Manut</option>
                                        <option value="microcenter">Microcenter</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row" style="width: 100%;">
                                <div class="col-md-6">
                                    <label>índice do Contrato</label>
                                    <select name="indice" id="indice" class="form-control" required>
                                        <option value="IGPM">IGPM</option>
                                        <option value="INPC">INPC</option>
                                        <option value="IPCA">IPCA</option>
                                    </select>
                                </div>
                                <div class="col-md-6">
                                    <label>Periodicidade do Pagamento</label>
                                    <select name="periodicidade_pagamento" id="periodicidade_pagamento" class="form-control" required>
                                        <option value="anual">Anual</option>
                                        <option value="mensal">Mensal</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row" style="width: 100%;">
                                <div>
                                    <label>Observações</label>
                                    <textarea name="observacoes" id="observacoes" class="form-control" maxlength="400"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="reset" class="btn btn-secondary defaultButton" data-bs-dismiss="modal" style="display: none;">Fechar</button>
                        <button class="btn btn-primary defaultButton" style="display: none;">Salvar Mudanças</button>
                        <button class="btn btn-primary loadingButton" type="button" disabled>
                            <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                            Salvando...
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <?php
    include_once '..componentes/trabalhando_agora.html';
    ?>
  
    <div class="spinner-border text-primary" role="status">
        <span class="visually-hidden">Loading...</span>
    </div>

    <script src="../../js/jquery.js?<?php echo VERSION; ?>"></script>
    <script src="../../libs/jqueryMask/jqueryMask.js?<?php echo VERSION; ?>"></script>
    <script src="../../libs/maskmoney.js?<?php echo VERSION; ?>"></script>
    <script src="../../libs/bootstrap/js/bootstrap.min.js?<?php echo VERSION; ?>"></script>
    <script src="../../js/functions.js?<?php echo VERSION; ?>"></script>
    <script src="../../js/base.js?<?php echo VERSION; ?>"></script>
    <script src="../../js/contratosFaturar.js?<?php echo VERSION; ?>"></script>
</body>

</html>