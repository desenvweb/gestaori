<?php

//Alterar sempre que atualizar arquivos js/css para não bugar com o cache
//define('VERSION', '22112v5');
define('VERSION', '220613v2');


function validaLogin()
{
    if (empty($_SESSION['logado']) || empty($_COOKIE['user'])) {
        if (!empty($_SESSION['logado'])) {
            unset($_SESSION['logado']);
        }

        if (!empty($_COOKIE['user'])) {
            unset($_COOKIE['user']);
        }

        header('location:../../index.php');
    }
}

function validaPermissao($permissao)
{
    if ($permissao === 1 && $_SESSION['tipo_usuario'] != 1) {
        header("Location:../paginas/clientes/contratos.php");
    }
}


function logout(){
    if (isset($_GET['sair'])) {
        unset($_SESSION['logado']);
        unset($_COOKIE['user']);
        header('location:../../index.php');
    }
}