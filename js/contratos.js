
const formInserir = document.getElementById('form#inserir');
const formEditar = document.getElementById('form#editar');
const formContrato = document.getElementById('formCadastarContrato');
const formCadastrarHoras = document.getElementById('formCadastrarHoras');
const tbody = document.getElementById('tbody');
const tbodySearch = document.getElementById('tbodysearch');
const modalDados = new bootstrap.Modal(document.getElementById('modalExibirDados'), {})
const modalContratos = new bootstrap.Modal(document.getElementById('modalContratos'), {})
const modalInserirContratos = new bootstrap.Modal(document.getElementById('modalCadastrarContrato'), {})
const modalCadastrarHoras = new bootstrap.Modal(document.getElementById('modalCadastrarHoras'), {})
const modalCadastrarAdendo = new bootstrap.Modal(document.getElementById('modalCadastrarAdendo'), {});
const modalRelatorioHoras = new bootstrap.Modal(document.getElementById('modalRelatorioHoras'), {});
const anexoDiv = document.getElementsByClassName('anexo--listItens')[0];
const tpContrato = document.querySelector('input[name="btnradio-tpcontratos"]:checked').value;
let currentTpcontrato = 'todos';

var table = {
    data: false,
    data_search : false,
    search : false,
    pagina: 0,
    loadingScroll: true,

    criarTable: function (data) {
        this.data = data;
        let linhas = this.gerarLinhas(this.data);
        tbody.innerHTML = linhas;
    },

    gerarLinhas: function (data) {
        var linhas = '';

        for (let i in data) {
            let somaContrato = 0;
            let peridiocidade  = '';
            let dataFim = '';
            let status = 'Não Possui'
            let prazo = 0;

            let bgcolor = 'bg--orange';
            let qtdContratos = data[i].contratos.length;

            if (data[i].contratos.length > 0){
                dataFim = bancoToFrontDateUTC(data[i].contratos[qtdContratos-1].dt_vigenciafinal);
                let present_date = new Date();
                let init_date = new Date(data[i].contratos[qtdContratos-1].dt_vigenciafinal);
                let Difference_In_Time = init_date.getTime() - present_date.getTime();
                let Difference_In_Days =  Math.ceil(Difference_In_Time / (1000 * 3600 * 24));
                prazo = Difference_In_Days;
            }

            for (let j in data[i].contratos) {
                somaContrato += parseFloat(data[i].contratos[j].valor_total);
                peridiocidade += data[i].contratos[j].periodicidade_pagamento + ' ';
            }

            if (peridiocidade.length == 0 && data[i].id > 1){
                peridiocidade = 'Help Desk';
            }else if (data[i].id == 1){
                peridiocidade = 'RI'; 
            }

            let valor_total =  parseBRL(somaContrato) 

            if (prazo > 30 ){
                bgcolor = 'bg--green';
                status = 'Ativo';

            } else if (prazo < 0 ){
                bgcolor = 'bg--red';
                status = 'Vencido';
            } else if (prazo != 0){
                bgcolor = 'bg--orange';
                status = 'A vencer';
            }

            linhas += `<tr data-ref="${data[i].id}">`;

            linhas += `<td>${data[i].nome_fantasia}</td>`;
            linhas += `<td>${dataFim}</td>`;
            linhas += `<td>${data[i].saldo_horas}</td>`;
            linhas += `<td>${data[i].qnt_usuariosSaaS}</td>`;
            if(Painel.user.super_admin == 1){
                linhas += `<td>${valor_total}</td>`;
                linhas += `<td>${peridiocidade}</td>`;
            }
            linhas += `<td><span class="table--status ${bgcolor}">${status}</span></td>`;
            linhas += `<td>
                            <span class="flex-box" onclick="exibirRelatorioHs(${data[i].id});" data-toggle="tooltip" title="Horas Lançadas">
                                <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" fill="currentColor" class="bi bi-alarm" viewBox="0 0 20 20">
                                    <path d="M8.5 5.5a.5.5 0 0 0-1 0v3.362l-1.429 2.38a.5.5 0 1 0 .858.515l1.5-2.5A.5.5 0 0 0 8.5 9V5.5z"/>
                                    <path d="M6.5 0a.5.5 0 0 0 0 1H7v1.07a7.001 7.001 0 0 0-3.273 12.474l-.602.602a.5.5 0 0 0 .707.708l.746-.746A6.97 6.97 0 0 0 8 16a6.97 6.97 0 0 0 3.422-.892l.746.746a.5.5 0 0 0 .707-.708l-.601-.602A7.001 7.001 0 0 0 9 2.07V1h.5a.5.5 0 0 0 0-1h-3zm1.038 3.018a6.093 6.093 0 0 1 .924 0 6 6 0 1 1-.924 0zM0 3.5c0 .753.333 1.429.86 1.887A8.035 8.035 0 0 1 4.387 1.86 2.5 2.5 0 0 0 0 3.5zM13.5 1c-.753 0-1.429.333-1.887.86a8.035 8.035 0 0 1 3.527 3.527A2.5 2.5 0 0 0 13.5 1z"/>
                                </svg>
                            </span>
                        </td>`;
            //if(Painel.user.super_admin == 1){
            if (Painel.user.tipo_usuario == 1) {
                linhas += `<td>
                                <span class="flex-box" onclick="exibirDados(${data[i].id});"  data-toggle="tooltip" title="Dados do Cliente">
                                    <svg xmlns="http://www.w3.org/2000/svg" class="svg--color" width="30" height="30" fill="currentColor" class="bi bi-journals" viewBox="0 0 20 20">
                                        <path d="M14.5 3a.5.5 0 0 1 .5.5v9a.5.5 0 0 1-.5.5h-13a.5.5 0 0 1-.5-.5v-9a.5.5 0 0 1 .5-.5h13zm-13-1A1.5 1.5 0 0 0 0 3.5v9A1.5 1.5 0 0 0 1.5 14h13a1.5 1.5 0 0 0 1.5-1.5v-9A1.5 1.5 0 0 0 14.5 2h-13z"/>
                                        <path d="M3 8.5a.5.5 0 0 1 .5-.5h9a.5.5 0 0 1 0 1h-9a.5.5 0 0 1-.5-.5zm0 2a.5.5 0 0 1 .5-.5h6a.5.5 0 0 1 0 1h-6a.5.5 0 0 1-.5-.5zm0-5a.5.5 0 0 1 .5-.5h9a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-9a.5.5 0 0 1-.5-.5v-1z"/>
                                    </svg>
                                    
                                </span>
                            </td>`;
                linhas += `<td>
                            <span class="flex-box" onclick="exibirContratos(${data[i].id});" data-toggle="tooltip" title="Contratos">
                                <svg xmlns="http://www.w3.org/2000/svg" class="text-success" width="30" height="30" fill="currentColor" class="bi bi-files" viewBox="0 0 20 20">
                                    <path d="M13 0H6a2 2 0 0 0-2 2 2 2 0 0 0-2 2v10a2 2 0 0 0 2 2h7a2 2 0 0 0 2-2 2 2 0 0 0 2-2V2a2 2 0 0 0-2-2zm0 13V4a2 2 0 0 0-2-2H5a1 1 0 0 1 1-1h7a1 1 0 0 1 1 1v10a1 1 0 0 1-1 1zM3 4a1 1 0 0 1 1-1h7a1 1 0 0 1 1 1v10a1 1 0 0 1-1 1H4a1 1 0 0 1-1-1V4z"/>
                                </svg>
                                
                            </span>
                        </td>`;
            }
            if(Painel.user.super_admin == 1){
                linhas += `<td>
                                <span class="flex-box" onclick="excluirCliente(${data[i].id});"  data-toggle="tooltip" title="Excluir Cliente">
                                    <svg xmlns="http://www.w3.org/2000/svg" class="text-danger" width="30" height="30" fill="currentColor" class="bi bi-trash" viewBox="0 0 20 20">
                                        <path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z"/>
                                        <path fill-rule="evenodd" d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z"/>
                                    </svg>
                                </span>
                            </td>`;
            }
            linhas += '</tr>';

            linhas += '</tr>';
        }

        return linhas;
    },

    adicionarTable: function (data) {
        let linhas = this.gerarLinhas(data);
        tbody.innerHTML += linhas;
        for(let i in data){
            this.data.push(data[i])
        }
    },

    pesquisa: async function (search) {
        let data = await getDataGET(`${url_base}/api/get_empresas?condicao=${search}&limite=100&tpcontrato=${currentTpcontrato}`);

        data = isJson(data);
        this.data_search = data.data;

        if (search != '') {
            let linhas = this.gerarLinhas(data.data);
            tbodySearch.innerHTML = linhas;
            $(tbody).hide();
            $(tbodySearch).show();
            this.search = true;
        } else {
            $(tbody).show();
            $(tbodySearch).hide();
            this.search = false;
        }
    }
}

var modalData = {
    empresa_data: false,
    contrato_id : false,
}

var tableRelatorio = {
    data: false,

    criarTable: function (data) {
        this.data = data;
        let linhas = this.gerarLinhas(this.data);
        tbodyRelatorio.innerHTML = linhas;
    },

    gerarLinhas: function (data) {
        var linhas = '';

        for (let i in data) {
                    
                let dt_inicio = bancoToFrontHoras(data[i].dt_inicio)
                let dt_fim = bancoToFrontHoras(data[i].dt_fim)

                linhas += `<tr>`;
                linhas += `<td>${data[i].id_ticket}</td>`
                linhas += `<td>${data[i].nome}</td>`
                linhas += `<td>${dt_inicio}</td>`
                linhas += `<td>${dt_fim}</td>`
                linhas += `<td>${data[i].diff_horas}</td>`; 

                linhas += '</tr>';
        }
        
        return linhas;
    },
}

$(document).ready(function () {
    if(Painel.user.super_admin == 1){
    //if (Painel.user.tipo_usuario == 2) {
        $('[data-bs-target="#modalInserirCliente"], [data-bs-target="#modalDadosGerais"]').show();
        $('.defaultButton').show();
    }else{
        $('.formpermiss :input').prop('disabled', true);
        $('.formpermiss formEditarContrato :input').prop('disabled', true);
        $('.hidepermiss').hide();
    }
        
    if(Painel.user.super_admin == 1){
        dadosGerais();
    }
    
    (async function () {
        let data = await getDataGET(`${url_base}/api/get_empresas?limite=${limite}&tpcontrato=${currentTpcontrato}`);
        data = isJson(data);
        table.criarTable(data.data);
        
        $('.spinner-border').hide();
    })()
})

formEditar.addEventListener('submit', async function (e) {
    e.preventDefault();
    let formData = new FormData(formEditar);

    ajaxDataForm(`${url_base}/api/editar_empresa`, formData,
        function (resp) {
            resp = isJson(resp);
            $('.defaultButton').show();
            $('.loadingButton').hide();

            if (resp.status == 1) {
                alert('Salvo com sucesso.')
                window.location.reload();
                $('#searchInput').val('');
                $(tbody).show();
                $(tbodySearch).hide();
            
            } else {
                alert(resp.message ? resp.message : resp.error);
            }
        }, function () {
            //Before send
            $('.defaultButton').hide();
            $('.loadingButton').show();
        })

        $('.defaultButton').show();
        $('.loadingButton').hide();
})

formCadastrarHoras.addEventListener('submit', async function (e) {
    e.preventDefault();
    if (modalData.empresa_data !== false) {
        empresa = modalData.empresa_data;

        let msg = `Tem certeza que deseja cadastrar horas para o cliente ${empresa.nome_fantasia}?`;
        if (confirm(msg) === false) return;

        let formData = new FormData(formCadastrarHoras);
        formData.set('id_empresa', empresa.id);

        ajaxDataForm(`/api/cadastrar_horasCompradas`, formData,
            function (resp) {
                resp = isJson(resp);
                $('.defaultButton').show();
                $('.loadingButton').hide();

                if (resp.status == 1) {
                    alert('Salvo com sucesso.')
                    window.location.reload();
                    $('#searchInput').val('');
                    $(tbody).show();
                    $(tbodySearch).hide();
                
                } else {
                    alert(resp.message ? resp.message : resp.error);
                }
            }, function () {
                //Before send
                $('.defaultButton').hide();
                $('.loadingButton').show();
            })

        $('.defaultButton').show();
        $('.loadingButton').hide();
    }
})

function exibirDados(Id){
    let id = Id;
    if (!id) return

    if(!table.search){
        var data = table.data.filter(el => el.id == id);
    }else{
        var data = table.data_search.filter(el => el.id == id);
    }

    $('#idCliente').val(id);

    for (let i in data[0]) {
        $('#' + i).val(data[0][i]);
    }

    if (data[0].horas_compradas.length > 0){
        document.getElementById('accordionHorasAvulsas').style.display = 'inline';
        $('#accordionHorasAvulsas').html(accordion_hora_html(data[0].horas_compradas));
    }else{
        document.getElementById('accordionHorasAvulsas').style.display = 'none';
    }

    modalDados.show();
}

function exibirRelatorioHs(Id){

    let id = Id;//$(this).data('ref');
    if (!id) return

    if(!table.search){
        var data = table.data.filter(el => el.id == id);
    }else{
        var data = table.data_search.filter(el => el.id == id);
    }

    modalData.empresa_data = data[0];

    modalRelatorioHoras.show();
}

function exibirContratos(Id){
    let id = Id;//$(this).data('ref');
    if (!id) return

    if(!table.search){
        var data = table.data.filter(el => el.id == id);
    }else{
        var data = table.data_search.filter(el => el.id == id);
    }

    modalData.empresa_data = data[0];

    $('#idCliente').val(id);

    if (data[0].contratos.length == 0){
        document.getElementById('addAdendo').style.display = 'none';
        if(Painel.user.super_admin == 1){
            modalInserirContratos.show();
        }else{
            alert('Não há contrato cadastrado para visualização !');
        }
    }else{
        if(Painel.user.super_admin == 1){
            document.getElementById('addAdendo').style.display = 'inline';
        }else{
            document.getElementById('addAdendo').style.display = 'none';
        }
        $('#accordionContrato').html(acordion_contratos_html(data[0].contratos));
        changeTipoContrato(currentTpcontrato);

        modalContratos.show();
    }

    changeTipoContrato(data[0].contratos[0].tipo_contrato);
    
}

function excluirCliente(Id){

    if (!confirm('Tem certeza que deseja excluir esse cliente?')) return false;

    ajaxRequestGet(`${url_base}/api/excluir_empresa?id_empresa=${Id}`, function (resp) {
        resp = isJson(resp);
        if (resp.status == 1) {
            alert('Excluido com sucesso.')
            document.location.reload();
            $('#searchInput').val('');
            $(tbody).show();
            $(tbodySearch).hide();
        
        } else {
            alert(resp.message ? resp.message : resp.error);
        }
    })
}

function excluirHs(Id, id_empresa, hs){

    if (!confirm('Tem certeza que deseja excluir estas horas avulsas ?')) return false;

    ajaxRequestGet(`${url_base}/api/excluir_horasCompradas?id_lancamento=${Id}&id_lancamento=${id_empresa}&total_horas=${hs}`, function (resp) {
        resp = isJson(resp);
        if (resp.status == 1) {
            alert('Excluido com sucesso.')
            document.location.reload();
            $('#searchInput').val('');
            $(tbody).show();
            $(tbodySearch).hide();
        
        } else {
            alert(resp.message ? resp.message : resp.error);
        }
    })
}

function excluirContrato(Id){

    if (!confirm('Tem certeza que deseja excluir esse contrato?')) return false;

    ajaxRequestGet(`${url_base}/api/excluir_contrato?id_contrato=${Id}`, function (resp) {
        resp = isJson(resp);
        if (resp.status == 1) {
            alert('Excluido com sucesso.');
            document.location.reload();
            $('#searchInput').val('');
            $(tbody).show();
            $(tbodySearch).hide();
        
        } else {
            alert(resp.message ? resp.message : resp.error);
        }
    })
}


$('.table-responsive').on('scroll', function () {
    if(table.loadingScroll){
        let value = parseInt( Math.round(this.scrollTop) / (this.scrollHeight - this.clientHeight));
        if (value == 1) {
            table.pagina++;

            let contratada = $('#contratada_filtro').val() ?? '';
            let periodicidade_pag = $('#periodicidade_pagamento_filtro').val() ?? '';

            let query = `limite=${limite}&pagina=${table.pagina}
            &contratada=${contratada}&periodicidade_pagamento_filtro=${periodicidade_pag}`;

            (async function () {
                let resp = await getDataGET(`${url_base}/api/get_empresas?${query}&limite=${limite}&tpcontrato=${currentTpcontrato}`);
                resp = isJson(resp);
                if (resp.status == 1) {
                    table.adicionarTable(resp.data);
                }else{
                    //table.pagina--;
                }
    
            })()
        };
    }
})

formContrato.addEventListener('submit', async function (e) {
    e.preventDefault();

    if (modalData.empresa_data !== false) {
        empresa = modalData.empresa_data;

        let msg = `Tem certeza que deseja cadastrar um contrato para o cliente ${empresa.nome_fantasia}? Confira os dados, alguns deles não são possíveis editar após cadastrar.`;
        if (confirm(msg) === false) return;

        let formData = new FormData(formContrato);
        formData.set('id_empresa', empresa.id);

        ajaxDataForm(`/api/add_contrato`, formData,
            function (resp) {
                resp = isJson(resp);
                $('.defaultButton').show();
                $('.loadingButton').hide();

                if (resp.status == 1) {
                    alert('Salvo com sucesso.')
                    window.location.reload();
                    $('#searchInput').val('');
                    $(tbody).show();
                    $(tbodySearch).hide();
                
                } else {
                    alert(resp.message ? resp.message : resp.error);
                }
            }, function () {
                //Before send
                $('.defaultButton').hide();
                $('.loadingButton').show();
            })

        $('.defaultButton').show();
        $('.loadingButton').hide();

    }
})

$('#searchInput').keyup(function () {
    table.pesquisa($(this).val());
})

$('#filtros').submit(async function(e){
    e.preventDefault();

    let data_inicio = $('#dtvgfinal_inicio').val();
    let data_fim = $('#dtvgfinal_fim').val();
    let contratada = $('#contratada_filtro').val() ?? '';
    let periodicidade_pag = $('#periodicidade_pagamento_filtro').val() ?? '';

    let query = `dtvgfinal_inicio=${data_inicio}&dtvgfinal_fim=${data_fim}&contratada=${contratada}&periodicidade_pagamento_filtro=${periodicidade_pag}`;
    table.pagina = 0;

    let data = await getDataGET(`${url_base}/api/get_empresas?${query}&limite=${limite}&tpcontrato=${currentTpcontrato}`);
    data = isJson(data);
    table.criarTable(data.data);
})

$('#btn--limparFiltros').click(async function(){
    let data = await getDataGET(`${url_base}/api/get_empresas?limite=${limite}&tpcontrato=${currentTpcontrato}`);
    data = isJson(data);
    table.criarTable(data.data);
})

async function dadosGerais(){
    let data = await getDataGET(`${url_base}/api/dados_gerais`);
    data = isJson(data);
    data = data.data;

    let valor_mensal_brl = parseBRL(data.valor_mensal_total)
    let valor_anual_brl =  parseBRL(data.valor_anual_total) 
    let valor_total =  parseBRL(data.valor_total) 
    
    $('#soma_valor_mensal_contratos').text(valor_mensal_brl);
    $('#soma_valor_anual_contratos').text(valor_anual_brl);
    $('#soma_valor_geral_contratos').text(valor_total);
    $('#total_clientes').text(data.total_clientes + ' usuários Saas');
    $('#qntTotal_usuarios_saas').text(data.qnt_usuarios_saas + ' clientes')

}

$("[name='tipo_licenca']").change(function(){
    viewSaas(this.value);
})

formInserir.addEventListener('submit', async function (e) {
    e.preventDefault();
    let formData = new FormData(formInserir);
    ajaxDataForm(`${url_base}/api/cadastrar_empresa`, formData,
        function (resp) {
            resp = isJson(resp);
            $('.defaultButton').show();
            $('.loadingButton').hide();
            if (resp.status == 1) {
                alert('Salvo com sucesso.')
                window.location.reload();
                $('#searchInput').val('');
                $(tbody).show();
                $(tbodySearch).hide();
            
            } else {
                alert(resp.message ? resp.message : resp.error);
            }
        }, function () {
            //Before send
            $('.defaultButton').hide();
            $('.loadingButton').show();
        })
    $('.defaultButton').show();
    $('.loadingButton').hide();
})

$('#relatorio_form').submit(function(e){
    e.preventDefault();
    
    let formData = new FormData(this);

    let empresa = modalData.empresa_data;
    formData.set('id_empresa', empresa.id);

    ajaxDataForm(`${url_base}/api/relatorio_horas_empresa`, formData,
    function (resp) {
        resp = isJson(resp);
        $('.defaultButton').show();
        $('.loadingButton').hide();
        $('#gerar_pdf').show();
        if (resp.status == 1) {
            resp = resp.data
            
            $('#tempo_periodo').text(resp.info.hhmm_trabalhado ?? 0)
            $('#total_servicos').text(resp.info.qnt_servicos ?? 0)
            $('#total_projetos').text(resp.info.total_projetos ?? 0)
            $('#tempo_total_projetos').text(resp.info.horas_totais_projetos ?? 0)


            $('#gerar_pdf').show();
            tableRelatorio.criarTable(resp.horas);
        } else {
            alert(resp.message ? resp.message : resp.error);
        }
    }, function () {
        //Before send
        $('.defaultButton').hide();
        $('.loadingButton').show();
        $('#gerar_pdf').hide();
    })
})

$('[data-bs-target="#modalCadastrarHoras"]').click(function(){
    modalDados.hide();
    $('#limpar_relatorio').click();
})

$('#limpar_relatorio').click(function(){
    $('#tempo_periodo, #total_servicos, #total_projetos, #tempo_total_projetos').text('');
    $('#gerar_pdf').hide();
    tbodyRelatorio.innerHTML = '';
})

$('#gerar_pdf').click(function(){
    let empresa = modalData.empresa_data;

    let a = document.createElement('a');
    let dt_inicial = $('[name="dt_inicial"]').val();
    let dt_final = $('[name="dt_final"]').val();
    let id_empresa = empresa.id;

    let query = `id_empresa=${id_empresa}&dt_inicial=${dt_inicial}&dt_final=${dt_final}` 
    a.href = `/api/relatorio_horas_empresa_pdf?${query}`;
    a.target = '_blank';
    a.click();
})

function viewSaas(value){
    console.log(value);
    if(value === 'saas'){
        $('.saas-div').show();
    }else{
        $('.saas-div').hide();
    }
}

formCadastrarAdendo.addEventListener('submit', async function(e){
    e.preventDefault();

    let contratos = modalData.empresa_data.contratos.length;
    if (modalData.empresa_data.contratos[contratos-1].id !== false) {

        let formData = new FormData(formCadastrarAdendo);
        formData.set('id_contrato', modalData.empresa_data.contratos[contratos-1].id);

        ajaxDataForm(`/api/add_adendo`, formData,
            function (resp) {
                resp = isJson(resp);
                $('.defaultButton').show();
                $('.loadingButton').hide();

                if (resp.status == 1) {
                    alert('Salvo com sucesso.')
                    window.location.reload();
                    $('#searchInput').val('');
                    $(tbody).show();
                    $(tbodySearch).hide();
                
                } else {
                    alert(resp.message ? resp.message : resp.error);
                }
            }, function () {
                //Before send
                $('.defaultButton').hide();
                $('.loadingButton').show();
            })

        $('.defaultButton').show();
        $('.loadingButton').hide();

    }

})

$('#inputTipoContrato').change(function () {
 
    let value = this.value;
    console.log('inputTipoContrato '+ value);

    switch (value) {
        case 'saas':
            $('#formCadastarContrato [data-nameInput="usuarios_saas"]').show();
            $('#formCadastarContrato [name="usuarios_saas"]').prop('required', true);

            $('#formCadastarContrato [data-nameInput="horas_totais"]').hide();
            $('#formCadastarContrato [name="horas_totais"]').prop('required', false);
            break;

        case 'manutencao':
            $('#formCadastarContrato [data-nameInput="usuarios_saas"]').hide();
            $('#formCadastarContrato [name="usuarios_saas"]').prop('required', false);

            $('#formCadastarContrato [data-nameInput="horas_totais"]').show();
            $('#formCadastarContrato [name="horas_totais"]').prop('required', true);
            break;
        case 'manutencao_saas':
            $('#formCadastarContrato [data-nameInput="usuarios_saas"]').show();
            $('#formCadastarContrato [name="usuarios_saas"]').prop('required', true);

            $('#formCadastarContrato [data-nameInput="horas_totais"]').show();
            $('#formCadastarContrato [name="horas_totais"]').prop('required', true);

    }
})

function changeTipoContrato(value){
    console.log('Edit tipo contrato '+ value);
   
    switch (value) {
        case 'saas':
            console.log('entrou saas');
            $('[data-nameInput="usuarios_saas_edit"]').show();
            $('[name="usuarios_saas"]').prop('required', true);

            $('[data-nameInput="horas_totais_edit"]').hide();
            $('[name="horas_totais"]').prop('required', false);
            break;

        case 'manutencao':
            console.log('entrou manutencao');
            $('[data-nameInput="usuarios_saas_edit"]').hide();
            $('[name="usuarios_saas"]').prop('required', false);

            $('[data-nameInput="horas_totais_edit"]').show();
            $('[name="horas_totais"]').prop('required', true);
            break;
        default:
            console.log('entrou manutencaosaas');
            $('[data-nameInput="usuarios_saas_edit"]').show();
            $('[name="usuarios_saas"]').prop('required', true);

            $('[data-nameInput="horas_totais_edit"]').show();
            $('[name="horas_totais"]').prop('required', true);

    }
}

$(document).on('submit', '.formEditarContrato', function(e){
    e.preventDefault();

    console.log('editar contato');

    let formData = new FormData(this);
    
    $("body").css("cursor", "progress");
    ajaxDataForm(`/api/editar_contrato`, formData,
    function (resp) {
        resp = isJson(resp);

        $("body").css("cursor", "default");

        if (resp.status == 1) {
            alert('Salvo com sucesso.')
            window.location.reload();
            $('#searchInput').val('');
            $(tbody).show();
            $(tbodySearch).hide();
        
        } else {
            alert(resp.message ? resp.message : resp.error);
        }
    })

})

function handleClickTpContrato(myRadio) {
    
    currentTpcontrato = myRadio.value;
    console.log(currentTpcontrato);
    $('#searchInput').val('');
    $(tbody).show();
    $(tbodySearch).hide();

    table.pagina = 0;
    (async function () {
        $('.spinner-border').show();
        tbody.innerHTML = '';
        let data = await getDataGET(`${url_base}/api/get_empresas?limite=${limite}&tpcontrato=${currentTpcontrato}`);
        data = isJson(data);
        table.criarTable(data.data);
        
        $('.spinner-border').hide();
    })()
}

function acordion_tabcontratos_html(data) {
    let linhas = '';
    linhas += '<ul class="nav nav-pills" id="TabContratos" role="tablist">';
//    linhas += '     <li class="nav-item" role="presentation">';
    //linhas += '         <button class="nav-link active" id="home-tab" data-bs-toggle="tab" data-bs-target="#home" type="button" role="tab" aria-controls="home" aria-selected="true">Novo Contrato</button>';
    //linhas += '     </li>';
    let tipo_contrato = '';
    
    for (let i in data) {
        switch (data[i].tipo_contrato) {
            case 'manutencao':
                tipo_contrato = 'Contrato de Manutenção';
                break;

            case 'saas':
                tipo_contrato = 'Contrato SaaS';
                break;
            default:
                tipo_contrato = 'Contrato de Manutenção + SaaS';
        }
        linhas += '     <li class="nav-item" role="presentation">';
        if (i==0){
            linhas += `         <button class="nav-link active" id="contrato_${data[i].id}-tab" data-bs-toggle="tab" data-bs-target="#contrato_${data[i].id}" type="button" role="tab" aria-controls="contrato_${data[i].id}" aria-selected="false">${tipo_contrato} &nbsp; - &nbsp; ${data[i].status_descricao}</button>`;
        }else{
            linhas += `         <button class="nav-link" id="contrato_${data[i].id}-tab" data-bs-toggle="tab" data-bs-target="#contrato_${data[i].id}" type="button" role="tab" aria-controls="contrato_${data[i].id}" aria-selected="false">${tipo_contrato} &nbsp; - &nbsp; ${data[i].status_descricao}</button>`;
        }
        
        linhas += '     </li>';
    
    }

    linhas += '</ul>';

    return linhas
}

function acordion_novocontrato_html(data) {
    let linhas = '';
    linhas +=  '<form enctype="multipart/form-data" id="formCadastarContrato">';
    linhas +=  '<br>';
    linhas +=  '<div class="form--row">';
    linhas +=  '<label>Tipo do Contrato&nbsp;</label>';
    linhas +=  '<select name="tipo_contrato" id="inputTipoContrato" onchange="SetTipoContrato(this.value)" class="form-control" required>';
    linhas +=  '<option value="manutencao">Manutenção</option>';
    linhas +=  '<option value="saas">SaaS</option>';
    linhas +=  '</select>';
    linhas +=  '</div>';
    linhas +=  '<div class="form--row">';
    linhas +=  '<div>';
    linhas +=  '<label>Vigência Inicial&nbsp;</label>';
    linhas +=  '<input type="date" name="dt_vigenciainicial" class="form-control" required>';
    linhas +=  '</div>';
    linhas +=  '            <div>';
    linhas +=  '                <label>Vigência Final&nbsp;</label>';
    linhas +=  '                <input type="date" name="dt_vigenciafinal" class="form-control" required>';
    linhas +=  '            </div>';
    linhas +=  '        </div>';
    linhas +=  '        <div class="form--row">';
    linhas +=  '            <div>';
    linhas +=  '                <label>Data do Reajuste</label>';
    linhas +=  '                <input type="date" name="dt_proximoReajuste" class="form-control">';
    linhas +=  '            </div>';
    linhas +=  '        </div>';
    linhas +=  '        <div class="form--row" data-nameInput="horas_totais">';
    linhas +=  '            <label>Total de Horas&nbsp;</label>';
    linhas +=  '            <input type="text" name="horas_totais" class="form-control onlynumber" required>';
    linhas +=  '        </div>';
    linhas +=  '        <div class="form--row">';
    linhas +=  '            <label>Valor do Contrato</label>';
    linhas +=  '            <input type="text" name="valor_total" class="form-control money" required>';
    linhas +=  '        </div>';
    linhas +=  '        <div class="form--row">';
    linhas +=  '            <label>Multa</label>';
    linhas +=  '            <input type="text" name="multa" class="form-control" required>';
    linhas +=  '        </div>';
    linhas +=  '        <div class="form--row">';
    linhas +=  '            <label>Empresa Contratada</label>';
    linhas +=  '            <select name="empresa_contratada" class="form-control" required>';
    linhas +=  '                <option value="sigma">Sigma</option>';
    linhas +=  '                <option value="manut">Manut</option>';
    linhas +=  '                <option value="microcenter">Microcenter</option>';
    linhas +=  '            </select>';
    linhas +=  '        </div>';
    linhas +=  '        <div class="form--row">';
    linhas +=  '            <label>índice do Contrato</label>';
    linhas +=  '            <select name="indice" class="form-control" required>';
    linhas +=  '                <option value="IGPM">IGPM</option>';
    linhas +=  '                <option value="INPC">INPC</option>';
    linhas +=  '                <option value="IPCA">IPCA</option>';
    linhas +=  '            </select>';
    linhas +=  '        </div>';
    linhas +=  '        <div class="form--row">';
    linhas +=  '            <label>Periodicidade do Pagamento</label>';
    linhas +=  '            <select name="periodicidade_pagamento" class="form-control" required>';
    linhas +=  '                <option value="anual">Anual</option>';
    linhas +=  '                <option value="mensal">Mensal</option>';
    linhas +=  '            </select>';
    linhas +=  '        </div>';
    linhas +=  '        <div class="form--row" data-nameInput="usuarios_saas" style="display: none;">';
    linhas +=  '            <label>Qntd. de Usuários SaaS</label>';
    linhas +=  '            <input type="text" name="usuarios_saas" class="form-control onlynumber">';
    linhas +=  '        </div>';
    linhas +=  '        <div class="form--row">';
    linhas +=  '            <label>Observações</label>';
    linhas +=  '            <textarea name="obs" class="form-control"></textarea>';
    linhas +=  '        </div>';
    linhas +=  '        <div class="form--row">';
    linhas +=  '            <label>Adicionar anexo de documentos</label>';
    linhas +=  '            <input type="file" name="anexo[]" class="form-control" multiple="multiple">';
    linhas +=  '        </div>';
    linhas +=  '        <button type="reset" class="btn btn-secondary defaultButton" data-bs-dismiss="modal" style="display: none;">Cancelar</button>';
    linhas +=  '        <button class="btn btn-primary defaultButton" style="display: none;">Cadastrar Contrato</button>';
    linhas +=  '        <button class="btn btn-primary loadingButton" type="button" disabled>';
    linhas +=  '            <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>';
    linhas +=  '            Salvando...';
    linhas +=  '        </button>';
    linhas +=  '</form>';

    return linhas;
}

function acordion_contratos_html(data) {

    let linhas = '';
    linhas = acordion_tabcontratos_html(data);
    linhas += '<div class="tab-content" id="TabContentContratos">';
    for (let i in data) {
        if (i == 0){
            linhas += `     <div class="tab-pane fade show active" id="contrato_${data[i].id}" role="tabpanel" aria-labelledby="contrato_${data[i].id}-tab">`;
        }else{
            linhas += `     <div class="tab-pane fade" id="contrato_${data[i].id}" role="tabpanel" aria-labelledby="contrato_${data[i].id}-tab">`;
        }
        
        let tipo_contrato = '';
        let select_tipo_contrato = '';

        switch (data[i].tipo_contrato) {
            case 'manutencao':
                tipo_contrato = 'Contrato de Manutenção';
                select_tipo_contrato = '<option value="manutencao" selected>Manutenção</option> <option value="saas">SaaS</option> <option value="manutencao_saas">Manutenção + SaaS</option>';
                break;

            case 'saas':
                tipo_contrato = 'Contrato SaaS';
                select_tipo_contrato = '<option value="manutencao">Manutenção</option> <option value="saas" selected>SaaS</option> <option value="manutencao_saas">Manutenção + SaaS</option>';
                break;
            case 'manutencao_saas':
                tipo_contrato = 'Contrato de Manutenção + SaaS';
                select_tipo_contrato = '<option value="manutencao">Manutenção</option> <option value="saas">SaaS</option> <option value="manutencao_saas" selected>Manutenção + SaaS</option>';
                break;
        }


        linhas += '<br>';
        linhas += `<button class="btn--padrao defaultButton" style="display: none;" id="excluirContratoBtn" onclick="excluirContrato(${data[i].id});" style="background-color: #ff2800;">Excluir Contrato</button>`;
        
        linhas += '<form class="formpermiss formEditarContrato">';

        linhas += `<input type="hidden" name="id_contrato" value="${data[i].id}">`;
        

        linhas += '<div class="form--row"> <label>Tipo do Contrato</label>';
        
        linhas += `<select name="tipo_contrato" id="inputTipoContratoEdit" onchange="changeTipoContrato(this.value)" class="form-control" required>`
        linhas += select_tipo_contrato;                
        linhas +=  '</select>';
        linhas += '</div>';

        linhas += '<div class="form--row">';
        linhas += '<div> <label>Vigência Inicial</label>';
        linhas += `<input type="date" name="dt_vigenciainicial" value="${data[i].dt_vigenciainicial}" class="form-control">`;
        linhas += '</div>';

        linhas += '<div> <label>Vigência Final</label>';
        linhas += `<input type="date" name="dt_vigenciafinal" value="${data[i].dt_vigenciafinal}" class="form-control">`;
        linhas += '</div>';
        linhas += '</div>';

        linhas += '<div> <label>Próximo Reajuste</label>';
        linhas += `<input type="date" name="dt_proximoReajuste" value="${data[i].dt_proximoReajuste}" class="form-control">`;
        linhas += '</div>';

        
            linhas += '<div class="form--row" data-nameInput="horas_totais_edit">';
            linhas += '<div> <label>Total de Horas</label>';
            if (data[i].tipo_contrato === 'manutencao' || data[i].tipo_contrato === 'manutencao_saas') {
                linhas += `<input type="text" name="horas_totais" value="${data[i].horas_totais}" class="form-control">`;
            }else{
                linhas += `<input type="text" name="horas_totais" class="form-control">`;
            }
            linhas += '</div>';
            linhas += '<div> <label> Horas Mês </label>';
            if (data[i].tipo_contrato === 'manutencao' || data[i].tipo_contrato === 'manutencao_saas') {
                linhas += `<input type="text" name="horas_mes" value="${data[i].horas_mes}" class="form-control">`;
            }else{
                linhas += `<input type="text" name="horas_mes" class="form-control">`;
            }
            linhas += '</div> </div>';

        
            linhas += '<div class="form--row" data-nameInput="usuarios_saas_edit">';
            linhas += '<label>Usuários SaaS</label>';
            if (data[i].tipo_contrato === 'saas' || data[i].tipo_contrato === 'manutencao_saas') {
                linhas += `<input type="text" name="usuarios_saas" value="${data[i].usuarios_saas}" class="form-control">`;
            }else{
                linhas += `<input type="text" name="usuarios_saas" class="form-control">`;
            }
            linhas += '</div>';

        linhas += '<div class="form--row"> <label>Valor do Contrato</label>';
        linhas += `<input type="text" name="valor_total" value="${data[i].valor_total}" class="form-control">`;
        linhas += '</div>';


        linhas += '<div class="form--row"> <label>Multa</label>';
        linhas += `<input type="text" name="multa" value="${data[i].multa}" class="form-control">`;
        linhas += '</div>';

        let empresas = {
            'Sigma' : {
                'value' : 'sigma',
                'selected' : data[i].empresa_contratada === 'sigma' ? true : false
            },
            'Manut' : {
                'value' : 'manut',
                'selected' : data[i].empresa_contratada === 'manut' ? true : false
            },
            'Microcenter' : {
                'value' : 'microcenter',
                'selected' : data[i].empresa_contratada === 'microcenter' ? true : false
            }
        }

        let selectEMP = gerar_select('empresa_contratada', 'form-control', empresas);

        linhas += '<div class="form--row"> <label>Empresa Contratada</label>';
        linhas += selectEMP.outerHTML;
        linhas += '</div>';

        let pp = {
            'Anual' : {
                'value' : 'anual',
                'selected' : data[i].periodicidade_pagamento === 'anual' ? true : false
            },
            'Mensal' : {
                'value' : 'mensal',
                'selected' : data[i].periodicidade_pagamento === 'mensal' ? true : false
            }
        }

        let selectPP = gerar_select('periodicidade_pagamento', 'form-control', pp);

        linhas += '<div class="form--row"> <label>Periodicidade do Pagamento</label>';
        linhas += selectPP.outerHTML;
        linhas += '</div>';


        let indices = {
            'IGPM' : {
                'value' : 'IGPM',
                'selected' : data[i].indice === 'IGPM' ? true : false
            },
            'INPC' : {
                'value' : 'INPC',
                'selected' : data[i].indice === 'INPC' ? true : false
            },
            'IPCA' : {
                'value' : 'IPCA',
                'selected' : data[i].indice === 'IPCA' ? true : false
            }
        }

        let selectIndices = gerar_select('indice', 'form-control hidepermiss', indices);

        linhas += '<div class="form--row"> <label>Indice do Contrato</label>';
        linhas += selectIndices.outerHTML;
        linhas += '</div>';

        linhas += '<div class="form--row"> <label>Observações</label>';
        linhas += `<textarea class="form-control" name="obs">${data[i].obs}</textarea>`;
        linhas += '</div>';
    
        
        linhas += '<div>';
        linhas += '<h3>Anexos</h3>';
        linhas += '<div class="form--row">';
        linhas +=  '<label>Adicionar anexo de documentos</label>';
        linhas +=  '<input type="file" name="anexo[]" class="form-control" multiple="multiple">';
        linhas +=  '</div>';



        linhas += '<div class="form--row anexo--listItens hidepermiss">';
        let hide = Painel.user.tipo_usuario == 2 ? 'hide' : '';
        for(let a in data[i].anexos){
            linhas += `<div data-id_anexo='${data[i].anexos[a].id}'>`;
            linhas += '<div class="anexo--item">';
            linhas += `<a href="${data[i].anexos[a].link}" target="_blank">${data[i].anexos[a].nome}</a>`
            if(Painel.user.super_admin == 1){
                linhas += `<svg class="${hide}" style="cursor: pointer;" onclick="excludeAnexo(${data[i].anexos[a].id})" xmlns="http://www.w3.org/2000/svg" height="24px" viewBox="0 0 24 24" width="24px" fill="#000000"><path d="M0 0h24v24H0V0z" fill="none"/><path d="M16 9v10H8V9h8m-1.5-6h-5l-1 1H5v2h14V4h-3.5l-1-1zM18 7H6v12c0 1.1.9 2 2 2h8c1.1 0 2-.9 2-2V7z"/></svg>`
            }
            linhas += '</div>';
            linhas += '</div>';
        }
        linhas += '</div>'
        linhas += '<br>';
        if(Painel.user.super_admin == 1){
            linhas += '<button class="btn btn-primary defaultButton"">Salvar Mudanças</button>';
        }

        linhas += '<hr>';
        linhas += '<div>';
        linhas += '<div>';
        linhas += `<h3 style="display: inline-block;">Adendos</h3>`
        for(let a in data[i].adendos){
            let data_antiga = bancoToFrontDateUTC(data[i].adendos[a].dt_vigenciafinal_antiga);
            let data_nova = bancoToFrontDateUTC(data[i].adendos[a].dt_vigenciafinal_nova);

            linhas += `<p>Adendo extende a validade desse contrato de <b>${data_antiga}</b> até <b>${data_nova}</b></p>`;
        }

        linhas += '</div>';
        linhas += '</div>';

        linhas += '<hr>'
        

        linhas += '</form>';
        
        linhas += '</div>';

        linhas += '</div>';
    }

    return linhas
}

function accordion_hora_html(data) {

    let linhas = '';
    linhas += '<ul class="nav nav-tabs" id="myTab" role="tablist">';
    for (let j in data) {
            if (j == 0){
                linhas += '<li class="nav-item" role="presentation">';
                linhas += `<button class="nav-link active" id="tab-${data[j].id}" data-bs-toggle="tab" data-bs-target="#tab${data[j].id}" type="button" role="tab" aria-controls="tab${data[j].id}" aria-selected="true">${data[j].total_horas} Horas Compradas</button>`;
                linhas += '</li>';
            }
            else{
                linhas += '<li class="nav-item" role="presentation">';
                linhas += `<button class="nav-link" id="tab-${data[j].id}" data-bs-toggle="tab" data-bs-target="#tab${data[j].id}" type="button" role="tab" aria-controls="tab${data[j].id}" aria-selected="false">${data[j].total_horas} Horas Compradas</button>`;
                linhas += '</li>';
        }
        
    }
    linhas += '</ul>';

    linhas += '     <div class="tab-content" id="myTabContent">';

    for (let i in data) {
        if (i == 0){
            linhas += ` <div class="tab-pane fade show active" id="tab${data[i].id}" role="tabpanel" aria-labelledby="home-tab-${data[i].id}">`;
        }else{
            linhas += ` <div class="tab-pane fade" id="tab${data[i].id}" role="tabpanel" aria-labelledby="home-tab-${data[i].id}">`;
        }
        linhas += '     <div class="container">';

        linhas += '         <div class="form--row">';
        linhas += '             <label>Valor da Compra</label>';
        linhas += `             <input type="text" value="${data[i].valor_total}" class="form-control">`;
        linhas += '         </div>';

        linhas += '         <div class="form--row">';
        linhas += '             <label>Cadastrante</label>';
        linhas += `             <input type="text" value="${data[i].cadastrante}" class="form-control">`;
        linhas += '         </div>';
        
        linhas += '         <div class="form--row">';
        linhas += '             <label>Data de Cadastro</label>';
        let datetime = bancoToFrontHoras(data[i].dt_cadastro);
        linhas += `             <input type="text" value="${datetime}" class="form-control">`;
        linhas += '         </div>';
        linhas += '     </div>';
        linhas += '     <div class="container">';
        linhas += '         <div class="form--row">';
        linhas += '             <label>Observação</label>';
        linhas += `             <textarea class="form-control">${data[i].obs}</textarea>`;
        linhas += '         </div>';
        linhas += '     </div>';
        linhas += `        <button type="button" class="btn--padrao btn-warning mt-3 defaultButton" style="display: none;" onclick="excluirHs(${data[i].id}, ${data[i].id_empresa}, ${data[i].total_horas});">`;
        linhas += '            Excluir Horas';
        linhas += '        </button>';

        linhas += ' </div>';


        

    }
    linhas += '</div>';
    return linhas
}

$('[data-bs-target="#modalCadastrarContrato"]').click(function(){
    modalContratos.hide();
})

$('[data-bs-target="#modalCadastrarHoras"]').click(function(){
    $("#formCadastrarHoras [name='empresa']").val(document.getElementById('nome_fantasia').value);
    modalContratos.hide();
})

$('[data-bs-target="#modalCadastrarAdendo"]').click(function(){
    let contratos = currentTpcontrato;
    console.log('Adendo ' + contratos);
    switch (contratos) {
        case 'saas':
            $('#formCadastrarAdendo [data-nameInput="usuarios_saas_nova"]').show();
            $('#formCadastrarAdendo [name="usuarios_saas_nova"]').prop('required', true);

            $('#formCadastrarAdendo [data-nameInput="horas_totais_nova"]').hide();
            $('#formCadastrarAdendo [name="horas_totais_nova"]').prop('required', false);
            break;

        case 'manutencao':
            $('#formCadastrarAdendo [data-nameInput="usuarios_saas_nova"]').hide();
            $('#formCadastrarAdendo [name="usuarios_saas_nova"]').prop('required', false);

            $('#formCadastrarAdendo [data-nameInput="horas_totais_nova"]').show();
            $('#formCadastrarAdendo [name="horas_totais_nova"]').prop('required', true);
            break;
        case 'manutencao_saas':
            $('#formCadastrarAdendo [data-nameInput="usuarios_saas_nova"]').show();
            $('#formCadastrarAdendo [name="usuarios_saas_nova"]').prop('required', true);

            $('#formCadastrarAdendo [data-nameInput="horas_totais_nova"]').show();
            $('#formCadastrarAdendo [name="horas_totais_nova"]').prop('required', true);
            break;
    }
    modalContratos.hide();
})

$('#formRelatorioHoras').submit(function(e){
    e.preventDefault();
    
    let formData = new FormData(this);

    let empresa = modalData.empresa_data;
    formData.set('id_empresa', empresa.id);

    ajaxDataForm(`${url_base}/api/relatorio_horas_empresa`, formData,
    function (resp) {
        resp = isJson(resp);
        $('.defaultButton').show();
        $('.loadingButton').hide();
        $('#gerar_pdf').show();
        if (resp.status == 1) {
            resp = resp.data
            
            $('#tempo_periodo').text(resp.info.hhmm_trabalhado ?? 0)
            $('#total_servicos').text(resp.info.qnt_servicos ?? 0)
            $('#total_projetos').text(resp.info.total_projetos ?? 0)
            $('#tempo_total_projetos').text(resp.info.horas_totais_projetos ?? 0)


            $('#gerar_pdf').show();
            tableRelatorio.criarTable(resp.horas);
        } else {
            alert(resp.message ? resp.message : resp.error);
        }
    }, function () {
        //Before send
        $('.defaultButton').hide();
        $('.loadingButton').show();
        $('#gerar_pdf').hide();
    })
})

function excludeAnexo(id){
    if(!confirm('Você tem certeza que deseja excluir esse anexo?')) return false;

    ajaxRequestGet(`/api/excluir_anexo?id_anexo=${id}&tipo_contratado=anexo_contrato`, function(resp){
        if(resp.status == 1){
            $(`[data-id_anexo='${id}'`).remove();
        }
        alert(resp.message)
    })
}

//Masks
$('input[name="cnpj"]').mask('00.000.000/0000-00', { reverse: false });
$('input[name="valor_contrato"]').mask('###.00', {
    reverse: true
});
$('input[name="cep"]').mask('00000-000');
$('input[name="telefone"]').mask('(00) 0000-0000');
$('.onlynumber').mask('#');


