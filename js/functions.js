

async function ajaxRequestGet(url, success, error, beforeSend) {
    error = error || function (resp) { resp = isJson(resp.responseText); alert(resp.error) }
    success = success || function () { };
    beforeSend = beforeSend || function () { }
    $.ajax({
        type: 'GET',
        url: url,
        timeout: 20000,
        cache: false,
        success: function (response) {
            // console.log(response)
            success(isJson(response));
        },
        error: error,
        beforeSend: beforeSend
    })
}

async function ajaxRequestPost(url, data, success, error, beforeSend) {
    error = error || function (resp) { resp = isJson(resp.responseText); alert(resp.error) }
    success = success || function () { };
    beforeSend = beforeSend || function () { };

    $.ajax({
        type: 'POST',
        url: url,
        data: data,
        timeout: 20000,
        cache: false,
        success: function (response) {
            success(isJson(response));
        },
        error: error,
        beforeSend: beforeSend
    })
}

async function ajaxDataForm(url, data, success, beforeSend, error) {
    success = success || function () { };
    beforeSend = beforeSend || function () { };

    $.ajax({
        type: 'POST',
        url: url,
        data: data,
        contentType: false,
        processData: false,
        timeout: 20000,
        cache: false,
        success: function (response) {
            success(isJson(response));
        },
        error: function (response, status, error) {
            let msg = isJson(response.responseText);
            alert(`Status : ${response.status}  Mensgem : ${msg.message}`)
            return ;
        },
        beforeSend: beforeSend
    })
}

async function ajaxReturnDataForm(url, data, success, error, beforeSend) {
    error = error || function (resp) { resp = isJson(resp.responseText); alert(resp.error) }
    success = success || function () { };
    beforeSend = beforeSend || function () { };

    return $.ajax({
        type: 'POST',
        url: url,
        data: data,
        contentType: false,
        processData: false,
        timeout: 20000,
        cache: false,
        error: function (request, status, error) {
            alert(request.responseText);
        },
        beforeSend: beforeSend
    })
}

function getDataPost(url, data) {
    return $.ajax({
        type: 'POST',
        url: url,
        data: data,
        timeout: 20000,
        cache: false
    })
}

async function returnDataPost(url, data) {
    if (typeof url == undefined || typeof data == undefined) return false;
    return await isJson(getDataPost());
}

async function getDataGET(url) {
    return await $.ajax({
        type: 'GET',
        url: url,
        timeout: 15000,
        cache: false
    })
}

async function returnDataGet(url) {
    if (typeof url == undefined) return false;
    return await getDataGET(url);
}

function objectSize(obj) {
    var size = 0;
    for (key in obj) {
        if (obj.hasOwnProperty(key)) size++;
    }
    return size;
};

function isJson(str) {
    try {
        str = JSON.parse(str);
        return str;
    } catch (e) {
        return str;
    }
}

function addOptions(data, indice1, indice2, selector) {
    let select = document.getElementsByClassName(selector);

    if(data){
        //console.log('addOptions');
        for (let a in select) {
            if (select[a].options) {
                for (let i = 0; i < data.length; i++) {
                    let option = new Option(data[i][indice1], data[i][indice2]);
                    select[a].options[i + 1] = option;
                }
            }
        }
    }
}




function gerar_select(name, classselect, options){
    
    let select = document.createElement('select');

    select.name = name;
    $(select).addClass(classselect);

    let count = 0;
    
    for(let i in options){
        let option = new Option(i, options[i].value, options[i].selected)
        select.options[count] = option;
        count++;
    }

    return select;
}

function getCookie(name) {
    const value = `; ${document.cookie}`;
    const parts = value.split(`; ${name}=`);
    if (parts.length === 2) return parts.pop().split(';').shift();
}

function setCookie(name, data, time) {
    document.cookie = "user=" + data + ";expires=" + tomorrow;
}

function objectifyForm(formArray) {
    var returnArray = {};

    for (var i = 0; i < formArray.length; i++) {
        returnArray[formArray[i]['name']] = formArray[i]['value'];
    }
    return JSON.stringify(returnArray);
}

function toDateTime(date) {
    var str = '';
    var year, month, day, hour, min;
    year = date.getUTCFullYear();
    month = date.getUTCMonth() + 1;
    month = month < 10 ? '0' + month : month;
    day = date.getUTCDate();
    day = day < 10 ? '0' + day : day;
    hour = date.getUTCHours();
    hour = hour < 10 ? '0' + hour : hour;
    min = date.getUTCMinutes();
    min = min < 10 ? '0' + min : min;

    str += year + '-' + month + '-' + day;
    str += ' ' + hour + ':' + min;
    return str;
}
function bancoToFrontHorasUTC(data) {

    let dataFim = new Date(data);
    dataFim = dataFim.toLocaleString('pt-BR', {
        timeZone: 'UTC'
    });

    return dataFim;
}

function bancoToFrontDateUTC(data) {

    let dataFim = new Date(data);
    dataFim = dataFim.toLocaleDateString('pt-BR', {
        timeZone: 'UTC'
    });

    return dataFim;
}

function bancoToFrontHoras(data) {

    let dataFim = new Date(data);
    dataFim = dataFim.toLocaleString('pt-BR');

    return dataFim;
}

function nowToBanco(){
    let now = new Date().toLocaleString().replace(',','').split(' ');
    now[0] = now[0].split("/").reverse().join("-");
    now = now.join(' ');

    return now
}

function parseBRL(value){
   return  parseFloat(value).toLocaleString('pt-br',{style: 'currency', currency: 'BRL'})
}

function validFileType(file) {
    return fileTypes.includes(file.type);
  }