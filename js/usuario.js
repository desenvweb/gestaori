const formInserir = document.getElementById('form#inserir');
const formEditar = document.getElementById('form#editar');
const tbody = document.getElementById('tbody');
const tbodySearch = document.getElementById('tbodysearch');
const tbodyRelatorio = document.getElementById('tbodyRelatorio');
const modalDados = new bootstrap.Modal(document.getElementById('modalExibirDados'), {});
const modalTrabalho = new bootstrap.Modal(document.getElementById('modalExibirTrabalho'), {});
const modalRelatorioHs = new bootstrap.Modal(document.getElementById('modalRelatórios'), {});
const anexoDiv = document.getElementsByClassName('anexo--listItens')[0];
const super_admin = document.getElementById('super_admin');
const mostrar_dados = document.getElementById("dadosUser");

var table = {
    data: false,
    data_search: false,
    pagina: 0,

    criarTable: function (data) {
        data = isJson(data);
        this.data = data.data;
        data = this.data;
        let linhas = this.gerarLinhas(data);
        tbody.innerHTML = linhas;
    },

    gerarLinhas: function (data) {
        var linhas = '';

        for (let i in data) {
            let bgcolor = '';
            let status = '';
            //console.log(data[i])

            switch(data[i].situacao.situacao){
                case 'ausente' : 
                    bgcolor = 'bg--red';
                    status = 'Ausente';
                break;

                case 'Trabalhando' : 
                bgcolor = 'bg--orange';
                status = 'Trabalhando';
                break;

                case 'presente' : 
                bgcolor = 'bg--green';
                status = 'Presente';
                break;
            }

            let ibgcolor = '';
            let avaliacao = data[i].avaliacao ?? '';
            let instru = parseFloat(data[i].instrucao);
            // console.log(data[i])

            if (instru <= 6){
                ibgcolor = 'bg--red';
            }else if (instru > 6 && instru <= 7){
                ibgcolor = 'bg--orange';
            }else if (instru > 9.5){
                ibgcolor = 'bg--blue';
            }else{
                ibgcolor = 'bg--green';
            }

            linhas += `<tr>`;
            linhas += `<td>${data[i].nome}</td>`
            linhas += `<td>${data[i].funcao}</td>`
            let inicio = data[i].dt_inicio_trabalho ?? '';
            if (inicio != ''){
                let present_date = new Date();
                let init_date = new Date(inicio);
                let Difference_In_Time = Math.abs(present_date.getTime() - init_date.getTime());
                let Difference_In_Days =  Math.ceil(Difference_In_Time / (1000 * 3600 * 24));
                
                linhas += `<td>${Difference_In_Days}</td>`
            }else{
                linhas += `<td></td>`
            }

            if(Painel.user.super_admin == 1){
                linhas += `<td><span class="table--status ${ibgcolor}">${data[i].instrucao}</span></td>`
                linhas += `<td>${avaliacao}</td>`
            }
            linhas += `<td class="flex-box" onclick="exibirTrabalho(${data[i].id});"><span class="table--status ${bgcolor}">${status}</span></td>`
            linhas += `<td>
                            <span class="flex-box" onclick="exibirDados(${data[i].id});"  data-toggle="tooltip" title="Dados do Usuário">
                                <svg xmlns="http://www.w3.org/2000/svg" class="svg--color width="30" height="30" fill="currentColor" class="bi bi-journals" viewBox="0 0 20 20">
                                    <path d="M14.5 3a.5.5 0 0 1 .5.5v9a.5.5 0 0 1-.5.5h-13a.5.5 0 0 1-.5-.5v-9a.5.5 0 0 1 .5-.5h13zm-13-1A1.5 1.5 0 0 0 0 3.5v9A1.5 1.5 0 0 0 1.5 14h13a1.5 1.5 0 0 0 1.5-1.5v-9A1.5 1.5 0 0 0 14.5 2h-13z"/>
                                    <path d="M3 8.5a.5.5 0 0 1 .5-.5h9a.5.5 0 0 1 0 1h-9a.5.5 0 0 1-.5-.5zm0 2a.5.5 0 0 1 .5-.5h6a.5.5 0 0 1 0 1h-6a.5.5 0 0 1-.5-.5zm0-5a.5.5 0 0 1 .5-.5h9a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-9a.5.5 0 0 1-.5-.5v-1z"/>
                                </svg>
                                
                            </span>
                        </td>`

            linhas += `<td>
                            <span class="flex-box" onclick="exibirRelatorioHs(${data[i].id});"  data-toggle="tooltip" title="Horas Lançadas">
                                <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" fill="currentColor" class="bi bi-alarm" viewBox="0 0 20 20">
                                    <path d="M8.5 5.5a.5.5 0 0 0-1 0v3.362l-1.429 2.38a.5.5 0 1 0 .858.515l1.5-2.5A.5.5 0 0 0 8.5 9V5.5z"/>
                                    <path d="M6.5 0a.5.5 0 0 0 0 1H7v1.07a7.001 7.001 0 0 0-3.273 12.474l-.602.602a.5.5 0 0 0 .707.708l.746-.746A6.97 6.97 0 0 0 8 16a6.97 6.97 0 0 0 3.422-.892l.746.746a.5.5 0 0 0 .707-.708l-.601-.602A7.001 7.001 0 0 0 9 2.07V1h.5a.5.5 0 0 0 0-1h-3zm1.038 3.018a6.093 6.093 0 0 1 .924 0 6 6 0 1 1-.924 0zM0 3.5c0 .753.333 1.429.86 1.887A8.035 8.035 0 0 1 4.387 1.86 2.5 2.5 0 0 0 0 3.5zM13.5 1c-.753 0-1.429.333-1.887.86a8.035 8.035 0 0 1 3.527 3.527A2.5 2.5 0 0 0 13.5 1z"/>
                                </svg>
                            </span>
                        </td>`
            linhas += '</tr>';
        }

        return linhas;
    },

    adicionarTable: function (data) {
        data = isJson(data);
        data = data.data;
        let linhas = this.gerarLinhas(data);
        tbody.innerHTML += linhas;
        for(let i in data){
            this.data.push(data[i])
        }
    },

    pesquisa: async function (search) {
        let data = await getDataGET(`${url_base}/api/get_usuarios?condicao=${search}&limite=${limite}`);
        data = isJson(data);

        this.data_search = data.data;

        if (search != '') {
            let linhas = this.gerarLinhas(data.data);
            tbodySearch.innerHTML = linhas;
            $(tbody).hide();
            $(tbodySearch).show();
            this.search = true;
        } else {
            $(tbody).show();
            $(tbodySearch).hide();
            this.search = false;
        }
    }
}

var tableRelatorio = {
    data: false,

    criarTable: function (data) {
        this.data = data;
        let linhas = this.gerarLinhas(this.data);
        tbodyRelatorio.innerHTML = linhas;
    },

    gerarLinhas: function (data) {
        var linhas = '';
        
        for (let i in data) {
            if(i != 'info'){
                let dt_inicio = bancoToFrontHoras(data[i].dt_inicio)
                let dt_fim = bancoToFrontHoras(data[i].dt_fim)

                linhas += `<tr>`;
                linhas += `<td>${data[i].id_ticket}</td>`
                linhas += `<td>${data[i].nome_fantasia}</td>`
                linhas += `<td>${dt_inicio}</td>`
                linhas += `<td>${dt_fim}</td>`
                linhas += `<td>${data[i].diff_horas}</td>`; 

                linhas += '</tr>';
            }
        }
        
        return linhas;
    },
}


$(document).ready(function () {
    //if(Painel.user.tipo_usuario == 1){
    if(Painel.user.super_admin == 1){
        $('[data-bs-target="#modalInserirCliente"]').show();
        $('.defaultButton').show();
        $('.divShow').show();
    }else{
        $('.formpermiss :input').prop('disabled', true);
        $('.formpermiss :button').hide();
        $('.hidepermiss').hide();
    }

    (async function () {
        let clientes = await getDataGET(`${url_base}/api/get_clientes?limite=1000`);
        clientes = isJson(clientes);
        addOptions(clientes.data, 'nome_fantasia', 'id', 'cliente--select');

        let setores = await getDataGET(`${url_base}/api/get_setores`);
        setores = isJson(setores);
        addOptions(setores.data, 'nome', 'id', 'setor--select');

        let data = await getDataGET(`${url_base}/api/get_usuarios?limite=${limite}`);
        table.criarTable(data);

        $('.spinner-border').hide();
    })()
})

formInserir.addEventListener('submit', async function (e) {
    e.preventDefault();

    if ($('#senhaInicial').val() != $('#confirmSenha').val()) return alert('Senhas digitadas não conferem.;');

    let formData = new FormData(formInserir);
    ajaxDataForm(`${url_base}/api/cadastrar_usuario`, formData, function (resp) {
        resp = isJson(resp);
        $('.defaultButton').show();
        $('.loadingButton').hide();
        if (resp.status == 1) {
            alert('Salvo com sucesso.')
            window.location.reload();
        } else {
            alert(resp.message);
        }
    }, function () {
        //Before send
        $('.defaultButton').hide();
        $('.loadingButton').show();
    })
})

formEditar.addEventListener('submit', async function (e) {
    e.preventDefault();

    if ($('#senhaInicial2').val() != $('#confirmSenha2').val()) return alert('Senhas digitadas não conferem.');

    let formData = new FormData(formEditar);
    formData.set('id', $('#idUsuario').val());

    if (super_admin.checked){
        formData.set('super_admin', 1);
    } else {
        formData.set('super_admin', 0);
    }

    ajaxDataForm(`${url_base}/api/editar_usuario`, formData,
        function (resp) {
            resp = isJson(resp);
            $('.defaultButton').show();
            $('.loadingButton').hide();
            if (resp.status == 1) {
                alert('Salvo com sucesso.')
                window.location.reload();
            } else {
                alert(resp.message ? resp.message : resp.error);
            }
        }, function () {
            //Before send
            $('.defaultButton').hide();
            $('.loadingButton').show();
    })

})
async function exibirTrabalho(UserId){
   // console.log(UserId);
    let data = await getDataGET(`${url_base}/api/get_infos?id_usuario=${UserId}`);
    data = isJson(data);

    if (data.data.trabalhando.status == 1){
       // console.log(data.data.trabalhando.data.dt_inicio);
        $('#ticketTrabalho').val(data.data.trabalhando.data.id_ticket);
        $('#tituloTrabalho').val(data.data.trabalhando.data.titulo);
        $('#empresaTrabalho').val(data.data.trabalhando.data.nome_fantasia);
        $('#atividadeTrabalho').val(data.data.trabalhando.data.atividade);
        $('#dt_inicioTrabalho').val(data.data.trabalhando.data.dt_inicio);
        $('#dt_previstaTrabalho').val(data.data.trabalhando.data.dt_prevista);
        $('#informacoesTrabalho').val(data.data.trabalhando.data.descricao);
        modalTrabalho.show();
    }else{
        let dataI = bancoToFrontHoras(data.data.status.dt_inicio);
        alert(data.data.trabalhando.message + '\n' +'Data : ' + dataI);
    }
}

async function exibirRelatorioHs(UserId){
    $('#idUsuario').val(UserId);
    $('#limpar_relatorio').click();
    tbodyRelatorio.innerHTML = '';
    $('#tempo_periodo, #total_servicos').text('');
    modalRelatorioHs.show();
}

function exibirDados(UserId){
    //if(Painel.user.tipo_usuario == 1){
        let id = UserId;
        if (!id) return

        if(Painel.user.tipo_usuario == 2){
            if(Painel.user.id == id){
                $('.formpermiss :input').prop('disabled', false);
                $('.defaultButton').show();
                gerar_linha_anexo(id);
                $('.anexo--listItens').show();
                $('[data-bs-target="#modalRelatórios"]').show();
                $('#alterar_senha').show();
            }else{
                $('.formpermiss :input').prop('disabled', false);
                //$('.formpermiss :input').prop('disabled', true);
                $('.formpermiss :button').hide();
                $('.anexo--listItens').hide();
                $('#alterar_senha').hide();
                $('[data-bs-target="#modalRelatórios"]').show();
            }
        }else{
            gerar_linha_anexo(id);
        }

        $('#idUsuario').val(id);
        

        if(!table.search){
            var data = table.data.filter(el => el.id == id);
        }else{
            var data = table.data_search.filter(el => el.id == id);
        }

        if((!data[0].cnpj) && (data[0].tipo_colaborador == 'CLT')){
            $('#cnpj').attr('name', 'cpf').attr('id', 'cpf');
            $('[for="cnpj"]').text('CPF').attr('for', 'cpf');
            //$('#tipo_colaborador').val('CLT');
        }else{
            $('#cpf').attr('name', 'cnpj').attr('id', 'cnpj');
            $('[for="cpf"]').text('CNPJ').attr('for', 'cnpj');
            //$('#tipo_colaborador').val('PJ');
        }

        $('#senhaInicial2').val('');

        if (data[0].super_admin == 1) {
            super_admin.checked = true;
            
        } else {
            super_admin.checked = false;
        }

        for (let i in data[0]) {
            $('#' + i).val(data[0][i]);
        }

        modalDados.show();
    //}else{
    //    alert('Acesso somente para Administradores.')
    //}
}

$('#excluirBtn').click(function (e) {
    e.preventDefault();

    if (!confirm('Tem certeza que deseja excluir esse usuário?')) return false;

    let id = $('#idUsuario').val();
    ajaxRequestGet(`${url_base}/api/excluir_usuario?id=${id}`, function (resp) {
        resp = isJson(resp);

        if (resp.status == 1) {
            alert('Excluido com sucesso.')
            document.location.reload()
        } else {
            alert(resp.message ? resp.message : resp.error);
        }
    })
})


function excludeAnexo(id){
    if(!confirm('Você tem certeza que deseja excluir esse anexo?')) return false;

    ajaxRequestGet(`/api/excluir_anexo?id_anexo=${id}&tipo_contratado=PS`, function(resp){
        if(resp.status == 1){
            $(`[data-id_anexo='${id}'`).remove();
        }
        alert(resp.message)
    })
}

async function gerar_linha_anexo(id) {
    let data = await getDataGET(`${url_base}/api/get_anexos?id_contratado=${id}&tipo_contratado=PS`);
    data = isJson(data);

    anexoDiv.innerHTML = '';
    for (let i in data.data) {
        let path = data.data[i].path.split('/');
        let path_length = path.length;
        let path_name = path[path_length - 1];

        let div = document.createElement('div');
        div.dataset.id_anexo = data.data[i].id;
        let div_anexo = document.createElement('div');
        div_anexo.classList.add('anexo--item');

        let a = document.createElement('a');
        a.innerText = data.data[i].nome
        a.href = `${url_base}/anexos/${path_name}`;
        a.target = '_blank';

        let hide = Painel.user.tipo_usuario == 2 ? 'hide' : '';
        div_anexo.appendChild(a);
        div_anexo.innerHTML += `<svg class="${hide}" style="cursor: pointer;" onclick="excludeAnexo(${data.data[i].id})" xmlns="http://www.w3.org/2000/svg" height="24px" viewBox="0 0 24 24" width="24px" fill="#000000"><path d="M0 0h24v24H0V0z" fill="none"/><path d="M16 9v10H8V9h8m-1.5-6h-5l-1 1H5v2h14V4h-3.5l-1-1zM18 7H6v12c0 1.1.9 2 2 2h8c1.1 0 2-.9 2-2V7z"/></svg>`
        div.appendChild(div_anexo);
        anexoDiv.appendChild(div)
    }
}

$('.table-responsive').on('scroll', function () {
   // console.log('scroll');
    let value = parseInt(this.scrollTop / (this.scrollHeight - this.clientHeight));
    if (value == 1) {

    //if ($(this).scrollTop() + $(this).innerHeight() >= this.scrollHeight) {
        table.pagina++;
        //console.log('scroll entrou');

        (async function () {
            let data = await getDataGET(`${url_base}/api/get_usuarios?pagina=${table.pagina}&limite=${limite}`);
            data = isJson(data);
            if (data.status == 1) {
                table.adicionarTable(data);
            }else{
                table.pagina--;
            }
        })()
    }
})


$('#relatorio_form').submit(function(e){
    e.preventDefault();
    
    let formData = new FormData(document.getElementById('relatorio_form'));

    tbodyRelatorio.innerHTML = '';
    $('#tempo_periodo, #total_servicos').text('');

    formData.set('colaborador', $('#idUsuario').val());

    ajaxDataForm(`${url_base}/api/relatorio_horas_usuario`, formData,
    function (resp) {
        resp = isJson(resp);
        $('.defaultButton').show();
        $('.loadingButton').hide();
        $('#gerar_pdf').show();
        if (resp.status == 1) {
            resp = resp.data
            $('#tempo_periodo').text(resp.info.hhmm_trabalhado)
            $('#total_servicos').text(resp.info.qnt_servicos)

            $('#gerar_pdf').show();
            tableRelatorio.criarTable(resp);
        } else {
            alert(resp.message ? resp.message : resp.error);
        }
    }, function () {
        //Before send
        $('.defaultButton').hide();
        $('.loadingButton').show();
        $('#gerar_pdf').hide();
    })
})

$('[data-bs-target="#modalRelatórios"]').click(function(){
    modalDados.hide();
    $('#limpar_relatorio').click();
})

$('#limpar_relatorio').click(function(){
    $('#tempo_periodo, #total_servicos').text('');
    $('#gerar_pdf').hide();
    tbodyRelatorio.innerHTML = '';
})

$('#gerar_pdf').click(function(){
    let a = document.createElement('a');
    let cliente = $('[name="cliente"]').val() ?? '';
    let dt_inicial = $('[name="dt_inicial"]').val();
    let dt_final = $('[name="dt_final"]').val();
    let user = $('#idUsuario').val();

    let query = `colaborador=${user}&cliente=${cliente}&dt_inicial=${dt_inicial}&dt_final=${dt_final}` 
    //console.log(query);
    a.href = `/api/relatorio_horas_usuario_pdf?${query}`;
    a.target = '_blank';
    a.click();
})


$('#tipo_colaborador').change(function(){
    if(this.value === 'CLT'){
        $('#cnpj').attr('name', 'cpf').attr('id', 'cpf');
        $('[for="cnpj"]').text('CPF').attr('for', 'cpf');
    }else {
        $('#cpf').attr('name', 'cnpj').attr('id', 'cnpj');
        $('[for="cpf"]').text('CNPJ').attr('for', 'cnpj');
    }
})

$('#tipoColaborador_insert').change(function(){
    if(this.value === 'CLT'){
        $('#cnpj').attr('name', 'cpf');
        $('[for="certidao_insert"]').text('CPF');
    }else {
        $('#certidao_insert').attr('name', 'cnpj');
        $('[for="certidao_insert"]').text('CNPJ');
    }
})

$('#searchInput').keyup(function () {
    table.pesquisa($(this).val());
})

$('input[name="telefone"]').mask('(00) 00000-0000');
$('input[name="cnpj"]').mask('00.000.000/0000-00', { reverse: false });
$('.money').mask('###.00', {
    reverse: true
});
$('input[name="cep"]').mask('00000-000');


$(function () {
    $('[data-toggle="tooltip"]').tooltip()
  })