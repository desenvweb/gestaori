const formInserir = document.getElementById('form#inserir');
const formEditar = document.getElementById('form#editar');
const tbody = document.getElementById('tbody');
const tbodySearch = document.getElementById('tbodysearch');
const modalDados = new bootstrap.Modal(document.getElementById('modalExibirDados'), {});

var table = {
    data: false,
    data_search: false,
    pagina: 0,

    criarTable: function (data) {
        data = isJson(data);
        this.data = data.data;
        data = this.data;
        let linhas = this.gerarLinhas(data);
        tbody.innerHTML = linhas;
    },

    gerarLinhas: function (data) {
        var linhas = '';
        console.log(data)
        for (let i in data) {
            let bgcolor = '';
            let status = '';
            

            linhas += `<tr>`;
            linhas += `<td>${data[i].nome_fantasia}</td>`
            linhas += `<td>${data[i].categoria}</td>`
            
            linhas += `<td>
                            <span class="flex-box" onclick="exibirDados(${data[i].id});"  data-toggle="tooltip" title="Dados do Fornecedor">
                                <svg xmlns="http://www.w3.org/2000/svg" class="svg--color width="30" height="30" fill="currentColor" class="bi bi-journals" viewBox="0 0 20 20">
                                    <path d="M14.5 3a.5.5 0 0 1 .5.5v9a.5.5 0 0 1-.5.5h-13a.5.5 0 0 1-.5-.5v-9a.5.5 0 0 1 .5-.5h13zm-13-1A1.5 1.5 0 0 0 0 3.5v9A1.5 1.5 0 0 0 1.5 14h13a1.5 1.5 0 0 0 1.5-1.5v-9A1.5 1.5 0 0 0 14.5 2h-13z"/>
                                    <path d="M3 8.5a.5.5 0 0 1 .5-.5h9a.5.5 0 0 1 0 1h-9a.5.5 0 0 1-.5-.5zm0 2a.5.5 0 0 1 .5-.5h6a.5.5 0 0 1 0 1h-6a.5.5 0 0 1-.5-.5zm0-5a.5.5 0 0 1 .5-.5h9a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-9a.5.5 0 0 1-.5-.5v-1z"/>
                                </svg>
                                
                            </span>
                        </td>`
                        if(Painel.user.super_admin == 1){
                            linhas += `<td>
                                            <span class="flex-box" onclick="excluirFornecedor(${data[i].id});"  data-toggle="tooltip" title="Excluir Fornecedor">
                                                <svg xmlns="http://www.w3.org/2000/svg" class="text-danger" width="30" height="30" fill="currentColor" class="bi bi-trash" viewBox="0 0 20 20">
                                                    <path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z"/>
                                                    <path fill-rule="evenodd" d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z"/>
                                                </svg>
                                            </span>
                                        </td>`;
                        }
            linhas += '</tr>';
        }

        return linhas;
    },

    adicionarTable: function (data) {
        data = isJson(data);
        data = data.data;
        let linhas = this.gerarLinhas(data);
        tbody.innerHTML += linhas;
        for(let i in data){
            this.data.push(data[i])
        }
    },

    pesquisa: async function (search) {
        let data = await getDataGET(`${url_base}/api/get_fornecedores?condicao=${search}&limite=${limite}`);
        data = isJson(data);

        this.data_search = data.data;

        if (search != '') {
            let linhas = this.gerarLinhas(data.data);
            tbodySearch.innerHTML = linhas;
            $(tbody).hide();
            $(tbodySearch).show();
            this.search = true;
        } else {
            $(tbody).show();
            $(tbodySearch).hide();
            this.search = false;
        }
    }
}

var tableRelatorio = {
    data: false,

    criarTable: function (data) {
        this.data = data;
        let linhas = this.gerarLinhas(this.data);
        tbodyRelatorio.innerHTML = linhas;
    },

    gerarLinhas: function (data) {
        var linhas = '';
        
        for (let i in data) {
            if(i != 'info'){
                linhas += `<tr>`;
                linhas += `<td>${data[i].nome_fantasia}</td>`
                linhas += `<td>${data[i].categoria}</td>`; 

                linhas += '</tr>';
            }
        }
        
        return linhas;
    },
}


$(document).ready(function () {
    //if(Painel.user.tipo_fornecedor == 1){
    if(Painel.user.super_admin == 1){
        $('[data-bs-target="#modalInserirFornecedor"]').show();
        $('.defaultButton').show();
        $('.divShow').show();
    }else{
        $('.formpermiss :input').prop('disabled', true);
        $('.formpermiss :button').hide();
        $('.hidepermiss').hide();
    }

    (async function () {

        let data = await getDataGET(`${url_base}/api/get_fornecedores?limite=${limite}`);
        table.criarTable(data);

        $('.spinner-border').hide();
    })()
})

formInserir.addEventListener('submit', async function (e) {
    e.preventDefault();

    let formData = new FormData(formInserir);
    ajaxDataForm(`${url_base}/api/cadastrar_fornecedor`, formData, function (resp) {
        resp = isJson(resp);
        $('.defaultButton').show();
        $('.loadingButton').hide();
        if (resp.status == 1) {
            alert('Salvo com sucesso.')
            window.location.reload();
        } else {
            alert(resp.message);
        }
    }, function () {
        //Before send
        $('.defaultButton').hide();
        $('.loadingButton').show();
    })
})

formEditar.addEventListener('submit', async function (e) {
    e.preventDefault();

    let formData = new FormData(formEditar);
    formData.set('id', $('#idFornecedor').val());

    ajaxDataForm(`${url_base}/api/editar_fornecedor`, formData,
        function (resp) {
            resp = isJson(resp);
            $('.defaultButton').show();
            $('.loadingButton').hide();
            if (resp.status == 1) {
                alert('Salvo com sucesso.')
                window.location.reload();
            } else {
                alert(resp.message ? resp.message : resp.error);
            }
        }, function () {
            //Before send
            $('.defaultButton').hide();
            $('.loadingButton').show();
    })

})
function exibirDados(FornecedorId){
    //if(Painel.user.tipo_fornecedor == 1){
        let id = FornecedorId;
        if (!id) return

        if(Painel.user.tipo_usuario == 2){
            if(Painel.user.id == id){
                $('.formpermiss :input').prop('disabled', false);
                $('.defaultButton').show();
            }else{
                $('.formpermiss :input').prop('disabled', false);
                //$('.formpermiss :input').prop('disabled', true);
                $('.formpermiss :button').hide();
            }
        }

        $('#idFornecedor').val(id);
        

        if(!table.search){
            var data = table.data.filter(el => el.id == id);
        }else{
            var data = table.data_search.filter(el => el.id == id);
        }

        for (let i in data[0]) {
            $('#' + i).val(data[0][i]);
        }

        modalDados.show();
    //}else{
    //    alert('Acesso somente para Administradores.')
    //}
}

function excluirFornecedor(Id){
    if (!confirm('Tem certeza que deseja excluir esse fornecedor?')) return false;

    ajaxRequestGet(`${url_base}/api/excluir_fornecedor?id_fornecedor=${Id}`, function (resp) {
        resp = isJson(resp);

        if (resp.status == 1) {
            alert('Excluido com sucesso.')
            document.location.reload()
        } else {
            alert(resp.message ? resp.message : resp.error);
        }
    })
}


$('.table-responsive').on('scroll', function () {
   // console.log('scroll');
    let value = parseInt(this.scrollTop / (this.scrollHeight - this.clientHeight));
    if (value == 1) {

    //if ($(this).scrollTop() + $(this).innerHeight() >= this.scrollHeight) {
        table.pagina++;
        //console.log('scroll entrou');

        (async function () {
            let data = await getDataGET(`${url_base}/api/get_fornecedores?pagina=${table.pagina}&limite=${limite}`);
            data = isJson(data);
            if (data.status == 1) {
                table.adicionarTable(data);
            }else{
                table.pagina--;
            }
        })()
    }
})


$('#searchInput').keyup(function () {
    table.pesquisa($(this).val());
})

$('input[name="fone"]').mask('(00) 0000-0000');
$('input[name="celular"]').mask('(00) 00000-0000');
$('input[name="cnpj"]').mask('00.000.000/0000-00', { reverse: false });
$('input[name="cep"]').mask('00000-000');


$(function () {
    $('[data-toggle="tooltip"]').tooltip()
  })