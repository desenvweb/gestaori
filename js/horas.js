const formInserir = document.getElementById('form#inserir');
const formEditar = document.getElementById('form#editar');
const tbody = document.getElementById('tbody');
const modalDados = new bootstrap.Modal(document.getElementById('modalExibirDados'), {})
const anexoDiv = document.getElementsByClassName('anexos--list')[0];
const filtros = document.getElementsByClassName('filtro--list')

var table = {
    data: false,
    clientes: false,
    usuarios: false,
    pagina: 0,
    loadingScroll: true,

    criarTable: function (data) {
        this.data = data;
        let linhas = this.gerarLinhas(this.data);
        tbody.innerHTML = linhas;
    },

    gerarLinhas: function (data) {
        var linhas = '';

        for (let i in data) {
            let usuarios = this.usuarios.filter(el => el.id == data[i].id_usuario);

            if (usuarios[0]) {
                let dt_inicio = bancoToFrontHoras(data[i].dt_inicio)
                let dt_fim = bancoToFrontHoras(data[i].dt_fim)

                linhas += `<tr data-ref="${data[i].id}">`;
                linhas += `<td>${data[i].cnpj} &nbsp;&nbsp;&nbsp; ${data[i].razao_social}</td>`
                linhas += `<td>${data[i].id_ticket}</td>`
                linhas += `<td>${data[i].descricao}</td>`
                linhas += `<td>${usuarios[0].nome}</td>`
                linhas += `<td>${dt_inicio}</td>`
                linhas += `<td>${dt_fim}</td>`
                linhas += `<td>${data[i].diff_horas}</td>`;

                linhas += '</tr>';
            }
        }

        return linhas;
    },

    adicionarTable: function (data) {
        for (let i in data) {
            this.data.push(data[i]);
        }
        let linhas = this.gerarLinhas(data);

        tbody.innerHTML += linhas;
    },
}


$(document).ready(async function () {

    if (Painel.user.tipo_usuario == 1) {
        $('[data-bs-target="#modalInserirHoras"], .defaultButton').show();
        $('.colaborador--select').css('display', 'inline-block');

    } else {
        $('.formpermiss :input').prop('disabled', true);
        $('.formpermiss :button').hide();
    }

    let usuarios = await getDataGET(`${url_base}/api/get_usuarios?limite=1000`);
    usuarios = isJson(usuarios);
    table.usuarios = usuarios.data;
    addOptions(usuarios.data, 'nome', 'id', 'colaborador--select');

    let horas = await getDataGET(`${url_base}/api/get_horas?limite=${limite}`);
    horas = isJson(horas);
    table.criarTable(horas.data);

    $('.spinner-border').hide();

    let atividade = await getDataGET(`${url_base}/api/get_atividades`);
    atividade = isJson(atividade);
    addOptions(atividade.data, 'nome', 'id', 'atividade--select');

    let projetos = await getDataGET(`${url_base}/api/get_projetos`);
    projetos = isJson(projetos);
    addOptions(projetos.data, 'nome', 'id', 'projetos--select');

    $('.selectTicket').select2({
        dropdownParent: $('#modalIniciarLD'),
        theme: 'bootstrap-5',
        placeholder: "Selecione um Ticket",
        ajax: {
            url: '/api/get_tickets',
            type: 'get',
            dataType: 'json',
            delay: 250,
            data: function (params) {

                var query = {
                    condicao: params.term,
                    limite: 20,
                    id_responsavel : Painel.user.id
                }
                return query
            },
            processResults: function (response) {

                return {
                    results: $.map(response.data, function (item) {
                        return {
                            id: item.id,
                            text: `${item.id} (${item.titulo})`
                        }
                    })
                }
            }
        },
        cache: true
    })

    $('#selectTicket').select2({
        dropdownParent: $('#modalInserirHoras'),
        theme: 'bootstrap-5',
        placeholder: "Selecione um Ticket",
        ajax: {
            url: '/api/get_tickets',
            type: 'get',
            dataType: 'json',
            delay: 250,
            data: function (params) {

                var query = {
                    condicao: params.term,
                    limite: 20,
                    id_responsavel : Painel.user.id
                }
                return query
            },
            processResults: function (response) {

                return {
                    results: $.map(response.data, function (item) {
                        return {
                            id: item.id,
                            text: `${item.id} (${item.titulo})`
                        }
                    })
                }
            }
        },
        cache: true
    })

    $('#selectTicket2').select2({
        dropdownParent: $('#modalExibirDados'),
        theme: 'bootstrap-5',
        placeholder: "Selecione um Ticket",
        ajax: {
            url: '/api/get_tickets',
            type: 'get',
            dataType: 'json',
            delay: 250,
            data: function (params) {

                var query = {
                    condicao: params.term,
                    limite: 20
                }
                return query
            },
            processResults: function (response) {

                return {
                    results: $.map(response.data, function (item) {
                        return {
                            id: item.id,
                            text: `${item.id} (${item.titulo})`
                        }
                    })
                }
            }
        },
        cache: true
    })

    $('.selectEmpresa').select2({
        dropdownParent: $('#modalIniciarLD'),
        theme: 'bootstrap-5',
        placeholder: "Selecione uma Empresa",
        ajax: {
            url: '/api/get_empresas_com_horas',
            type: 'get',
            dataType: 'json',
            delay: 250,
            data: function (params) {

                var query = {
                    condicao: params.term,
                    limite: 20
                }
                return query
            },
            processResults: function (response) {

                return {
                    results: $.map(response.data, function (item) {
                        return {
                            id: item.id,
                            text: `${item.nome_fantasia}     -     ${item.saldo_horas}`
                        }
                    })
                }
            }
        },
        cache: true
    })

    $('#selectEmpresa').select2({
        dropdownParent: $('#modalInserirHoras'),
        theme: 'bootstrap-5',
        placeholder: "Selecione uma Empresa",
        ajax: {
            url: '/api/get_empresas_com_horas',
            type: 'get',
            dataType: 'json',
            delay: 250,
            data: function (params) {

                var query = {
                    condicao: params.term,
                    limite: 20
                }
                return query
            },
            processResults: function (response) {

                return {
                    results: $.map(response.data, function (item) {
                        return {
                            id: item.id,
                            text: `${item.nome_fantasia}     -     ${item.saldo_horas}`
                        }
                    })
                }
            }
        },
        cache: true
    })

    $('#selectEmpresa2').select2({
        dropdownParent: $('#modalExibirDados'),
        theme: 'bootstrap-5',
        placeholder: "Selecione uma Empresa",
        ajax: {
            url: '/api/get_empresas_com_horas',
            type: 'get',
            dataType: 'json',
            delay: 250,
            data: function (params) {

                var query = {
                    condicao: params.term,
                    limite: 20
                }
                return query
            },
            processResults: function (response) {

                return {
                    results: $.map(response.data, function (item) {
                        return {
                            id: item.id,
                            text: `${item.nome_fantasia}     -     ${item.saldo_horas}`
                        }
                    })
                }
            }
        },
        cache: true
    })
})



formInserir.addEventListener('submit', async function (e) {
    e.preventDefault();
    let formData = new FormData(formInserir);

    ajaxDataForm(`${url_base}/api/lancar_horas`, formData, function (resp) {
        resp = isJson(resp);
        $('.defaultButton').show();
        $('.loadingButton').hide();
        //console.log('aqui vai o console.log', resp);
        if (resp.status == 1) {
            alert('Salvo com sucesso.')
            window.location.reload();
        } else {
            alert(resp.message)
        }
    }, function () {
        //Before send
        $('.defaultButton').hide();
        $('.loadingButton').show();
    })
})

formEditar.addEventListener('submit', async function (e) {
    e.preventDefault();
    let formData = new FormData(formEditar);
 
    ajaxDataForm(`${url_base}/api/editar_horas`, formData, function (resp) {
        resp = isJson(resp);
        $('.defaultButton').show();
        $('.loadingButton').hide();
        
        if (resp.status == 1) {
            alert('Salvo com sucesso!.')
            window.location.reload();
        } else {
            alert(resp.message);
        }
    }, function () {
        //Before send
        $('.defaultButton').hide();
        $('.loadingButton').show();
    })
})


$(document).on('click', 'tr', function (e) {
    let id = $(this).data('ref');
    if (!id) return
    let data = table.data.filter(el => el.id == id);

    $('#id_lancamento').val(id);

    data = data[0];

    let time = data.dt_fim.split(' ');
    let data_acao = time[0];
    let hor_fim = time[1];

    time = data.dt_inicio.split(' ');
    let hor_inicio = time[1];

    for (let i in data) {
        $('#' + i).val(data[i]);
    }

    let $newOption = $("<option selected='selected'></option>").val(data.id_empresa).text(data.razao_social)
    $("#selectEmpresa2").append($newOption).trigger('change');

    $newOption = $("<option selected='selected'></option>").val(data.id_ticket).text(data.ticket_titulo)
    $("#selectTicket2").append($newOption).trigger('change');

    $('#ini_lancamento').val(hor_inicio);
    $('#fim_lancamento').val(hor_fim);
    $('#dt_lancamento').val(data_acao);

    modalDados.show();
})

$('.filtro--list').change(async function (e) {
    e.preventDefault();

    table.pagina = 0;

    let query = get_query();
    let data = await getDataGET(`${url_base}/api/get_horas?limite=${limite}&${query}`);
    data = isJson(data);
    table.criarTable(data.data);
})

$('#btn--limparFiltro').click(async function (e) {
    e.preventDefault();

    filtros[0].value = '';
    // filtros[1].value = '';

    console.log('foi');
    let data = await getDataGET(`${url_base}/api/get_horas?limite=${limite}`);
    data = isJson(data);
    table.criarTable(data.data);
})

function get_query(){
    
    // let cliente = filtros[0].value ?? '';
    let colaborador = filtros[0].value;

    // let data_inicio = $('#dtvgfinal_inicio').val() ?? '';
    // let data_fim = $('#dtvgfinal_fim').val() ?? '';

    return `id_usuario=${colaborador}`;
}

$('.table-responsive').on('scroll', function () {
    if (table.loadingScroll) {
        let value = parseInt(this.scrollTop / (this.scrollHeight - this.clientHeight));
        if (value == 1) {
            table.pagina++;

            (async function () {

                let query = get_query();
                let data = await getDataGET(`${url_base}/api/get_horas?limite=${limite}&page=${table.pagina}&${query}`);
                data = isJson(data);

                if (data.status == 1) {
                    table.adicionarTable(data.data);
                } else {
                    table.pagina--
                }
            })()
        };
    }
})

$('#iniciarLD').submit(function (e) {
    e.preventDefault();

    let dados = new FormData(this);

    let obj = {
        id_empresa: dados.get('id_empresa'),
        descricao: dados.get('descricao'),
        id_ticket: dados.get('id_ticket') ?? '',
        id_atividade: dados.get('atividade') ?? '',
        id_projeto: dados.get('id_projeto') ?? ''
    }

    Painel.iniciar_ld(obj);
})

$('#excluirBtn').click(function (e) {
    e.preventDefault();

    if (!confirm('Tem certeza que deseja excluir esse lançamento?')) return false;

    let id = $('#id_lancamento').val();
    ajaxRequestGet(`${url_base}/api/excluir_lancamento?id_lancamento=${id}`, function (resp) {
        resp = isJson(resp);
        if (resp.status == 1) {
            alert('Excluido com sucesso.')
            document.location.reload()
        } else {
            alert(resp.message ? resp.message : resp.error);
        }
    })
})

$('#btn--filtros').click(async function () {
    let data_inicio = $('#dtvgfinal_inicio').val() ?? '';
    let data_fim = $('#dtvgfinal_fim').val() ?? '';

    let colaborador = filtros[0].value;

    let data = await getDataGET(`${url_base}/api/get_horas?limite=${limite}&colaborador=${colaborador}&dt_inicial=${data_inicio}&dt_final=${data_fim}`);
    data = isJson(data);
    table.criarTable(data.data);
})

$('#btn--limparFiltros').click(async function () {
    $('#dtvgfinal_inicio').val('');
    $('#dtvgfinal_fim').val('');

    let data = await getDataGET(`${url_base}/api/get_horas?limite=${limite}`);
    data = isJson(data);
    table.criarTable(data.data);
})


$('#checkbox_projeto').change(function () {
    if ($('#projeto_ld').prop('disabled')) {
        $('#projeto_ld').prop('disabled', false)
    } else {
        $('#projeto_ld').prop('disabled', true)
        $('#cliente_ld').removeClass('readonly');
        $('#cliente_ld').val('');
    }
})


$('#projeto_ld').change(async function () {
    let data = await getDataGET(`/api/get_projeto?id_projeto=${this.value}`);
    data = isJson(data);
    console.log(data);
    $('#cliente_ld').val(data.data[0].id_cliente);
    $('#cliente_ld').addClass('readonly');
})