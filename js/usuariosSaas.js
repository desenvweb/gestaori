
const formInserir = document.getElementById('form#inserir');
const formEditar = document.getElementById('form#editar');
const tbody = document.getElementById('tbody');
const tbodySearch = document.getElementById('tbodysearch');
const modalDados = new bootstrap.Modal(document.getElementById('modalExibirDados'), {})


var table = {
    data: false,
    data_search: false,
    search: false,
    pagina: 0,
    loadingScroll: true,

    criarTable: function (data) {
        this.data = data;
        let linhas = this.gerarLinhas(this.data);
        tbody.innerHTML = linhas;
    },

    gerarLinhas: function (data) {
        var linhas = '';

        for (let i in data) {
            let dataCadastro = bancoToFrontDateUTC(data[i].dt_cadastro);

            let empresa = data[i].empresa ?? 'Sem empresa';
            linhas += `<tr data-ref="${data[i].id}">`;

            linhas += `<td>${empresa}</td>`
            linhas += `<td>${data[i].nome}</td>`
            linhas += `<td>${data[i].email1}</td>`

            linhas += '</tr>';
        }

        return linhas;
    },

    adicionarTable: function (data) {
        let linhas = this.gerarLinhas(data);

        if (this.search === true) {
            var array = 'data_search'
            tbodySearch.innerHTML += linhas;
        } else {
            var array = 'data';
            tbody.innerHTML += linhas;
        }

        for (let i in data) {
            this[array].push(data[i]);
        }
    },

    pesquisa: async function (search) {
        let data = await getDataGET(`${url_base}/api/get_ususaas?condicao=${search}`);

        data = isJson(data);
        this.data_search = data.data;
        this.pagina = 0;

        if (search != '') {
            let linhas = this.gerarLinhas(data.data);
            tbodySearch.innerHTML = linhas;
            $(tbody).hide();
            $(tbodySearch).show();
            this.search = true;
        } else {
            $(tbody).show();
            $(tbodySearch).hide();
            this.search = false;
        }
    }
}



$(document).ready(async function () {
    if (Painel.user.tipo_usuario == 1) {
        $('[data-bs-target="#modalInserirCliente"], [data-bs-target="#modalDadosGerais"]').show();
        $('.defaultButton').show();
    } else {
        $('.formpermiss :input').prop('disabled', true);
        $('.hidepermiss').hide();
    }

    (async function () {
        let data = await getDataGET(`${url_base}/api/get_ususaas?limite=${limite}`);
        data = isJson(data);
        table.criarTable(data.data);

        $('.spinner-border').hide();
    })()

    $('#input1Empresa').select2({
        dropdownParent: $('#modalInserirUsuario'),
        theme: 'bootstrap-5',
        placeholder: "Pesquise pelo Nome Fantasia",
        ajax: {
            url: '/api/get_todos_clientes',
            type: 'get',
            dataType: 'json',
            delay: 250,
            data: function (params) {

                var query = {
                    condicao: params.term,
                    limite: 20
                }
                return query
            },
            processResults: function (response) {

                return {
                    results: $.map(response.data, function (item) {
                        return {
                            id: item.id,
                            text: `${item.nome_fantasia}`
                        }
                    })
                }
            }
        },
        cache: true
    })

    $('#input2Empresa').select2({
        dropdownParent: $('#modalExibirDados'),
        theme: 'bootstrap-5',
        placeholder: "Pesquise pelo Nome Fantasia",
        ajax: {
            url: '/api/get_todos_clientes',
            type: 'get',
            dataType: 'json',
            delay: 250,
            data: function (params) {

                var query = {
                    condicao: params.term,
                    limite: 30
                }
                return query
            },
            processResults: function (response) {

                return {
                    results: $.map(response.data, function (item) {
                        return {
                            id: item.id,
                            text: `${item.nome_fantasia}`
                        }
                    })
                }
            }
        },
        cache: true
    })

})

formInserir.addEventListener('submit', async function (e) {
    e.preventDefault();

    let data = nowToBanco().split(' ');

    let formData = new FormData(formInserir);
    formData.set('id_cadastrante', Painel.user.id);
    formData.set('dt_cadastro', data[0]);

    ajaxDataForm(`${url_base}/api/cadastrar_ususaas`, formData,
        function (resp) {
            resp = isJson(resp);
            $('.defaultButton').show();
            $('.loadingButton').hide();
            if (resp.status == 1) {
                alert('Salvo com sucesso.')
                window.location.reload();
            } else {
                alert(resp.message ? resp.message : resp.error);
            }
        }, function () {
            //Before send
            $('.defaultButton').hide();
            $('.loadingButton').show();
        })
    $('.defaultButton').show();
    $('.loadingButton').hide();
})

formEditar.addEventListener('submit', async function (e) {
    e.preventDefault();
    let formData = new FormData(formEditar);

    ajaxDataForm(`${url_base}/api/editar_ususaas`, formData,
        function (resp) {
            resp = isJson(resp);
            $('.defaultButton').show();
            $('.loadingButton').hide();

            if (resp.status == 1) {
                alert('Salvo com sucesso.')
                window.location.reload();
            } else {
                alert(resp.message ? resp.message : resp.error);
            }
        }, function () {
            //Before send
            $('.defaultButton').hide();
            $('.loadingButton').show();
        })

    $('.defaultButton').show();
    $('.loadingButton').hide();
})

$(document).on('click', 'tr', function (e) {
    let id = $(this).data('ref');
    if (!id) return

    if (!table.search) {
        var data = table.data.filter(el => el.id == id);
    } else {
        var data = table.data_search.filter(el => el.id == id);
    }

    data = data[0];

    $('#idUsuario').val(id);

    for (let i in data) {
        $('#' + i).val(data[i]);
    }

    let select2 = $('#input2Empresa');
    if(data.id_empresa != '0' && data.id_empresa != false){

        let option = new Option(data.empresa, data.id_empresa, true, true);

        select2.append(option).trigger('change');

        select2.trigger({
            type: 'select2:select',
            params: {
                id: data.id_empresa,
                text: data.empresa
            }
        });
    }else{
        select2.val(null).trigger('change');
    }

    modalDados.show();
})

$('#excluirBtn').click(function (e) {
    e.preventDefault();

    if (!confirm('Tem certeza que deseja excluir esse cliente?')) return false;

    let id = $('#idCliente').val();
    ajaxRequestGet(`${url_base}/api/excluir_cliente?id=${id}`, function (resp) {
        resp = isJson(resp);
        if (resp.status == 1) {
            alert('Excluido com sucesso.')
            document.location.reload()
        } else {
            alert(resp.message ? resp.message : resp.error);
        }
    })
})


$('.table-responsive').on('scroll', function () {
    if (table.loadingScroll) {
        let value = parseInt(this.scrollTop / (this.scrollHeight - this.clientHeight));
        if (value == 1) {
            table.pagina++;

            let val = $('#searchInput').val();
            let condicao = val ? `&condicao=${val}` : '';

            (async function () {
                let resp = await getDataGET(`${url_base}/api/get_ususaas?limite=${limite}&pagina=${table.pagina}${condicao}`);
                resp = isJson(resp);
                if (resp.status == 1) {
                    table.adicionarTable(resp.data);
                }
            })()
        };
    }
})



$('#searchInput').keyup(function () {
    table.pesquisa($(this).val());
})