
const formInserir = document.getElementById('form#inserir');
const formEditar = document.getElementById('form#editar');
const formContrato = document.getElementById('formCadastarContrato');
const formCadastrarHoras = document.getElementById('formCadastrarHoras');
const formCadastrarAdendo = document.getElementById('formCadastrarAdendo');

const tbody = document.getElementById('tbody');
const tbodySearch = document.getElementById('tbodysearch');
const anexoDiv = document.getElementsByClassName('anexo--listItens')[0];

const modalDados = new bootstrap.Modal(document.getElementById('modalExibirDados'), {})
const modalInserir = new bootstrap.Modal(document.getElementById('modalInserirCliente'), {})
const modalDadosGerais = new bootstrap.Modal(document.getElementById('modalDadosGerais'), {})
const modalInserirContratos = new bootstrap.Modal(document.getElementById('modalCadastrarContrato'), {})
const modalCadastrarHoras = new bootstrap.Modal(document.getElementById('modalCadastrarHoras'), {})
const modalCadastrarAdendo = new bootstrap.Modal(document.getElementById('modalCadastrarAdendo'), {});
const modalRelatorioHoras = new bootstrap.Modal(document.getElementById('modalRelatorioHoras'), {});


var table = {
    data: false,
    search: false,
    pagina: 0,
    loadingScroll: true,

    criarTable: function (data) {
        this.data = data;
        let linhas = this.gerarLinhas(this.data);
        tbody.innerHTML = linhas;
    },

    gerarLinhas: function (data) {
        var linhas = '';

        for (let i in data) {
            linhas += `<tr data-ref="${data[i].id}">`;

            linhas += `<td>${data[i].nome_fantasia}</td>`
            linhas += `<td>${data[i].saldo_horas}</td>`
            linhas += `<td>${data[i].qnt_usuariosSaaS}</td>`

            linhas += '</tr>';
        }

        return linhas;
    },

    adicionarTable: function (data) {
        let linhas = this.gerarLinhas(data);
        this.adicionar_data(data);
        
        tbody.innerHTML += linhas;
    },

    pesquisa: async function (search) {
        let data = await getDataGET(`${url_base}/api/get_empresas?condicao=${search}&limite=100`);

        data = isJson(data);

        this.adicionar_data(data.data);
        if (search != '') {
            let linhas = this.gerarLinhas(data.data);
            tbodySearch.innerHTML = linhas;
            $(tbody).hide();
            $(tbodySearch).show();
            this.search = true;
        } else {
            $(tbody).show();
            $(tbodySearch).hide();
            this.search = false;
        }
    },
    
    adicionar_data : function(data){
        for(let i in data){
            let data_existe = this.data.filter(el => el.id == data[i].id);

            if(data_existe.length === 0){
                this.data.push(data[i]);
            }
        }
    }
}

var modalData = {
    empresa_data: false,
    contrato_id : false,
}


var tableRelatorio = {
    data: false,

    criarTable: function (data) {
        this.data = data;
        let linhas = this.gerarLinhas(this.data);
        tbodyRelatorio.innerHTML = linhas;
    },

    gerarLinhas: function (data) {
        var linhas = '';

        for (let i in data) {
                    
                let dt_inicio = bancoToFrontHoras(data[i].dt_inicio)
                let dt_fim = bancoToFrontHoras(data[i].dt_fim)

                linhas += `<tr>`;
                linhas += `<td>${data[i].id_ticket}</td>`
                linhas += `<td>${data[i].nome}</td>`
                linhas += `<td>${dt_inicio}</td>`
                linhas += `<td>${dt_fim}</td>`
                linhas += `<td>${data[i].diff_horas}</td>`; 

                linhas += '</tr>';
        }
        
        return linhas;
    },
}


$(document).ready(function () {
    if (Painel.user.tipo_usuario == 1) {
        $('[data-bs-target="#modalInserirCliente"], [data-bs-target="#modalDadosGerais"]').show();
        $('.defaultButton').show();
    } else {
        $('.formpermiss :input').prop('disabled', true);
        $('.hidepermiss').hide();
    }

    (async function () {
        let data = await getDataGET(`${url_base}/api/get_empresas?limite=${limite}`);
        data = isJson(data);
        table.criarTable(data.data);

        $('.spinner-border').hide();
    })()
})

formInserir.addEventListener('submit', async function (e) {
    e.preventDefault();
    let formData = new FormData(formInserir);
    ajaxDataForm(`${url_base}/api/cadastrar_empresa`, formData,
        function (resp) {
            resp = isJson(resp);
            $('.defaultButton').show();
            $('.loadingButton').hide();
            if (resp.status == 1) {
                alert('Salvo com sucesso.')
                window.location.reload();
            } else {
                alert(resp.message ? resp.message : resp.error);
            }
        }, function () {
            //Before send
            $('.defaultButton').hide();
            $('.loadingButton').show();
        })
    $('.defaultButton').show();
    $('.loadingButton').hide();
})

formEditar.addEventListener('submit', async function (e) {
    e.preventDefault();
    let formData = new FormData(formEditar);

    ajaxDataForm(`${url_base}/api/editar_empresa`, formData,
        function (resp) {
            resp = isJson(resp);
            $('.defaultButton').show();
            $('.loadingButton').hide();

            if (resp.status == 1) {
                alert('Salvo com sucesso.')
                window.location.reload();
            } else {
                alert(resp.message ? resp.message : resp.error);
            }
        }, function () {
            //Before send
            $('.defaultButton').hide();
            $('.loadingButton').show();
        })

    $('.defaultButton').show();
    $('.loadingButton').hide();
})

formContrato.addEventListener('submit', async function (e) {
    e.preventDefault();

    if (modalData.empresa_data !== false) {
        empresa = modalData.empresa_data;

        let msg = `Tem certeza que deseja cadastrar um contrato para o cliente ${empresa.nome_fantasia}? Confira os dados, alguns deles não são possíveis editar após cadastrar.`;
        if (confirm(msg) === false) return;

        let formData = new FormData(formContrato);
        formData.set('id_empresa', empresa.id);

        ajaxDataForm(`/api/add_contrato`, formData,
            function (resp) {
                resp = isJson(resp);
                $('.defaultButton').show();
                $('.loadingButton').hide();

                if (resp.status == 1) {
                    alert('Salvo com sucesso.')
                    window.location.reload();
                } else {
                    alert(resp.message ? resp.message : resp.error);
                }
            }, function () {
                //Before send
                $('.defaultButton').hide();
                $('.loadingButton').show();
            })

        $('.defaultButton').show();
        $('.loadingButton').hide();

    }
})

formCadastrarHoras.addEventListener('submit', async function (e) {
    e.preventDefault();

    if (modalData.empresa_data !== false) {
        empresa = modalData.empresa_data;

        let msg = `Tem certeza que deseja cadastrar horas para o cliente ${empresa.nome_fantasia}?`;
        if (confirm(msg) === false) return;

        let formData = new FormData(formCadastrarHoras);
        formData.set('id_empresa', empresa.id);

        ajaxDataForm(`/api/cadastrar_horasCompradas`, formData,
            function (resp) {
                resp = isJson(resp);
                $('.defaultButton').show();
                $('.loadingButton').hide();

                if (resp.status == 1) {
                    alert('Salvo com sucesso.')
                    window.location.reload();
                } else {
                    alert(resp.message ? resp.message : resp.error);
                }
            }, function () {
                //Before send
                $('.defaultButton').hide();
                $('.loadingButton').show();
            })

        $('.defaultButton').show();
        $('.loadingButton').hide();
    }
})

formCadastrarAdendo.addEventListener('submit', async function(e){
    e.preventDefault();

    let contratos = modalData.empresa_data.contratos.length;
    if (modalData.empresa_data.contratos[contratos-1].id !== false) {

        let formData = new FormData(formCadastrarAdendo);
        formData.set('id_contrato', modalData.empresa_data.contratos[contratos-1].id);

        ajaxDataForm(`/api/add_adendo`, formData,
            function (resp) {
                resp = isJson(resp);
                $('.defaultButton').show();
                $('.loadingButton').hide();

                if (resp.status == 1) {
                    alert('Salvo com sucesso.')
                    window.location.reload();
                } else {
                    alert(resp.message ? resp.message : resp.error);
                }
            }, function () {
                //Before send
                $('.defaultButton').hide();
                $('.loadingButton').show();
            })

        $('.defaultButton').show();
        $('.loadingButton').hide();

    }

})


$('#formRelatorioHoras').submit(function(e){
    e.preventDefault();
    
    let formData = new FormData(this);

    let empresa = modalData.empresa_data;
    formData.set('id_empresa', empresa.id);

    ajaxDataForm(`${url_base}/api/relatorio_horas_empresa`, formData,
    function (resp) {
        resp = isJson(resp);
        $('.defaultButton').show();
        $('.loadingButton').hide();
        $('#gerar_pdf').show();
        if (resp.status == 1) {
            resp = resp.data
            
            $('#tempo_periodo').text(resp.info.hhmm_trabalhado ?? 0)
            $('#total_servicos').text(resp.info.qnt_servicos ?? 0)
            $('#total_projetos').text(resp.info.total_projetos ?? 0)
            $('#tempo_total_projetos').text(resp.info.horas_totais_projetos ?? 0)


            $('#gerar_pdf').show();
            tableRelatorio.criarTable(resp.horas);
        } else {
            alert(resp.message ? resp.message : resp.error);
        }
    }, function () {
        //Before send
        $('.defaultButton').hide();
        $('.loadingButton').show();
        $('#gerar_pdf').hide();
    })
})




$(document).on('submit', '.formEditarContrato', function(e){
    e.preventDefault();

    let formData = new FormData(this);
    
    $("body").css("cursor", "progress");
    ajaxDataForm(`/api/editar_contrato`, formData,
    function (resp) {
        resp = isJson(resp);

        $("body").css("cursor", "default");

        if (resp.status == 1) {
            alert('Salvo com sucesso.')
            window.location.reload();
        } else {
            alert(resp.message ? resp.message : resp.error);
        }
    })

})

$(document).on('click', 'tr', function (e) {
    let id = $(this).data('ref');
    if (id === false) return

    let data = table.data.filter(el => el.id == id);
    data = data[0];
    modalData.empresa_data = data;

    //Popula os campos
    for (let i in data) {
        $('[data-preencher="' + i + '"]').val(data[i])
    }
    if (data.contratos.length == 0) {
        document.getElementById('addAdendo').style.display = 'none';
    }else{
        document.getElementById('addAdendo').style.display = 'inline';
    }

    //Cria HTML dos contratos

    if (data.contratos) {
        let linhas = '';

        linhas = acordion_contratos_html(data.contratos);
        $('#accordionContrato').html(linhas);
    }
    //

    //Criar HTML das Horas Compradas
    if (data.horas_compradas) {
        let linhas = '';
        linhas = accordion_hora_html(data.horas_compradas);

        $('#accordionHorasAvulsas').html(linhas);

    }

    $("#formCadastrarHoras [name='empresa']").val(data.nome_fantasia)

    modal.abrirModal(modalDados)
})

$('#excluirBtn').click(function (e) {
    e.preventDefault();

    let empresa = modalData.empresa_data;

    if (!confirm(`Tem certeza que deseja excluir a empresa ${empresa.nome_fantasia}?`)) return false;


    ajaxRequestGet(`${url_base}/api/excluir_empresa?id_empresa=${empresa.id}`, function (resp) {
        resp = isJson(resp);
        if (resp.status == 1) {
            alert('Excluido com sucesso.')
            document.location.reload()
        } else {
            alert(resp.message ? resp.message : resp.error);
        }
    })
})


$('.table-responsive').on('scroll', function () {
    if (table.loadingScroll) {
        let value = parseInt(this.scrollTop / (this.scrollHeight - this.clientHeight));
        if (value == 1) {
            table.pagina++;



            let data_inicio = $('#dtvgfinal_inicio').val() ?? '';
            let data_fim = $('#dtvgfinal_fim').val() ?? '';
            let contratada = $('#contratada_filtro').val() ?? '';
            let periodicidade_pag = $('#periodicidade_pagamento_filtro').val() ?? '';
            let query = '';

            if (data_inicio == '' && data_fim == '' && contratada == '' && periodicidade_pag == ''){
                query = `limite=${limite}&pagina=${table.pagina}`;
            }else{
                query = `dtvgfinal_inicio=${data_inicio}&dtvgfinal_fim=${data_fim}&pagina=${table.pagina}
                &contratada=${contratada}&periodicidade_pagamento_filtro=${periodicidade_pag}`;
            }

            // console.log(query);
            (async function () {
                let resp = await getDataGET(`${url_base}/api/get_empresas?${query}&limite=${limite}`);
                resp = isJson(resp);
                if (resp.status == 1) {
                    table.adicionarTable(resp.data);
                }
            })()
        };
    }
})


$('#searchInput').keyup(function () {
    table.pesquisa($(this).val());
})

$('#gerar_pdf').click(function(){

    let empresa = modalData.empresa_data;

    let a = document.createElement('a');
    let dt_inicial = $('[name="dt_inicial"]').val();
    let dt_final = $('[name="dt_final"]').val();
    let id_empresa = empresa.id;

    let query = `id_empresa=${id_empresa}&dt_inicial=${dt_inicial}&dt_final=${dt_final}` 
    a.href = `/api/relatorio_horas_empresa_pdf?${query}`;
    a.target = '_blank';
    a.click();
})


$('#filtros').submit(async function (e) {
    e.preventDefault();

    let data_inicio = $('#dtvgfinal_inicio').val();
    let data_fim = $('#dtvgfinal_fim').val();
    let contratada = $('#contratada_filtro').val() ?? '';
    let periodicidade_pag = $('#periodicidade_pagamento_filtro').val() ?? '';

    let query = `dtvgfinal_inicio=${data_inicio}&dtvgfinal_fim=${data_fim}&pagina=${table.pagina}
                 &contratada=${contratada}&periodicidade_pagamento_filtro=${periodicidade_pag}`;
    table.pagina = 0;

    let data = await getDataGET(`${url_base}/api/get_clientes?${query}&limite=${limite}`);
    data = isJson(data);
    table.criarTable(data.data);
})

$('#btn--limparFiltros').click(async function () {
    let data = await getDataGET(`${url_base}/api/get_clientes?limite=${limite}`);
    data = isJson(data);
    table.criarTable(data.data);
})

$('[data-bs-target="#modalDadosGerais"]').click(async function () {
    let data = await getDataGET(`${url_base}/api/dados_gerais`);
    data = isJson(data);
    data = data.data;

    let valor_mensal_brl = parseBRL(data.valor_mensal_total)
    let valor_anual_brl = parseBRL(data.valor_anual_total)
    let valor_total = parseBRL(data.valor_total)

    $('#soma_valor_mensal_contratos').text(valor_mensal_brl);
    $('#soma_valor_anual_contratos').text(valor_anual_brl);
    $('#soma_valor_geral_contratos').text(valor_total);
    $('#total_clientes').text(data.total_clientes);
    $('#qntTotal_usuarios_saas').text(data.qnt_usuarios_saas)

})


function acordion_contratos_html(data) {

    let linhas = '';

    for (let i in data) {

        let tipo_contrato = '';

        switch (data[i].tipo_contrato) {
            case 'manutencao':
                tipo_contrato = 'Contrato de Manutenção';
                break;

            case 'saas':
                tipo_contrato = 'Contrato SaaS';
                break;
        }

        linhas += '<div class="accordion-item">';

        linhas += ' <h2 class="accordion-header" id="headingOne">';
        linhas += `<button class="accordion-button collapsed" onclick="modalData.contrato_id = ${data[i].id}" type="button" data-bs-toggle="collapse" data-bs-target="#contratoAccordion${i}" aria-expanded="false" aria-controls="collapse">`;
        linhas += `${tipo_contrato} &nbsp; - &nbsp; ${data[i].status_descricao}`;
        linhas += '</button></h2>';

        linhas += `<div id="contratoAccordion${i}" class="accordion-collapse collapse" aria-labelledby="headingTwo" data-bs-parent="#accordionContrato" style="">`;
        linhas += '<div class="accordion-body">';

        linhas += '<form class="formpermiss formEditarContrato">';

        linhas += `<input type="hidden" name="id_contrato" value="${data[i].id}">`;
        
        linhas += '<div class="form--row"> <label>Tipo do Contrato</label>';
        linhas += `<input type="text" name="tipo_contrato" value="${tipo_contrato}"class="form-control" disabled>`;
        linhas += '</div>';

        linhas += '<div class="form--row">';
        linhas += '<div> <label>Vigência Inicial</label>';
        linhas += `<input type="date" name="dt_vigenciainicial" value="${data[i].dt_vigenciainicial}"class="form-control" disabled>`;
        linhas += '</div>';

        linhas += '<div> <label>Vigência Final</label>';
        linhas += `<input type="date" name="dt_vigenciafinal" value="${data[i].dt_vigenciafinal}" class="form-control" disabled>`;
        linhas += '</div>';
        linhas += '</div>';

        linhas += '<div> <label>Próximo Reajuste</label>';
        linhas += `<input type="date" name="dt_proximoReajuste" value="${data[i].dt_proximoReajuste}" class="form-control">`;
        linhas += '</div>';

        if (data[i].tipo_contrato === 'manutencao') {
            linhas += '<div class="form--row">';
            linhas += '<div> <label>Total de Horas</label>';
            linhas += `<input type="text" value="${data[i].horas_totais}" class="form-control" disabled>`;
            linhas += '</div>';
            linhas += '<div> <label> Horas Mês </label>';
            linhas += `<input type="text" value="${data[i].horas_mes}" class="form-control" disabled>`;
            linhas += '</div> </div>';
        }

        if (data[i].tipo_contrato === 'saas') {
            linhas += '<div class="form--row">';
            linhas += '<label>Usuários SaaS</label>';
            linhas += `<input type="text" name="usuarios_saas" value="${data[i].usuarios_saas}" class="form-control">`;
            linhas += '</div>';
        }

        linhas += '<div class="form--row"> <label>Valor do Contrato</label>';
        linhas += `<input type="text" name="valor_total" value="${data[i].valor_total}" class="form-control">`;
        linhas += '</div>';


        linhas += '<div class="form--row"> <label>Multa</label>';
        linhas += `<input type="text" name="multa" value="${data[i].multa}" class="form-control">`;
        linhas += '</div>';

        let empresas = {
            'Sigma' : {
                'value' : 'sigma',
                'selected' : data[i].empresa_contratada === 'sigma' ? true : false
            },
            'Manut' : {
                'value' : 'manut',
                'selected' : data[i].empresa_contratada === 'manut' ? true : false
            },
            'Microcenter' : {
                'value' : 'microcenter',
                'selected' : data[i].empresa_contratada === 'microcenter' ? true : false
            }
        }

        let selectEMP = gerar_select('empresa_contratada', 'form-control', empresas);

        linhas += '<div class="form--row"> <label>Empresa Contratada</label>';
        linhas += selectEMP.outerHTML;
        linhas += '</div>';

        let pp = {
            'Anual' : {
                'value' : 'anual',
                'selected' : data[i].periodicidade_pagamento === 'anual' ? true : false
            },
            'Mensal' : {
                'value' : 'mensal',
                'selected' : data[i].periodicidade_pagamento === 'mensal' ? true : false
            }
        }

        let selectPP = gerar_select('periodicidade_pagamento', 'form-control', pp);

        linhas += '<div class="form--row"> <label>Periodicidade do Pagamento</label>';
        linhas += selectPP.outerHTML;
        linhas += '</div>';


        let indices = {
            'IGPM' : {
                'value' : 'IGPM',
                'selected' : data[i].indice === 'IGPM' ? true : false
            },
            'IPCA' : {
                'value' : 'IPCA',
                'selected' : data[i].indice === 'IPCA' ? true : false
            }
        }

        let selectIndices = gerar_select('indice', 'form-control', indices);

        linhas += '<div class="form--row"> <label>Indice do Contrato</label>';
        linhas += selectIndices.outerHTML;
        linhas += '</div>';

        linhas += '<div class="form--row"> <label>Observações</label>';
        linhas += `<textarea class="form-control" name="obs">${data[i].obs}</textarea>`;
        linhas += '</div>';
    
        linhas += '<br>';

        linhas += '<button class="btn btn-primary defaultButton">Salvar Mudanças</button>';

        linhas += '<hr>';
        linhas += '<div>';
        linhas += '<div>';
        linhas += `<h3 style="display: inline-block;">Adendos</h3>`
        //adicionado botão de novo adendo no menu na página de clientes
        // &#8226; <span onclick="modal.abrirModal(modalCadastrarAdendo)" class="text-muted pointer">Novo Adendo</span>`;
        for(let a in data[i].adendos){
            //console.log(data[i].adendos[a]);
            let data_antiga = bancoToFrontDateUTC(data[i].adendos[a].dt_vigenciafinal_antiga);
            let data_nova = bancoToFrontDateUTC(data[i].adendos[a].dt_vigenciafinal_nova);

            linhas += `<p>Adendo extende a validade desse contrato de <b>${data_antiga}</b> até <b>${data_nova}</b></p>`;
        }

        linhas += '</div>';
        linhas += '</div>';

        linhas += '<hr>'
        linhas += '<div>';
        linhas += '<h3>Anexos</h3>';

        linhas += '<div class="form--row">'
        for(let a in data[i].anexos){

            linhas += '<div class="anexo--item">';
            linhas += `<a href="${data[i].anexos[a].link}" target="_blank">${data[i].anexos[a].nome}</a>`
            linhas += '</div>';
        }
        linhas += '</div>'

        linhas += '</form>';
        
        linhas += '</div>';

        linhas += '</div>';
        linhas += '</div>';

        linhas += '</div>';

    }

    return linhas
}

function accordion_hora_html(data) {

    let linhas = '<h3 style="text-align: center;">Horas Avulsas</h3>';

    for (let i in data) {

        linhas += '<div class="accordion-item">';

        linhas += ' <h2 class="accordion-header" id="headingOne">';
        linhas += `<button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#horasAccordion${i}" aria-expanded="false" aria-controls="collapse">`;
        linhas += `${data[i].total_horas} Horas Compradas`;
        linhas += '</button></h2>';

        linhas += `<div id="horasAccordion${i}" class="accordion-collapse collapse" aria-labelledby="headingTwo" data-bs-parent="#accordionContrato" style="">`;
        linhas += '<div class="accordion-body">';

        linhas += '<div class="form--row">';
        linhas += '<div> <label>Valor da Compra</label>';
        linhas += `<input type="text" value="${data[i].valor_total}" class="form-control" disabled>`;
        linhas += '</div>';

        linhas += '<div> <label>Cadastrante</label>';
        linhas += `<input type="text" value="${data[i].cadastrante}" class="form-control" disabled>`;
        linhas += '</div>';
        linhas += '</div>';
        
        linhas += '<div class="form--row">';
        linhas += '<div> <label>Data de Cadastro</label>';

        let datetime = bancoToFrontHoras(data[i].dt_cadastro);
        linhas += `<input type="text" value="${datetime}" class="form-control" disabled>`;
        linhas += '</div>';

        linhas += '<div> <label>Observação</label>';
        linhas += `<textarea class="form-control" disabled>${data[i].obs}</textarea>`;
        linhas += '</div>';
        linhas += '</div>';


        linhas += '</div>';
        linhas += '</div>';

        linhas += '</div>';
    }

    return linhas
}

$('#inputTipoContrato').change(function () {

    let value = this.value;

    switch (value) {
        case 'saas':
            $('#formCadastarContrato [data-nameInput="usuarios_saas"]').show();
            $('#formCadastarContrato [name="usuarios_saas"]').prop('required', true);

            $('#formCadastarContrato [data-nameInput="horas_totais"]').hide();
            $('#formCadastarContrato [name="horas_totais"]').prop('required', false);
            break;

        case 'manutencao':
            $('#formCadastarContrato [data-nameInput="usuarios_saas"]').hide();
            $('#formCadastarContrato [name="usuarios_saas"]').prop('required', false);

            $('#formCadastarContrato [data-nameInput="horas_totais"]').show();
            $('#formCadastarContrato [name="horas_totais"]').prop('required', true);
            break;
    }
})

$('#addAdendo').click(function (e) {
    e.preventDefault();
    console.log('Adendo');
    let contratos = modalData.empresa_data.contratos.length;
    switch (modalData.empresa_data.contratos[contratos-1].tipo_contrato) {
        case 'saas':
            $('#formCadastrarAdendo [data-nameInput="usuarios_saas_nova"]').show();
            $('#formCadastrarAdendo [name="usuarios_saas_nova"]').prop('required', true);

            $('#formCadastrarAdendo [data-nameInput="horas_totais_nova"]').hide();
            $('#formCadastrarAdendo [name="horas_totais_nova"]').prop('required', false);
            break;

        case 'manutencao':
            $('#formCadastrarAdendo [data-nameInput="usuarios_saas_nova"]').hide();
            $('#formCadastrarAdendo [name="usuarios_saas_nova"]').prop('required', false);

            $('#formCadastrarAdendo [data-nameInput="horas_totais_nova"]').show();
            $('#formCadastrarAdendo [name="horas_totais_nova"]').prop('required', true);
            break;
    }
    
})

//Masks
$('input[name="cnpj"]').mask('00.000.000/0000-00', { reverse: false });
$('.money').mask('###.00', {
    reverse: true
});
$('input[name="cep"]').mask('00000-000');
$('input[name="telefone"]').mask('(00) 0000-0000');


