﻿var url_string = window.location.href;
var url = new URL(url_string);
var url_base = url.origin;


$(document).ready(async function () {
    var query = location.search.slice(1);
    var partes = query.split('&');
    var data = {};
    partes.forEach(function (parte) {
        var chaveValor = parte.split('=');
        var chave = chaveValor[0];
        var valor = chaveValor[1];
        data[chave] = valor;
    });
    console.log(data); 

    
    (async function () {
        let resp = await getDataGET(`${url_base}/api/get_avaliacao_ticket?token=${data['token']}`);
        resp = isJson(resp);
        if (resp.status == 1) {
            $('.show--ticket').text('Ticket ' + resp.data[0].id_ticket);

            if(resp.data[0].avaliacao === null){
                let formData = new FormData();
                formData.set('avaliacao', data['avaliacao']);
                formData.set('id_ticket', resp.data[0].id_ticket);
                formData.set('token', data['token']);
                ajaxDataForm(`${url_base}/api/add_avaliacao_ticket`, formData,
                function (resp) {
                    resp = isJson(resp);
                    if (resp.status != 1) {
                       // window.location.reload();
                    //} else {
                        alert(resp.error ?? resp.message);
                    }
                })
            }else{
                let dt_avaliacao = bancoToFrontHoras(resp.data[0].dt_avaliacao)
                $('.show--descricao').text('Avaliação já avaliada em '+ dt_avaliacao);
            }
        }
    })()
    


    $('.spinner-border').hide();


})

