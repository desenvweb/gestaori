
const formInserir = document.getElementById('forminserir');
const formEditar = document.getElementById('form#editar');
const formContrato = document.getElementById('formCadastarContrato');
const formCadastrarHoras = document.getElementById('formCadastrarHoras');
const tbody = document.getElementById('tbody');
const tbodySearch = document.getElementById('tbodysearch');
const modalDados = new bootstrap.Modal(document.getElementById('modalExibirDados'), {})

var table = {
    data: false,
    data_search : false,
    search : false,
    pagina: 0,
    loadingScroll: true,

    criarTable: function (data) {
        this.data = data;
        let linhas = this.gerarLinhas(this.data);
        tbody.innerHTML = linhas;
    },

    gerarLinhas: function (data) {
        var linhas = '';

        for (let i in data) {
            let dtaFaturamento = '';
            dtaFaturamento = bancoToFrontDateUTC(data[i].dt_faturamento);
            let tipo_contrato = '';
    
            switch (data[i].tipo_contrato) {
                case 'manutencao':
                    tipo_contrato = 'Contrato de Manutenção';
                    break;
    
                case 'saas':
                    tipo_contrato = 'Contrato SaaS';
                    break;
                default:
                    tipo_contrato = 'Contrato de Manutenção + SaaS';
            }
            let valor_total =  parseBRL(data[i].valor_total) 

            linhas += `<tr data-ref="${data[i].id}">`;

            linhas += `<td>${data[i].razao_social}</td>`;
            linhas += `<td>${dtaFaturamento}</td>`;
            linhas += `<td>${valor_total}</td>`;
            linhas += `<td>${data[i].indice}</td>`;
            linhas += `<td>${tipo_contrato}</td>`;
            linhas += `<td>${data[i].periodicidade_pagamento}</td>`;
            linhas += `<td>
                <span class="flex-box" onclick="exibirDados(${data[i].id});" data-toggle="tooltip" title="Visualizar">
                    <svg xmlns="http://www.w3.org/2000/svg" class="text-success" width="30" height="30" fill="currentColor" class="bi bi-files" viewBox="0 0 20 20">
                        <path d="M13 0H6a2 2 0 0 0-2 2 2 2 0 0 0-2 2v10a2 2 0 0 0 2 2h7a2 2 0 0 0 2-2 2 2 0 0 0 2-2V2a2 2 0 0 0-2-2zm0 13V4a2 2 0 0 0-2-2H5a1 1 0 0 1 1-1h7a1 1 0 0 1 1 1v10a1 1 0 0 1-1 1zM3 4a1 1 0 0 1 1-1h7a1 1 0 0 1 1 1v10a1 1 0 0 1-1 1H4a1 1 0 0 1-1-1V4z"/>
                    </svg>
                </span>
            </td>`;
            if (data[i].status == 1 && (Painel.user.super_admin == 1 || (Painel.user.id_setor == 3))){
                linhas += `<td>
                    <span class="flex-box" onclick="enviarFaturamento(${data[i].id});" data-toggle="tooltip" title="Enviar para Faturamento">
                        <svg xmlns="http://www.w3.org/2000/svg" class="text-warning" width="30" height="30" fill="currentColor" class="bi bi-coin" viewBox="0 0 20 20">
                            <path d="M5.5 9.511c.076.954.83 1.697 2.182 1.785V12h.6v-.709c1.4-.098 2.218-.846 2.218-1.932 0-.987-.626-1.496-1.745-1.76l-.473-.112V5.57c.6.068.982.396 1.074.85h1.052c-.076-.919-.864-1.638-2.126-1.716V4h-.6v.719c-1.195.117-2.01.836-2.01 1.853 0 .9.606 1.472 1.613 1.707l.397.098v2.034c-.615-.093-1.022-.43-1.114-.9H5.5zm2.177-2.166c-.59-.137-.91-.416-.91-.836 0-.47.345-.822.915-.925v1.76h-.005zm.692 1.193c.717.166 1.048.435 1.048.91 0 .542-.412.914-1.135.982V8.518l.087.02z"/>
                            <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"/>
                            <path d="M8 13.5a5.5 5.5 0 1 1 0-11 5.5 5.5 0 0 1 0 11zm0 .5A6 6 0 1 0 8 2a6 6 0 0 0 0 12z"/>
                        </svg>
                    </span>
                </td>`;
            } else if (data[i].status == 2 && (Painel.user.super_admin == 1 || (Painel.user.id_setor == 8))){
                linhas += `<td>
                    <span class="flex-box" onclick="confirmarFaturamento(${data[i].id});" data-toggle="tooltip" title="Confirmar Faturamento">
                        <svg xmlns="http://www.w3.org/2000/svg" class="text-warning" width="30" height="30" fill="currentColor" class="bi bi-cash-coin" viewBox="0 0 20 20">
                            <path fill-rule="evenodd" d="M11 15a4 4 0 1 0 0-8 4 4 0 0 0 0 8zm5-4a5 5 0 1 1-10 0 5 5 0 0 1 10 0z"/>
                            <path d="M9.438 11.944c.047.596.518 1.06 1.363 1.116v.44h.375v-.443c.875-.061 1.386-.529 1.386-1.207 0-.618-.39-.936-1.09-1.1l-.296-.07v-1.2c.376.043.614.248.671.532h.658c-.047-.575-.54-1.024-1.329-1.073V8.5h-.375v.45c-.747.073-1.255.522-1.255 1.158 0 .562.378.92 1.007 1.066l.248.061v1.272c-.384-.058-.639-.27-.696-.563h-.668zm1.36-1.354c-.369-.085-.569-.26-.569-.522 0-.294.216-.514.572-.578v1.1h-.003zm.432.746c.449.104.655.272.655.569 0 .339-.257.571-.709.614v-1.195l.054.012z"/>
                            <path d="M1 0a1 1 0 0 0-1 1v8a1 1 0 0 0 1 1h4.083c.058-.344.145-.678.258-1H3a2 2 0 0 0-2-2V3a2 2 0 0 0 2-2h10a2 2 0 0 0 2 2v3.528c.38.34.717.728 1 1.154V1a1 1 0 0 0-1-1H1z"/>
                            <path d="M9.998 5.083 10 5a2 2 0 1 0-3.132 1.65 5.982 5.982 0 0 1 3.13-1.567z"/>
                        </svg>
                    </span>
                </td>`;
            }
            if (Painel.user.super_admin == 1 || Painel.user.id_setor == 3 || Painel.user.id_setor == 8){
                linhas += `<td>
                    <span class="flex-box" onclick="excluirContrato(${data[i].id});"  data-toggle="tooltip" title="Excluir">
                        <svg xmlns="http://www.w3.org/2000/svg" class="text-danger" width="30" height="30" fill="currentColor" class="bi bi-trash" viewBox="0 0 20 20">
                            <path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z"/>
                            <path fill-rule="evenodd" d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z"/>
                        </svg>
                    </span>
                </td>`;
            }


            linhas += '</tr>';

        }

        return linhas;
    },

    adicionarTable: function (data) {
        let linhas = this.gerarLinhas(data);
        tbody.innerHTML += linhas;
        for(let i in data){
            this.data.push(data[i])
        }
    },

    pesquisa: async function (search) {
        let data = await getDataGET(`${url_base}/api/get_contrato_faturar`);

        data = isJson(data);
        this.data_search = data.data;

        if (search != '') {
            let linhas = this.gerarLinhas(data.data);
            tbodySearch.innerHTML = linhas;
            $(tbody).hide();
            $(tbodySearch).show();
            this.search = true;
        } else {
            $(tbody).show();
            $(tbodySearch).hide();
            this.search = false;
        }
    }
}

var modalData = {
    empresa_data: false,
    contrato_id : false,
}

var tableRelatorio = {
    data: false,

    criarTable: function (data) {
        this.data = data;
        let linhas = this.gerarLinhas(this.data);
        tbodyRelatorio.innerHTML = linhas;
    },

    gerarLinhas: function (data) {
        var linhas = '';

        for (let i in data) {
                    
                let dt_inicio = bancoToFrontHoras(data[i].dt_inicio)
                let dt_fim = bancoToFrontHoras(data[i].dt_fim)

                linhas += `<tr>`;
                linhas += `<td>${data[i].id_ticket}</td>`
                linhas += `<td>${data[i].nome}</td>`
                linhas += `<td>${dt_inicio}</td>`
                linhas += `<td>${dt_fim}</td>`
                linhas += `<td>${data[i].diff_horas}</td>`; 

                linhas += '</tr>';
        }
        
        return linhas;
    },
}

$(document).ready(function () {
    if((Painel.user.super_admin == 1) || (Painel.user.id_setor == 3)) {
        $('[data-bs-target="#modalInserir"]').show();
        $('.defaultButton').show();
    }else{
        $('.formpermiss :input').prop('disabled', true);
        $('.hidepermiss').hide();
    }

    //COMERCIAL
        
    (async function () {
        let data = await getDataGET(`${url_base}/api/get_contrato_faturar`);
        data = isJson(data);
        table.criarTable(data);
        
        $('.spinner-border').hide();
    })()
})

$('.table-responsive').on('scroll', function () {
    if(table.loadingScroll){
        let value = parseInt( Math.round(this.scrollTop) / (this.scrollHeight - this.clientHeight));
        if (value == 1) {
            table.pagina++;

            let contratada = $('#contratada_filtro').val() ?? '';
            let periodicidade_pag = $('#periodicidade_pagamento_filtro').val() ?? '';

            let query = `limite=${limite}&pagina=${table.pagina}`;

            (async function () {
                let resp = await getDataGET(`${url_base}/api/get_contrato_faturar?${query}`);
                resp = isJson(resp);
                if (resp.status == 1) {
                    table.adicionarTable(resp.data);
                }else{
                    //table.pagina--;
                }
    
            })()
        };
    }
})

formInserir.addEventListener('submit', async function (e) {
    e.preventDefault();
    let formData = new FormData(formInserir);
    formData.set('id_cadastrante', Painel.user.id);
    
    ajaxDataForm(`${url_base}/api/add_contrato_faturar`, formData,
        function (resp) {
            resp = isJson(resp);
            $('.defaultButton').show();
            $('.loadingButton').hide();
            if (resp.status == 1) {
                alert('Salvo com sucesso.')
                window.location.reload();
                $('#searchInput').val('');
                $(tbody).show();
                $(tbodySearch).hide();
            
            } else {
                alert(resp.message ? resp.message : resp.error);
            }
        }, function () {
            //Before send
            $('.defaultButton').hide();
            $('.loadingButton').show();
        })
    $('.defaultButton').show();
    $('.loadingButton').hide();
})

formEditar.addEventListener('submit', async function (e) {
    e.preventDefault();
    let formData = new FormData(formEditar);

    ajaxDataForm(`${url_base}/api/editar_contrato_faturar`, formData,
        function (resp) {
            resp = isJson(resp);
            $('.defaultButton').show();
            $('.loadingButton').hide();

            if (resp.status == 1) {
                alert('Salvo com sucesso.')
                window.location.reload();
                $('#searchInput').val('');
                $(tbody).show();
                $(tbodySearch).hide();
            
            } else {
                alert(resp.message ? resp.message : resp.error);
            }
        }, function () {
            //Before send
            $('.defaultButton').hide();
            $('.loadingButton').show();
        })

        $('.defaultButton').show();
        $('.loadingButton').hide();
})

$('#inputTipoContrato').change(function () {
 
    let value = this.value;
    console.log('inputTipoContrato '+ value);

    switch (value) {
        case 'saas':
            $('#forminserir [data-nameInput="usuarios_saas"]').show();
            $('#forminserir [name="usuarios_saas"]').prop('required', true);

            $('#forminserir [data-nameInput="horas_totais"]').hide();
            $('#forminserir [name="horas_totais"]').prop('required', false);
            break;

        case 'manutencao':
            $('#forminserir [data-nameInput="usuarios_saas"]').hide();
            $('#forminserir [name="usuarios_saas"]').prop('required', false);

            $('#forminserir [data-nameInput="horas_totais"]').show();
            $('#forminserir [name="horas_totais"]').prop('required', true);
            break;
        case 'manutencao_saas':
            $('#forminserir [data-nameInput="usuarios_saas"]').show();
            $('#forminserir [name="usuarios_saas"]').prop('required', true);

            $('#forminserir [data-nameInput="horas_totais"]').show();
            $('#forminserir [name="horas_totais"]').prop('required', true);

    }
})

function exibirDados(Id){
    let id = Id;//$(this).data('ref');
    if (!id) return

    if(!table.search){
        var data = table.data.filter(el => el.id == id);
    }else{
        var data = table.data_search.filter(el => el.id == id);
    }

    for (let i in data[0]) {
        $('#' + i).val(data[0][i]);
    }
    
    modalDados.show();

    changeTipoContrato(data[0].tipo_contrato);
    
}

function enviarFaturamento(Id){
    if (confirm("Deseja enviar contrato para faturamento?") == true) {
        let formData = new FormData();
        formData.set('id_cadastrante', Painel.user.id);
        formData.set('id', Id);
        formData.set('status', 2);

        ajaxDataForm(`${url_base}/api/editar_contrato_faturar`, formData,
            function (resp) {
                resp = isJson(resp);
                $('.defaultButton').show();
                $('.loadingButton').hide();

                if (resp.status == 1) {
                    alert('Salvo com sucesso.')
                    window.location.reload();
                    $('#searchInput').val('');
                    $(tbody).show();
                    $(tbodySearch).hide();
                
                } else {
                    alert(resp.message ? resp.message : resp.error);
                }
            }, function () {
                //Before send
                $('.defaultButton').hide();
                $('.loadingButton').show();
            })

            $('.defaultButton').show();
            $('.loadingButton').hide();
    }
}


function confirmarFaturamento(Id){
    if (confirm("Confirma o faturamento do contrato ?") == true) {
        let formData = new FormData();
        formData.set('id_cadastrante', Painel.user.id);
        formData.set('id', Id);
        formData.set('status', 3);

        ajaxDataForm(`${url_base}/api/editar_contrato_faturar`, formData,
            function (resp) {
                resp = isJson(resp);
                $('.defaultButton').show();
                $('.loadingButton').hide();

                if (resp.status == 1) {
                    alert('Salvo com sucesso.')
                    window.location.reload();
                    $('#searchInput').val('');
                    $(tbody).show();
                    $(tbodySearch).hide();
                
                } else {
                    alert(resp.message ? resp.message : resp.error);
                }
            }, function () {
                //Before send
                $('.defaultButton').hide();
                $('.loadingButton').show();
            })

            $('.defaultButton').show();
            $('.loadingButton').hide();
    }
}

function excluirContrato(Id){
    if (confirm("Deseja excluir o faturamento do contrato ?") == true) {
        ajaxRequestGet(`${url_base}/api/excluir_contrato_faturar?id=${Id}`, function (resp) {
            resp = isJson(resp);
    
            if (resp.status == 1) {
                alert('Excluido com sucesso.')
                document.location.reload()
            } else {
                alert(resp.message ? resp.message : resp.error);
            }
        })
    }
}

function changeTipoContrato(value){
    console.log('Edit tipo contrato '+ value);
   
    switch (value) {
        case 'saas':
            console.log('entrou saas');
            $('#inputTipoContratoEdit').val('saas');
            $('[data-nameInput="usuarios_saas_edit"]').show();
            $('[name="usuarios_saas"]').prop('required', true);

            $('[data-nameInput="horas_totais_edit"]').hide();
            $('[name="horas_totais"]').prop('required', false);
            break;

        case 'manutencao':
            console.log('entrou manutencao');
            $('#inputTipoContratoEdit').val('manutencao');
            $('[data-nameInput="usuarios_saas_edit"]').hide();
            $('[name="usuarios_saas"]').prop('required', false);

            $('[data-nameInput="horas_totais_edit"]').show();
            $('[name="horas_totais"]').prop('required', true);
            break;
        default:
            console.log('entrou manutencaosaas');
            $('#inputTipoContratoEdit').val('manutencao_saas');
            $('[data-nameInput="usuarios_saas_edit"]').show();
            $('[name="usuarios_saas"]').prop('required', true);

            $('[data-nameInput="horas_totais_edit"]').show();
            $('[name="horas_totais"]').prop('required', true);

    }
}

//Masks
$('input[name="cnpj"]').mask('00.000.000/0000-00', { reverse: false });
$('input[name="valor_contrato"]').mask('###.00', {
    reverse: true
});
$('input[name="cep"]').mask('00000-000');
$('input[name="telefone"]').mask('(00) 0000-0000');
$('.onlynumber').mask('#');


