
const formInserir = document.getElementById('form#inserir');
const formEditar = document.getElementById('form#editar');
const tbody = document.getElementById('tbody');
const tbodySearch = document.getElementById('tbodysearch');
const modalDados = new bootstrap.Modal(document.getElementById('modalExibirDados'), {})
const anexoDiv = document.getElementsByClassName('anexo--listItens')[0];

var table = {
    data: false,
    data_search : false,
    search : false,
    pagina: 0,
    loadingScroll: true,

    criarTable: function (data) {
        this.data = data;
        let linhas = this.gerarLinhas(this.data);
        tbody.innerHTML = linhas;
    },

    gerarLinhas: function (data) {
        var linhas = '';

        for (let i in data) {
            let dataFim = bancoToFrontDateUTC(data[i].vigencia_final_adendo);

            // let horas_restantes = data[i].horas_restantes.hhmm_restantes ?? '00:00';

            linhas += `<tr data-ref="${data[i].id}">`;

            linhas += `<td>${data[i].nome_fantasia}</td>`
            // linhas += `<td>${horas_restantes}</td>`
            linhas += `<td>${dataFim}</td>`
            linhas += `<td>${data[i].status}</td>`

            linhas += '</tr>';
        }

        return linhas;
    },

    adicionarTable: function (data) {
        for (let i in data) {
            this.data.push(data[i]);

            let tbodyFake = document.createElement('tbody');
            let linhas = '';

            let dataFim = bancoToFrontDateUTC(data[i].vigencia_final_adendo);

            // let horas_restantes = data[i].horas_restantes.hhmm_restantes ?? '00:00';

            linhas += `<tr data-ref="${data[i].id}">`;

            linhas += `<td>${data[i].nome_fantasia}</td>`
            // linhas += `<td>${horas_restantes}</td>`
            linhas += `<td>${dataFim}</td>`
            linhas += `<td>${data[i].status}</td>`

            linhas += '</tr>';

            tbodyFake.innerHTML = linhas;
            tbody.appendChild(tbodyFake.firstChild);
        }
    },

    pesquisa: async function (search) {
        let data = await getDataGET(`${url_base}/api/get_clientes?condicao=${search}`);

        data = isJson(data);
        this.data_search = data.data;

        if (search != '') {
            let linhas = this.gerarLinhas(data.data);
            tbodySearch.innerHTML = linhas;
            $(tbody).hide();
            $(tbodySearch).show();
            this.search = true;
        } else {
            $(tbody).show();
            $(tbodySearch).hide();
            this.search = false;
        }
    }
}

var tableRelatorio = {
    data: false,

    criarTable: function (data) {
        this.data = data;
        let linhas = this.gerarLinhas(this.data);
        tbodyRelatorio.innerHTML = linhas;
    },

    gerarLinhas: function (data) {
        var linhas = '';

        for (let i in data) {
                    
                let dt_inicio = bancoToFrontHoras(data[i].dt_inicio)
                let dt_fim = bancoToFrontHoras(data[i].dt_fim)

                linhas += `<tr>`;
                linhas += `<td>${data[i].nome}</td>`
                linhas += `<td>${dt_inicio}</td>`
                linhas += `<td>${dt_fim}</td>`
                linhas += `<td>${data[i].diff_horas}</td>`; 

                linhas += '</tr>';
        }
        
        return linhas;
    },
}

function excludeAnexo(id){
    if(!confirm('Você tem certeza que deseja excluir esse anexo?')) return false;

    ajaxRequestGet(`/api/excluir_anexo?id_anexo=${id}&tipo_contratado=cliente`, function(resp){
        if(resp.status == 1){
            $(`[data-id_anexo='${id}'`).remove();
        }
        alert(resp.message)
    })
}


$(document).ready(function () {
    if(Painel.user.tipo_usuario == 1){
        $('[data-bs-target="#modalInserirCliente"], [data-bs-target="#modalDadosGerais"]').show();
        $('.defaultButton').show();
    }else{
        $('.formpermiss :input').prop('disabled', true);
        $('.hidepermiss').hide();
    }
    
    (async function () {
        let data = await getDataGET(`${url_base}/api/get_clientes?limite=${limite}`);
        data = isJson(data);
        table.criarTable(data.data);
        
        $('.spinner-border').hide();
    })()
})

formInserir.addEventListener('submit', async function (e) {
    e.preventDefault();
    let formData = new FormData(formInserir);
    ajaxDataForm(`${url_base}/api/cadastrar_cliente`, formData,
        function (resp) {
            resp = isJson(resp);
            $('.defaultButton').show();
            $('.loadingButton').hide();
            if (resp.status == 1) {
                alert('Salvo com sucesso.')
                window.location.reload();
            } else {
                alert(resp.message ? resp.message : resp.error);
            }
        }, function () {
            //Before send
            $('.defaultButton').hide();
            $('.loadingButton').show();
        })
        $('.defaultButton').show();
        $('.loadingButton').hide();
})

formEditar.addEventListener('submit', async function (e) {
    e.preventDefault();
    let formData = new FormData(formEditar);

    ajaxDataForm(`${url_base}/api/editar_cliente`, formData,
        function (resp) {
            resp = isJson(resp);
            $('.defaultButton').show();
            $('.loadingButton').hide();

            if (resp.status == 1) {
                alert('Salvo com sucesso.')
                window.location.reload();
            } else {
                alert(resp.message ? resp.message : resp.error);
            }
        }, function () {
            //Before send
            $('.defaultButton').hide();
            $('.loadingButton').show();
        })

        $('.defaultButton').show();
        $('.loadingButton').hide();
})

$(document).on('click', 'tr', function (e) {
    let id = $(this).data('ref');
    if (!id) return

    if(!table.search){
        var data = table.data.filter(el => el.id == id);
    }else{
        var data = table.data_search.filter(el => el.id == id);
    }

    $('#idCliente').val(id);
    $('#email1').val(data[0].email[0])
    $('#email2').val(data[0].email[1])

    for (let i in data[0]) {
        $('#' + i).val(data[0][i]);
    }
    
    viewSaas(data[0].tipo_licenca);

    if(Painel.user.tipo_usuario == 1){
        (async function () {
            let data = await getDataGET(`${url_base}/api/get_anexos?id_contratado=${id}&tipo_contratado=cliente`);
            data = isJson(data);
            anexoDiv.innerHTML = '';


            for (let i in data.data) {
                let path = data.data[i].path.split('/');
                let path_length = path.length;
                let path_name = path[path_length - 1];

                let div = document.createElement('div');
                div.dataset.id_anexo = data.data[i].id;
                let div_anexo = document.createElement('div');
                div_anexo.classList.add('anexo--item');

                let a = document.createElement('a');
                a.innerText = data.data[i].nome
                a.href = `${url_base}/anexos/${path_name}`;
                a.target = '_blank';

                let hide = Painel.user.tipo_usuario == 2 ? 'hide' : '';
                div_anexo.appendChild(a);
                div_anexo.innerHTML += `<svg class="${hide}" style="cursor: pointer;" onclick="excludeAnexo(${data.data[i].id})" xmlns="http://www.w3.org/2000/svg" height="24px" viewBox="0 0 24 24" width="24px" fill="#000000"><path d="M0 0h24v24H0V0z" fill="none"/><path d="M16 9v10H8V9h8m-1.5-6h-5l-1 1H5v2h14V4h-3.5l-1-1zM18 7H6v12c0 1.1.9 2 2 2h8c1.1 0 2-.9 2-2V7z"/></svg>`
                div.appendChild(div_anexo);
                anexoDiv.appendChild(div)
            }

        })()
    }else{
        $('#valor_contrato').val('');
    }

    modalDados.show();
})

$('#excluirBtn').click(function (e) {
    e.preventDefault();

    if (!confirm('Tem certeza que deseja excluir esse cliente?')) return false;

    let id = $('#idCliente').val();
    ajaxRequestGet(`${url_base}/api/excluir_cliente?id=${id}`, function (resp) {
        resp = isJson(resp);
        if (resp.status == 1) {
            alert('Excluido com sucesso.')
            document.location.reload()
        } else {
            alert(resp.message ? resp.message : resp.error);
        }
    })
})


$('.table-responsive').on('scroll', function () {
    if(table.loadingScroll){
        let value = parseInt(this.scrollTop / (this.scrollHeight - this.clientHeight));
        if (value == 1) {
            table.pagina++;

            let data_inicio = $('#dtvgfinal_inicio').val();
            let data_fim = $('#dtvgfinal_fim').val();
            let contratada = $('#contratada_filtro').val() ?? '';
            let periodicidade_pag = $('#periodicidade_pagamento_filtro').val() ?? '';

            let query = `limite=${limite}&dtvgfinal_inicio=${data_inicio}&dtvgfinal_fim=${data_fim}&pagina=${table.pagina}
            &contratada=${contratada}&periodicidade_pagamento_filtro=${periodicidade_pag}`;

            (async function () {
                let resp = await getDataGET(`${url_base}/api/get_clientes?${query}&limite=${limite}`);
                resp = isJson(resp);
                if (resp.status == 1) {
                    table.adicionarTable(resp.data);
                }
            })()
        };
    }
})


$('#searchInput').keyup(function () {
    table.pesquisa($(this).val());
})

$('#filtros').submit(async function(e){
    e.preventDefault();

    let data_inicio = $('#dtvgfinal_inicio').val();
    let data_fim = $('#dtvgfinal_fim').val();
    let contratada = $('#contratada_filtro').val() ?? '';
    let periodicidade_pag = $('#periodicidade_pagamento_filtro').val() ?? '';

    let query = `dtvgfinal_inicio=${data_inicio}&dtvgfinal_fim=${data_fim}&pagina=${table.pagina}
                 &contratada=${contratada}&periodicidade_pagamento_filtro=${periodicidade_pag}`;
    table.pagina = 0;

    let data = await getDataGET(`${url_base}/api/get_clientes?${query}&limite=${limite}`);
    data = isJson(data);
    table.criarTable(data.data);
})

$('#btn--limparFiltros').click(async function(){
    let data = await getDataGET(`${url_base}/api/get_clientes?limite=${limite}`);
    data = isJson(data);
    table.criarTable(data.data);
})

$('[data-bs-target="#modalDadosGerais"]').click(async function(){
    let data = await getDataGET(`${url_base}/api/dados_gerais`);
    data = isJson(data);
    data = data.data;

    let valor_mensal_brl = parseBRL(data.valor_mensal_total)
    let valor_anual_brl =  parseBRL(data.valor_anual_total) 
    let valor_total =  parseBRL(data.valor_total) 
    
    $('#soma_valor_mensal_contratos').text(valor_mensal_brl);
    $('#soma_valor_anual_contratos').text(valor_anual_brl);
    $('#soma_valor_geral_contratos').text(valor_total);
    $('#total_clientes').text(data.total_clientes);
    $('#qntTotal_usuarios_saas').text(data.qnt_usuarios_saas)

})

$("[name='tipo_licenca']").change(function(){
    viewSaas(this.value);
})

$('#relatorio_form').submit(function(e){
    e.preventDefault();
    
    let formData = new FormData(document.getElementById('relatorio_form'));

    formData.set('cliente', $('#idCliente').val());

    ajaxDataForm(`${url_base}/api/relatorio_horas_cliente`, formData,
    function (resp) {
        resp = isJson(resp);
        $('.defaultButton').show();
        $('.loadingButton').hide();
        $('#gerar_pdf').show();
        if (resp.status == 1) {
            resp = resp.data
            $('#tempo_periodo').text(resp.info.hhmm_trabalhado ?? 0)
            $('#total_servicos').text(resp.info.qnt_servicos ?? 0)
            $('#total_projetos').text(resp.info.totais_projetos ?? 0)
            $('#tempo_total_projetos').text(resp.info.horas_totais_projetos ?? 0)


            $('#gerar_pdf').show();
            tableRelatorio.criarTable(resp.horas);
        } else {
            alert(resp.message ? resp.message : resp.error);
        }
    }, function () {
        //Before send
        $('.defaultButton').hide();
        $('.loadingButton').show();
        $('#gerar_pdf').hide();
    })
})

$('[data-bs-target="#modalRelatórios"]').click(function(){
    modalDados.hide();
    $('#limpar_relatorio').click();
})

$('#limpar_relatorio').click(function(){
    $('#tempo_periodo, #total_servicos, #total_projetos, #tempo_total_projetos').text('');
    $('#gerar_pdf').hide();
    tbodyRelatorio.innerHTML = '';
})

$('#gerar_pdf').click(function(){
    let a = document.createElement('a');
    let dt_inicial = $('[name="dt_inicial"]').val();
    let dt_final = $('[name="dt_final"]').val();
    let user = $('#idCliente').val();

    let query = `cliente=${user}&dt_inicial=${dt_inicial}&dt_final=${dt_final}` 
    a.href = `/api/relatorio_horas_cliente_pdf?${query}`;
    a.target = '_blank';
    a.click();
})

function viewSaas(value){
    if(value === 'SAAS'){
        $('.saas-div').show();
    }else{
        $('.saas-div').hide();
    }
}

//Masks
$('input[name="cnpj"]').mask('00.000.000/0000-00', { reverse: false });
$('input[name="valor_contrato"]').mask('###.00', {
    reverse: true
});
$('input[name="cep"]').mask('00000-000');
$('input[name="telefone"]').mask('(00) 0000-0000');
$('.onlynumber').mask('#');


