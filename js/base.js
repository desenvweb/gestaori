var url_string = window.location.href;
var url = new URL(url_string);
var url_base = url.origin;

const notificationItens = document.getElementsByClassName('notification-itens')[0];
const modalExibirDialogo = new bootstrap.Modal(document.getElementById('modalExibirDialogo'), {})
const chatreadyonly = document.getElementsByClassName('chat--readonly')[0];

var height = $('.table-responsive').height();
const limite = Math.ceil(height / 33);

const Painel = {
    user: JSON.parse(getCookie('user')),
    trabalho: false,

    setHeader: function () {
        if(Painel.user.super_admin == 1){
            $('.boxpermiss').show();
        }else{
            $('.boxpermiss').hide();

        }
    
        document.getElementById('header#nome').innerHTML = this.user.nome;
        let tipo = this.user.tipo_usuario === '1' ? 'Administrador' : 'Colaborador';
        document.getElementById('header#tipo').innerHTML = tipo;
    },

    getData: function () {
        return this.user;
    },

    iniciar_ld: async function (dados) {
        let formData = new FormData();

        for (let i in dados) {
            formData.set(i, dados[i]);
        }

        ajaxDataForm(`${url_base}/api/iniciar_ld`, formData, function (resp) {
            resp = isJson(resp);
            alert(resp.message);
            if (resp.status == 1) {
                window.location.reload();
            }
        })
    },

    verhoras: async function () {
        ajaxRequestGet(`${url_base}/api/fim_expediente`, function (resp) {
            resp = isJson(resp);
            
            if(resp.status === 1){
                window.location.reload();
            }
        })
    },

    finalizar_ld: async function () {
        let trabalho = this.trabalho;

        let formData = new FormData();
        formData.set('id_empresa', trabalho.id_empresa);
        formData.set('dt_inicio', trabalho.dt_inicio);

        ajaxDataForm(`${url_base}/api/finalizar_ld`, formData, function (resp) {
            resp = isJson(resp);
            alert(resp.message);
            if(resp.status === 1){
                window.location.reload();
            }
        })
    },

    verifica_trabalho: function (data) {
        console.log(data);
        if (data.status == 1) {
            data = data.data;

            this.trabalho = {
                id: data.id,
                id_usuario: data.id_usuario,
                id_empresa: data.id_empresa,
                dt_inicio: data.dt_inicio,
                descricao: data.descricao,
                id_atividade : data.id_atividade,
                ticket: data.ticket
            }

            this.criar_linhas_trabalho(data);
            $('#trabalhando_agora').show();

            setInterval(function () {
                Painel.verifica_trabalho();
            }, 500000)

        } else {
            $('#trabalhando_agora').hide();
        }

    },

    criar_linhas_trabalho: function (data) {
        let dt_inicio = bancoToFrontHoras(data.dt_inicio);
        data.atividade = data.atividade ?? '';

        let linhas = '';
        linhas += '<div class="card">';
        linhas += `<div class="card-header space-between"><b>${data.nome_fantasia}</b>`;
        
        linhas += `<button type="button" class="btn-close text-reset" data-bs-dismiss="offcanvas" aria-label="Close"></button>`
        linhas += '</div>'
        linhas += '<div class="card-body">';
        linhas += `<h6 class="card-title"><b>Ticket:</b> ${data.id_ticket ?? ''}</h6>`;
        linhas += `<h6 class="card-title"><b>Título:</b> ${data.titulo ?? ''}</h6>`;
        linhas += `<h6 class="card-title"><b>Atividade:</b> ${data.atividade}</h6>`;
        linhas += `<h6 class="card-title"><b>Projeto:</b> ${data.projeto ?? ''}</h6>`;
        
        linhas += `<h6 class="card-title"><b>Início:</b> ${dt_inicio}</h6>`;
        linhas += '<br>';
        linhas += `<p class="card-text mb-3">${data.descricao}</p>`
        linhas += `<button onclick="Painel.finalizar_ld(${data.id})" class="btn--padrao btn-warning">Finalizar Trabalho</button>`
        linhas += '</div>'
        linhas += '</div>'
        $('#div_trabalho').html(linhas);
    },

    setHeaderAjax: function () {
        $.ajaxSetup({
            headers: { 'AUTH-TOKEN': this.user.AUTH_TOKEN }
        });
    },

    set_status: function (status) {
        let formData = new FormData();

        formData.set("id_usuario", this.user.id);
        formData.set("status", status);

        ajaxDataForm('/api/set_status', formData);
    },

    verifica_status: function (status) {
        if (status.situacao == 'presente' || status.situacao == 'Trabalhando') {
            $('.status').removeClass('bg--red').addClass('bg--green');
            $('#statusSelect').val('presente');
        } else {
            $('.status').removeClass('bg--green').addClass('bg--red');
            $('#statusSelect').val('ausente');
        }
    },

    get_infos: async function () {
        let data = await getDataGET(`/api/get_infos`);
        data = isJson(data);

        this.verifica_status(data.data.status);
        this.verifica_trabalho(data.data.trabalhando)
        this.criar_linhas_notificacao(data.data.notificacao);
        this.criar_linhas_pj(data.data.contratos_pj);
        this.criar_linhas_contratos(data.data.contratos_vencer);
        this.verhoras();
    },

    criar_linhas_notificacao: function (data) {
        //nova barra de notificação
        if (data.length > 0) {
            let nao_lidas = 0;
            let linhas = '';
            linhas += '<div class="card">';
            linhas += `<div class="card-header space-between"><b>Notificações</b>`;
            
            linhas += `<button type="button" class="btn-close text-reset" data-bs-dismiss="offcanvas" aria-label="Close"></button>`
            linhas += '</div>'
    

            for (let i in data) {
                if (data[i].dt_leiturausuario == null && data[i].id_usuario == Painel.user.id) nao_lidas++;

                let dt_cadastro = bancoToFrontHoras(data[i].dt_cadastro);

                data[i].descricao.split(" ", 3);
                linhas += '<div class="card">';
                linhas += '<div class="card-body">';
                linhas += `<h6 class="card-title">${data[i].descricao}</h6>`;

                linhas += `<span class="text-muted">${dt_cadastro}</span>`;
                linhas += '</div>';
                let pos = data[i].descricao.indexOf('transferido para sua responsabilidade');
                if (pos >=0){
                    linhas += `<div class="card-footer">`;
                    linhas += `<div class="row">`;
                    linhas += `<div class="col col-md-7">`;
                    linhas += `<select name="id_responsavel" id="id_responsavel" class="form-control colaborador--select form-select-sm">`;
                    linhas += `<option value="" selected disabled>Selecione um Responsável</option>`;
                    linhas += `</select>`;
                    linhas += `</div>`;
                    linhas += `<div class="col col-md-5">`;
                    linhas += `<button class="btn btn-primary btn-sm" onclick="transferir_ticket(${data[i].ticket}, ${data[i].id})">Transferir Ticket</button>`;
                    linhas += `</div>`;
                    linhas += `</div>`;
                    linhas += '</div>';
                }else{
                    pos = data[i].descricao.indexOf('Novo comentário cadastrado');
                    if (pos >=0){
                        linhas += `<div class="card-footer">`;
                        linhas += `<button class="btn btn-primary btn-sm" onclick="dialogo_ticket(${data[i].ticket}, ${data[i].id_usuario})">Dialogo Ticket</button>`;
                        linhas += '</div>';
                    }else{
                        linhas += `<div class="card-footer">`;
                        linhas += `<button class="btn btn-primary btn-sm" onclick="marcar_lido(${data[i].id})">Marcar como lido</button>`;
                        linhas += '</div>';
                    }
    
                    }
                
                linhas += '</div>';

            }
            
            if (nao_lidas > 0) {
                //$('#trabalhando_agora').show();
                $('.notification-text').text(nao_lidas).show();
            }
            $('#div_notificacao').html(linhas);
        }

        
    },

    criar_linhas_pj: function (data) {
        //nova barra de notificação
        if (data.length > 0) {
            let contratos = data.length;
            let linhas = '';
            linhas += '<div class="card">';
            linhas += `<div class="card-header space-between"><b>Contratos PJ a vencer nos próximos 15 dias</b>`;
            
            linhas += `<button type="button" class="btn-close text-reset" data-bs-dismiss="offcanvas" aria-label="Close"></button>`
            linhas += '</div>'
    

            for (let i in data) {

                let dt_vcto = bancoToFrontDateUTC(data[i].vencimento_contrato);
                let dt_inicio = bancoToFrontDateUTC(data[i].dt_inicio_trabalho);

                linhas += '<div class="card">';
                linhas += '<div class="card-body">';
                linhas += `<h6 class="card-title">${data[i].nome} - ${data[i].funcao}</h6>`;
                linhas += `<span class="text-muted">Data Vencimento ${dt_vcto}</span>`;
                linhas += '<br>';
                linhas += `<span class="text-muted">Início do contrato ${dt_inicio}</span>`;
                linhas += '</div>';
                linhas += '</div>';
            }
            
            $('.notification_pj-text').text(contratos).show();
            $('#div_notificacao_pj').html(linhas);
        }
    },

    criar_linhas_contratos: function (data) {
        //nova barra de notificação
        if (data.length > 0) {
            let contratos = data.length;
            let linhas = '';
            linhas += '<div class="card">';
            linhas += `<div class="card-header space-between"><b>Contratos a vencer no próximo mês</b>`;
            
            linhas += `<button type="button" class="btn-close text-reset" data-bs-dismiss="offcanvas" aria-label="Close"></button>`
            linhas += '</div>'
    

            for (let i in data) {

                let dt_vcto = bancoToFrontDateUTC(data[i].dt_proximoReajuste);
                let dt_inicio = bancoToFrontDateUTC(data[i].dt_vigenciainicial);
                let dt_fim = bancoToFrontDateUTC(data[i].dt_vigenciafinal);
                let valor_total = parseBRL(data[i].valor_total);

                linhas += '<div class="card">';
                linhas += '<div class="card-body">';
                linhas += `<h6 class="card-title">${data[i].razao_social}</h6>`;
                linhas += `<span class="text-muted">Data do Reajuste ${dt_vcto}</span>`;
                linhas += '<br>';
                linhas += `<span class="text-muted">Vigência ${dt_inicio} a ${dt_fim}</span>`;
                linhas += '<br>';
                linhas += `<span class="text-muted">Contrato ${data[i].tipo_contrato} Pagamento ${data[i].periodicidade_pagamento}</span>`;
                linhas += '<br>';
                linhas += `<span class="text-muted">Valor total ${valor_total} Indice ${data[i].indice} </span>`;

                linhas += '</div>';
                linhas += '</div>';
            }
            
            $('.notification_contratos-text').text(contratos).show();
            $('#div_notificacao_contratos').html(linhas);
        }
    }
    
}
$(document).ready(async function () {
    //$('.boxpermiss').hide();
    
    let usuarios = await getDataGET(`${url_base}/api/get_usuarios?limite=1000`);
    usuarios = isJson(usuarios);
    addOptions(usuarios.data, 'nome', 'id', 'colaborador--select');


})

function transferir_ticket(id_ticket, id_notificacao){
    let id_responsavel = document.getElementById('id_responsavel').value;
    if (parseInt(id_responsavel)>0){
        let formData = new FormData();
        formData.append('id_ticket', id_ticket);
        formData.append('id_responsavel', id_responsavel);
        formData.append('id_notificacao', id_notificacao);

        ajaxDataForm(`${url_base}/api/editar_ticket`, formData,
            function (resp) {
                resp = isJson(resp);
                if (resp.status == 1) {
                    alert('Salvo com sucesso.')
                    window.location.reload();
                } else {
                    alert(resp.error ?? resp.message);
                }
            }, function () {
                //Before send
                Painel.get_infos();
        })

    }else{
        alert('Não há responsável selecionado para a transferência do ticket !');
    }
}

var modal = {

    open: false,
    modalOpen: false,

    abrirModal: function (modal) {
        this.fecharModalAberto();

        this.open = true;
        this.modalOpen = modal;

        modal.show();
    },

    fecharModalAberto() {

        if (this.open === true) {
            this.modalOpen.hide();
        }
    }

}


Painel.setHeaderAjax();
Painel.setHeader();
Painel.get_infos();
setInterval(() => {
    Painel.get_infos();
}, 120000)

$(".item--list").on({
    mouseenter: function () {
        $(this).children("svg")[0].classList.add('svg--hover')
    },
    mouseleave: function () {
        $(this).children("svg")[0].classList.remove('svg--hover')
    }
});


//Selecionar status
$('.status').click(function () {
    $('#statusSelect').show();
    $('#statusSelect').focus();
})

$('#statusSelect').focusout(function () {
    $(this).hide();
})

$('#statusSelect').change(function () {

    Painel.set_status(this.value);

    if (this.value === 'ausente') {
        $('.status').removeClass('bg--green').addClass('bg--red');
    } else {
        $('.status').removeClass('bg--red').addClass('bg--green');
    }

    $(this).hide();
})

$('.loadingButton').hide();

/*$(document).on("click", function (e) {
    if (notificationItens.style.display == 'block') {
        var fora = !notificationItens.contains(e.target);
        if (fora) {
            $(notificationItens).hide();
        }
    }
});*/

$('.notification-div').click(function () {
    setTimeout(() => { $('.notification-itens').show() }, 50);
    let data = getDataGET('/api/set_visualizado');
    $('.notification-text').hide();
})

$('.notification-box').click(function () {
    let data = getDataGET('/api/set_visualizado');
    $('.notification-text').hide();

})

async function dialogo_ticket(id_ticket, id_usuario){
    $('#idTicket_coment').val(id_ticket);
    $('#id_usuario').val(id_usuario);

    //modalDados.hide();

    let data = await getDataGET(`/api/get_comentarios?id_ticket=${id_ticket}`);
    data = isJson(data);

    $('.formpermiss :input').prop('disabled', false);
    $('.show--ticket').text('Ticket ' + id_ticket);

    if (data.status == 1) {
        let linhas = '';

        data = data.data;

        for (let i = 0; i < data.length; i++) {
            if (data[i].texto) {
                let data_cadastro = bancoToFrontHorasUTC(data[i].dt_cadastro);
                let anexos =  data[i].anexos;

                linhas += '<div class="dialog--chat">';
                linhas += `<p class="chat--header">${data_cadastro} - ${data[i].nome}</p>`
                linhas += `<p class="chat--text">${data[i].texto}</p>`
                linhas += '<p class="chat--anexos">';
                    for(let a = 0; a < anexos.length; a++){
                       linhas += `<a  class="a--normal" target="_blank" href="${anexos[a].link}">`; 
                       linhas += anexos[a].nome + '.' + anexos[a].extensao
                       linhas += '</a>';
                    }
                linhas += '</p>'
                linhas += '</div>';
            }
        }

        chatreadyonly.innerHTML = linhas;
    } else {
        chatreadyonly.innerHTML = '';
    }
    modalExibirDialogo.show();
}


async function marcar_lido(id){

    let formData = new FormData();
    formData.set('id', id);

    ajaxDataForm('/api/marcar_lido', formData, function (resp) {
        window.location.reload();
    })
   
}

$('#form_dialog').submit(function (e) {
    e.preventDefault();

    let formData = new FormData(this);
    let ticket = $('#idTicket_coment').val();

    //formData.set('id_usuario', Painel.user.id);
    formData.set('id_ticket', ticket);
    formData.set('cliente', 'N');
    formData.delete('idTicket_coment');

    ajaxDataForm('/api/add_comentario', formData, function (resp) {
        resp = isJson(resp);

        if (resp.status == 1) {
            window.location.reload();
        }

        alert(resp.message);
    })
})

//Masks
//$('.onlynumber').mask('#');