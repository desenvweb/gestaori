var url_string = window.location.href;
var url = new URL(url_string);
var url_base = url.origin;


$('#form-login').submit(function (e) {
	e.preventDefault();

	ajaxDataForm(`${url_base}/api/login`, new FormData(this), function (response) {
		if (response.status == 1) {
			let tomorrow = new Date();
			tomorrow.setDate(new Date().getDate()+1)
			tomorrow = tomorrow.toGMTString()

			let data = JSON.stringify(response.data);
			document.cookie = "user=" + data + ";expires=" + tomorrow;

			window.location.href = `${url_base}/paginas/clientes/contratos.php`;
		} else {
			alert('E-mail ou senha inválidos.');
		}
	})

})
