﻿
const formInserir = document.getElementById('form#inserir');
const formEditar = document.getElementById('form#editar');
const tbody = document.getElementById('tbody');
const tbodySearch = document.getElementById('tbodysearch');
const modalDados = new bootstrap.Modal(document.getElementById('modalExibirDados'), {})
//const chatreadyonly = document.getElementsByClassName('chat--readonly')[0];
const gravidade = document.getElementById('gravidade');
const urgencia = document.getElementById('urgencia');
const tendencia = document.getElementById('tendencia');
const gut = document.getElementById('gut');
const lgut = document.getElementById('l_gut');
const ngravidade = document.getElementById('gut_gravidade');
const nurgencia = document.getElementById('gut_urgencia');
const ntendencia = document.getElementById('gut_tendencia');
const ngut = document.getElementById('gut_gut');
const nlgut = document.getElementById('lgut_gut');
const groupbyCliente = document.getElementById('CbGroupbyCliente');
const groupbyGut = document.getElementById('CbGroupbyGut');


var table = {
    data: false,
    data_search: false,
    search: false,
    pagina: 0,
    loadingScroll: true,

    criarTable: function (data) {
        this.data = data;
        let linhas = this.gerarLinhas(this.data);
        tbody.innerHTML = linhas;
    },

    gerarLinhas: function (data) {
        var linhas = '';

        for (let i in data) {
            let dt_prevista = 'Não Informada';
            if(data[i].dt_prevista){
                dt_prevista = bancoToFrontDateUTC(data[i].dt_prevista);
            }
            let present_date = new Date();
            let end_date = new Date(data[i].dt_prevista);
            let Difference_In_Time = present_date.getTime() - end_date.getTime();

            if ((Difference_In_Time > 1) && (data[i].status != 'Concluído')){
                linhas += `<tr style="color: red" data-ref="${data[i].id}">`;
            }else{
                linhas += `<tr data-ref="${data[i].id}">`;
            }

            linhas += `<td>${data[i].id}</td>`
            linhas += `<td>${data[i].nome_fantasia ?? 'Não definido'}`
            linhas += ` (${data[i].cnpj ?? ''})</td>`
            linhas += `<td>${data[i].titulo}</td>`
            linhas += `<td>${data[i].responsavel ?? 'Não definido'}</td>`
            linhas += `<td>${data[i].gut ?? '---'}</td>`
            linhas += `<td>${dt_prevista}</td>`
            linhas += `<td>${data[i].status ?? 'Informar Status'}</td>`

            linhas += '</tr>';
        }

        return linhas;
    },

    adicionarTable: function (data) {
        for (let i in data) {
            this.data.push(data[i]);

            let tbodyFake = document.createElement('tbody');
            let linhas = '';

            let dt_prevista = 'Não Informada';
            if(data[i].dt_prevista){
                dt_prevista = bancoToFrontDateUTC(data[i].dt_prevista);
            }
            let present_date = new Date();
            let end_date = new Date(data[i].dt_prevista);
            let Difference_In_Time = present_date.getTime() - end_date.getTime();

            if ((Difference_In_Time > 0) && (data[i].status != 'Concluído')){
                linhas += `<tr style="color: red" data-ref="${data[i].id}">`;
            }else{
                linhas += `<tr data-ref="${data[i].id}">`;
            }

            linhas += `<td>${data[i].id}</td>`
            linhas += `<td>${data[i].nome_fantasia ?? 'Não definido'}`
            linhas += ` (${data[i].cnpj ?? ''})</td>`
            
            linhas += `<td>${data[i].titulo}</td>`
            linhas += `<td>${data[i].responsavel ?? 'Não definido'}</td>`
            linhas += `<td>${data[i].gut ?? '---'}</td>`
            linhas += `<td>${dt_prevista}</td>`
            linhas += `<td>${data[i].status ?? 'Informar Status'}</td>`

            linhas += '</tr>';

            tbodyFake.innerHTML = linhas;
            tbody.appendChild(tbodyFake.firstChild);
        }
    },

    pesquisa: async function (search) {
        let data = await getDataGET(`${url_base}/api/get_tickets?condicao=${search}`);

        data = isJson(data);
        this.data_search = data.data;

        if (search != '') {
            let linhas = this.gerarLinhas(data.data);
            tbodySearch.innerHTML = linhas;
            $(tbody).hide();
            $(tbodySearch).show();
            this.search = true;
        } else {
            $(tbody).show();
            $(tbodySearch).hide();
            this.search = false;
        }
    }
}


$(document).ready(async function () {
    //Verifica se é adm
    if (Painel.user.tipo_usuario == 2) {
        $('.formpermiss :input').prop('disabled', true);
        $('.hidepermiss').hide();
        groupbyCliente.checked = true;
        groupbyGut.checked = false;
    }else{
        groupbyCliente.checked = false;
        groupbyGut.checked = true;
    }

    let usuarios = await getDataGET(`${url_base}/api/get_usuarios?limite=1000`);
    usuarios = isJson(usuarios);
    addOptions(usuarios.data, 'nome', 'id', 'colaborador--select');

    let setor = await getDataGET(`${url_base}/api/get_setores?limite=1000`);
    setor = isJson(setor);
    addOptions(setor.data, 'nome', 'id', 'setor--select');

    let categorias = await getDataGET(`/api/get_categorias`);
    categorias = isJson(categorias);
    addOptions(categorias.data, 'descricao', 'id', 'categoria--select');

    let statusTicket = await getDataGET(`/api/get_status_ticket`);
    statusTicket = isJson(statusTicket);
    addOptions(statusTicket.data, 'descricao', 'id', 'statusTicket--select');

    let gutUrgencia = await getDataGET(`/api/get_gut_urgencia`);
    gutUrgencia = isJson(gutUrgencia);
    addOptions(gutUrgencia.data, 'descricao', 'id', 'gutUrgencia--select');

    let gutTendencia = await getDataGET(`/api/get_gut_tendencia`);
    gutTendencia = isJson(gutTendencia);
    addOptions(gutTendencia.data, 'descricao', 'id', 'gutTendencia--select');

    let gutGravidade = await getDataGET(`/api/get_gut_gravidade`);
    gutGravidade = isJson(gutGravidade);
    addOptions(gutGravidade.data, 'descricao', 'id', 'gutGravidade--select');

    let query = '';

    if (groupbyCliente.checked && CbGroupbyGut.checked){
        query = `id_responsavel=${Painel.user.id}&status_nao=3&groupby=gutnomefantasia`;
    }else if (groupbyCliente.checked){
        query = `id_responsavel=${Painel.user.id}&status_nao=3&groupby=nomefantasia`;
    }else if (CbGroupbyGut.checked){
        query = `id_responsavel=${Painel.user.id}&status_nao=3&groupby=gut`;
    }else{
        query = `id_responsavel=${Painel.user.id}&status_nao=3`;
    }

    
    let data = await getDataGET(`${url_base}/api/get_tickets?limite=${limite}&${query}`);
    data = isJson(data);
    table.criarTable(data.data);

    $('.spinner-border').hide();

    $('#mySelect2').select2({
        dropdownParent: $('#modalInserirTicket'),
        theme: 'bootstrap-5',
        placeholder: "Pesquise pelo e-mail",
        ajax: {
            url: '/api/get_usuclientes',
            type: 'get',
            dataType: 'json',
            delay: 250,
            data: function (params) {

                var query = {
                    condicao: params.term,
                    limite: 20
                }
                return query
            },
            processResults: function (response) {

                return {
                    results: $.map(response.data, function (item) {
                        return {
                            id: item.id,
                            text: `${item.nome} (${item.email1})`
                        }
                    })
                }
            }
        },
        cache: true
    })
    $('#mySelect3').select2({
        dropdownParent: $('#modalInserirTicket'),
        theme: 'bootstrap-5',
        placeholder: "Pesquise pelo Nome Fantasia",
        ajax: {
            url: '/api/get_empresas',
            //url: '/api/get_empresas_com_horas',
            type: 'get',
            dataType: 'json',
            delay: 150,
            data: function (params) {

                var query = {
                    condicao: params.term,
                    limite: 50
                }
                return query
            },
            processResults: function (response) {

                return {
                    results: $.map(response.data, function (item) {
                        return {
                            id: item.id,
                            text: `${item.nome_fantasia} (${item.cnpj})`
                        }
                    })
                }
            }
        },
        cache: true
    })
})

formInserir.addEventListener('submit', async function (e) {
    e.preventDefault();

    let formData = new FormData(formInserir);
    ajaxDataForm(`${url_base}/api/cadastrar_ticket`, formData,
        function (resp) {
            resp = isJson(resp);
            $('.defaultButton').show();
            $('.loadingButton').hide();
            if (resp.status == 1) {
                alert('Salvo com sucesso.')
                window.location.reload();
            } else {
                alert(resp.error ?? resp.message);
            }
        }, function () {
            //Before send
            $('.defaultButton').hide();
            $('.loadingButton').show();
        })
    $('.defaultButton').show();
    $('.loadingButton').hide();
})

formEditar.addEventListener('submit', async function (e) {
    e.preventDefault();
    let formData = new FormData(formEditar);

    ajaxDataForm(`${url_base}/api/editar_ticket`, formData,
        function (resp) {
            resp = isJson(resp);
            $('.defaultButton').show();
            $('.loadingButton').hide();

            if (resp.status == 1) {
                alert('Salvo com sucesso.')
                window.location.reload();
            } else {
                alert(resp.error ?? resp.message);
            }
        }, function () {
            //Before send
            $('.defaultButton').hide();
            $('.loadingButton').show();
        })

    $('.defaultButton').show();
    $('.loadingButton').hide();
})

$(document).on('click', 'tr', function (e) {
    let id = $(this).data('ref');
    
    if (!id) return

    if (!table.search) {
        var data = table.data.filter(el => el.id == id);
    } else {
        var data = table.data_search.filter(el => el.id == id);
    }

    $('#idTicket').val(id);
    $('#idTicket_coment').val(id);
    $('#id_usuario').val(Painel.user.id);

    $('.show--ticket').text('Ticket ' + id);

    for (let i in data[0]) {
        $('#' + i).val(data[0][i]);
    }

    if (data[0].id_responsavel == Painel.user.id && Painel.user.tipo_usuario == 2) {
        $('.formpermiss :input').prop('disabled', false);
    }
    else if (data[0].id_responsavel != Painel.user.id && Painel.user.tipo_usuario == 2) {
        $('.formpermiss :input').prop('disabled', true);
    }

    calcular_gut();
    modalDados.show();
})

$('.table-responsive').on('scroll', function () {
    if (table.loadingScroll) {
        let value = parseInt(this.scrollTop / (this.scrollHeight - this.clientHeight));
        if (value == 1) {
            table.pagina++;

            let condicao = $('#searchInput').val();
            let query = '';

            let empresa = $('#empresa').val()  ?? '';
            let usuario_cli = $('#usuario_cli').val() ?? '';
            let responsavel = $('#responsavel').val() ?? '';

            if (empresa == '' && usuario_cli == '' && responsavel == '' )
            {
                query = table.search ? '' : `&id_responsavel=${Painel.user.id}&status_nao=3`;
            }else{
                const radioButtons = document.querySelectorAll('input[name="tkt"]');
                let status_tkt;
                for (const radioButton of radioButtons) {
                    if (radioButton.checked) {
                        status_tkt = radioButton.value;
                        break;
                    }
                }
                //console.log(status_tkt);
            
                query  = table.search ? '' :`&status_tkt=${status_tkt}&empresa=${empresa}&usuario_cli=${usuario_cli}&responsavel=${responsavel}`;
            }
            
            //console.log(query);

            let resp = '';
            (async function () {
                if (groupbyCliente.checked && CbGroupbyGut.checked){
                    resp = await getDataGET(`${url_base}/api/get_tickets?limite=${limite}&pagina=${table.pagina}&condicao=${condicao}${query}&groupby=gutnomefantasia`);
                }else if (groupbyCliente.checked){
                    resp = await getDataGET(`${url_base}/api/get_tickets?limite=${limite}&pagina=${table.pagina}&condicao=${condicao}${query}&groupby=nomefantasia`);
                }else if (CbGroupbyGut.checked){
                    resp = await getDataGET(`${url_base}/api/get_tickets?limite=${limite}&pagina=${table.pagina}&condicao=${condicao}${query}&groupby=gut`);
                }else{
                    resp = await getDataGET(`${url_base}/api/get_tickets?limite=${limite}&pagina=${table.pagina}&condicao=${condicao}${query}`);
                }
               
                resp = isJson(resp);
                if (resp.status == 1) {
                    table.adicionarTable(resp.data);
                }
            })()
        };
    }
})


$('#searchInput').keyup(function () {
    table.pesquisa($(this).val());
})



$('#anexo--icon').click(function () {
    $('#inputFileComent').click();
})

$('#filtros').submit(async function(e){
    e.preventDefault();

    let empresa = $('#empresa').val();
    let usuario_cli = $('#usuario_cli').val();
    let responsavel = $('#responsavel').val();

    const radioButtons = document.querySelectorAll('input[name="tkt"]');
    let status_tkt;
    for (const radioButton of radioButtons) {
        if (radioButton.checked) {
            status_tkt = radioButton.value;
            break;
        }
    }
    //console.log(status_tkt);
    let query  = '';
    if (groupbyCliente.checked && groupbyGut.checked){
        query  = `status_tkt=${status_tkt}&empresa=${empresa}&usuario_cli=${usuario_cli}&responsavel=${responsavel}&groupby=gutnomefantasia`;
    }else if (groupbyCliente.checked){
        query  = `status_tkt=${status_tkt}&empresa=${empresa}&usuario_cli=${usuario_cli}&responsavel=${responsavel}&groupby=nomefantasia`;
    }else if (groupbyGut.checked){
        query  = `status_tkt=${status_tkt}&empresa=${empresa}&usuario_cli=${usuario_cli}&responsavel=${responsavel}&groupby=gut`;
    }else{
        query  = `status_tkt=${status_tkt}&empresa=${empresa}&usuario_cli=${usuario_cli}&responsavel=${responsavel}`;
    }


    table.pagina = 0;

    let data = await getDataGET(`${url_base}/api/get_tickets?${query}&limite=${limite}`);
    data = isJson(data);
    table.criarTable(data.data);
})

$('#btn--limparFiltros').click(async function(){
    let data = await getDataGET(`${url_base}/api/get_tickets?limite=${limite}`);
    data = isJson(data);
    table.criarTable(data.data);
})

$('#gerar_pdf').click(function(){

    let empresa = $('#empresa').val();
    let usuario_cli = $('#usuario_cli').val();
    let responsavel = $('#responsavel').val();

    const radioButtons = document.querySelectorAll('input[name="tkt"]');
    let status_tkt;
    for (const radioButton of radioButtons) {
        if (radioButton.checked) {
            status_tkt = radioButton.value;
            break;
        }
    }
    //console.log(status_tkt);

    let query  = `status_tkt=${status_tkt}&empresa=${empresa}&usuario_cli=${usuario_cli}&responsavel=${responsavel}`;


    let a = document.createElement('a');
    let id_empresa = 1;
    a.href = `/api/relatorio_tickets_pdf?${query}`;
    a.target = '_blank';
    a.click();
})

$('[data-bs-target="#modalExibirDialogo"]').click(async function () {
    modalDados.hide();
    let ticket = $('#idTicket').val();

    let data = await getDataGET(`/api/get_comentarios?id_ticket=${ticket}`);
    data = isJson(data);

    if (data.status == 1) {
        let linhas = '';

        data = data.data;

        for (let i = 0; i < data.length; i++) {
            if (data[i].texto) {
                let data_cadastro = bancoToFrontHorasUTC(data[i].dt_cadastro);
                let anexos =  data[i].anexos;

                linhas += '<div class="dialog--chat">';
                linhas += `<p class="chat--header">${data_cadastro} - ${data[i].nome}</p>`
                linhas += `<p class="chat--text">${data[i].texto}</p>`
                linhas += '<p class="chat--anexos">';
                    for(let a = 0; a < anexos.length; a++){
                       linhas += `<a  class="a--normal" target="_blank" href="${anexos[a].link}">`; 
                       linhas += anexos[a].nome + '.' + anexos[a].extensao
                       linhas += '</a>';
                    }
                linhas += '</p>'
                linhas += '</div>';
            }
        }

        chatreadyonly.innerHTML = linhas;
    } else {
        chatreadyonly.innerHTML = '';
    }
})

gravidade.addEventListener('change', function() {
    calcular_gut();
});

urgencia.addEventListener('change', function() {
    calcular_gut();
});

tendencia.addEventListener('change', function() {
    calcular_gut();
});

function calcular_gut(){
    gut.value = gravidade.value * urgencia.value * tendencia.value;
    lgut.innerHTML = 'Matriz GUT (para priorizar problemas a serem tratados) - G x U x T = '+ gut.value;
}

ngravidade.addEventListener('change', function() {
    calcularn_gut();
});

nurgencia.addEventListener('change', function() {
    calcularn_gut();
});

ntendencia.addEventListener('change', function() {
    calcularn_gut();
});

groupbyCliente.addEventListener('change', GetTicket);

groupbyGut.addEventListener('change', GetTicket);

async function GetTicket()  {
    $('.spinner-border').show();
    let query = '';

    if (groupbyCliente.checked && groupbyGut.checked){
        query = `id_responsavel=${Painel.user.id}&status_nao=3&groupby=gutnomefantasia`;
    } else if (groupbyCliente.checked){
        query = `id_responsavel=${Painel.user.id}&status_nao=3&groupby=nomefantasia`;
    } else if (groupbyGut.checked){
        query = `id_responsavel=${Painel.user.id}&status_nao=3&groupby=gut`;
    }else{
        query = `id_responsavel=${Painel.user.id}&status_nao=3`
    }
    
    let data = await getDataGET(`${url_base}/api/get_tickets?limite=${limite}&${query}`);
    data = isJson(data);
    table.criarTable(data.data);

    $('.spinner-border').hide();

}

function calcularn_gut(){
    ngut.value = ngravidade.value * nurgencia.value * ntendencia.value;
    nlgut.innerHTML = 'Matriz GUT (para priorizar problemas a serem tratados) - G x U x T = '+ ngut.value;
}