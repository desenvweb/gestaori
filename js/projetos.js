
const formInserir = document.getElementById('form#inserir');
const formEditar = document.getElementById('form#editar');
const tbody = document.getElementById('tbody');
const tbodySearch = document.getElementById('tbodysearch');
const modalDados = new bootstrap.Modal(document.getElementById('modalExibirDados'), {})

var table = {
    data: false,
    data_search: false,
    search: false,
    pagina: 0,
    loadingScroll: true,

    criarTable: function (data) {
        this.data = data;
        let linhas = this.gerarLinhas(this.data);
        tbody.innerHTML = linhas;
    },

    gerarLinhas: function (data) {
        var linhas = '';

        for (let i in data) {

            let prev_termino = data[i].prev_termino ? bancoToFrontDateUTC(data[i].prev_termino) : 'Sem previsão';

            let present_date = new Date();
            let end_date = new Date(data[i].prev_termino);
            let Difference_In_Time = present_date.getTime() - end_date.getTime();

            if ((Difference_In_Time > 0) && (data[i].status != 'CONCLUIDO')){
                linhas += `<tr style="color: red" data-ref="${data[i].id}">`;
            }else{
                linhas += `<tr data-ref="${data[i].id}">`;
            }
           

            linhas += `<td>${data[i].nome}</td>`
            linhas += `<td>${data[i].responsavel}</td>`
            linhas += `<td>${prev_termino}</td>`
            linhas += `<td>${data[i].status}</td>`

            linhas += '</tr>';
        }

        return linhas;
    },

    adicionarTable: function (data) {
        for (let i in data) {
            this.data.push(data[i]);

            let tbodyFake = document.createElement('tbody');
            let linhas = '';

            let dataFim = bancoToFrontDateUTC(data[i].vigencia_final);

            let present_date = new Date();
            let end_date = new Date(data[i].prev_termino);
            let Difference_In_Time = present_date.getTime() - end_date.getTime();

            if ((Difference_In_Time > 0) && (data[i].status != 'CONCLUIDO')){
                linhas += `<tr style="color: red" data-ref="${data[i].id}">`;
            }else{
                linhas += `<tr data-ref="${data[i].id}">`;
            }
            linhas += `<td>${data[i].nome}</td>`
            linhas += `<td>${data[i].responsavel}</td>`
            linhas += `<td>${bancoToFrontDateUTC(data[i].prev_termino)}</td>`
            linhas += `<td>${data[i].status}</td>`

            linhas += '</tr>';

            tbodyFake.innerHTML = linhas;
            tbody.appendChild(tbodyFake.firstChild);
        }
    },

    pesquisa: async function (search) {
        let data = await getDataGET(`${url_base}/api/get_projetos?condicao=${search}`);

        data = isJson(data);
        this.data_search = data.data;

        if (search != '') {
            let linhas = this.gerarLinhas(data.data);
            tbodySearch.innerHTML = linhas;
            $(tbody).hide();
            $(tbodySearch).show();
            this.search = true;
        } else {
            $(tbody).show();
            $(tbodySearch).hide();
            this.search = false;
        }
    }
}


$(document).ready(async function () {
    //Verifica se é adm ou o user é do setor Comercial
    /*if (Painel.user.tipo_usuario == 1 || Painel.user.id_setor == 3) {
        $('[data-bs-target="#modalInserirProjeto"]').show();
        $('.defaultButton').show();
    } 
    
    if(Painel.user.tipo_usuario != 1){
        $('.formpermiss :input').prop('disabled', true);
        $('.hidepermiss').hide();
    }*/

    //todos com acesso a cria projto
    $('[data-bs-target="#modalInserirProjeto"]').show();
    $('.defaultButton').show();
    
    let usuarios = await getDataGET(`${url_base}/api/get_usuarios?limite=1000`);
    usuarios = isJson(usuarios);
    addOptions(usuarios.data, 'nome', 'id', 'colaborador--select');

    let clientes = await getDataGET(`${url_base}/api/get_empresas`);//?limite=1000`);
    clientes = isJson(clientes);
    addOptions(clientes.data, 'nome_fantasia', 'id', 'cliente--select');

    let status = await getDataGET(`${url_base}/api/get_status_projeto?limite=1000`);
    status = isJson(status);
    addOptions(status.data, 'nome', 'id', 'status--select');

    let data = await getDataGET(`${url_base}/api/get_projetos?limite=${limite}`);
    data = isJson(data);
    table.criarTable(data.data);

    $('#id_empresa').select2({
        dropdownParent: $('#modalExibirDados'),
        theme: 'bootstrap-5',
        placeholder: "Pesquise pelo Nome Fantasia",
        ajax: {
            url: '/api/get_empresas',
            //url: '/api/get_empresas_com_horas',
            type: 'get',
            dataType: 'json',
            delay: 150,
            data: function (params) {

                var query = {
                    condicao: params.term,
                    limite: 50
                }
                return query
            },
            processResults: function (response) {

                return {
                    results: $.map(response.data, function (item) {
                        return {
                            id: item.id,
                            text: `${item.nome_fantasia} (${item.cnpj})`
                        }
                    })
                }
            }
        },
        cache: true
    })

    $('#mySelect3').select2({
        dropdownParent: $('#modalInserirProjeto'),
        theme: 'bootstrap-5',
        placeholder: "Pesquise pelo Nome Fantasia",
        ajax: {
            url: '/api/get_empresas',
            //url: '/api/get_empresas_com_horas',
            type: 'get',
            dataType: 'json',
            delay: 150,
            data: function (params) {

                var query = {
                    condicao: params.term,
                    limite: 50
                }
                return query
            },
            processResults: function (response) {

                return {
                    results: $.map(response.data, function (item) {
                        return {
                            id: item.id,
                            text: `${item.nome_fantasia} (${item.cnpj})`
                        }
                    })
                }
            }
        },
        cache: true
    })

    $('.spinner-border').hide();
})

formInserir.addEventListener('submit', async function (e) {
    e.preventDefault();

    let formData = new FormData(formInserir);
    ajaxDataForm(`${url_base}/api/cadastrar_projeto`, formData,
        function (resp) {
            resp = isJson(resp);
            $('.defaultButton').show();
            $('.loadingButton').hide();
            if (resp.status == 1) {
                alert('Salvo com sucesso.')
                window.location.reload();
            } else {
                alert(resp.error ?? resp.message);
            }
        }, function () {
            //Before send
            $('.defaultButton').hide();
            $('.loadingButton').show();
        })
    $('.defaultButton').show();
    $('.loadingButton').hide();
})

formEditar.addEventListener('submit', async function (e) {
    e.preventDefault();
    let formData = new FormData(formEditar);

    ajaxDataForm(`${url_base}/api/editar_projeto`, formData,
        function (resp) {
            resp = isJson(resp);
            $('.defaultButton').show();
            $('.loadingButton').hide();

            if (resp.status == 1) {
                alert('Salvo com sucesso.')
                window.location.reload();
            } else {
                alert(resp.error ?? resp.message);
            }
        }, function () {
            //Before send
            $('.defaultButton').hide();
            $('.loadingButton').show();
        })

    $('.defaultButton').show();
    $('.loadingButton').hide();
})

$(document).on('click', 'tr', async function (e) {
    let id = $(this).data('ref');
    if (!id) return

    if (!table.search) {
        var data = table.data.filter(el => el.id == id);
    } else {
        var data = table.data_search.filter(el => el.id == id);
    }

    $('#idProjeto').val(id);


    for (let i in data[0]) {
        $('#' + i).val(data[0][i]);
    }

    if(data[0]['desconta_horas'] == 'S'){
        $('#desconta_horas').prop("checked", true);
    }else{
        $('#desconta_horas').prop("checked", false);
    }

    if(data[0]['id_responsavel'] == Painel.user.id || Painel.user.tipo_usuario == 1){
        $('.formpermiss :input').prop('disabled', false);
    }else{
        $('.formpermiss :input').prop('disabled', true);
    }

    let dados = await getDataGET(`/api/get_empresa?id_empresa=${data[0]['id_empresa']}`);
    nomeFantasia = isJson(dados);
    var newOption = new Option(nomeFantasia.data.nome_fantasia, data[0]['id_empresa'], false, false);
    $('#id_empresa').append(newOption).trigger('change');

    modalDados.show();
})

$('.table-responsive').on('scroll', function () {
    if (table.loadingScroll) {
        let value = parseInt(this.scrollTop / (this.scrollHeight - this.clientHeight));
        if (value == 1) {
            table.pagina++;

            let condicao = $('#searchInput').val();
            (async function () {
                let resp = await getDataGET(`${url_base}/api/get_projetos?limite=${limite}&pagina=${table.pagina}&condicao=${condicao}`);
                resp = isJson(resp);
                if (resp.status == 1) {
                    table.adicionarTable(resp.data);
                }
            })()
        };
    }
})

$('#searchInput').keyup(function () {
    table.pesquisa($(this).val());
})

$('[data-bs-target="#modalInserirProjeto"]').click(function(){
    $('#inputComercial_insert').val(Painel.user.id);
})

