const formInserir = document.getElementById('form#inserir');
const formEditar = document.getElementById('form#editar');
const tbody = document.getElementById('tbody');
const tbodySearch = document.getElementById('tbodysearch');
const tbodyRelatorio = document.getElementById('tbodyRelatorio');
const modalDados = new bootstrap.Modal(document.getElementById('modalExibirDados'), {})
const anexoDiv = document.getElementsByClassName('anexo--listItens')[0];
const super_admin = document.getElementById('super_admin');

var table = {
    data: false,
    data_search: false,
    pagina: 0,

    criarTable: function (data) {
        data = isJson(data);
        this.data = data.data;
        data = this.data;
        let linhas = this.gerarLinhas(data);
        tbody.innerHTML = linhas;
    },

    gerarLinhas: function (data) {
        var linhas = '';

        console.log(data)
        for (let i in data) {
            let bgcolor = '';
            let status = '';

            let dt_inicio = bancoToFrontHoras(data[i].dt_inicio);

            linhas += `<tr data-ref="${data[i].id}">`;
            linhas += `<td>${data[i].nome}</td>`
            linhas += `<td>${data[i].atividade}</td>`
            linhas += `<td>${dt_inicio}</td>`
            linhas += `<td>${data[i].id_ticket}</td>`
            linhas += `<td>${data[i].nome_fantasia}</td>`
            linhas += `<td>${data[i].titulo}</td>`
            linhas += `<td>${data[i].descricao}</td>`

            linhas += '</tr>';

        }

        return linhas;
    },

    adicionarTable: function (data) {
        data = isJson(data);
        data = data.data;
        let linhas = this.gerarLinhas(data);
        tbody.innerHTML += linhas;
        for(let i in data){
            this.data.push(data[i])
        }
    },

    pesquisa: async function (search) {
        let data = await getDataGET(`${url_base}/api/get_usuarios?condicao=${search}&limite=${limite}`);
        data = isJson(data);

        this.data_search = data.data;

        if (search != '') {
            let linhas = this.gerarLinhas(data.data);
            tbodySearch.innerHTML = linhas;
            $(tbody).hide();
            $(tbodySearch).show();
            this.search = true;
        } else {
            $(tbody).show();
            $(tbodySearch).hide();
            this.search = false;
        }
    }
}


$(document).ready(function () {

    (async function () {

        let data = await getDataGET(`${url_base}/api/get_trabalhos`);
        table.criarTable(data);

        $('.spinner-border').hide();
    })()
})





