<?php

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

include_once dirname(__FILE__) . '/../vendor/autoload.php';

function tira_Acentos($string)
{
    $comAcentos = array('à', 'á', 'â', 'ã', 'ä', 'å', 'ç', 'è', 'é', 'ê', 'ë', 'ì', 'í', 'î', 'ï', 'ñ', 'ò', 'ó', 'ô', 'õ', 'ö', 'ù', 'ü', 'ú', 'ÿ', 'À', 'Á', 'Â', 'Ã', 'Ä', 'Å', 'Ç', 'È', 'É', 'Ê', 'Ë', 'Ì', 'Í', 'Î', 'Ï', 'Ñ', 'Ò', 'Ó', 'Ô', 'Õ', 'Ö', 'O', 'Ù', 'Ü', 'Ú');
    $semAcentos = array('a', 'a', 'a', 'a', 'a', 'a', 'c', 'e', 'e', 'e', 'e', 'i', 'i', 'i', 'i', 'n', 'o', 'o', 'o', 'o', 'o', 'u', 'u', 'u', 'y', 'A', 'A', 'A', 'A', 'A', 'A', 'C', 'E', 'E', 'E', 'E', 'I', 'I', 'I', 'I', 'N', 'O', 'O', 'O', 'O', 'O', '0', 'U', 'U', 'U');
    $string = str_replace($comAcentos, $semAcentos, $string);
    return $string;
}

function sanitizeString($str)
{
    $str = preg_replace('/[áàãâä]/ui', 'a', $str);
    $str = preg_replace('/[éèêë]/ui', 'e', $str);
    $str = preg_replace('/[íìîï]/ui', 'i', $str);
    $str = preg_replace('/[óòõôö]/ui', 'o', $str);
    $str = preg_replace('/[úùûü]/ui', 'u', $str);
    $str = preg_replace('/[ç]/ui', 'c', $str);
    // $str = preg_replace('/[,(),;:|!"#$%&/=?~^><ªº-]/', '_', $str);
    $str = preg_replace('/[^a-z0-9]/i', '_', $str);
    $str = preg_replace('/_+/', '_', $str); // ideia do Bacco :)
    return $str;
}


function get_usuarioEmail($value)
{
    try {

        $db = getDB();

        $sql1 = "SELECT * FROM usuario WHERE email = :EMAIL";

        $stmt1 = $db->prepare($sql1);
        $stmt1->bindParam("EMAIL", $value, PDO::PARAM_STR);
        $stmt1->execute();
        $data = $stmt1->fetchAll(PDO::FETCH_OBJ);
        return $data;
    } catch (PDOException $e) {
        $error = '{"error":{"text":' . $e->getMessage() . '}}';
        die(json_encode(array(
            "status" => 500,
            "error" => $error,
            "message" => "Erro no servidor"
        )));
    }
}

function get_usuarioID($id)
{
    try {

        $db = getDB();

        $sql = "SELECT * FROM usuario WHERE id = :ID";
        $stmt = $db->prepare($sql);
        $stmt->bindParam('ID', $id, PDO::PARAM_INT);
        $stmt->execute();

        $data = $stmt->fetchAll(PDO::FETCH_OBJ);
        return $data;
    } catch (PDOException $e) {
        $error = '{"error":{"text":' . $e->getMessage() . '}}';
        die(json_encode(array(
            "status" => 500,
            "error" => $error,
            "message" => "Erro no servidor"
        )));
    }
}

function get_usuarioSetor($id)
{
    try {

        $db = getDB();

        $sql = "SELECT * FROM usuario WHERE id_setor = :ID";
        $stmt = $db->prepare($sql);
        $stmt->bindParam('ID', $id, PDO::PARAM_INT);
        $stmt->execute();

        $data = $stmt->fetchAll(PDO::FETCH_OBJ);
        return $data;
    } catch (PDOException $e) {
        $error = '{"error":{"text":' . $e->getMessage() . '}}';
        die(json_encode(array(
            "status" => 500,
            "error" => $error,
            "message" => "Erro no servidor"
        )));
    }
}

function get_cliente($id)
{
    try {

        $db = getDB();

        $sql1 = "SELECT * FROM cliente WHERE id = :ID and excluido = 'N'";

        $stmt1 = $db->prepare($sql1);
        $stmt1->bindParam("ID", $id, PDO::PARAM_INT);
        $stmt1->execute();
        $data = $stmt1->fetchAll(PDO::FETCH_OBJ);

        return $data;
    } catch (PDOException $e) {
        $error = '{"error":{"text":' . $e->getMessage() . '}}';
        die(json_encode(array(
            "status" => 500,
            "error" => $error,
            "message" => "Erro no servidor"
        )));
    }
}

function get_anexo($id, $tipo_contratado)
{
    try {

        $db = getDB();

        $tabela = $tipo_contratado === 'PS' ? 'anexo_ps' : 'anexo_cliente';

        $sql1 = "SELECT * FROM {$tabela} WHERE id = :ID and excluido = 'N'";

        $stmt1 = $db->prepare($sql1);
        $stmt1->bindParam("ID", $id, PDO::PARAM_INT);
        $stmt1->execute();
        $data = $stmt1->fetchAll(PDO::FETCH_OBJ);
        return $data;
    } catch (PDOException $e) {
        $error = '{"error":{"text":' . $e->getMessage() . '}}';
        die(json_encode(array(
            "status" => 500,
            "error" => $error,
            "message" => "Erro no servidor"
        )));
    }
}

function diff_horas_em_minutos($hora_ini = "00:00", $hora_fim = "00:00")
{
    $h1 = explode(":", $hora_ini)[0] * 60;
    $m1 = explode(":", $hora_ini)[1];

    $h2 = explode(":", $hora_fim)[0] * 60;
    $m2 = explode(":", $hora_fim)[1];

    $m_ini = $h1 + $m1;
    $m_fim = $h2 + $m2;

    return $m_fim - $m_ini;
}

function convert_minutes_in_hhmm($valor = 0)
{
    $horas = intval($valor / 60);
    $minutos = $valor % 60;

    return str_pad($horas, 2, "0", STR_PAD_LEFT) . ":" . str_pad($minutos, 2, "0", STR_PAD_LEFT);
}

function convert_hhmm_in_minutes($hora = "00:00")
{
    $h1 = explode(":", $hora)[0] * 60;
    $m1 = explode(":", $hora)[1];

    $m_ini = $h1 + $m1;

    return $m_ini;
}

function envia_email($subject, $txt)
{
    $to = "joaoeduardomcmannis@gmail.com";
    $from = "info@telemetriawifi.com.br";

    $headers  = "";
    $headers .= "From: Rede Industrial <info@telemetriawifi.com.br> \r\n";
    $headers .= "Reply-To:" . $from . "\r\n" . "X-Mailer: PHP/" . phpversion();
    $headers .= 'MIME-Version: 1.0' . "\r\n";
    $headers .= 'Content-type: text/html; charset=utf8' . "\r\n";

    $message  = '<html><body>';
    $message .= '<h3>' . $txt . '</h3>';
    $message .= '</body></html>';

    $ok = mail($to, $subject, $message, $headers, $from);
}

function enviar_email($to, $assunto, $conteudo, $nome = '')
{
    $mail = new PHPMailer();

    try {
        //Server settings
        $mail->CharSet = "utf-8";
        $mail->SMTPDebug = SMTP::DEBUG_OFF;                      //Enable verbose debug output
        $mail->isSMTP(true);                                            //Send using SMTP
        $mail->Host       = "smtp.gmail.com";                     //Set the SMTP server to send through
        $mail->SMTPAuth   = true;                                   //Enable SMTP authentication
        $mail->Username   = 'helpdesk@redeindustrial.com.br';                     //SMTP username
        $mail->Password   = 'rwF*i!lFhK6A';                               //SMTP password
        $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;            //Enable implicit TLS encryption
        $mail->Port       = 587;                                    //TCP port to connect to; use 587 if you have set `SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS`

        $mail->SMTPOptions = array(
            'ssl' => array(
                'verify_peer' => false,
                'verify_peer_name' => false,
                'allow_self_signed' => true
            )
        );

        //Recipients
        $mail->addAddress($to, $nome);

        //Content
        $mail->isHTML(true);                                  //Set email format to HTML
        $mail->Subject = $assunto;
        $mail->Body    = $conteudo;
        $mail->AltBody = $conteudo;

        $ok = $mail->send();
        return $ok;
    } catch (Exception $e) {
        echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
    }
}

function diff_meses($dataInicial, $dataFim)
{
    $data1 = new DateTime($dataInicial);
    $data2 = new DateTime($dataFim);

    $interval = $data2->diff($data1);
    $meses = intval($interval->days / 30);

    $meses = $data1->getTimestamp() > $data2->getTimestamp() ?  $meses * -1 : $meses;
    
    return $meses;
}

function horario_comercial($dataInicial, $dataFim)
{
    $data1 = new DateTime($dataInicial);
    $data2 = new DateTime($dataFim);

    $interval = $data2->diff($data1);

    return intval($interval->days / 30);
}

function verifica_permiss($auth_token, $auth, $setor = [])
{
    try {
        $db = getDB();

        $sql = "SELECT * FROM log_acesso
                WHERE TOKEN_ACESSO = :TOKEN_ACESSO";
        $stmt = $db->prepare($sql);
        $stmt->bindParam("TOKEN_ACESSO", $auth_token, PDO::PARAM_STR);
        $stmt->execute();
        $data = $stmt->fetchAll(PDO::FETCH_OBJ);

        $db = null;

        $data_login = new DateTime($data[0]->DATA_LOGIN);
        $now = (new DateTime())->modify('-3 hours');

        $diff = $data_login->diff($now);

        if (sizeof($data) <= 0) {
            dieForbiden();

            //Verifica se a rota é apenas para administrador e se o usuário é administrador
        } else if ($auth && $data[0]->TIPO_USUARIO != 1) {
            dieForbiden();
            //Verifica se o usuário é do mesmo setor da rota
        } else if (!in_array($data[0]->ID_SETOR, $setor) && $data[0]->TIPO_USUARIO != 1) {
            dieForbiden();
            //Verifica exp do token
        } else if ($diff->d > 0) {
            dieForbiden();
        } else {
            return $data[0];
        }
    } catch (PDOException $e) {
        $error = '{"error":{"text":' . $e->getMessage() . '}}';
        die(json_encode(array(
            "status" => 500,
            "error" => $error,
            "message" => "Erro no servidor"
        )));
    }
}

function registra_log($id, $auth_token, $tipo_usuario, $id_setor)
{

    $ip = $_SERVER['REMOTE_ADDR'];
    $clientDetails = json_decode(file_get_contents("http://ipinfo.io/$ip/json"));

    $ip_request = $clientDetails->ip;

    // Servicor de produção tem 3horas adiantiadas
    $data_login = date('Y-m-d H:i:s', time() - 3 * 60 * 60);

    try {
        $db = getDB();

        $sql = "INSERT INTO log_acesso (ID_USUARIO, IP, DATA_LOGIN, TOKEN_ACESSO, TIPO_USUARIO, ID_SETOR)
            VALUES (:ID_USUARIO,:IP,:DATA_LOGIN,:TOKEN_ACESSO, :TIPO_USUARIO, :ID_SETOR)";

        $stmt = $db->prepare($sql);
        $stmt->bindParam("ID_USUARIO", $id, PDO::PARAM_INT);
        $stmt->bindParam("IP", $ip_request, PDO::PARAM_STR);
        $stmt->bindParam("DATA_LOGIN", $data_login, PDO::PARAM_STR);
        $stmt->bindParam("TOKEN_ACESSO", $auth_token, PDO::PARAM_STR);
        $stmt->bindParam("TIPO_USUARIO", $tipo_usuario, PDO::PARAM_INT);
        $stmt->bindParam("ID_SETOR", $id_setor, PDO::PARAM_INT);
        $stmt->execute();

        $db = null;
    } catch (PDOException $e) {
        $error = '{"error":{"text":' . $e->getMessage() . '}}';
        die(json_encode(array(
            "status" => 500,
            "error" => $error,
            "message" => "Erro no servidor"
        )));
    }
}

function formatDMY($data)
{
    $dataformat = date('d/m/Y', strtotime($data));
    return $dataformat;
}

function formatDMYHHMMSS($data_original)
{

    $data = explode(' ', $data_original);
    $DMY = formatDMY($data[0]);

    return $DMY . ' ' . $data[1];
}

function dieForbiden()
{
    http_response_code(403);
    die(json_encode(array(
        "status" => 403,
        "error" => "Não tem permissão válida para acessar esse conteúdo",
        "message" => "Não tem permissão válida para acessar esse conteúdo"
    )));
}

function fieldsToInsertQuery($fields)
{
    $keys = array_keys($fields);

    $table_fields = implode(', ', $keys);

    foreach ($keys as $key => $value) {
        $binds[] = ':' . $keys[$key];
    }

    $binds = implode(', ', $binds);

    return [$table_fields, $binds];
}

function fieldsToEditQuery($fields)
{
    $keys = array_keys($fields);

    foreach ($keys as $key => $value) {
        $edit_fields[] = $value . ' = ' . ':' . $value;
    }

    return implode(', ', $edit_fields);
}


function valida_campos(array $fields, array $request)
{

    try {
        foreach ($fields as $field => $options) {

            if ($options['required'] === true && empty($request[$field])) {
                throw new Exception("Campo {$field} não informado", 400);
            } else if ($options['required'] === false && empty($request[$field])) {
                continue;
            }

            if (is_array($request[$field])) {
                throw new Exception("Campo {$field} é um array", 400);
            }

            switch ($options['type']) {

                case 'string':
                    $request[$field] = filter_var($request[$field]);
                    break;

                case 'int':
                    $request[$field] = filter_var($request[$field], FILTER_SANITIZE_NUMBER_INT);
                    if (!filter_var($request[$field], FILTER_VALIDATE_INT)) {
                        throw new Exception("Campo {$field} não é int", 400);
                    }
                    break;
                case 'float':
                    if (!filter_var($request[$field], FILTER_VALIDATE_FLOAT)) {
                        throw new Exception("Campo {$field} não é float", 400);
                    }
                    break;
                default:
                    $request[$field] = filter_var($request[$field]);
                    break;
            }


            foreach ($request as $key => $value) {
                if (!isset($fields[$key])) {
                    unset($request[$key]);
                }

                if(empty($request[$key])){
                    unset($request[$key]);
                }
            }

        }
            return [
                'success' => true,
                'data' => $request ?? []
            ];

    } catch (Exception $e) {
        return array(
            'message' => $e->getMessage(),
            'status' => $e->getCode(),
            'success' => false
        );
    }
}


function verifica_dominio()
{
    $dominios_aceitos = [
        'http://localhost:8075',
        'http://localhost:8071',
        'http://35.198.47.229:8075',
        'http://35.198.47.229:8071',
        'https://gestaori.redeindustrial.com.br/',
        'https://helpdesk.redeindustrial.com.br/'
    ];

    if (!empty($_SERVER['HTTP_ORIGIN'])) {

        $http_origin = $_SERVER['HTTP_ORIGIN'];
        if (in_array($http_origin, $dominios_aceitos)) {
            header("Access-Control-Allow-Origin: $http_origin");
        }
    }
}

function anti_sql($string)
{
    $string = htmlspecialchars($string);
    $string = htmlentities($string);
    $string = strip_tags($string);
    return $string;
}
