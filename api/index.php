<?php
ob_start();
session_start();
header('Content-Type: text/html; charset=utf-8');
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require_once 'Slim/Slim.php';
require_once 'config.php';
include_once 'utils.php';


verifica_dominio();

\Slim\Slim::registerAutoloader();
$app = new \Slim\Slim();

//Inclue todos arquivos da pasta controllers
foreach (glob("controllers/*.php") as $filename)
{
    include_once $filename;
}

//Inclue todos arquivos da pasta rotas
foreach (glob("rotas/*.php") as $filename)
{
    include_once $filename;
}

//Inclue todos arquivos da pasta models
foreach (glob("models/*.php") as $filename)
{
    include_once $filename;
}


$app->run();
