<?php

//Rotas fornecedors
$app->post("/cadastrar_fornecedor", function () {
    $request = \Slim\Slim::getInstance()->request();
    verifica_permiss($request->headers->get('AUTH-TOKEN'), false, [1,2,3,4,5,6,7,8]);
    FornecedorController::cadastrar_fornecedor($request);
});

$app->post("/editar_fornecedor", function () {
    $request = \Slim\Slim::getInstance()->request();
    verifica_permiss($request->headers->get('AUTH-TOKEN'), false, [1,2,3,4,5,6,7,8]);
    FornecedorController::editar_fornecedor($request);
});

$app->get("/excluir_fornecedor", function () {
    $request = \Slim\Slim::getInstance()->request();
    $user = verifica_permiss($request->headers->get('AUTH-TOKEN'), false, [1,2,3,4,5,6,7,8]);
    FornecedorController::excluir_fornecedor($request, $user);
});


$app->get("/get_fornecedores", function () {
    $request = \Slim\Slim::getInstance()->request();
    verifica_permiss($request->headers->get('AUTH-TOKEN'), false, [1,2,3,4,5,6,7,8]);
    FornecedorController::get_fornecedores($request);
});
