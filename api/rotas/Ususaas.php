
<?php

//Rotas Usuários dos Clientes
$app->post("/cadastrar_ususaas", function () {
    $request = \Slim\Slim::getInstance()->request();
    $data = verifica_permiss($request->headers->get('AUTH-TOKEN'), true, []);

    $fields['id_cadastrante'] = $data->ID_USUARIO;
    $fields = $request->post('');
    UsuarioSaasController::cadastrar_usuario($fields);
});
$app->post("/editar_ususaas", function () {
    $request = \Slim\Slim::getInstance()->request();
    verifica_permiss($request->headers->get('AUTH-TOKEN'), true, []);
    $fields = $request->post('');
    UsuarioSaasController::editar_usuario($fields);
});
$app->get("/excluir_ususaas", function () {
    $request = \Slim\Slim::getInstance()->request();
    verifica_permiss($request->headers->get('AUTH-TOKEN'), true, []);
    $fields = $request->get('');
    UsuarioSaasController::excluir_usuario($fields);
});
$app->get("/get_ususaas", function () {
    $request = \Slim\Slim::getInstance()->request();
    verifica_permiss($request->headers->get('AUTH-TOKEN'), false, [1,2,3,4,5,6,7,8]);
    UsuarioSaasController::get_usuarios($request);
});

$app->get("/get_ususaas_email", function () {
    $request = \Slim\Slim::getInstance()->request();
    verifica_permiss($request->headers->get('AUTH-TOKEN'), false, [1,2,3,4,5,6,7,8]);
    $fields = $request->get('');
    UsuarioSaasController::get_usuario_email($fields);
});