<?php



//Rotas Projetos
$app->post("/cadastrar_projeto", function () {
    $request = \Slim\Slim::getInstance()->request();
    $fields = $request->post('');
    //verifica_permiss($request->headers->get('AUTH-TOKEN'), false, [3]);
    verifica_permiss($request->headers->get('AUTH-TOKEN'), false, [1,2,3,4,5,6,7,8]);
    ProjetosController::cadastrar_projeto($fields);
});
$app->post("/editar_projeto", function () {
    $request = \Slim\Slim::getInstance()->request();
    $fields = $request->post('');
    $permiss = verifica_permiss($request->headers->get('AUTH-TOKEN'), false, [1,2,3,4,5,6,7,8]);

    $projeto = Projeto::get_projeto($fields['id']);

    if($permiss->TIPO_USUARIO == 2 && $projeto[0]->id_responsavel != $permiss->ID_USUARIO){
        dieForbiden();
    }

    ProjetosController::editar_projeto($fields);
});
$app->get("/get_projetos", function () {
    $request = \Slim\Slim::getInstance()->request();
    verifica_permiss($request->headers->get('AUTH-TOKEN'), false, [1,2,3,4,5,6,7,8]);
    ProjetosController::get_projetos($request);
});
$app->get("/get_projetos_empresa", function () {
    $request = \Slim\Slim::getInstance()->request();
    $fields = $request->get('');
    verifica_permiss($request->headers->get('AUTH-TOKEN'), false, [1,2,3,4,5,6,7,8]);
    ProjetosController::get_projetos_empresa($fields);
});
$app->get("/excluir_projeto", function () {
    $request = \Slim\Slim::getInstance()->request();
    $fields = $request->get('');
    verifica_permiss($request->headers->get('AUTH-TOKEN'), false, [1,2,3,4,5,6,7,8]);
    ProjetosController::excluir_projeto($fields);
});
$app->get("/lancamentos_projeto", function () {
    $request = \Slim\Slim::getInstance()->request();
    $fields = $request->get('');
    verifica_permiss($request->headers->get('AUTH-TOKEN'), false, [1,2,3,4,5,6,7,8]);
    ProjetosController::lancamentos_projeto($fields);
});
$app->get("/get_projeto", function () {
    $request = \Slim\Slim::getInstance()->request();
    $fields = $request->get('');
    verifica_permiss($request->headers->get('AUTH-TOKEN'), false, [1,2,3,4,5,6,7,8]);
    ProjetosController::get_projeto($fields);
});