<?php


$app->get("/get_clientes", function () {
    $request = \Slim\Slim::getInstance()->request();
    // verifica_permiss($request->headers->get('AUTH-TOKEN'), false, [1,2,3,4,5,6,7,8]);
    $fields = $request->get('');
    ClientesController::get_clientes($fields);
});
$app->post("/cadastrar_cliente", function () {
    $request = \Slim\Slim::getInstance()->request();
    verifica_permiss($request->headers->get('AUTH-TOKEN'), true, []);
    $fields = $request->post('');
    ClientesController::cadastrar_cliente($fields);
});
$app->get("/excluir_cliente", function () {
    $request = \Slim\Slim::getInstance()->request();
    verifica_permiss($request->headers->get('AUTH-TOKEN'), true, []);
    $fields = $request->get('');
    ClientesController::excluir_cliente($fields);
});
$app->post("/editar_cliente", function () {
    $request = \Slim\Slim::getInstance()->request();
    verifica_permiss($request->headers->get('AUTH-TOKEN'), true, []);
    $fields = $request->post('');
    ClientesController::editar_cliente($fields);
});
$app->get("/get_cliente", function () {
    $request = \Slim\Slim::getInstance()->request();
    verifica_permiss($request->headers->get('AUTH-TOKEN'), false, [1,2,3,4,5,6,7,8]);
    $fields = $request->get('');
    ClientesController::get_cliente($fields);
});
$app->get("/get_horas_restantes_cliente", function () {
    $request = \Slim\Slim::getInstance()->request();
    verifica_permiss($request->headers->get('AUTH-TOKEN'), false, [1,2,3,4,5,6,7,8]);
    $fields = $request->get('');
    ClientesController::get_horas_restantes($fields);
});
// $app->get("/verifica_contratos", function () {
//     $request = \Slim\Slim::getInstance()->request();
//     $fields = $request->get('');
//     ClientesController::verifica_contratos();
// });
//$app->get("/dados_gerais", function () {
//    $request = \Slim\Slim::getInstance()->request();
//    verifica_permiss($request->headers->get('AUTH-TOKEN'), true);
//    $fields = $request->get('');
//    ClientesController::dados_gerais();
//});

