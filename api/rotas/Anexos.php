<?php

//Rotas anexos
$app->post("/adicionar_anexo", function () {
    $request = \Slim\Slim::getInstance()->request();
    verifica_permiss($request->headers->get('AUTH-TOKEN'), true, []);
    $fields = $request->post('');
    AnexoController::adicionar_anexo($fields, false, [1,2,3,4,5,6,7,8]);
});
$app->get("/excluir_anexo", function () {
    $request = \Slim\Slim::getInstance()->request();
    verifica_permiss($request->headers->get('AUTH-TOKEN'), true, []);
    $fields = $request->get('');
    AnexoController::excluir_anexo($fields);
});


$app->get("/get_anexos", function () {
    $request = \Slim\Slim::getInstance()->request();
    $permiss = verifica_permiss($request->headers->get('AUTH-TOKEN'), false, [1,2,3,4,5,6,7,8]);
    $fields = $request->get('');

    //Verificação excepcional dessa rota
    if($permiss->TIPO_USUARIO == 2){
        if(($fields['id_contratado'] != $permiss->ID_USUARIO) || $fields['tipo_contratado'] != 'PS'){
            dieForbiden();
        }
    }
    AnexoController::get_anexos($fields);
});
