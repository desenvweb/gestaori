<?php

//Rotas Tickets 
$app->get("/get_tickets", function () {
    $request = \Slim\Slim::getInstance()->request();
    $fields = $request->get('');
    verifica_permiss($request->headers->get('AUTH-TOKEN'), false, [1,2,3,4,5,6,7,8]);
    TicketsController::get_tickets($fields);
});

$app->post("/cadastrar_ticket", function () {
    $request = \Slim\Slim::getInstance()->request();
    $user = verifica_permiss($request->headers->get('AUTH-TOKEN'), false, [1,2,3,4,5,6,7,8]);
    TicketsController::cadastrar_ticket($request, $user);
});

$app->get("/excluir_ticket", function () {
    $request = \Slim\Slim::getInstance()->request();
    verifica_permiss($request->headers->get('AUTH-TOKEN'), false, [1,2,3,4,5,6,7,8]);

    TicketsController::excluir_ticket($request);
});

$app->post("/editar_ticket", function () {
    $request = \Slim\Slim::getInstance()->request();
    $fields = $request->post('');
    verifica_permiss($request->headers->get('AUTH-TOKEN'), false, [1,2,3,4,5,6,7,8]);
    TicketsController::editar_ticket($fields);
});


$app->post("/marcar_lido", function () {
    $request = \Slim\Slim::getInstance()->request();
    $fields = $request->post('');
    verifica_permiss($request->headers->get('AUTH-TOKEN'), false, [1,2,3,4,5,6,7,8]);
    TicketsController::marcar_lido($fields);
});


$app->post("/add_comentario", function () {
    $request = \Slim\Slim::getInstance()->request();
    $fields = $request->post('');
    verifica_permiss($request->headers->get('AUTH-TOKEN'), false, [1,2,3,4,5,6,7,8]);
    TicketsController::add_comentario($fields);
});

$app->get("/get_comentarios", function () {
    $request = \Slim\Slim::getInstance()->request();
    verifica_permiss($request->headers->get('AUTH-TOKEN'), false, [1,2,3,4,5,6,7,8]);
    TicketsController::get_comentarios($request);
});

$app->get("/relatorio_ticket", function () {
    $request = \Slim\Slim::getInstance()->request();
    // verifica_permiss($request->headers->get('AUTH-TOKEN'), false, [1,2,3,4,5,6,7,8]);
    TicketsController::relatorio($request);
});

$app->get("/relatorio_tickets_pdf", function () {
    $request = \Slim\Slim::getInstance()->request();
    TicketsController::relatorio_tickets_pdf($request);
});

$app->get("/get_gut_gravidade", function () {
    $request = \Slim\Slim::getInstance()->request();
    $fields = $request->get('');
    verifica_permiss($request->headers->get('AUTH-TOKEN'), false, [1,2,3,4,5,6,7,8]);
    TicketsController::get_gut_gravidade();
});

$app->get("/get_gut_urgencia", function () {
    $request = \Slim\Slim::getInstance()->request();
    $fields = $request->get('');
    verifica_permiss($request->headers->get('AUTH-TOKEN'), false, [1,2,3,4,5,6,7,8]);
    TicketsController::get_gut_urgencia();
});

$app->get("/get_gut_tendencia", function () {
    $request = \Slim\Slim::getInstance()->request();
    $fields = $request->get('');
    verifica_permiss($request->headers->get('AUTH-TOKEN'), false, [1,2,3,4,5,6,7,8]);
    TicketsController::get_gut_tendencia();
});

$app->get("/get_avaliacao_ticket", function () {
    $request = \Slim\Slim::getInstance()->request();
    TicketsController::get_avaliacao_ticket($request);
});

$app->post("/add_avaliacao_ticket", function () {
    $request = \Slim\Slim::getInstance()->request();
    $fields = $request->post('');
    TicketsController::add_avaliacao_ticket($fields);
});
