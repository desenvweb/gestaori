<?php


$app->post("/add_adendo", function () {
    $request = \Slim\Slim::getInstance()->request();
    $requester = verifica_permiss($request->headers->get('AUTH-TOKEN'), false, [1,2,3,4,5,6,7,8]);

    AdendosController::cadastrar($request, $requester);
});
