<?php


//Rotas Status
$app->get("/get_status_projeto", function () {
    $request = \Slim\Slim::getInstance()->request();
    $fields = $request->get('');
    verifica_permiss($request->headers->get('AUTH-TOKEN'), false, [1,2,3,4,5,6,7,8]);
    StatusController::get_status_projeto($fields);
});

$app->get("/get_status_ticket", function () {
    $request = \Slim\Slim::getInstance()->request();
    $fields = $request->get('');
    verifica_permiss($request->headers->get('AUTH-TOKEN'), false, [1,2,3,4,5,6,7,8]);
    StatusController::get_status_ticket($fields);
});
