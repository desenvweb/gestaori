
<?php

//Rotas Usuários dos Clientes
$app->post("/cadastrar_usucliente", function () {
    $request = \Slim\Slim::getInstance()->request();
    $data = verifica_permiss($request->headers->get('AUTH-TOKEN'), true, []);

    $fields['id_cadastrante'] = $data->ID_USUARIO;
    $fields = $request->post('');
    UsuarioClienteController::cadastrar_usuario($fields);
});
$app->post("/editar_usucliente", function () {
    $request = \Slim\Slim::getInstance()->request();
    verifica_permiss($request->headers->get('AUTH-TOKEN'), true, []);
    $fields = $request->post('');
    UsuarioClienteController::editar_usuario($fields);
});
$app->get("/excluir_usucliente", function () {
    $request = \Slim\Slim::getInstance()->request();
    verifica_permiss($request->headers->get('AUTH-TOKEN'), true, []);
    $fields = $request->get('');
    UsuarioClienteController::excluir_usuario($fields);
});
$app->get("/get_usuclientes", function () {
    $request = \Slim\Slim::getInstance()->request();
    verifica_permiss($request->headers->get('AUTH-TOKEN'), false, [1,2,3,4,5,6,7,8]);
    UsuarioClienteController::get_usuarios($request);
});

$app->get("/get_usucliente_email", function () {
    $request = \Slim\Slim::getInstance()->request();
    verifica_permiss($request->headers->get('AUTH-TOKEN'), false, [1,2,3,4,5,6,7,8]);
    $fields = $request->get('');
    UsuarioClienteController::get_usuario_email($fields);
});