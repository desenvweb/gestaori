<?php


$app->get("/get_empresas", function () {
    $request = \Slim\Slim::getInstance()->request();
    verifica_permiss($request->headers->get('AUTH-TOKEN'), false, [1,2,3,4,5,6,7,8]);
    EmpresasController::get_empresas($request);
});

$app->get("/get_empresas_com_horas", function () {
    $request = \Slim\Slim::getInstance()->request();
    verifica_permiss($request->headers->get('AUTH-TOKEN'), false, [1,2,3,4,5,6,7,8]);
    EmpresasController::get_empresas_com_horas($request);
});

$app->get("/get_empresa", function () {
    $request = \Slim\Slim::getInstance()->request();
    verifica_permiss($request->headers->get('AUTH-TOKEN'), false, [1,2,3,4,5,6,7,8]);
    EmpresasController::get_empresa($request);
});

$app->post("/editar_empresa", function () {
    $request = \Slim\Slim::getInstance()->request();
    verifica_permiss($request->headers->get('AUTH-TOKEN'), false, [1,2,3,4,5,6,7,8]);
    EmpresasController::editar_empresa($request);
});

$app->post("/cadastrar_empresa", function () {
    $request = \Slim\Slim::getInstance()->request();
    verifica_permiss($request->headers->get('AUTH-TOKEN'), false, [1,2,3,4,5,6,7,8]);
    EmpresasController::cadastrar_empresa($request);
});

$app->get("/excluir_empresa", function () {
    $request = \Slim\Slim::getInstance()->request();
    $user = verifica_permiss($request->headers->get('AUTH-TOKEN'), false, [1,2,3,4,5,6,7,8]);
    EmpresasController::excluir_empresa($request, $user);
});

$app->get("/dados_gerais", function () {
    $request = \Slim\Slim::getInstance()->request();
    verifica_permiss($request->headers->get('AUTH-TOKEN'), true);
    $fields = $request->get('');
    EmpresasController::dados_gerais();
});

$app->get("/get_todos_clientes", function () {
    $request = \Slim\Slim::getInstance()->request();
    verifica_permiss($request->headers->get('AUTH-TOKEN'), true);
    $fields = $request->get('');
    EmpresasController::get_todos_clientes($fields);
});

$app->get("/get_clientes_saas", function () {
    $request = \Slim\Slim::getInstance()->request();
    verifica_permiss($request->headers->get('AUTH-TOKEN'), true);
    $fields = $request->get('');
    EmpresasController::get_clientes_saas($fields);
});
