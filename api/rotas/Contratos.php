<?php
$app->post("/add_contrato", function () {
    $request = \Slim\Slim::getInstance()->request();
    $requester = verifica_permiss($request->headers->get('AUTH-TOKEN'), false, [1,2,3,4,5,6,7,8]);
    ContratosController::cadastrar($request, $requester);
});

$app->post("/add_contrato_faturar", function () {
    $request = \Slim\Slim::getInstance()->request();
    $requester = verifica_permiss($request->headers->get('AUTH-TOKEN'), false, [1,2,3,4,5,6,7,8]);
    
    ContratosController::cadastrar_contrato_faturar($request, $requester);
});

$app->get("/get_contrato_ativo", function () {
    $request = \Slim\Slim::getInstance()->request();
    $requester = verifica_permiss($request->headers->get('AUTH-TOKEN'), false, [1,2,3,4,5,6,7,8]);
    ContratosController::get_contrato_ativo($request);
});

$app->get("/get_contrato_faturar", function () {
    $request = \Slim\Slim::getInstance()->request();
    $requester = verifica_permiss($request->headers->get('AUTH-TOKEN'), false, [1,2,3,4,5,6,7,8]);
    ContratosController::get_contratos_faturar($request);
});

$app->get("/verifica_contratos", function () {
    $request = \Slim\Slim::getInstance()->request();
    $requester = verifica_permiss($request->headers->get('AUTH-TOKEN'), false, [1,2,3,4,5,6,7,8]);
    ContratosController::verifica_contrato($request);
});

$app->get("/excluir_contrato", function () {
    $request = \Slim\Slim::getInstance()->request();
    $requester = verifica_permiss($request->headers->get('AUTH-TOKEN'), false, [1,2,3,4,5,6,7,8]);
    ContratosController::excluir($request);
});

$app->get("/excluir_contrato_faturar", function () {
    $request = \Slim\Slim::getInstance()->request();
    $requester = verifica_permiss($request->headers->get('AUTH-TOKEN'), false, [1,2,3,4,5,6,7,8]);
    ContratosController::excluir_contrato_faturar($request);
});

$app->post("/editar_contrato", function () {
    $request = \Slim\Slim::getInstance()->request();
    $requester = verifica_permiss($request->headers->get('AUTH-TOKEN'), false, [1,2,3,4,5,6,7,8]);
    ContratosController::editar($request);
});

$app->post("/editar_contrato_faturar", function () {
    $request = \Slim\Slim::getInstance()->request();
    $requester = verifica_permiss($request->headers->get('AUTH-TOKEN'), false, [1,2,3,4,5,6,7,8]);
    ContratosController::editar_contrato_faturar($request);
});

$app->get("/debita_horas_mensal", function () {
    Contratos::verificacao_mensal();
});


