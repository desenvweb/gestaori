<?php

$app->post("/cadastrar_horasCompradas", function () {
    $request = \Slim\Slim::getInstance()->request();
    $requester = verifica_permiss($request->headers->get('AUTH-TOKEN'), false, [1,2,3,4,5,6,7,8]);

    HorasCompradasController::cadastrar($request, $requester);
});

$app->get("/excluir_horasCompradas", function () {
    $request = \Slim\Slim::getInstance()->request();
    verifica_permiss($request->headers->get('AUTH-TOKEN'), true, []);
    $fields = $request->get('');
    HorasCompradasController::excluir($fields);
});
