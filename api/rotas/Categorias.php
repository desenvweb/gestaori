<?php


//Rotas Categorias 
$app->get("/get_categorias", function () {
    $request = \Slim\Slim::getInstance()->request();
    $fields = $request->get('');
    verifica_permiss($request->headers->get('AUTH-TOKEN'), false, [1,2,3,4,5,6,7,8]);
    CategoriaController::get_categorias();
});
