<?php 

$app->post("/login", function () {
    $request = \Slim\Slim::getInstance()->request();
    $fields = $request->post('');
    UsuarioController::login($fields);
});
$app->post("/cadastrar_usuario", function () {
    $request = \Slim\Slim::getInstance()->request();
    verifica_permiss($request->headers->get('AUTH-TOKEN'), true, []);
    $fields = $request->post('');
    UsuarioController::cadastrar_usuario($fields);
});
$app->get("/excluir_usuario", function () {
    $request = \Slim\Slim::getInstance()->request();
    verifica_permiss($request->headers->get('AUTH-TOKEN'), true, []);
    $fields = $request->get('');
    UsuarioController::excluir_usuario($fields);
});
$app->post("/editar_usuario", function () {
    $request = \Slim\Slim::getInstance()->request();
    $permiss = verifica_permiss($request->headers->get('AUTH-TOKEN'), false, [1,2,3,4,5,6,7,8]);
    UsuarioController::editar($request);
});
/*$app->get("/get_pj_vencer", function () {
    $request = \Slim\Slim::getInstance()->request();
    verifica_permiss($request->headers->get('AUTH-TOKEN'), false, [1,2,3,4,5,6,7,8]);
    $fields = $request->get('');
    UsuarioController::get_pj_vencer($fields);
});*/

$app->get("/get_usuarios", function () {
    $request = \Slim\Slim::getInstance()->request();
    verifica_permiss($request->headers->get('AUTH-TOKEN'), false, [1,2,3,4,5,6,7,8]);
    $fields = $request->get('');
    UsuarioController::get_usuarios($fields);
});

$app->get("/get_usuario", function () {
    $request = \Slim\Slim::getInstance()->request();
    verifica_permiss($request->headers->get('AUTH-TOKEN'), false, [1,2,3,4,5,6,7,8]);
    $fields = $request->get('');
    UsuarioController::get_usuario($fields);
});
$app->post("/nova_senha_app", function () {
    $request = \Slim\Slim::getInstance()->request();
    verifica_permiss($request->headers->get('AUTH-TOKEN'), false, [1,2,3,4,5,6,7,8]);
    $fields = $request->post('');
    UsuarioController::nova_senha_app($fields);
});
$app->post("/set_status", function () {
    $request = \Slim\Slim::getInstance()->request();
    verifica_permiss($request->headers->get('AUTH-TOKEN'), false, [1,2,3,4,5,6,7,8]);
    $fields = $request->post('');
    UsuarioController::set_status($fields);
});
$app->get("/get_infos", function () {
    $request = \Slim\Slim::getInstance()->request();
    $user = verifica_permiss($request->headers->get('AUTH-TOKEN'), false, [1,2,3,4,5,6,7,8]);
    $fields = $request->get('');
    if (!$fields['id_usuario']){
        $fields['id_usuario'] = $user->ID_USUARIO;
    }
    
    UsuarioController::get_infos($fields);
});
$app->get("/set_visualizado", function () {
    $request = \Slim\Slim::getInstance()->request();
    $user = verifica_permiss($request->headers->get('AUTH-TOKEN'), false, [1,2,3,4,5,6,7,8]);
    $fields = $request->get('');
    $fields['id_usuario'] = $user->ID_USUARIO;
    UsuarioController::set_visualizado($fields);
});
$app->get("/get_trabalhos", function () {
    $request = \Slim\Slim::getInstance()->request();
    $user = verifica_permiss($request->headers->get('AUTH-TOKEN'), false, [1,2,3,4,5,6,7,8]);
    UsuarioController::get_trabalhos();
});
