<?php


//Rotas horas trabalhadas
$app->post("/lancar_horas", function () {
    $request = \Slim\Slim::getInstance()->request();
    verifica_permiss($request->headers->get('AUTH-TOKEN'), true, []);
    HorasTrabController::lancar_horas($request);
});
$app->get("/get_horasUsuario", function () {
    $request = \Slim\Slim::getInstance()->request();
    $fields = $request->get('');
    $permiss = verifica_permiss($request->headers->get('AUTH-TOKEN'), false, [1,2,3,4,5,6,7,8]);
    $fields['id_usuario'] = $permiss->ID_USUARIO;
    HorasTrabController::get_horasUsuario($fields);
});
$app->get("/get_horas", function () {
    $request = \Slim\Slim::getInstance()->request();
    $user = verifica_permiss($request->headers->get('AUTH-TOKEN'), false, [1,2,3,4,5,6,7,8]);

    HorasTrabController::get_horas($request, $user);
});
$app->post("/editar_horas", function () {
    $request = \Slim\Slim::getInstance()->request();
    verifica_permiss($request->headers->get('AUTH-TOKEN'), true, []);
    HorasTrabController::editar_horas($request);
});

$app->get("/verifica_trabalho", function () {
    $request = \Slim\Slim::getInstance()->request();
    $permiss = verifica_permiss($request->headers->get('AUTH-TOKEN'), false, [1,2,3,4,5,6,7,8]);
    $fields['id_usuario'] = $permiss->ID_USUARIO;
    $fields = $request->get('');
    HorasTrabController::verifica_trabalho($fields);
});


$app->post("/iniciar_ld", function () {
    $request = \Slim\Slim::getInstance()->request();
    $requester = verifica_permiss($request->headers->get('AUTH-TOKEN'), false, [1,2,3,4,5,6,7,8]);
    HorasTrabController::iniciar_ld($request, $requester);
});

$app->post("/finalizar_ld", function () {
    $request = \Slim\Slim::getInstance()->request();
    $requester = verifica_permiss($request->headers->get('AUTH-TOKEN'), false, [1,2,3,4,5,6,7,8]);
    HorasTrabController::finalizar_ld($request, $requester);
});

$app->get("/excluir_lancamento", function () {
    $request = \Slim\Slim::getInstance()->request();
    verifica_permiss($request->headers->get('AUTH-TOKEN'), true, []);
    $fields = $request->get('');
    HorasTrabController::excluir_lancamento($fields);
});

$app->get("/fim_expediente", function () {
    $request = \Slim\Slim::getInstance()->request();
    $fields = $request->get('');
    HorasTrabController::fim_expediente();
});
$app->post("/relatorio_horas_usuario", function () {
    $request = \Slim\Slim::getInstance()->request();
    $fields = $request->post('');
    $permiss = verifica_permiss($request->headers->get('AUTH-TOKEN'), false, [1,2,3,4,5,6,7,8]);

    //Todos usuários podem ver horas de todos
    //if($permiss->TIPO_USUARIO == 2){
    //    $fields['colaborador'] = $permiss->ID_USUARIO;
    //}

    HorasTrabController::relatorio_horas($fields);
});

 $app->get("/relatorio_horas_usuario_pdf", function () {
    $request = \Slim\Slim::getInstance()->request();
    // verifica_permiss($request->headers->get('AUTH-TOKEN'), false, [1,2,3,4,5,6,7,8]);
    HorasTrabController::relatorio_horas_pdf($request);
 });

$app->post("/relatorio_horas_empresa", function () {
    $request = \Slim\Slim::getInstance()->request();
    verifica_permiss($request->headers->get('AUTH-TOKEN'), false, [1,2,3,4,5,6,7,8]);

    HorasTrabController::relatorio_horas_e_projetos($request);
});

$app->get("/relatorio_horas_empresa_pdf", function () {
    $request = \Slim\Slim::getInstance()->request();
    HorasTrabController::relatorio_horas_empresa_pdf($request);
});