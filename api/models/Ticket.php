<?php

require dirname(__DIR__) . '../../vendor/autoload.php';

use Dompdf\Dompdf;

class Ticket {
    public static function get_tickets($id)
    {
        try {
            $db = getDB();

            $sql = "SELECT t.id, t.titulo, t.dt_abertura, t.dt_fechamento, t.id_categoria, t.id_cliente, 
            t.id_usucliente, t.id_status, st.descricao as status, c.nome as usuario, t.id_responsavel,
            e.razao_social, e.cnpj, ct.descricao as categoria, t.id_setor, t.dt_prevista, t.informacoes_gerais,
            t.gut, t.gravidade, t.urgencia, t.tendencia, u.nome as responsavel
            FROM ticket as t          
            LEFT JOIN status_ticket as st ON st.id = t.id_status
            LEFT JOIN usu_cliente as c ON c.id = t.id_usucliente
            LEFT JOIN empresas as e ON e.id = c.id_empresa
            LEFT JOIN categoria_ticket as ct ON ct.id = t.id_categoria
            LEFT JOIN usuario as u ON u.id = t.id_responsavel

            WHERE t.id = :id";

            $stmt = $db->prepare($sql);
            $stmt->bindParam('id', $id, PDO::PARAM_INT);
            $stmt->execute();
            $data = $stmt->fetchAll(PDO::FETCH_OBJ);

            return $data;
            
            $db = null;
        } catch (PDOException $e) {
            $error = '{"error":{"text":' . $e->getMessage() . '}}';
            die(json_encode(array(
                "status" => 500,
                "error" => $error,
                "message" => "Erro no servidor"
            )));
        }
    }

    public static function get_anexos_comentario($id_comentario){
        try {
            $db = getDB();

            $sql = "SELECT * FROM anexo_comentariotk
                    WHERE id_comentario = {$id_comentario}";

            $stmt = $db->prepare($sql);
            $stmt->execute();
            $data = $stmt->fetchAll(PDO::FETCH_OBJ);

            return $data;
            
            $db = null;
        } catch (PDOException $e) {
            $error = '{"error":{"text":' . $e->getMessage() . '}}';
            die(json_encode(array(
                "status" => 500,
                "error" => $error,
                "message" => "Erro no servidor"
            )));
        }
    }

    public static function excluir_ticket($fields = [])
    {
        $id = $fields['id_ticket'];

        try {
            $db = getDB();

            $sql = "DELETE FROM ticket
                    WHERE id = :id";

            $stmt = $db->prepare($sql);
            $stmt->bindParam('id', $id, PDO::PARAM_INT);

            $stmt->execute();

            return[
                "status" => 1,
                "message" => "Ticket excluido com sucesso"
            ];

            $db = null;
        } catch (PDOException $e) {
            $error = '{"error":{"text":' . $e->getMessage() . '}}';
            die(json_encode(array(
                "status" => 500,
                "error" => $error,
                "message" => "Erro no servidor"
            )));
        }
    }

    public static function get_ticket(array $fields){
        try {
            $db = getDB();

            $sql = "SELECT t.id, t.titulo, t.dt_abertura, t.dt_fechamento, t.id_categoria, t.id_cliente, 
            t.id_usucliente, t.id_status, st.descricao as status, c.nome as usuario, t.id_responsavel,
            e.razao_social, e.cnpj, ct.descricao as categoria, t.id_setor, t.dt_prevista, t.informacoes_gerais,
            t.gut, t.gravidade, t.urgencia, t.tendencia, u.nome as responsavel
            FROM ticket as t          
            LEFT JOIN status_ticket as st ON st.id = t.id_status
            LEFT JOIN usu_cliente as c ON c.id = t.id_usucliente
            LEFT JOIN empresas as e ON e.id = c.id_empresa
            LEFT JOIN categoria_ticket as ct ON ct.id = t.id_categoria
            LEFT JOIN usuario as u ON u.id = t.id_responsavel
            WHERE t.id = :id_ticket
            LIMIT :limite OFFSET :qnt";

            // die($sql);

            $stmt = $db->prepare($sql);
            $stmt->bindParam('id_ticket', $fields['id_ticket'], PDO::PARAM_INT);
            $stmt->execute();
            $data = $stmt->fetch(PDO::FETCH_OBJ);


            $data->comentarios = self::get_comentarios(['id_ticket' => $fields['id_ticket']]);

            return $data;

            $db = null;
        } catch (PDOException $e) {
            $error = '{"error":{"text":' . $e->getMessage() . '}}';
            die(json_encode(array(
                "status" => 500,
                "error" => $error,
                "message" => "Erro no servidor"
            )));
        }
    }

    public static function get_statusticket($id)
    {
        try {
            $db = getDB();

            $sql = "SELECT t.id_status, st.descricao as status
            FROM ticket as t          
            LEFT JOIN status_ticket as st ON st.id = t.id_status

            WHERE t.id = :id";

            $stmt = $db->prepare($sql);
            $stmt->bindParam('id', $id, PDO::PARAM_INT);
            $stmt->execute();
            $data = $stmt->fetchAll(PDO::FETCH_OBJ);

            return $data;
            
            $db = null;
        } catch (PDOException $e) {
            $error = '{"error":{"text":' . $e->getMessage() . '}}';
            die(json_encode(array(
                "status" => 500,
                "error" => $error,
                "message" => "Erro no servidor"
            )));
        }
    }

    public static function get_comentarios(array $fields)
    {
        $id = $fields['id_ticket'];

        try {
            $db = getDB();

            $sql = "SELECT ti.codticketitem, ti.privado, ti.id_usuario, ti.id_ticket, ti.arquivoanexo, ti.url, 
                    ti.anexodeletado, ti.codarquivo, ti.id_usucliente, ti.dt_leituracliente, ti.dt_cadastro,
                    dt.texto
                    FROM ticketitem as ti
                    LEFT JOIN dialogo_ticket as dt ON dt.codticketitem = ti.codticketitem
                    WHERE ti.id_ticket = :id
                    ORDER BY ti.codticketitem DESC";

            $stmt = $db->prepare($sql);
            $stmt->bindParam('id', $id, PDO::PARAM_INT);
            $stmt->execute();

            $data = $stmt->fetchAll(PDO::FETCH_OBJ);

            foreach ($data as $d) {
                $user = $d->id_usucliente == 0 ? Usuario::get_user(['id_usuario' => $d->id_usuario]) : UsuarioCliente::get_usuario(['id_usucliente' => $d->id_usucliente]);
                $d->nome = $user->nome;

                $d->anexos = Ticket::get_anexos_comentario($d->codticketitem);
            }

            return $data;

            $db = null;
        } catch (PDOException $e) {
            $error = '{"error":{"text":' . $e->getMessage() . '}}';
            die(json_encode(array(
                "status" => 500,
                "error" => $error,
                "message" => "Erro no servidor"
            )));
        }
    }

    public static function get_avaliacao_ticket(array $fields)
    {

        $token = $fields['token'];
        try {
            $db = getDB();

            $sql = "SELECT *
                    FROM avaliacao_ticket
                    WHERE token = :token";

            $stmt = $db->prepare($sql);
            $stmt->bindParam('token', $token, PDO::PARAM_STR);
            $stmt->execute();

            $data = $stmt->fetchAll(PDO::FETCH_OBJ);

            return $data;

            $db = null;
        } catch (PDOException $e) {
            $error = '{"error":{"text":' . $e->getMessage() . '}}';
            die(json_encode(array(
                "status" => 500,
                "error" => $error,
                "message" => "Erro no servidor"
            )));
        }
    }

    public static function remover_comentario($fields = [])
    {
        $id = $fields['id_comentario'];

        try {
            $db = getDB();

            $sql = "DELETE FROM ticketitem
                    WHERE id = :id";

            $stmt = $db->prepare($sql);
            $stmt->bindParam('id', $id, PDO::PARAM_INT);

            $stmt->execute();

            return[
                "status" => 1,
                "message" => "Comentário excluido com sucesso"
            ];

            $db = null;
        } catch (PDOException $e) {
            $error = '{"error":{"text":' . $e->getMessage() . '}}';
            die(json_encode(array(
                "status" => 500,
                "error" => $error,
                "message" => "Erro no servidor"
            )));
        }
    }

    public static function cadastrar_ticket(array $fields)
    {

        //Definições iniciais do ticket
        $fields['id_status'] = 1;
        $fields['dt_abertura'] = (new DateTime())->modify('-3 hours')->format('Y-m-d H:i:s');

        try {

            $usucliente = UsuarioCliente::get_usuario(['id_usucliente' => $fields['id_usucliente']]);

            $usuario = Usuario::get_user(['id_usuario' => $fields['id_responsavel']]);

            if ($usucliente) {
                $db = getDB();

                //$fields['id_cliente'] = $usucliente->id_empresa;
 
                $query = fieldsToInsertQuery($fields);
                //die(json_encode($query));
                $sql = "INSERT INTO ticket ({$query[0]}) VALUES ({$query[1]})";
                $stmt = $db->prepare($sql);


                $int = ['id_usucliente', 'id_setor', 'id_categoria', 'id_responsavel', 'id_status', 'id_empresa'];
                foreach ($fields as $key => &$value) {
                    $param = array_search($key, $int) ? PDO::PARAM_INT : PDO::PARAM_STR;
                    $stmt->bindParam(':' . $key, $value, $param);
                }

                $stmt->execute();
                $titulo = "{$fields['titulo']} - Ticket Criado";
                
                $body = "<p>Um ticket foi aberto pelo agente <b>{$usuario->nome}</b> com o titulo <b>{$fields['titulo']}</b>.";
                enviar_email($usucliente->email1, $titulo, $body, $usucliente->nome);

                return [
                    "status" => 1,
                    "message" => "Ticket cadastrado com sucesso"
                ];

                $db = null;
            }
        } catch (PDOException $e) {
            $error = '{"error":{"text":' . $e->getMessage() . '}}';
            die(json_encode(array(
                "status" => 500,
                "error" => $error,
                "message" => "Erro no servidor"
            )));
        }
    }

    public static function relatorio_tickets_pdf()
    {

        //$where  = 'status_tkt='.$fields['status_tkt'].'&empresa='.$fields['empresa'].'&usuario_cli='.$fields['usuario_cli'].'&responsavel='.$fields['responsavel'];
        //die($_GET['empresa']);

        try {
            $db = getDB();
            $where = 'WHERE 1 ';

            if (!empty($_GET['condicao'])) {
                $condicao = $_GET['condicao'];
                $where .= " AND CONVERT(t.id, CHAR) LIKE '{$condicao}%'";
            }
            //die(json_encode($_SESSION));

            //if (!empty($_GET['id_responsavel'] and $_SESSION['tipo_usuario'] != 1)) {
            if(empty($_GET['empresa']) and empty($_GET['usuario_cli']) and empty($_GET['responsavel']) and ($_SESSION['tipo_usuario'] != 1)){
                $id_responsavel = $_SESSION['id_user'];
                $where .= " AND t.id_responsavel = {$id_responsavel}";
                $where .= " AND t.id_status != 3";
            }

            if(!empty($_GET['status_nao'])) {
                $status_nao = $_GET['status_nao'];
                $where .= " AND t.id_status != {$status_nao}";
            }


            if(!empty($_GET['status_tkt'])) {
                $status_tkt = $_GET['status_tkt'];
                switch ($status_tkt) {
                    case 'finalizados_tkt':
                        $where .= " AND t.id_status = 3";
                        $f_status = 'Finalizados';
                        break;
                    case 'pendentes_tkt':
                        $where .= " AND t.id_status != 3";
                        $f_status = 'Pendentes';
                        break;
                    default:
                    $f_status = 'Todos';
                }
            }

            
            if(!empty($_GET['empresa'])) {
                $empresa = '%'.$_GET['empresa'].'%';
                //$where .= " AND (e.nome_fantasia LIKE '{$empresa}' or e.cnpj like '{$empresa}' or e.razao_social LIKE '{$empresa}')";
                $where .= " AND c.id in (select uc.id from usu_cliente uc where uc.id_empresa in (select e2.id FROM empresas e2 where (e2.razao_social LIKE '{$empresa}' or e2.cnpj like '{$empresa}' or e2.nome_fantasia LIKE '{$empresa}')))";
                $f_empresa = $_GET['empresa'];
            }else{
                $f_empresa = 'Todas';
            }

            if(!empty($_GET['usuario_cli'])) {
                $usuario_cli = '%'.$_GET['usuario_cli'].'%';
                $where .= " AND (c.nome LIKE '{$usuario_cli}' or c.email1 LIKE '{$usuario_cli}' or c.email2 LIKE '{$usuario_cli}')";
                $f_usuario = $_GET['usuario_cli'];
            }else{
                $f_usuario = 'Todos';
            }

            if(!empty($_GET['responsavel'])) {
                $responsavel = '%'.$_GET['responsavel'].'%';
                $where .= " AND u.nome LIKE '{$responsavel}'";
                $f_responsavel = $_GET['responsavel'];
            }else{
                $f_responsavel = 'Todos';
            }

            $sql = "SELECT t.id, t.titulo, t.dt_abertura, t.dt_fechamento,st.descricao as status, 
            c.nome as usuario, t.id_responsavel, u.nome as responsavel ,ct.descricao as categoria, 
            e.razao_social, e.cnpj, t.dt_prevista, t.informacoes_gerais,
            t.gut, gg.descricao as gravidade, gu.descricao as urgencia, gt.descricao as tendencia
            FROM ticket as t          
            LEFT JOIN status_ticket as st ON st.id = t.id_status
            LEFT JOIN usu_cliente as c ON c.id = t.id_usucliente
            LEFT JOIN empresas as e ON e.id = c.id_empresa
            LEFT JOIN categoria_ticket as ct ON ct.id = t.id_categoria
            LEFT JOIN gut_gravidade as gg ON gg.id = t.gravidade 
            LEFT JOIN gut_urgencia as gu ON gu.id = t.urgencia
            LEFT JOIN gut_tendencia as gt ON gt.id = t.tendencia 
            LEFT JOIN usuario as u ON u.id = t.id_responsavel
            {$where}
            AND (t.id_cliente is null or t.id_cliente = 0)

            UNION 

            SELECT t.id, t.titulo, t.dt_abertura, t.dt_fechamento,st.descricao as status, 
            c.nome as usuario, t.id_responsavel, u.nome as responsavel ,ct.descricao as categoria, 
            e.razao_social, e.cnpj, t.dt_prevista, t.informacoes_gerais,
            t.gut, gg.descricao as gravidade, gu.descricao as urgencia, gt.descricao as tendencia
            FROM ticket as t          
            LEFT JOIN status_ticket as st ON st.id = t.id_status
            LEFT JOIN usu_cliente as c ON c.id = t.id_usucliente
            LEFT JOIN empresas as e ON e.id = t.id_cliente
            LEFT JOIN categoria_ticket as ct ON ct.id = t.id_categoria
            LEFT JOIN gut_gravidade as gg ON gg.id = t.gravidade 
            LEFT JOIN gut_urgencia as gu ON gu.id = t.urgencia
            LEFT JOIN gut_tendencia as gt ON gt.id = t.tendencia 
            LEFT JOIN usuario as u ON u.id = t.id_responsavel
            {$where}
            AND t.id_cliente > 0

            ORDER BY razao_social, id DESC";

            //die($sql);

            $stmt = $db->prepare($sql);
            $stmt->execute();
            $data = $stmt->fetchAll(PDO::FETCH_OBJ);


            $html = '<style>
                        table {
                            border-collapse: collapse;
                            border-spacing: 0;
                            width: 100%;
                          }
                          
                          thead tr {
                            background-color: #7a7a7a;
                            color: #ffffff;
                            text-align: center;
                          }
                          th, td {
                            font-size: 12px;
                            text-align: left;
                            padding: 8px;
                          }
                    </style>';
            $head = '<p align="center"><b>Relação de Tickets</p>';

            $head .= '<table style="width: 100%">';
            $head .= '<tr>';
        
            $head .= '<td align="justify"><b>Empresa:</b> ' . $f_empresa.'</th>';
            $head .= '<td align="justify">Usuário:</b> ' . $f_usuario.'</th>';
            $head .= '<td align="justify">Responsável:</b> ' . $f_responsavel.'</th>';
            $head .= '<td align="justify">Status:</b> ' . $f_status.'</th>';
            $head .= '</table>';

            $head .= '<hr>';


            $body = '<table style="width: 100%; border: 1px solid #ddd">';
            $body .= '<thead>';
            $body .= '<tr>';
            $body .= '<th rowspan="2">Ticket</th>';
            $body .= '<th>Título</th>';
            $body .= '<th scope="col">Abertura</th>';
            $body .= '<th scope="col">Prazo Finalização</th>';
            $body .= '<th scope="col">Fechamento</th>';
            $body .= '<th scope="col">Status</th>';
            $body .= '<th scope="col">Responsável</th>';
            $body .= '</tr>';
            $body .= '<tr>';
            $body .= '<th colspan="2">Informações Gerais</th>';
            $body .= '<th scope="col">GUT</th>';
            $body .= '<th scope="col">Gravidade</th>';
            $body .= '<th scope="col">Urgência</th>';
            $body .= '<th scope="col">Tendência</th>';
            $body .= '</tr>';
            $body .= '</thead>';
            $body .= '<tbody>';

            $fantasia = '';

            $contador = 1;

            foreach ($data as $key => $value) {
                $resto = fmod($contador, 2);
                if ($fantasia != $value->razao_social){
                    $fantasia = $value->razao_social;
                    $body .= '<tr>';
                
                    $body .= '<td colspan=7  style="background-color: #bcbcbc;"><b>Empresa:</b> ' . $value->cnpj. ' ' . $fantasia .'</th>';
                    $body .= '</tr>';
                }
                if($resto==0){
                    $body .= '<tr style="background-color: #f2f2f2;">';
                }else{
                    $body .= '<tr>';
                }
                
                $body .= '<td rowspan="2">' . $value->id . '</td>';
                $body .= '<td>' . $value->titulo . '</td>';
                $body .= '<td>' . formatDMYHHMMSS($value->dt_abertura) . '</td>';
                if (is_null($value->dt_prevista)){
                    $body .= '<td></td>';
                }else{
                    $body .= '<td>' . formatDMY($value->dt_prevista) . '</td>';
                }
                if (is_null($value->dt_fechamento)){
                    $body .= '<td></td>';
                }else{
                    $body .= '<td>' . formatDMYHHMMSS($value->dt_fechamento) . '</td>';
                }
                $body .= '<td>' . $value->status . '</td>';
                $body .= '<td>' . $value->responsavel . '</td>';
                $body .= '</tr>';
                if($resto==0){
                    $body .= '<tr style="background-color: #f2f2f2;">';
                }else{
                    $body .= '<tr>';
                }
                $body .= '<td colspan="2">' . $value->informacoes_gerais . '</td>';
                $body .= '<td>' . $value->gut . '</td>';
                $body .= '<td>' . $value->gravidade . '</td>';
                $body .= '<td>' . $value->urgencia . '</td>';
                $body .= '<td>' . $value->tendencia . '</td>';
                $body .= '</tr>';
                ++$contador;
            }

            $body .= '<tbody>';
            $body .= '</table>';

            $html .= $head;
            $html .= $body;
    
            $db = null;
    
            $pdf = new Dompdf();
            $pdf->set_option('isHtml5ParserEnabled', true);
            $pdf->set_paper("a4", "landscape");
            $pdf->loadHtml($html);
            $pdf->render();
            echo $pdf->output();
    
            header('Content-Type: application/pdf');
            die();
            if (sizeof($data) > 0) {
                die(json_encode(array(
                    "status" => 1,
                    "data" => $data,
                    "message" => "Tickets retornados com sucesso"
                )));
            } else {
                die(json_encode(array(
                    "status" => 2,
                    "message" => "Nenhum ticket encontrado"
                )));
            }

            $db = null;
        } catch (PDOException $e) {
            $error = '{"error":{"text":' . $e->getMessage() . '}}';
            die(json_encode(array(
                "status" => 500,
                "error" => $error,
                "message" => "Erro no servidor"
            )));
        }
       
    }

    public static function cadastrar_avaliacao(array $fields)
    {

        $fields['dt_envio'] = (new DateTime())->modify('-3 hours')->format('Y-m-d H:i:s');

        try {
            $db = getDB();
            $query = fieldsToInsertQuery($fields);
            //die( $fields['token']);
            $sql = "INSERT INTO avaliacao_ticket ({$query[0]}) VALUES ({$query[1]})";
            //die($sql);
            $stmt = $db->prepare($sql);

            $int = ['id_ticket'];
            foreach ($fields as $key => &$value) {
                $param = array_search($key, $int) ? PDO::PARAM_INT : PDO::PARAM_STR;
                $stmt->bindParam(':' . $key, $value, $param);
            }

            $stmt->execute();

            return [
                "status" => 1,
                "message" => "Ticket cadastrado com sucesso"
            ];

            $db = null;
    } catch (PDOException $e) {
            $error = '{"error":{"text":' . $e->getMessage() . '}}';
            die(json_encode(array(
                "status" => 500,
                "error" => $error,
                "message" => "Erro no servidor"
            )));
        }
    }
}