<?php


class Contratos
{
    //Status que o contrato é considerado ativo
    public static $status_ativos = [1, 2, 6];
    public static $status_faturar = [1, 2];

    public static function cadastrar(array $fields)
    {
        
        $now = new DateTime();
        $now->modify('-3 hours');

        $fields['dt_cadastro'] = $now->format('Y-m-d H:i:s');
        $fields['id_status'] = 1;

        $verifica = self::get_contrato_ativo($fields);
        if (!empty($verifica['data'])) {
            return [
                "status" => 2,
                "message" => 'Já existe um contrato do mesmo tipo vigente para o cliente',
                "http" => 400
            ];
        }


        $dt_vigenciaIni = new DateTime($fields['dt_vigenciainicial']);
        $dt_vigenciaFinal = new DateTime($fields['dt_vigenciafinal']);

        if($fields['tipo_contrato'] === 'manutencao' || $fields['tipo_contrato'] === 'manutencaosaas'){
            $meses = diff_meses($fields['dt_vigenciainicial'], $fields['dt_vigenciafinal']);
            $horas_contrato = $fields['horas_totais'];
            //die($meses);
            if($fields['horas_totais'] <= 0){
                return [
                    "status" => 2,
                    "message" => 'As horas do contrato não podem ser igual ou menor a 0.'
                ];
            }

            if ($meses > 0) {
                if ($meses < 12) {
                    $fields['horas_mes'] = ceil($horas_contrato / $meses);
                } else {
                    $fields['horas_mes'] = ceil($horas_contrato / 12);
                }
            } else {
                return [
                    "status" => 2,
                    "message" => 'Contrato com menos de 1 mês de validade',
                    "http" => 400
                ];
            }

        }

        //Verifica se a data de cadastro do contrato está dentro do período do contrato. Se sim, contrato ativo, caso contrário contrato inativo
        if ($now->getTimestamp() >= $dt_vigenciaIni->getTimestamp() && $now->getTimestamp() <= $dt_vigenciaFinal->getTimestamp()) {
            $fields['id_status'] = 1;
        } else if ($now->getTimestamp() < $dt_vigenciaIni->getTimestamp()) {
            $fields['id_status'] = 4;
        } else if ($now->getTimestamp() > $dt_vigenciaFinal->getTimestamp()) {
            $fields['id_status'] = 3;
        }

        //die('cadastrar 68');
        try {

            $db = getDB();

            $query = fieldsToInsertQuery($fields);
            $sql = "INSERT INTO contratos ({$query[0]}) VALUES ({$query[1]})";
            $stmt = $db->prepare($sql);

            //die($sql);
            $int = ['id_empresa', 'horas_totais', 'id_cadastrante', 'id_status', 'horas_mes'];
            foreach ($fields as $key => &$value) {
                $param = array_search($key, $int) ? PDO::PARAM_INT : PDO::PARAM_STR;
                $stmt->bindParam(':' . $key, $value, $param);
            }

            $stmt->execute();

            $id_contrato = $db->lastInsertId();

            //Verifica se o contrato tem status de vigente
            if (in_array($fields['id_status'], self::$status_ativos) && $fields['tipo_contrato'] === 'manutencao') {
                $minutos_mes = ceil($fields['horas_mes'] * 60);

                SaldoHoras::lancar(["id_empresa" => $fields['id_empresa'], 'minutos' => $minutos_mes]);

                $horas_recebido = [
                    "minutos" => $minutos_mes,
                    "id_contrato" => $id_contrato
                ];

                HorasAReceber::cadastrar_horas_recebidas_contrato($horas_recebido);


                $now = (new DateTime())->modify('-3 hours');
                $horas_a_receber = [
                    "dt_inicial" => $now->format('Y-m-d'),
                    "dt_final" => $fields['dt_vigenciafinal'],
                    "minutos" => $minutos_mes,
                    "id_contrato" => $id_contrato
                ];

                HorasAReceber::cadastrar($horas_a_receber);
            }

            if ($_FILES['anexo']['error'][0] === 0) {
                $anexos = Anexos::adicionar_anexo([
                    "id_contrato" => $id_contrato,
                ], 'anexo_contrato');
            }

            return array(
                "status" => 1,
                "message" => "Contrato cadastrado com sucesso",
                "http" => 200
            );

            $db = null;
        } catch (PDOException $e) {
            $error = '{"error":{"text":' . $e->getMessage() . '}}';
            http_response_code(500);
            die(json_encode(array(
                "status" => 500,
                "error" => $error,
                "message" => "Erro no servidor"
            )));
        }
    }

    public static function cadastrar_contrato_faturar(array $fields)
    {
        $now = new DateTime();
        $now->modify('-3 hours');

        $fields['dt_cadastro'] = $now->format('Y-m-d H:i:s');
        $fields['status'] = 1;

        try {

            $db = getDB();

            $query = fieldsToInsertQuery($fields);
            $sql = "INSERT INTO contratos_faturar ({$query[0]}) VALUES ({$query[1]})";
            $stmt = $db->prepare($sql);


            $int = ['horas_totais', 'id_cadastrante', 'id_status', 'horas_mes'];
            foreach ($fields as $key => &$value) {
                $param = array_search($key, $int) ? PDO::PARAM_INT : PDO::PARAM_STR;
                $stmt->bindParam(':' . $key, $value, $param);
            }

            $stmt->execute();

           // $id_contrato = $db->lastInsertId();

            return array(
                "status" => 1,
                "message" => "Contrato cadastrado com sucesso",
                "http" => 200
            );

            $db = null;
        } catch (PDOException $e) {
            $error = '{"error":{"text":' . $e->getMessage() . '}}';
            http_response_code(500);
            die(json_encode(array(
                "status" => 500,
                "error" => $error,
                "message" => "Erro no servidor"
            )));
        }
    }

    public static function get_contrato_ativo(array $fields)
    {

        $where = '';
        if (!empty($fields['tipo_contrato'])) {
            $where = "and tipo_contrato = '{$fields['tipo_contrato']}'";
        }

        $status_ativo = "(" . implode(',', self::$status_ativos) . ")";

        try {

            $db = getDB();

            $sql = "SELECT * FROM contratos WHERE id_empresa = :id_empresa and id_status in {$status_ativo} {$where}";
            $stmt = $db->prepare($sql);

            $stmt->bindParam('id_empresa', $fields['id_empresa'], PDO::PARAM_INT);
            $stmt->execute();

            $data = $stmt->fetch(PDO::FETCH_OBJ);


            if ($data) {
                return array(
                    "status" => 1,
                    "message" => "Contrato encontrado com sucesso",
                    "data" => $data
                );
            } else {
                return array(
                    "status" => 2,
                    "message" => "Nenhum contrato encontrado"
                );
            }

            $db = null;
        } catch (PDOException $e) {
            $error = '{"error":{"text":' . $e->getMessage() . '}}';
            http_response_code(500);
            die(json_encode(array(
                "status" => 500,
                "error" => $error,
                "message" => "Erro no servidor"
            )));
        }
    }

    public static function get_contrato(array $fields)
    {

        try {

            $db = getDB();

            $sql = "SELECT * FROM contratos WHERE id = :id_contrato";
            $stmt = $db->prepare($sql);

            $stmt->bindParam('id_contrato', $fields['id_contrato'], PDO::PARAM_INT);
            $stmt->execute();

            $data = $stmt->fetch(PDO::FETCH_OBJ);


            return $data;

            $db = null;
        } catch (PDOException $e) {
            $error = '{"error":{"text":' . $e->getMessage() . '}}';
            http_response_code(500);
            die(json_encode(array(
                "status" => 500,
                "error" => $error,
                "message" => "Erro no servidor"
            )));
        }
    }

    public static function get_contrato_faturar(array $fields)
    {

        try {

            $db = getDB();
            

            $sql = "SELECT * FROM contratos_faturar WHERE id = :id";
            $stmt = $db->prepare($sql);

            $stmt->bindParam('id', $fields['id'], PDO::PARAM_INT);
            $stmt->execute();

            $data = $stmt->fetch(PDO::FETCH_OBJ);


            return $data;

            $db = null;
        } catch (PDOException $e) {
            $error = '{"error":{"text":' . $e->getMessage() . '}}';
            http_response_code(500);
            die(json_encode(array(
                "status" => 500,
                "error" => $error,
                "message" => "Erro no servidor"
            )));
        }
    }

    public static function verificar_contratos()
    {
        try {

            $db = getDB();

            $sql = "SELECT * FROM contratos";
            $stmt = $db->prepare($sql);
            $stmt->execute();
            $data = $stmt->fetchAll(PDO::FETCH_OBJ);

            foreach ($data as $d) {
                self::verificar_contrato($d);
            }

            $db = null;
        } catch (PDOException $e) {
            $error = '{"error":{"text":' . $e->getMessage() . '}}';
            http_response_code(500);
            die(json_encode(array(
                "status" => 500,
                "error" => $error,
                "message" => "Erro no servidor"
            )));
        }
    }


    public static function verificar_contrato($d)
    {

        $now = (new DateTime())->modify('-3 hours');
        $vigencia_final = new DateTime($d->dt_vigenciafinal);
        $vigencia_inicial = new DateTime($d->dt_vigenciainicial);
        $less15Days = $vigencia_final->sub(new DateInterval('P15D'));

        $cliente = Clientes::get_empresa($d->id_empresa);

        if ($now->getTimestamp() > $vigencia_final->getTimestamp()) {
            $txt = "Contrato Vencido"
                . "<br>Cliente: " . $cliente->razao_social
                . "<br>CNPJ: " . $cliente->cnpj
                . "<br>Data de Vencimento: " . $vigencia_final->format('d/m/Y');
            enviar_email('joaoeduardomcmannis@gmail.com', "Contrato Vencido", $txt, 'João');
        } else if ($now->getTimestamp() > $less15Days->getTimestamp()) {
            $txt = "Faltam menos de 15 dias para vencimento do contrato do cliente"
                . "<br>Cliente: " . $cliente->razao_social
                . "<br>CNPJ: " . $cliente->cnpj
                . "<br>Data de Vencimento: " . $vigencia_final->format('d/m/Y');

            enviar_email('joaoeduardomcmannis@gmail.com', "Contrato vence em breve", $txt, 'João');
        } else if (
            $now->getTimestamp() < $vigencia_final->getTimestamp() &&
            $now->getTimestamp() > $vigencia_inicial->getTimestamp()
        ) {
        }
    }

    public static function get_contratos(array $fields)
    {
        try {

            $db = getDB();

            $sql = "SELECT c.id, c.dt_vigenciainicial, c.dt_vigenciafinal, c.id_empresa, c.valor_total, c.indice, c.tipo_contrato, sc.descricao as status_descricao,
                           c.horas_totais, c.obs, c.dt_cadastro, c.id_cadastrante, c.horas_mes, c.dt_proximoReajuste, 
                           c.empresa_contratada, c.periodicidade_pagamento, sc.id as id_status, c.multa, c.usuarios_saas, c.obs
                    FROM contratos as c
                    LEFT JOIN status_contrato as sc ON sc.id = c.id_status
                    WHERE id_empresa = :id_empresa";

            $stmt = $db->prepare($sql);

            $stmt->bindParam('id_empresa', $fields['id_empresa'], PDO::PARAM_INT);
            $stmt->execute();

            $data = $stmt->fetchAll(PDO::FETCH_OBJ);

            foreach ($data as $d) {
                $d->anexos = self::get_anexos_contratos(['id_contrato' => $d->id]);

                $d->adendos = Adendos::get_adendos_contratos(['id_contrato' => $d->id]);
            }

            return $data;

            $db = null;
        } catch (PDOException $e) {
            $error = '{"error":{"text":' . $e->getMessage() . '}}';
            http_response_code(500);
            die(json_encode(array(
                "status" => 500,
                "error" => $error,
                "message" => "Erro no servidor"
            )));
        }
    }

    public static function get_contratos_vencer()
    {
        try {
            $lastDateOfNextMonth =strtotime('last day of next month') ;
            $firstDateOfNextMonth =strtotime('first day of next month') ;
            $lastDateOfNextMonth=date ('Y-m-d', $lastDateOfNextMonth);
            $firstDateOfNextMonth=date ('Y-m-d', $firstDateOfNextMonth);
            $db = getDB();
            $sql = "SELECT e.razao_social , c.dt_vigenciainicial ,c.dt_vigenciafinal ,c.dt_proximoReajuste ,c.valor_total ,c.indice ,c.tipo_contrato, c.periodicidade_pagamento  FROM contratos c
            left join empresas e on e.id = c.id_empresa 
            WHERE c.dt_proximoReajuste BETWEEN '{$firstDateOfNextMonth}' and '{$lastDateOfNextMonth}'
            order by c.dt_proximoReajuste  ";

            //die($sql);
            $stmt = $db->prepare($sql);
            $stmt->execute();
            $data = $stmt->fetchAll(PDO::FETCH_OBJ);

            $db = null;
            return $data;
        } catch (PDOException $e) {
            $error = '{"error":{"text":' . $e->getMessage() . '}}';
            die(json_encode(array(
                "status" => 500,
                "error" => $error,
                "message" => "Erro no servidor"
            )));
        }
    }

    public static function get_contratos_faturar()
    {
        $status_faturar = "(" . implode(',', self::$status_faturar) . ")";

        try {
            $db = getDB();
            $sql = "select * from contratos_faturar cf 
            where cf.status in {$status_faturar}";

            //die($sql);
            $stmt = $db->prepare($sql);
            $stmt->execute();
            $data = $stmt->fetchAll(PDO::FETCH_OBJ);

            $db = null;
            return $data;
        } catch (PDOException $e) {
            $error = '{"error":{"text":' . $e->getMessage() . '}}';
            die(json_encode(array(
                "status" => 500,
                "error" => $error,
                "message" => "Erro no servidor"
            )));
        }
    }

    public static function get_qnt_usuariosSaaS(array $fields)
    {

        $status_ativo = "(" . implode(',', self::$status_ativos) . ")";

        try {

            $db = getDB();

            $sql = "SELECT c.usuarios_saas
                    FROM contratos as c
                    WHERE id_empresa = :id_empresa and (tipo_contrato = 'saas' or tipo_contrato = 'manutencao_saas')  and id_status in {$status_ativo}";

            $stmt = $db->prepare($sql);
            // die($sql);

            $stmt->bindParam('id_empresa', $fields['id_empresa'], PDO::PARAM_INT);
            $stmt->execute();

            $data = $stmt->fetchColumn();

            $data = $data === false ? 0 : $data;

            return $data;

            $db = null;
        } catch (PDOException $e) {
            $error = '{"error":{"text":' . $e->getMessage() . '}}';
            http_response_code(500);
            die(json_encode(array(
                "status" => 500,
                "error" => $error,
                "message" => "Erro no servidor"
            )));
        }
    }

    public static function get_contratos_manutencao_ativos()
    {
        $status_ativo = "(" . implode(',', self::$status_ativos) . ")";

        try {

            $db = getDB();

            $sql = "SELECT * FROM contratos WHERE id_status in {$status_ativo} and (tipo_contrato = 'manutencao' or tipo_contrato = 'manutencao_sass')";

            die($sql);
            $stmt = $db->prepare($sql);
            $stmt->execute();

            $data = $stmt->fetchAll(PDO::FETCH_OBJ);

            return $data;

            $db = null;
        } catch (PDOException $e) {
            $error = '{"error":{"text":' . $e->getMessage() . '}}';
            http_response_code(500);
            die(json_encode(array(
                "status" => 500,
                "error" => $error,
                "message" => "Erro no servidor"
            )));
        }
    }

    public static function recebeu_horas_mes_passado(array $fields)
    {

        $primeiro_dia_mes  = (new DateTime())->modify('first day of last month')->format('Y-m-d');
        $ultimo_dia_mes  = (new DateTime())->modify('last day of last month')->format('Y-m-d');

        try {

            $db = getDB();

            $sql = "SELECT * FROM horas_a_receber WHERE id_contrato = :id_contrato and data_receber >= '{$primeiro_dia_mes}' and data_receber <= '{$ultimo_dia_mes}'
                    and recebido = true and adiantado = false";

            // die($sql);

            $stmt = $db->prepare($sql);
            $stmt->bindParam('id_contrato', $fields['id_contrato'], PDO::PARAM_INT);
            $stmt->execute();

            $data = $stmt->fetch(PDO::FETCH_OBJ);

            return $data ? true : false;

            $db = null;
        } catch (PDOException $e) {
            $error = '{"error":{"text":' . $e->getMessage() . '}}';
            http_response_code(500);
            die(json_encode(array(
                "status" => 500,
                "error" => $error,
                "message" => "Erro no servidor"
            )));
        }
    }


    public static function verificacao_mensal()
    {
        $primeiro_dia_mes  = (new DateTime())->modify('first day of this month');
        $now = new DateTime();

        //Verifica se é o primeiro dia do mês
        if ($now->format('Y-m-d') === $primeiro_dia_mes->format('Y-m-d')) {

            $contratos = self::get_contratos_manutencao_ativos();

            foreach ($contratos as $d) {

                $minutos_mes = ceil($d->horas_mes * 60);
                
                $verifica = self::recebeu_horas_mes_passado(['id_contrato' => $d->id]);

                if ($verifica) {
                    $saldo_gasto = SaldoHoras::horas_gastas_no_mes_passado(['id_empresa' => $d->id_empresa]);

                    $restante = $minutos_mes - $saldo_gasto;

                    if ($restante > 0) {
                        $restante = $restante * -1;
                        SaldoHoras::lancar(['id_empresa' => $d->id_empresa, 'minutos' => $restante]);
                    }
                }

            }
            HorasAReceber::verificar();
        }
    }

    public static function get_anexos_contratos(array $fields)
    {
        try {

            $db = getDB();

            $sql = "SELECT * FROM anexo_contrato WHERE id_contrato = :id_contrato and excluido = 'N'";

            // die($sql);

            $stmt = $db->prepare($sql);
            $stmt->bindParam('id_contrato', $fields['id_contrato'], PDO::PARAM_INT);
            $stmt->execute();

            $data = $stmt->fetchAll(PDO::FETCH_OBJ);

            return $data ? $data : [];

            $db = null;
        } catch (PDOException $e) {
            $error = '{"error":{"text":' . $e->getMessage() . '}}';
            http_response_code(500);
            die(json_encode(array(
                "status" => 500,
                "error" => $error,
                "message" => "Erro no servidor"
            )));
        }
    }

    public static function editar(array $fields){

        $id_contrato = $fields['id_contrato'];
        unset($fields['id_contrato']);
        
        $contrato = self::get_contrato(['id_contrato' => $id_contrato]);

        if($contrato === false){
            return [
                'status' => 2,
                'message' => 'Contrato não existe'
            ];
        }

        $query = fieldsToEditQuery($fields);

        try {

            $db = getDB();

            $sql = "UPDATE contratos SET {$query} WHERE id = :id_contrato";

             //die($query);

            $stmt = $db->prepare($sql);

            $int = ['id_contrato', 'id_status', 'usuarios_saas', 'horas_mes'];
            foreach ($fields as $key => &$value) {
                $param = array_search($key, $int) ? PDO::PARAM_INT : PDO::PARAM_STR;
                $stmt->bindParam(':' . $key, $value, $param);
            }

            $stmt->bindParam('id_contrato', $id_contrato, PDO::PARAM_INT);
            $ok = $stmt->execute();

            if ($_FILES['anexo']['error'][0] === 0) {
                $anexos = Anexos::adicionar_anexo([
                    "id_contrato" => $id_contrato,
                ], 'anexo_contrato');
            }

            return [
                "status" => 1,
                'message' => 'Editado com sucesso'
            ];

            $db = null;
        } catch (PDOException $e) {
            $error = '{"error":{"text":' . $e->getMessage() . '}}';
            http_response_code(500);
            die(json_encode(array(
                "status" => 500,
                "error" => $error,
                "message" => "Erro no servidor"
            )));
        }

    }

    public static function editar_contrato_faturar(array $fields){

        $id = $fields['id'];

        $query = fieldsToEditQuery($fields);

        try {

            $contrato = self::get_contrato_faturar(['id' => $id]);

            $db = getDB();

            $sql = "UPDATE contratos_faturar SET {$query} WHERE id = {$id}";


            $stmt = $db->prepare($sql);

            $int = ['id_cadastrante', 'usuarios_saas', 'horas_mes'];
            foreach ($fields as $key => &$value) {
                $param = array_search($key, $int) ? PDO::PARAM_INT : PDO::PARAM_STR;
                $stmt->bindParam(':' . $key, $value, $param);
            }
            
            //die($sql);
            $stmt->execute();

            
            if ($fields['status'] == 3){
                //inserir novo cliente
                $email = $contrato->email || '';
                $empresa = [
                    'razao_social' => $contrato->razao_social,
                    'nome_fantasia' => $contrato->nome_fantasia,
                    'cnpj' => $contrato->cnpj,
                    'inscricao_estadual' => $contrato->inscricao_estadual,
                    'cep' => $contrato->cep,
                    'logradouro' => $contrato->logradouro,
                    'numero' => $contrato->numero,
                    'bairro' => $contrato->bairro,
                    'cidade' => $contrato->cidade,
                    'uf' => $contrato->uf,
                    'contato' => $contrato->contato,
                    'fone' => $contrato->fone,
                    'email1' => $email
                ];
                Empresas::cadastrar_empresa($empresa);
                $dados = Empresas::get_empresa_cnpj(['cnpj' => $contrato->cnpj]);
                //inserir contrato
                $dt_vigenciainicial = (new DateTime($contrato->dt_faturamento));
                $dt_vigenciafinal =  (new DateTime($contrato->dt_faturamento));
                $dt_proximoReajuste = (new DateTime($contrato->dt_faturamento));
                $dt_vigenciafinal = $dt_vigenciafinal->add(new DateInterval('P1Y'));
                $dt_proximoReajuste =  $dt_proximoReajuste->add(new DateInterval('P1Y'));
                $dt_proximoReajuste =  $dt_proximoReajuste->sub(new DateInterval('P1M'));
                
                $contrato = [
                'dt_vigenciafinal' => $dt_vigenciafinal->format('Y-m-d'),
                'dt_vigenciainicial' => $dt_vigenciainicial->format('Y-m-d'),
                'horas_totais' => $contrato->horas_totais,
                'id_cadastrante' => $contrato->id_cadastrante,
                'id_empresa' =>$dados->id,
                'indice' => $contrato->indice,
                'multa' => $contrato->multa,
                'tipo_contrato' => $contrato->tipo_contrato,
                'valor_total' => $contrato->valor_total,
                'empresa_contratada' => $contrato->empresa_contratada,
                'periodicidade_pagamento' => $contrato->periodicidade_pagamento,
                'usuarios_saas' => $contrato->usuarios_saas,
                'dt_proximoReajuste' => $dt_proximoReajuste->format('Y-m-d')
                ];
                //die(json_encode($contrato));
                Contratos::cadastrar($contrato);
            }

            //die(json_encode($contrato->razao_social));
            if ($fields['status'] == 2 && $contrato->status == 1){
                $Notificar = get_usuarioSetor(8);
                //die(json_encode($Notificar));
                $dt_faturamento = (new DateTime($contrato->dt_faturamento))->format('d/m/Y');
                $razao_social = $contrato->razao_social;
                foreach ($Notificar as $value){
                    Notificacoes::add_notifcacao([
                        "descricao" => "Contrato a faturar enviado para faturamento: Empresa {$razao_social} Data Faturamento {$dt_faturamento}",
                        "id_usuario" => "{$value->id}"
                    ]);
                }
            }

            return [
                "status" => 1,
                'message' => 'Editado com sucesso'
            ];

            $db = null;
        } catch (PDOException $e) {
            $error = '{"error":{"text":' . $e->getMessage() . '}}';
            http_response_code(500);
            die(json_encode(array(
                "status" => 500,
                "error" => $error,
                "message" => "Erro no servidor"
            )));
        }

    }
    public static function get_contratos_ativos_empresa(array $fields)
    {
        $status_ativo = "(" . implode(',', self::$status_ativos) . ")";
        try {

            $db = getDB();

            $sql = "SELECT * FROM contratos WHERE id_status in {$status_ativo} and id_empresa = :id_empresa";

            $stmt = $db->prepare($sql);
            $stmt->bindParam('id_empresa', $fields['id_empresa'], PDO::PARAM_INT);
            $stmt->execute();

            $data = $stmt->fetchAll(PDO::FETCH_OBJ);

            return $data;

            $db = null;
        } catch (PDOException $e) {
            $error = '{"error":{"text":' . $e->getMessage() . '}}';
            http_response_code(500);
            die(json_encode(array(
                "status" => 500,
                "error" => $error,
                "message" => "Erro no servidor"
            )));
        }
    }

    public static function excluir(array $fields)
    {
        $id_contrato = $fields['id_contrato'];

        try {
            $db = getDB();

            $sql = "DELETE FROM adendos WHERE id_contrato = :id_contrato";

            $stmt = $db->prepare($sql);
            $stmt->bindParam('id_contrato', $id_contrato, PDO::PARAM_INT);
            $stmt->execute();

            /*return [
                "status" => 1,
                "message" => "Adendo excluido com sucesso"
            ];*/

            $db = null;
        } catch (PDOException $e) {
            $error = '{"error":{"text":' . $e->getMessage() . '}}';
            die(json_encode(array(
                "status" => 500,
                "error" => $error,
                "message" => "Erro no servidor"
            )));
        }


        try {
            $db = getDB();

            $sql = "DELETE FROM adendos WHERE id_contrato = :id_contrato";

            $stmt = $db->prepare($sql);
            $stmt->bindParam('id_contrato', $id_contrato, PDO::PARAM_INT);
            $stmt->execute();

            /*return [
                "status" => 1,
                "message" => "Adendo excluido com sucesso"
            ];*/

            $db = null;
        } catch (PDOException $e) {
            $error = '{"error":{"text":' . $e->getMessage() . '}}';
            die(json_encode(array(
                "status" => 500,
                "error" => $error,
                "message" => "Erro no servidor"
            )));
        }

        try {
            $db = getDB();

            $sql = "DELETE FROM contratos WHERE id = :id_contrato";

            //die($id_contrato);
            $stmt = $db->prepare($sql);
            $stmt->bindParam('id_contrato', $id_contrato, PDO::PARAM_INT);
            $stmt->execute();

            return [
                "status" => 1,
                "message" => "Contrato excluido com sucesso"
            ];

            $db = null;
        } catch (PDOException $e) {
            $error = '{"error":{"text":' . $e->getMessage() . '}}';
            die(json_encode(array(
                "status" => 500,
                "error" => $error,
                "message" => "Erro no servidor"
            )));
        }
    }

    public static function excluir_contrato_faturar(array $fields)
    {
        $id = $fields['id'];

        try {
            $db = getDB();

            $sql = "DELETE FROM contratos_faturar WHERE id = :id";

            //die($id_contrato);
            $stmt = $db->prepare($sql);
            $stmt->bindParam('id', $id, PDO::PARAM_INT);
            $stmt->execute();

            return [
                "status" => 1,
                "message" => "Contrato excluido com sucesso"
            ];

            $db = null;
        } catch (PDOException $e) {
            $error = '{"error":{"text":' . $e->getMessage() . '}}';
            die(json_encode(array(
                "status" => 500,
                "error" => $error,
                "message" => "Erro no servidor"
            )));
        }
    }

}
