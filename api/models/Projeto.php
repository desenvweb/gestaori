<?php


class Projeto
{
    public static function get_projeto($id)
    {
        try {
            $db = getDB();

            $sql = "SELECT * FROM projeto WHERE id = :id";
            $stmt = $db->prepare($sql);

            $stmt->bindParam('id', $id, PDO::PARAM_INT);
            $stmt->execute();
            $data = $stmt->fetchAll(PDO::FETCH_OBJ);

            if (sizeof($data) > 0) {
                return  $data;
            }

            return false;

            $db = null;
        } catch (PDOException $e) {
            $error = '{"error":{"text":' . $e->getMessage() . '}}';
            die(json_encode(array(
                "status" => 500,
                "error" => $error,
                "message" => "Erro no servidor"
            )));
        }
    }

    public static function get_projetos_cliente($id)
    {
        try {
            $db = getDB();

            $sql = "SELECT * FROM projeto WHERE id_cliente = :id";
            $stmt = $db->prepare($sql);

            $stmt->bindParam('id', $id, PDO::PARAM_INT);
            $stmt->execute();
            $data = $stmt->fetchAll(PDO::FETCH_OBJ);

            if (sizeof($data) > 0) {

                $total_projetos = 0;
                $qnt = 0;

                foreach ($data as $d) {
                    $total_projetos = $total_projetos + $d->total_horas;
                    $qnt++;
                }

                $data['info']->horas_totais_projetos = $total_projetos;
                $data['info']->totais_projetos = $qnt;
                
                return  $data;
            }

            return false;

            $db = null;
        } catch (PDOException $e) {
            $error = '{"error":{"text":' . $e->getMessage() . '}}';
            die(json_encode(array(
                "status" => 500,
                "error" => $error,
                "message" => "Erro no servidor"
            )));
        }
    }

    public static function get_projetos_descontados_empresa(array $fields)
    {
        try {
            $db = getDB();

            $sql = "SELECT SUM(total_horas) as total_horas, COUNT(*) as total_projetos from projeto
                    WHERE id_empresa = :id_empresa and desconta_horas = 'S'";
            $stmt = $db->prepare($sql);

            $stmt->bindParam('id_empresa', $fields['id_empresa'], PDO::PARAM_INT);
            $stmt->execute();
            $data = $stmt->fetch(PDO::FETCH_OBJ);

            $data->total_horas = $data->total_horas === NULL ? $data->total_horas = 0 : $data->total_horas;

            return  $data;

            $db = null;
        } catch (PDOException $e) {
            $error = '{"error":{"text":' . $e->getMessage() . '}}';
            die(json_encode(array(
                "status" => 500,
                "error" => $error,
                "message" => "Erro no servidor"
            )));
        }
    }

    public static function get_projetos($fields = [])
    {
        $pagina = isset($fields['pagina']) ? $fields['pagina']  : 0;
        $limite = isset($fields['limite']) ? $fields['limite'] : 10;

        $qnt = $pagina * $limite;

        $where = '';
        if (!empty($fields['condicao'])) {
            $condicao = $fields['condicao'];
            $where .= "WHERE (p.nome LIKE '%{$condicao}%')";
        }

        try {
            $db = getDB();

            $sql = "SELECT p.id, p.total_horas, p.prev_termino, p.descricao, c.nome_fantasia as cliente,
                    u.nome as responsavel, ua.nome as comercial, ub.nome as executante, p.nome, s.nome as status,
                    p.id_responsavel, p.id_comercial, p.id_executante, p.id_empresa, p.id_status, p.desconta_horas FROM projeto as p
                    LEFT JOIN cliente as c ON c.id = p.id_empresa
                    LEFT JOIN usuario as u ON u.id = p.id_responsavel
                    LEFT JOIN usuario as ua ON ua.id = p.id_comercial
                    LEFT JOIN usuario as ub ON ub.id = p.id_executante
                    LEFT JOIN status as s ON s.id = p.id_status
                    {$where}
                    LIMIT :LIMITE OFFSET :QTD";

            $stmt = $db->prepare($sql);
            $stmt->bindParam('LIMITE', $limite, PDO::PARAM_INT);
            $stmt->bindParam('QTD', $qnt, PDO::PARAM_INT);
            $stmt->execute();
            $data = $stmt->fetchAll(PDO::FETCH_OBJ);

            return $data;

            $db = null;
        } catch (PDOException $e) {
            $error = '{"error":{"text":' . $e->getMessage() . '}}';
            die(json_encode(array(
                "status" => 500,
                "error" => $error,
                "message" => "Erro no servidor"
            )));
        }
    }
}
