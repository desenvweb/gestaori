<?php


class SaldoHoras
{

    public static function lancar(array $fields)
    {
        
        $now = (new DateTime())->modify('-3 hours')->format("Y-m-d H:i:s");
        $fields['data_movimentacao'] = $now;

        try {

            $db = getDB();

            $query = fieldsToInsertQuery($fields);
            $sql = "INSERT INTO saldo_horas ({$query[0]}) VALUES ({$query[1]})";
            $stmt = $db->prepare($sql);

            $int = ['id_empresa', 'minutos'];
            foreach ($fields as $key => &$value) {
                $param = array_search($key, $int) ? PDO::PARAM_INT : PDO::PARAM_STR;
                $stmt->bindParam(':' . $key, $value, $param);
            }

            $stmt->execute();

            return array(
                "status" => 1,
                "message" => "Horas lançadas com sucesso",
                "http" => 200,
            );

            $db = null;
        } catch (PDOException $e) {
            $error = '{"error":{"text":' . $e->getMessage() . '}}';
            http_response_code(500);
            die(json_encode(array(
                "status" => 500,
                "error" => $error,
                "message" => "Erro no servidor"
            )));
        }
    }

    // metodo criado para editar as horas
    public static function editar(array $fields)
    {
        $db = getDB();

        $id = $fields['id_projeto'];
        $minutos = $fields['minutos'];
       
     
      
        $query = fieldsToEditQuery($fields);
        $sql = "UPDATE saldo_horas SET minutos = $minutos  WHERE id_projeto ='$id'";
        $stmt = $db->prepare($sql);
       
        $stmt->execute();

    }

    public static function get_saldo_empresa(array $fields)
    {

        try {

            $db = getDB();

            $sql = "SELECT SUM(minutos) as saldo FROM saldo_horas WHERE id_empresa = :id_empresa";
            $stmt = $db->prepare($sql);
            $stmt->bindParam("id_empresa", $fields['id_empresa'], PDO::PARAM_INT);

            $stmt->execute();
            $data = $stmt->fetchColumn();

            return $data ?? 0;

            $db = null;
        } catch (PDOException $e) {
            $error = '{"error":{"text":' . $e->getMessage() . '}}';
            http_response_code(500);
            die(json_encode(array(
                "status" => 500,
                "error" => $error,
                "message" => "Erro no servidor"
            )));
        }
    }

    public static function horas_gastas_no_mes_passado(array $fields)
    {
        try {

            $primeiro_dia_mes  = (new DateTime())->modify('first day of last month');
            $ultimo_dia_mes = (new DateTime())->modify('last day of last month');

            $primeiro_dia_mes = $primeiro_dia_mes->format('Y-m-d');
            $ultimo_dia_mes = $ultimo_dia_mes->format('Y-m-d');


            $db = getDB();

            $sql = "SELECT SUM(sd.minutos) FROM saldo_horas as sd 
                WHERE data_movimentacao >= '{$primeiro_dia_mes}' and data_movimentacao <= '{$ultimo_dia_mes}'
                and sd.minutos < 0 and id_empresa = :id_empresa";


            $stmt = $db->prepare($sql);
            $stmt->bindParam('id_empresa', $fields['id_empresa'], PDO::PARAM_INT);
            $stmt->execute();

            $data = $stmt->fetchColumn();

            return abs($data);
        } catch (PDOException $e) {
            $error = '{"error":{"text":' . $e->getMessage() . '}}';
            http_response_code(500);
            die(json_encode(array(
                "status" => 500,
                "error" => $error,
                "message" => "Erro no servidor"
            )));
        }
    }
}
