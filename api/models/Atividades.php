<?php



class Atividades
{

    public static function get_atividades()
    {

        try {
            $db = getDB();

            $sql = "SELECT * FROM atividade";

            $stmt = $db->prepare($sql);
            $stmt->execute();
            $data = $stmt->fetchAll(PDO::FETCH_OBJ);

            $db = null;

            if (sizeof($data) > 0) {
                return [
                    "status" => 1,
                    "data" => $data,
                    "message" => 'Atividades retornadas com sucesso'
                ];
            } else {
                return [
                    "status" => 2,
                    "message" => 'Nenhuma atividade encontrada'
                ];
            }
        } catch (PDOException $e) {
            $error = '{"error":{"text":' . $e->getMessage() . '}}';
            http_response_code(500);
            die(json_encode(array(
                "status" => 500,
                "error" => $error,
                "message" => "Erro no servidor"
            )));
        }
    }
}
