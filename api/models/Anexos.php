<?php

class Anexos
{
    public static function adicionar_anexo($fields, $tabela)
    {
        $arquivos = $_FILES['anexo'];

        for ($i = 0; $i < count($arquivos['name']); $i++) {
            $path_file = pathinfo($arquivos['name'][$i]);

            $nome_path = uniqid('file');
            $extensao = $path_file['extension'];
            $url = $nome_path . '.' . $extensao;
            $path = dirname(__FILE__) . "/../../anexos/" . $url;

            //campos padrão de tabela de anexos
            $fields['nome'] = $path_file['filename'];
            $fields['data_cadastro'] = date('Y-m-d H:i:s', time() - 3 * 60 * 60);
            $fields['path'] = $path;
            $fields['excluido'] = 'N';
            $fields['extensao'] = $extensao;
            $fields['link'] = "{$_SERVER['HTTP_ORIGIN']}/anexos/{$url}";

            $query = fieldsToInsertQuery($fields);

            try {
                $db = getDB();
                
                if (move_uploaded_file($arquivos['tmp_name'][$i], $path)) {
                    
                    $sql = "INSERT INTO {$tabela} ({$query[0]})
                            VALUES ({$query[1]})";

                    $stmt = $db->prepare($sql);

                    $int = ['id_comentario'];
                    
                    foreach ($fields as $key => &$value) {
                        $param = array_search($key, $int) ? PDO::PARAM_INT : PDO::PARAM_STR;
                        $stmt->bindParam(':' . $key, $value, $param);
                    }
                    
                    $stmt->execute();
                    
                } else {
                    return array(
                        "status" => 2,
                        "message" => 'Não foi possível fazer upload do anexo ' . $i,
                        'error' => $_FILES['map']['error'][$i]
                    );
                }

                $db = null;
            } catch (PDOException $e) {
                $error = '{"error":{"text":' . $e->getMessage() . '}}';
                return (array(
                    "status" => 500,
                    "error" => $error,
                    "message" => "Erro no servidor"
                ));
            }

        }
            return (array(
                "status" => 1,
                "message" => 'Anexo(s) salvo(s)'
            ));
    }

}
