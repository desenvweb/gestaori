<?php


class Empresas
{

    public static function get_empresas(array $fields)
    {
        $pagina = isset($fields['pagina']) ? $fields['pagina']  : 0;
        $limite = isset($fields['limite']) ? $fields['limite'] : 10;

        $qnt = $pagina * $limite;

        $where = 'WHERE 1';
        $join = '';

        //die(json_encode($fields));

        if (!empty($fields['contrato'])) {
            $contrato = $fields['contrato'];
            $where .= " and (e.contrato = '{$contrato}' )";
        }

        if (!empty($fields['tpcontrato'])) {
            $tpcontrato = $fields['tpcontrato'];
            if ($tpcontrato != 'todos'){
                $join = 'left join contratos c on c.id_empresa = e.id';
                $where .= " and (c.tipo_contrato like '{$tpcontrato}' )";
            }
        }

        if (!empty($fields['condicao'])) {
            $condicao = $fields['condicao'];
            $where .= " and (e.cnpj LIKE '%{$condicao}%' or e.nome_fantasia LIKE '%{$condicao}%' 
                            or e.razao_social LIKE '%{$condicao}%')";
        }

       if (!empty($fields['contratada'])) {
            $contratada = $fields['contratada'];
            $join = 'left join contratos c on c.id_empresa = e.id';
            $where .= " and (c.empresa_contratada = '{$contratada}')";
        }

        if (!empty($fields['periodicidade_pagamento_filtro'])) {
            $periodicidade_pagamento = $fields['periodicidade_pagamento_filtro'];
            $join = 'left join contratos c on c.id_empresa = e.id';
            $where .= " and (c.periodicidade_pagamento = '{$periodicidade_pagamento}')";
        }

        if (!empty($fields['dtvgfinal_inicio']) && strlen($fields['dtvgfinal_inicio'])>0) {
            $dtvgfinal_inicio = $fields['dtvgfinal_inicio'];
            $join = 'left join contratos c on c.id_empresa = e.id';
            $where .= " and (c.dt_vigenciainicial >= '{$dtvgfinal_inicio}')";
        }

        if (!empty($fields['dtvgfinal_fim'])) {
            $dtvgfinal_fim = $fields['dtvgfinal_fim'];
            $join = 'left join contratos c on c.id_empresa = e.id';
            $where .= " and (c.dt_vigenciafinal <= '{$dtvgfinal_inicio}')";
        }

        try {

            $db = getDB();

            $sql = "SELECT e.* FROM empresas e
                    {$join}
                    {$where}
                    LIMIT :LIMITE OFFSET :QTD";
            
            $stmt = $db->prepare($sql);
            $stmt->bindParam('LIMITE', $limite, PDO::PARAM_INT);
            $stmt->bindParam('QTD', $qnt, PDO::PARAM_INT);

            //die($sql);
            $stmt->execute();

            $data = $stmt->fetchAll(PDO::FETCH_OBJ);

            foreach ($data as $d) {
                $contrato = Contratos::get_contratos(['id_empresa' => $d->id]);
                $d->contratos = $contrato ?? false;

                $horas_compradas = HorasCompradas::get_horas_empresa(['id_empresa' => $d->id]);
                $d->horas_compradas = $horas_compradas ?? false;

                $d->qnt_usuariosSaaS =  Contratos::get_qnt_usuariosSaaS(['id_empresa' => $d->id]);

                $minutos_saldo = SaldoHoras::get_saldo_empresa(['id_empresa' => $d->id]);
                $hhmm_saldo = $minutos_saldo > 0 ? convert_minutes_in_hhmm($minutos_saldo) : convert_minutes_in_hhmm();
                $d->saldo_horas = $hhmm_saldo;
            }

            return $data;

            $db = null;
        } catch (PDOException $e) {
            $error = '{"error":{"text":' . $e->getMessage() . '}}';
            http_response_code(500);
            die(json_encode(array(
                "status" => 500,
                "error" => $error,
                "message" => "Erro no servidor"
            )));
        }
    }


    public static function get_empresas_com_horas(array $fields)
    {
       
        $pagina = isset($fields['pagina']) ? $fields['pagina']  : 0;
        $limite = isset($fields['limite']) ? $fields['limite'] : 10;

        $qnt = $pagina * $limite;

        $where = '';

        if (!empty($fields['condicao'])) {
            $condicao = $fields['condicao'];
            $where .= "WHERE (cnpj LIKE '%{$condicao}%' or nome_fantasia LIKE '%{$condicao}%' 
                            or razao_social LIKE '%{$condicao}%')";
        }

        try {

            $db = getDB();

            $sql = "SELECT SUM(minutos) as minutos, e.id, e.nome_fantasia, e.cnpj, e.logradouro, e.contato, e.obs, e.razao_social,
                    e.inscricao_estadual, e.fone, e.email1, e.email2,  e.dt_cadastro FROM saldo_horas as sh
                    LEFT JOIN empresas as e ON e.id = sh.id_empresa
                    {$where}
                    GROUP BY id_empresa
                    HAVING SUM(minutos) > 0
                    LIMIT :LIMITE OFFSET :QTD";

            $stmt = $db->prepare($sql);
            $stmt->bindParam('LIMITE', $limite, PDO::PARAM_INT);
            $stmt->bindParam('QTD', $qnt, PDO::PARAM_INT);

            $stmt->execute();
  
            $data = $stmt->fetchAll(PDO::FETCH_OBJ);

            foreach ($data as $key => $d) {
                $contrato = Contratos::get_contratos(['id_empresa' => $d->id]);
                $d->contratos = $contrato ?? false;

                $horas_compradas = HorasCompradas::get_horas_empresa(['id_empresa' => $d->id]);
                $d->horas_compradas = $horas_compradas ?? false;

                $d->qnt_usuariosSaaS =  Contratos::get_qnt_usuariosSaaS(['id_empresa' => $d->id]);

                $minutos_saldo = SaldoHoras::get_saldo_empresa(['id_empresa' => $d->id]);


                $hhmm_saldo = $minutos_saldo > 0 ? convert_minutes_in_hhmm($minutos_saldo) : convert_minutes_in_hhmm();
                $d->saldo_horas = $hhmm_saldo;
            }

            
            return $data;

            $db = null;
        } catch (PDOException $e) {
            $error = '{"error":{"text":' . $e->getMessage() . '}}';
            http_response_code(500);
            die(json_encode(array(
                "status" => 500,
                "error" => $error,
                "message" => "Erro no servidor"
            )));
        }
    }

    public static function get_empresa(array $fields)
    {
        try {

            $db = getDB();

            $sql = "SELECT * FROM empresas as e
                    LEFT JOIN contratos as c ON c.id_empresa = e.id
                    WHERE e.id = :id_empresa";

            $stmt = $db->prepare($sql);
            $stmt->bindParam('id_empresa', $fields['id_empresa'], PDO::PARAM_INT);

            //die($sql);
            $stmt->execute();

            $data = $stmt->fetch(PDO::FETCH_OBJ);

            return $data;

            $db = null;
        } catch (PDOException $e) {
            $error = '{"error":{"text":' . $e->getMessage() . '}}';
            http_response_code(500);
            die(json_encode(array(
                "status" => 500,
                "error" => $error,
                "message" => "Erro no servidor"
            )));
        }
    }

    public static function get_empresa_cnpj(array $fields)
    {
        try {

            $db = getDB();

            $sql = "SELECT * FROM empresas as e
                    WHERE e.cnpj = :cnpj";

            $stmt = $db->prepare($sql);
            $stmt->bindParam('cnpj', $fields['cnpj'], PDO::PARAM_STR);

            //die($sql);
            $stmt->execute();

            $data = $stmt->fetch(PDO::FETCH_OBJ);

            return $data;

            $db = null;
        } catch (PDOException $e) {
            $error = '{"error":{"text":' . $e->getMessage() . '}}';
            http_response_code(500);
            die(json_encode(array(
                "status" => 500,
                "error" => $error,
                "message" => "Erro no servidor"
            )));
        }
    }

    public static function editar_empresa($fields = [])
    {

        $id = $fields['id'];

        $query = fieldsToEditQuery($fields);

        try {
            $verifica = self::get_empresa(['id_empresa' => $id]);

            if (sizeof($verifica) > 0) {
                $db = getDB();

                $sql = "UPDATE empresas
                    SET {$query}
                    WHERE id = :id_empresa;";

                $stmt = $db->prepare($sql);

                $int = [];

                foreach ($fields as $key => &$value) {
                    $param = array_search($key, $int) ? PDO::PARAM_INT : PDO::PARAM_STR;
                    $stmt->bindParam(':' . $key, $value, $param);
                }

                $stmt->bindParam('id_empresa', $id, PDO::PARAM_INT);

                $stmt->execute();

                return [
                    "status" => 1,
                    "message" => "Cliente editado"
                ];

                $db = null;
            } else {
                return [
                    "status" => 2,
                    "message" => "Cliente não encontrado"
                ];
            }
        } catch (PDOException $e) {
            $error = '{"error":{"text":' . $e->getMessage() . '}}';
            die(json_encode(array(
                "status" => 500,
                "error" => $error,
                "message" => "Erro no servidor"
            )));
        }
    }

    public static function cadastrar_empresa($fields = [])
    {

        $now = (new DateTime())->modify('-3 hours');
        $fields['dt_cadastro'] = $now->format('Y-m-d H:i:s');

        try {
            $db = getDB();

            $query = fieldsToInsertQuery($fields);
            $sql = "INSERT INTO empresas ({$query[0]}) VALUES ({$query[1]})";

            $stmt = $db->prepare($sql);

            $int = [];
            foreach ($fields as $key => &$value) {
                $param = array_search($key, $int) ? PDO::PARAM_INT : PDO::PARAM_STR;
                $stmt->bindParam(':' . $key, $value, $param);
            }

            $stmt->execute();


            return [
                "status" => 1,
                "message" => "Cliente cadastrado"
            ];

            $db = null;
        } catch (PDOException $e) {
            $error = '{"error":{"text":' . $e->getMessage() . '}}';
            die(json_encode(array(
                "status" => 500,
                "error" => $error,
                "message" => "Erro no servidor"
            )));
        }
    }

    public static function excluir_empresa(array $fields)
    {

        $id_empresa = $fields['id_empresa'];

        if ($id_empresa == 1){
            return [
                'status' => 2,
                'message' => 'Empresa não pode ser excluida !!'
            ] ;
        }else{

            $contratos = Contratos::get_contratos_ativos_empresa(['id_empresa' => $id_empresa]);

            if(sizeof($contratos) > 0){

            
                //verificando se realmente contrato está ativo
                $vigencia_final = new DateTime($contratos->dt_vigenciafinal);
                $now = (new DateTime())->modify('-3 hours');

                if ($now->getTimestamp() > $vigencia_final->getTimestamp()) {

                    return [
                        'status' => 2,
                        'message' => 'Empresa não pode ser excluida tendo contratos ativos.'
                    ] ;
                }
            }

            $tickets_empresa = self::get_ticket_empresa(['id_empresa'  => $id_empresa]);


            if(sizeof($tickets_empresa) > 0){
                return [
                    'status' => 2,
                    'message' => "Empresa não pode ser excluida tendo Tickets vinculados a ela"
                ];
            }
            
            $usu_helpdesk = self::get_usuarios_helpdesk(['id_empresa'  => $id_empresa]);

            if(sizeof($usu_helpdesk) > 0){

                /*return [
                    'status' => 2,
                    'message' => "Empresa não pode ser excluida tendo usuários do HelpDesk vinculados a ela"
                ];*/
                try {
                    $db = getDB();
        
                    $sql = "DELETE FROM usu_cliente WHERE id_empresa = :id_empresa";
        
                    $stmt = $db->prepare($sql);
                    $stmt->bindParam('id_empresa', $id_empresa, PDO::PARAM_INT);
                    $stmt->execute();
        
                    $db = null;
                } catch (PDOException $e) {
                    $error = '{"error":{"text":' . $e->getMessage() . '}}';
                    die(json_encode(array(
                        "status" => 500,
                        "error" => $error,
                        "message" => "Erro no servidor"
                    )));
                }
            }
            

            $saldo_empresa = SaldoHoras::get_saldo_empresa(['id_empresa' => $id_empresa]);
            if($saldo_empresa > 0){
                /*return [
                    'status' => 2,
                    'message' => 'Empresa não pode ser excluida tendo saldo de horas disponível.'
                ];*/
                try {
                    $db = getDB();
        
                    $sql = "DELETE FROM saldo_horas WHERE id_empresa = :id_empresa";
        
                    $stmt = $db->prepare($sql);
                    $stmt->bindParam('id_empresa', $id_empresa, PDO::PARAM_INT);
                    $stmt->execute();
        
                    $db = null;
                } catch (PDOException $e) {
                    $error = '{"error":{"text":' . $e->getMessage() . '}}';
                    die(json_encode(array(
                        "status" => 500,
                        "error" => $error,
                        "message" => "Erro no servidor"
                    )));
                }
            }

            try {
                $db = getDB();

                $sql = "DELETE FROM horas_compradas WHERE id_empresa = :id_empresa";

                $stmt = $db->prepare($sql);
                $stmt->bindParam('id_empresa', $id_empresa, PDO::PARAM_INT);
                $stmt->execute();

                $db = null;
            } catch (PDOException $e) {
                $error = '{"error":{"text":' . $e->getMessage() . '}}';
                die(json_encode(array(
                    "status" => 500,
                    "error" => $error,
                    "message" => "Erro no servidor"
                )));
            }
            
            $horastrab = HorasTrab::get_horastrab_empresa(['id_empresa' => $id_empresa]);
            if(sizeof($horastrab)){
                return [
                    'status' => 2,
                    'message' => 'Empresa não pode ser excluida porquê tem horas lançadas pra ela.'
                ];
            }

            try {
                $db = getDB();

                $sql = "DELETE FROM empresas WHERE id = :id_empresa";

                $stmt = $db->prepare($sql);
                $stmt->bindParam('id_empresa', $id_empresa, PDO::PARAM_INT);
                $stmt->execute();

                return [
                    "status" => 1,
                    "message" => "Cliente excluido com sucesso"
                ];

                $db = null;
            } catch (PDOException $e) {
                $error = '{"error":{"text":' . $e->getMessage() . '}}';
                die(json_encode(array(
                    "status" => 500,
                    "error" => $error,
                    "message" => "Erro no servidor"
                )));
            }
        }
    }


    public static function get_usuarios_helpdesk(array $fields){

        try {
            $db = getDB();
    
            $sql = "SELECT * FROM usu_cliente WHERE id_empresa = :id_empresa";
    
            $stmt = $db->prepare($sql);
            $stmt->bindParam("id_empresa", $fields['id_empresa'], PDO::PARAM_INT);
            $stmt->execute();
            $data = $stmt->fetchAll(PDO::FETCH_OBJ);
    
            return $data;
        } catch (PDOException $e) {
            $error = '{"error":{"text":' . $e->getMessage() . '}}';
            die(json_encode(array(
                "status" => 500,
                "error" => $error,
                "message" => "Erro no servidor"
            )));
        }
        
    }

    public static function get_ticket_empresa(array $fields){

        try {
            $db = getDB();
    
            $sql = "select t.id from ticket t where t.id_cliente = :id_empresa or t.id_usucliente in (select uc.id from usu_cliente uc where uc.id_empresa = :id_empresa)";
    
            $stmt = $db->prepare($sql);
            $stmt->bindParam("id_empresa", $fields['id_empresa'], PDO::PARAM_INT);
            $stmt->execute();
            $data = $stmt->fetchAll(PDO::FETCH_OBJ);
    
            return $data;
        } catch (PDOException $e) {
            $error = '{"error":{"text":' . $e->getMessage() . '}}';
            die(json_encode(array(
                "status" => 500,
                "error" => $error,
                "message" => "Erro no servidor"
            )));
        }
        
    }

    public static function soma_valor_mensal_contratos()
    {
        try {
            $db = getDB();

            $sql = "SELECT SUM(valor_total) as soma_valores_total FROM contratos WHERE  periodicidade_pagamento = 'mensal' and dt_vigenciafinal >= NOW()";
            $stmt = $db->query($sql);
            $data = $stmt->fetch(PDO::FETCH_ASSOC);

            return ($data);

        } catch (PDOException $e) {
            $error = '{"error":{"text":' . $e->getMessage() . '}}';
            die(json_encode(array(
                "status" => 500,
                "error" => $error,
                "message" => "Erro no servidor"
            )));
        }
    }

    public static function soma_valor_anual_contratos()
    {
        try {
            $db = getDB();

            $sql = "SELECT SUM(valor_total) as soma_valores_total FROM contratos WHERE  periodicidade_pagamento = 'anual' and dt_vigenciafinal >= NOW()";
            $stmt = $db->query($sql);
            $data = $stmt->fetch(PDO::FETCH_ASSOC);

            return ($data);

        } catch (PDOException $e) {
            $error = '{"error":{"text":' . $e->getMessage() . '}}';
            die(json_encode(array(
                "status" => 500,
                "error" => $error,
                "message" => "Erro no servidor"
            )));
        }
    }

    public static function total_clientes(){
        try {
            $db = getDB();

            $sql = "SELECT COUNT(e.id) as total_clientes FROM empresas e
            left join contratos c on c.id_empresa = e.id
            WHERE c.dt_vigenciafinal >= NOW()";
            $stmt = $db->query($sql);
            $data = $stmt->fetch(PDO::FETCH_ASSOC);

            return ($data);

        } catch (PDOException $e) {
            $error = '{"error":{"text":' . $e->getMessage() . '}}';
            die(json_encode(array(
                "status" => 500,
                "error" => $error,
                "message" => "Erro no servidor"
            )));
        }
    }
}
