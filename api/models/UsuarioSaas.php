<?php


class UsuarioSaas {
    public static function get_usuario(array $fields){
        try {
            $db = getDB();
    
            $sql = "SELECT * FROM usu_saas WHERE id = :ID";
    
            $stmt = $db->prepare($sql);
            $stmt->bindParam("ID", $fields['id_ususaas'], PDO::PARAM_INT);
            $stmt->execute();
            $data = $stmt->fetch(PDO::FETCH_OBJ);

            return $data;
        } catch (PDOException $e) {
            $error = '{"error":{"text":' . $e->getMessage() . '}}';
            die(json_encode(array(
                "status" => 500,
                "error" => $error,
                "message" => "Erro no servidor"
            )));
        }
    }

    public static function get_usuario_email($email){
        try {
            $db = getDB();
    
            $sql = "SELECT * FROM usu_saas WHERE email1 = :email";

            $stmt = $db->prepare($sql);
            $stmt->bindParam("email", $email);
            $stmt->execute();
            $data = $stmt->fetchAll(PDO::FETCH_OBJ);
    
            return $data;
        } catch (PDOException $e) {
            $error = '{"error":{"text":' . $e->getMessage() . '}}';
            die(json_encode(array(
                "status" => 500,
                "error" => $error,
                "message" => "Erro no servidor"
            )));
        }
    }

    public static function get_usuarios(array $fields)
    {

        $pagina = $fields['pagina'] ? $fields['pagina']  : 0;
        $limite = $fields['limite'] ? $fields['limite'] : 10;

        $qnt = $pagina * $limite;

        $where = '';

        if (!empty($fields['condicao'])) {
            $condicao = $fields['condicao'];
            $where = "WHERE (us.email1 LIKE '%{$condicao}%' or us.nome LIKE '%{$condicao}%' or us.email1 LIKE '%{$condicao}%')";
        }
        try {
            $db = getDB();

            $sql = "SELECT us.id, us.nome, us.email1, us.email2,
                    us.id_empresa, e.nome_fantasia as empresa,  us.dt_cadastro, us.whatsapp
                    FROM usu_saas as us
                    LEFT JOIN empresas as e ON e.id = us.id_empresa
                    {$where}
                    ORDER BY e.nome_fantasia, us.nome
                    LIMIT :limite OFFSET :qtd";

            $stmt = $db->prepare($sql);
            $stmt->bindParam('limite', $limite, PDO::PARAM_INT);
            $stmt->bindParam('qtd', $qnt, PDO::PARAM_INT);
            $stmt->execute();

            $data = $stmt->fetchAll(PDO::FETCH_OBJ);

            return $data;
        } catch (PDOException $e) {
            $error = '{"error":{"text":' . $e->getMessage() . '}}';
            die(json_encode(array(
                "status" => 500,
                "error" => $error,
                "message" => "Erro no servidor"
            )));
        }
    }

}