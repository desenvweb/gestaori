<?php

require dirname(__DIR__) . '../../vendor/autoload.php';

use Dompdf\Dompdf;

class HorasTrab
{


    public static function finalizar_ld($fields = [])
    {

        $now = (new DateTime())->modify('-3 hours');
        $fields['dt_fim'] = $now->format('Y-m-d H:i:s');

        $id_empresa = $fields['id_empresa'];
        $id_usuario = $fields['id_usuario'];
        //VERIFICA SE O HORÁRIO DA FINAL ESTÁ MAIOR QUE
        //DA DATA INICIAL
        $message = HorasTrab::verifica_horaFinal($fields);

        if ($message != null) {
            return [
                "status" => 3,
                "message" => $message
            ];
        }
        //fim

        $fields['diff_minutos'] = diff_horas_em_minutos(
            explode(" ", $fields['dt_inicio'])[1],
            explode(" ", $fields['dt_fim'])[1]
        );


        if ($fields['diff_minutos'] <= 0) {
            return [
                "status" => 3,
                "message" => 'Lançamento muito curto.'
            ];
        }

        $fields['diff_horas'] = convert_minutes_in_hhmm($fields['diff_minutos']);

        try {

            $query = fieldsToEditQuery($fields);

            $db = getDB();
            $sql = "UPDATE horastrab 
                        SET {$query}
                        WHERE id_empresa = :id_empresa AND id_usuario = :id_usuario
                            AND dt_fim IS NULL";


            $stmt = $db->prepare($sql);
            $stmt->bindParam("id_empresa", $id_empresa, PDO::PARAM_INT);
            $stmt->bindParam("id_usuario", $id_usuario, PDO::PARAM_INT);

            $int = ['diff_minutos'];
            foreach ($fields as $key => &$value) {
                $param = array_search($key, $int) ? PDO::PARAM_INT : PDO::PARAM_STR;
                $stmt->bindParam(':' . $key, $value, $param);
            }

            $stmt->execute();

            $minutos_lancar = $fields['diff_minutos'] * -1;

            SaldoHoras::lancar(['id_empresa' => $id_empresa, 'minutos' => $minutos_lancar]);

            return [
                "status" => 1,
                "message" => "Horas lançadas com sucesso"
            ];

            $db = null;
        } catch (PDOException $e) {
            $error = '{"error":{"text":' . $e->getMessage() . '}}';
            die(json_encode(array(
                "status" => 500,
                "error" => $error,
                "message" => "Erro no servidor"
            )));
        }
    }

    public static function iniciar_ld(array $fields)
    {

        $now = (new DateTime())->modify('-3 hours');
        $fields['dt_inicio'] = $now->format('Y-m-d H:i:s');

        $cliente = Empresas::get_empresa(['id_empresa' => $fields['id_empresa']]);

        //verifica cliente
        if (sizeof($cliente) <= 0) {
            return [
                "status" => 5,
                "message" => "Cliente não encontrado"
            ];
        }
        //fim

        if (!$fields['id_projeto']) {
            //verifica saldo do cliente
            $saldo = SaldoHoras::get_saldo_empresa(["id_empresa" => $fields['id_empresa']]);
            if ($saldo <= 0 || !$saldo) {
                return [
                    "status" => 5,
                    "message" => "Cliente não possuí saldo de horas suficiente."
                ];
            }
        }
        //fimm

        $message = HorasTrab::verifica_trabalho($fields['id_usuario']);

        if ($message['status'] == 1) {
            $message['message'] = 'Usuário já está trabalhando.';
            return $message;
        }

        //VERIFICA SE O LANÇAMENTO INICIAL NÃO ESTÁ DENTRO DE OUTRO
        //LANÇAMENTO DE HORAS
        $message = HorasTrab::verifica_horaInicial($fields);

        if ($message != null) {
            return [
                "status" => 3,
                "message" => $message
            ];
        }
        //fim

        //verifica se início de serviço está fora de horário comercial
        // somente para CLT
        if  ($_SESSION['tipo_colaborador'] === 'CLT') {
            $dt = explode(" ", $fields['dt_inicio'])[0];

            $dt_inv_manha_ini = $dt . " 08:00:00";
            $dt_inv_manha_fim = $dt . " 12:00:00";

            $dt_inv_tarde_ini = $dt . " 13:00:00";
            $dt_inv_tarde_fim = $dt . " 17:30:00";

            if ((strtotime($dt_inv_manha_ini) > strtotime($fields['dt_inicio']))
                || (strtotime($dt_inv_tarde_fim) < strtotime($fields['dt_inicio']))
                || (strtotime($dt_inv_manha_fim) < strtotime($fields['dt_inicio']) && strtotime($dt_inv_tarde_ini) > strtotime($fields['dt_inicio']))
            ) {
                return [
                    "status" => 7,
                    "message" => "Fora do horário comercial"
                ];
            }
        }
        //fim


        try {

            $query = fieldsToInsertQuery($fields);

            $db = getDB();
            $sql = "INSERT INTO horastrab ({$query[0]}) VALUES ({$query[1]})";

            $stmt = $db->prepare($sql);

            $int = ['id_empresa', 'id_usuario', 'id_projeto', 'id_atividade'];
            foreach ($fields as $key => &$value) {
                $param = array_search($key, $int) ? PDO::PARAM_INT : PDO::PARAM_STR;
                $stmt->bindParam(':' . $key, $value, $param);
            }

            $stmt->execute();

            Usuario::set_status(["id_usuario" => $fields['id_usuario'], "status" => 'presente']);

            return [
                "status" => 1,
                "message" => "Horas lançadas com sucesso"
            ];
        } catch (PDOException $e) {
            $error = '{"error":{"text":' . $e->getMessage() . '}}';
            http_response_code(500);
            die(json_encode(array(
                "status" => 500,
                "error" => $error,
                "message" => "Erro no servidor"
            )));
        }
    }

    public static function verifica_horas($fields)
    {
        try {
            $db = getDB();

            $sql = "SELECT ht.dt_inicio, ht.dt_fim, ht.id
                FROM horastrab AS ht
                WHERE ht.id_usuario = :id_usuario
                    AND ht.id_empresa = :id_empresa
                    AND (:dt_inicio BETWEEN ht.dt_inicio AND ht.dt_fim 
                        OR :dt_fim BETWEEN ht.dt_inicio AND ht.dt_fim)";

            $stmt = $db->prepare($sql);
            $stmt->bindParam("id_usuario", $fields['id_usuario'], PDO::PARAM_INT);
            $stmt->bindParam("id_empresa", $fields['id_empresa'], PDO::PARAM_INT);
            $stmt->bindParam("dt_inicio", $fields['dt_inicio'], PDO::PARAM_STR);
            $stmt->bindParam("dt_fim", $fields['dt_fim'], PDO::PARAM_STR);
            $stmt->execute();
            $data = $stmt->fetchAll(PDO::FETCH_OBJ);

            $db = null;

            if (sizeof($data) > 0) {
                return array(
                    "id" => $data[0]->id,
                    "message" => "Já houve um lançamento de horas as " . $data[0]->dt_inicio . " - " . $data[0]->dt_fim
                );
            } else {
                return null;
            }
        } catch (PDOException $e) {
            $error = '{"error":{"text":' . $e->getMessage() . '}}';
            http_response_code(500);
            die(json_encode(array(
                "status" => 500,
                "error" => $error,
                "message" => "Erro no servidor"
            )));
        }
    }

    public static function horas_projetos($id_cliente)
    {

        try {
            $db = getDB();
            $sql = "SELECT SUM(total_horas) as total_horas FROM projeto as p
                    WHERE p.id_cliente = :id_cliente and p.desconta_horas = 'S'";

            $stmt = $db->prepare($sql);
            $stmt->bindParam("id_cliente", $id_cliente, PDO::PARAM_INT);
            $stmt->execute();
            $data = $stmt->fetch(PDO::FETCH_OBJ);

            if (sizeof($data) > 0) {
                return array(
                    "status" => 1,
                    "data" => $data,
                    "message" => "Horas retornadas com sucesso"
                );
            } else {
                return array(
                    "status" => 2,
                    "message" => "Não existem horas de projeto"
                );
            }
            $db = null;
        } catch (PDOException $e) {
            $error = '{"error":{"text":' . $e->getMessage() . '}}';
            die(json_encode(array(
                "status" => 500,
                "error" => $error,
                "message" => "Erro no servidor"
            )));
        }
    }

    public static function horas_usadas_pelo_cliente($fields)
    {
        try {
            $db = getDB();

            $cliente = get_cliente($fields['id_cliente']);

            if (sizeof($cliente) == 0)
                return array(
                    "status" => 2,
                    "message" => "Cliente não encontrado"
                );

            $sql = "SELECT SUM(diff_minutos) AS soma
                FROM horastrab AS ht
                WHERE ht.id_empresa = :id_cliente
                    AND ht.dt_inicio BETWEEN :inicio_contrato AND :fim_contrato";

            $inicio_contrato = $cliente[0]->vigencia_final_adendo . " " . "00:00:00";
            $fim_contrato = $cliente[0]->vigencia_inicial_adendo . " " . "23:59:59";

            $stmt = $db->prepare($sql);
            $stmt->bindParam("id_cliente", $fields['id_cliente'], PDO::PARAM_INT);
            $stmt->bindParam("inicio_contrato", $inicio_contrato, PDO::PARAM_STR);
            $stmt->bindParam("fim_contrato", $fim_contrato, PDO::PARAM_STR);
            $stmt->execute();
            $data = $stmt->fetch(PDO::FETCH_OBJ);

            $db = null;

            return array(
                "soma_em_hhmm" => convert_minutes_in_hhmm($data->soma),
                "soma_em_minutos" => $data->soma ?? 0,
            );
        } catch (PDOException $e) {
            $error = '{"error":{"text":' . $e->getMessage() . '}}';
            die(json_encode(array(
                "status" => 500,
                "error" => $error,
                "message" => "Erro no servidor"
            )));
        }
    }

    public static function get_horas($fields)
    {
        $pagina = $fields['page'] ? $fields['page'] : 0;
        $limite = $fields['limite'] ? $fields['limite'] : 10;

        $qnt = $pagina * $limite;

        $where = 'WHERE ht.dt_fim IS NOT NULL';


        if (!empty($fields['id_empresa'])) {
            $where .= " and ht.id_empresa = {$fields['id_empresa']}";
        }
        if (!empty($fields['id_usuario'])) {
            $where .= " and ht.id_usuario = {$fields['id_usuario']}";
        }
        if (!empty($fields['dt_inicial'])) {
            $dt = $fields['dt_inicial'] . ' 00:00:00';
            $where .= " and ht.dt_inicio > '{$dt}'";
        }
        if (!empty($fields['dt_final'])) {
            $dt = $fields['dt_final'] . ' 23:59:59';
            $where .= " and ht.dt_fim < '{$dt}'";
        }


        try {
            $db = getDB();

            $sql = "SELECT ht.id, ht.dt_inicio, ht.dt_fim, ht.diff_horas, ht.diff_minutos, 
            ht.id_empresa, ht.id_usuario, ht.descricao, c.razao_social, c.cnpj, ht.id_ticket, u.nome, ht.id_atividade,
            ht.id_projeto, t.titulo as ticket_titulo
            FROM horastrab AS ht 
            LEFT JOIN empresas AS c ON c.id = ht.id_empresa 
            LEFT JOIN usuario AS u ON u.id = ht.id_usuario
            LEFT JOIN ticket AS t ON t.id = ht.id_ticket
            {$where}
            ORDER BY ht.dt_inicio DESC 
            LIMIT :limite OFFSET :qnt";

            //die($sql);

            $stmt = $db->prepare($sql);
            $stmt->bindParam('qnt', $qnt, PDO::PARAM_INT);
            $stmt->bindParam('limite', $limite, PDO::PARAM_INT);
            $stmt->execute();
            $data = $stmt->fetchAll(PDO::FETCH_OBJ);

            if (sizeof($data) > 0) {
                return [
                    "status" => 1,
                    "data" => $data,
                    "message" => "Horas retornadas com sucesso"
                ];
            } else {
                return [
                    "status" => 2,
                    "message" => "Não existem horas lançadas"
                ];
            }


            $db = null;
        } catch (PDOException $e) {
            $error = '{"error":{"text":' . $e->getMessage() . '}}';

            http_response_code(500);
            die(json_encode(array(
                "status" => 500,
                "error" => $error,
                "message" => "Erro no servidor"
            )));
        }
    }

    public static function horas_restantes_mes($fields)
    {
        $id_cliente = $fields;

        try {
            $db = getDB();
            $cliente = get_cliente($id_cliente);

            if (sizeof($cliente) == 0) {
                return array(
                    "status" => 2,
                    "message" => "Cliente não encontrado"
                );
            }

            $fim_contrato = $cliente[0]->vigencia_final_adendo;

            $now = date('Y-m-d H:i:s');
            $primeiro_dia = date("Y-m-01 00:00:00");
            $ultimo_dia = date("Y-m-t 23:59:59");

            if (strtotime($fim_contrato) > strtotime($now)) {
                $sql = "SELECT SUM(diff_minutos) as total FROM horastrab
                        WHERE id_cliente = :ID_CLIENTE 
                        and dt_inicio BETWEEN '$primeiro_dia' and '$ultimo_dia'";

                $stmt = $db->prepare($sql);
                $stmt->bindParam("ID_CLIENTE", $id_cliente, PDO::PARAM_INT);
                $stmt->execute();

                $data = $stmt->fetch(PDO::FETCH_OBJ);
                $db = null;

                $minutos_trabalhados = $data->total;
                $minutos_mes = $cliente[0]->horas_mes * 60;

                $minutos_restantes = $minutos_mes - $minutos_trabalhados;
                $hhmm_restante = convert_minutes_in_hhmm($minutos_restantes);

                return array(
                    "hhmm_restantes" => $hhmm_restante,
                    "minutos_restantes" => $minutos_restantes,
                    "minutos_trabalhados" => $data->total ?? 0
                );
            } else {
                return array(
                    "hhmm_restante" => '00:00',
                    "minutos_restantes" => false,
                    "minutos_trabalhados" => false
                );
            }
        } catch (PDOException $e) {
            $error = '{"error":{"text":' . $e->getMessage() . '}}';
            die(json_encode(array(
                "status" => 500,
                "error" => $error,
                "message" => "Erro no servidor"
            )));
        }
    }

    public static function horas_restantes_contrato($fields)
    {
        $id_cliente = $fields;

        try {
            $db = getDB();
            $cliente = get_cliente($id_cliente);

            if (sizeof($cliente) == 0) {
                return array(
                    "status" => 2,
                    "message" => "Cliente não encontrado"
                );
            }

            $inicio_contrato = $cliente[0]->vigencia_inicial_adendo;
            $fim_contrato = $cliente[0]->vigencia_final_adendo . " " . "23:59:59";

            $now = date('Y-m-d H:m:s');
            $inicio_mes = date('Y-m-1') . " " . "00:00:00";

            if (strtotime($fim_contrato) > strtotime($now)) {
                $sql = "SELECT SUM(diff_minutos) as total FROM horastrab
                        WHERE id_cliente = :ID_CLIENTE 
                        and dt_inicio BETWEEN '$inicio_mes' and '$now' and id_projeto IS NULL";
                //id_projeto IS NULL porque lançamentos com projeto não são contabilizados

                $stmt = $db->prepare($sql);
                $stmt->bindParam("ID_CLIENTE", $id_cliente, PDO::PARAM_INT);
                $stmt->execute();
                $data = $stmt->fetch(PDO::FETCH_OBJ);
                $db = null;

                //Pega as horas de projetos do cliente
                $horas_projetos = HorasTrab::horas_projetos($id_cliente);
                $horas_projetos = $horas_projetos['data']->total_horas;

                //Horas convertidas p/ minutos p/ facilitar o cálculo
                $minutos_projetos =  $horas_projetos * 60;


                //Pega os meses que já passaram desde o inicio do contrato (horas gastas)
                $diff_meses = diff_meses($inicio_contrato, date('Y-m-d'));
                $minutos_mes = ceil($diff_meses * ($cliente[0]->horas_mes * 60));
                $minutos_mes = ($minutos_mes - $minutos_projetos) >= 0 ? $minutos_mes - $minutos_projetos : 0;
                $minutos_contrato = $cliente[0]->horas_contrato * 60;

                $minutos_gastos = $minutos_projetos + $minutos_mes;
                $minutos_gastos = $data->total + $minutos_gastos;

                $minutos_restantes = $minutos_contrato - $minutos_gastos;
                $minutos_restantes = $minutos_restantes >= 0 ? $minutos_restantes : 0;
                $hhmm_restantes = convert_minutes_in_hhmm($minutos_restantes);
                $hhmm_gastas = convert_minutes_in_hhmm($minutos_gastos);

                return array(
                    "hhmm_gastos" => $hhmm_gastas,
                    "hhmm_restantes" => $hhmm_restantes
                );
            } else {
                return array(
                    "horas_contrato" => false,
                    "hhmm_gastos" => false,
                    "hhmm_restante" => '00:00'
                );
            }
        } catch (PDOException $e) {
            $error = '{"error":{"text":' . $e->getMessage() . '}}';
            die(json_encode(array(
                "status" => 500,
                "error" => $error,
                "message" => "Erro no servidor"
            )));
        }
    }

    public static function verifica_horaInicial($fields)
    {
        try {
            $db = getDB();

            $sql = "SELECT ht.dt_inicio, ht.dt_fim
                FROM horastrab AS ht
                WHERE ht.id_usuario = :id_usuario
                    AND ht.id_empresa = :id_empresa
                    AND :dt_inicial BETWEEN ht.dt_inicio AND ht.dt_fim";

            $stmt = $db->prepare($sql);
            $stmt->bindParam("id_usuario", $fields['id_usuario'], PDO::PARAM_INT);
            $stmt->bindParam("id_empresa", $fields['id_cliente'], PDO::PARAM_INT);
            $stmt->bindParam("dt_inicial", $fields['dt_inicio'], PDO::PARAM_STR);
            $stmt->execute();
            $data = $stmt->fetchAll(PDO::FETCH_OBJ);

            $db = null;

            if (sizeof($data) > 0) {
                return $data[0]->dt_inicio . " - " . $data[0]->dt_fim;
            } else {
                return null;
            }
        } catch (PDOException $e) {
            $error = '{"error":{"text":' . $e->getMessage() . '}}';
            die(json_encode(array(
                "status" => 500,
                "error" => $error,
                "message" => "Erro no servidor"
            )));
        }
    }

    public static function verifica_horaFinal($fields)
    {

        try {
            $db = getDB();

            $sql = "SELECT ht.dt_inicio
                FROM horastrab AS ht
                WHERE ht.id_usuario = :id_usuario
                    AND ht.id_empresa = :id_empresa AND ht.dt_fim IS NULL
                    AND :dt_fim < ht.dt_inicio";

            $stmt = $db->prepare($sql);
            $stmt->bindParam("id_usuario", $fields['id_usuario'], PDO::PARAM_INT);
            $stmt->bindParam("id_empresa", $fields['id_empresa'], PDO::PARAM_INT);
            $stmt->bindParam("dt_fim", $fields['dt_fim'], PDO::PARAM_STR);
            $stmt->execute();
            $data = $stmt->fetch(PDO::FETCH_OBJ);

            $db = null;

            if (sizeof($data) > 0) {
                return $data->dt_inicio;
            } else {
                return null;
            }
        } catch (PDOException $e) {
            $error = '{"error":{"text":' . $e->getMessage() . '}}';
            die(json_encode(array(
                "status" => 500,
                "error" => $error,
                "message" => "Erro no servidor"
            )));
        }
    }

    public static function verifica_trabalho($id_usuario)
    {

        try {
            $db = getDB();
            $sql = "SELECT usu.nome, ht.id, ht.dt_inicio, ht.dt_fim, ht.diff_horas,
                ht.diff_minutos, ht.id_empresa, ht.id_usuario, ht.descricao, t.titulo,
                e.nome_fantasia, e.cnpj, usu.nome, ht.id_ticket, a.nome as atividade,
                ht.id_atividade, t.dt_prevista
                FROM horastrab AS ht
                LEFT JOIN usuario AS usu ON usu.id = ht.id_usuario  
                LEFT JOIN empresas AS e ON e.id = ht.id_empresa
                LEFT JOIN atividade AS a ON a.id = ht.id_atividade
                LEFT JOIN ticket AS t ON t.id = ht.id_ticket
                WHERE ht.id_usuario = :id_usuario AND ht.dt_fim IS NULL";

            $stmt = $db->prepare($sql);
            $stmt->bindParam("id_usuario", $id_usuario, PDO::PARAM_INT);
            $stmt->execute();
            $data = $stmt->fetch(PDO::FETCH_OBJ);

            if (!empty($data)) {
                return array(
                    "status" => 1,
                    "data" => $data,
                    "message" => "Trabalhando"
                );
            } else {
                return array(
                    "status" => 2,
                    "message" => "Não está em trabalho"
                );
            }
            $db = null;
        } catch (PDOException $e) {
            $error = '{"error":{"text":' . $e->getMessage() . '}}';
            die(json_encode(array(
                "status" => 500,
                "error" => $error,
                "message" => "Erro no servidor"
            )));
        }
    }

    public static function verifica_trabalhos($id_usuario)
    {

        try {
            $db = getDB();
            $sql = "SELECT usu.nome, ht.id, ht.dt_inicio, ht.dt_fim, ht.diff_horas,
                ht.diff_minutos, ht.id_empresa, ht.id_usuario, ht.descricao, t.titulo,
                e.nome_fantasia, e.cnpj, usu.nome, ht.id_ticket, a.nome as atividade,
                ht.id_atividade, t.dt_prevista
                FROM horastrab AS ht
                LEFT JOIN usuario AS usu ON usu.id = ht.id_usuario  
                LEFT JOIN empresas AS e ON e.id = ht.id_empresa
                LEFT JOIN atividade AS a ON a.id = ht.id_atividade
                LEFT JOIN ticket AS t ON t.id = ht.id_ticket
                WHERE ht.dt_fim IS NULL and usu.id = :id_usuario";
                $stmt = $db->prepare($sql);
                $stmt->bindParam("id_usuario", $id_usuario, PDO::PARAM_INT);
                $stmt->execute();
                $data = $stmt->fetch(PDO::FETCH_OBJ);

            return $data;
            $db = null;
        } catch (PDOException $e) {
            $error = '{"error":{"text":' . $e->getMessage() . '}}';
            die(json_encode(array(
                "status" => 500,
                "error" => $error,
                "message" => "Erro no servidor"
            )));
        }
    }

    public static function relatorio_horas(array $fields)
    {
        //die(json_encode($fields));

        $where = 'WHERE ht.dt_fim IS NOT NULL';

        if (!empty($fields['id_empresa'])) {
            $where .= " and ht.id_empresa = {$fields['id_empresa']}";
        }
        //if (!empty($fields['id_usuario'])) {
        //    $where .= " and ht.id_usuario = {$fields['id_usuario']}";
        //}

        if (!empty($fields['colaborador'])) {
            $where .= " and ht.id_usuario = {$fields['colaborador']}";
        }
        if (!empty($fields['dt_inicial'])) {
            $dt = $fields['dt_inicial'] . ' 00:00:00';
            $where .= " and ht.dt_inicio >= '{$dt}'";
        }
        if (!empty($fields['dt_final'])) {
            $dt = $fields['dt_final'] . ' 23:59:59';
            $where .= " and ht.dt_fim <= '{$dt}'";
        }

        try {
            $db = getDB();

            $sql = "SELECT ht.id, ht.dt_inicio, ht.dt_fim, ht.diff_horas, ht.diff_minutos, 
                    ht.descricao, ht.id_ticket, e.nome_fantasia, u.nome
            FROM horastrab AS ht 
            LEFT JOIN empresas AS e ON e.id = ht.id_empresa
            LEFT JOIN usuario AS u ON u.id = ht.id_usuario
            {$where}
            ORDER BY ht.dt_inicio DESC";

             //die($sql);

            $stmt = $db->prepare($sql);
            $stmt->execute();
            $data = $stmt->fetchAll(PDO::FETCH_OBJ);

            if (sizeof($data) > 0) {

                $total_trabalhado = 0;
                $qnt = 0;

                foreach ($data as $d) {
                    $total_trabalhado = $total_trabalhado + $d->diff_minutos;
                    $qnt++;
                }

                $data['info']->hhmm_trabalhado = convert_minutes_in_hhmm($total_trabalhado);
                $data['info']->qnt_servicos = $qnt;

                return array(
                    "status" => 1,
                    "data" => $data,
                    "message" => "Horas retornadas com sucesso"
                );
            } else {
                return array(
                    "status" => 2,
                    "message" => "Não existem horas lançadas"
                );
            }

            $db = null;
        } catch (PDOException $e) {
            $error = '{"error":{"text":' . $e->getMessage() . '}}';
            die(json_encode(array(
                "status" => 500,
                "error" => $error,
                "message" => "Erro no servidor"
            )));
        }
    }

    public static function relatorio_horas_pdf(array $fields)
    {

       // die(json_encode($fields));
        $horas = HorasTrab::relatorio_horas($fields);
        $usuario = get_usuarioID($fields['colaborador']);

        $horas = $horas['data'];

        $dt_inicial = $fields['dt_inicial'] ? formatDMYHHMMSS($fields['dt_inicial']) : 'Não especificado';
        $dt_final = $fields['dt_final'] ?  formatDMYHHMMSS($fields['dt_final']) : 'Não especificado';
        $hhmm_trabalhado = $horas['info']->hhmm_trabalhado ?? 0;
        $qnt_servicos = $horas['info']->qnt_servicos ?? 0;
        $html = '';

        $html = '';

        $head = '<fieldset>';
        $head .= '<p><b>Ticket:</b> ' . $usuario[0]->id_ticket . '</p>';
        $head .= '<p><b>Colaborador:</b> ' . $usuario[0]->nome . '</p>';
        $head .= '<p><b>Data início:</b> ' . $dt_inicial . '</p>';
        $head .= '<p><b>Data de fim:</b> ' . $dt_final . '</p>';
        $head .= '<hr>';
        $head .= '<p><b>Tempo Trabalhado no Período:</b> ' . $hhmm_trabalhado;
        $head .= '  &nbsp;&nbsp;&nbsp;&nbsp;<b>Total de Serviços:</b> ' . $qnt_servicos;
        $head .= '</p>';
        $head .= '</fieldset>';

        $body = '<fieldset>';
        $body .= '<table style="width: 100%">';
        $body .= '<thead>';
        $body .= '<tr>';
        $body .= '<th scope="col">Ticket</th>';
        $body .= '<th scope="col">Cliente</th>';
        $body .= '<th scope="col">Início</th>';
        $body .= '<th scope="col">Fim</th>';
        $body .= '<th scope="col">Diferença</th>';
        $body .= '</thead>';
        $body .= '<tbody>';

        foreach ($horas as $key => $value) {
            if ($value->nome) {
                $body .= '<tr>';
                $body .= '<td style="text-align:center; vertical-align:middle">' . $value->id_ticket . '</td>';
                $body .= '<td style="text-align:center; vertical-align:middle">' . $value->nome_fantasia . '</td>';
                $body .= '<td style="text-align:center; vertical-align:middle">' . formatDMYHHMMSS($value->dt_inicio) . '</td>';
                $body .= '<td style="text-align:center; vertical-align:middle">' . formatDMYHHMMSS($value->dt_fim) . '</td>';
                $body .= '<td style="text-align:center; vertical-align:middle">' . $value->diff_horas . '</td>';
            }
        }

        $body .= '<tbody>';
        $body .= '</table>';
        $body .= '</fieldset>';

        $html .= $head;
        $html .= $body;

        $pdf = new Dompdf();

        $pdf->loadHtml($html);
        $pdf->render();
        echo $pdf->output();

        header('Content-Type: application/pdf');
        die();
    }

    public static function get_horastrab_empresa(array $fields)
    {
        try {
            $db = getDB();

            $sql = "SELECT * FROM horastrab WHERE id_empresa = :id_empresa";

            $stmt = $db->prepare($sql);
            $stmt->bindParam('id_empresa', $fields['id_empresa'], PDO::PARAM_INT);
            $stmt->execute();
            $data = $stmt->fetchAll(PDO::FETCH_OBJ);

            return $data;

            $db = null;
        } catch (PDOException $e) {
            $error = '{"error":{"text":' . $e->getMessage() . '}}';
            http_response_code(500);
            die(json_encode(array(
                "status" => 500,
                "error" => $error,
                "message" => "Erro no servidor"
            )));
        }
    }

    public static function relatorio_horas_empresa_pdf(array $fields)
    {

        $horas = HorasTrab::relatorio_horas($fields);
        $projetos = Projeto::get_projetos_descontados_empresa($fields);

        $horas = $horas['data'];

        $empresa = Empresas::get_empresa(['id_empresa' => $fields['id_empresa']]);

        $empresa->nome_fantasia = $empresa->nome_fantasia ? $empresa->nome_fantasia : 'Não especificado';

        $dt_inicial = $fields['dt_inicial'] ? formatDMYHHMMSS($fields['dt_inicial']) : 'Não especificado';
        $dt_final = $fields['dt_final'] ?  formatDMYHHMMSS($fields['dt_final']) : 'Não especificado';
        $hhmm_trabalhado = $horas['info']->hhmm_trabalhado ?? 0;
        $qnt_servicos = $horas['info']->qnt_servicos ?? 0;
        $html = '';

        $head = '<fieldset>';
        $head .= '<p><b>Empresa:</b> ' . $empresa->nome_fantasia . '</p>';
        $head .= '<p><b>Horas de Contrato:</b> ' . $empresa->horas_contrato . '</p>';
        $head .= '<p><b>Data início:</b> ' . $dt_inicial . '</p>';
        $head .= '<p><b>Data de fim:</b> ' . $dt_final . '</p>';
        $head .= '<hr>';
        $head .= '<p><b>Tempo Trabalhado no Período:</b> ' . $hhmm_trabalhado;
        $head .= '  &nbsp;&nbsp;&nbsp;&nbsp;<b>Total de Serviços:</b> ' . $qnt_servicos;
        $head .= '  &nbsp;&nbsp;&nbsp;&nbsp;<b>Total de Projetos:</b> ' . $projetos->total_projetos;
        $head .= '  &nbsp;&nbsp;&nbsp;&nbsp;<b>Horas Descontadas de Projetos:</b> ' . $projetos->total_horas;
        $head .= '</p>';
        $head .= '</fieldset>';

        $body = '<fieldset>';
        $body .= '<table style="width: 100%">';
        $body .= '<thead>';
        $body .= '<tr>';
        $body .= '<th scope="col">Ticket</th>';
        $body .= '<th scope="col">Colaborador</th>';
        $body .= '<th scope="col">Início</th>';
        $body .= '<th scope="col">Fim</th>';
        $body .= '<th scope="col">Diferença</th>';
        $body .= '</thead>';
        $body .= '<tbody>';

        foreach ($horas as $key => $value) {
            if ($value->nome) {
                $body .= '<tr>';
                $body .= '<td style="text-align:center; vertical-align:middle">' . $value->id_ticket . '</td>';
                $body .= '<td style="text-align:center; vertical-align:middle">' . $value->nome . '</td>';
                $body .= '<td style="text-align:center; vertical-align:middle">' . formatDMYHHMMSS($value->dt_inicio) . '</td>';
                $body .= '<td style="text-align:center; vertical-align:middle">' . formatDMYHHMMSS($value->dt_fim) . '</td>';
                $body .= '<td style="text-align:center; vertical-align:middle">' . $value->diff_horas . '</td>';
            }
        }

        $body .= '<tbody>';
        $body .= '</table>';
        $body .= '</fieldset>';

        $html .= $head;
        $html .= $body;

        $pdf = new Dompdf();

        $pdf->loadHtml($html);
        $pdf->render();
        echo $pdf->output();

        header('Content-Type: application/pdf');
        die();
    }

    public static function lancar_horas(array $fields)
    {

      
        $fields['dt_inicio'] = $fields['dt_lancamento'] . " " . $fields['hora_ini'];
        $fields['dt_fim'] = $fields['dt_lancamento'] . " " . $fields['hora_fim'];

        $db = getDB();

        $empresa = Empresas::get_empresa(['id_empresa' => $fields['id_empresa']]);

        if (sizeof($empresa) <= 0) {
            return [
                "status" => 2,
                "message" => "Empresa não encontrada"
            ];
        }

        //VERIFICA SE JÁ FOI FEITO UM LANÇAMENTO DE HORAS NESSE HORÁRIO
        $message = HorasTrab::verifica_horas($fields);
        if ($message != null) {
            return [
                "status" => 2,
                "message" => $message['message']
            ];
        }

        $fields['diff_minutos'] = diff_horas_em_minutos($fields['hora_ini'], $fields['hora_fim']);
        $fields['diff_horas'] = convert_minutes_in_hhmm($fields['diff_minutos']);

        unset($fields['dt_lancamento']);
        unset($fields['hora_ini']);
        unset($fields['hora_fim']);
        
       

        //verifica horas restantes do contrato
        $saldo = SaldoHoras::get_saldo_empresa(["id_empresa" => $fields['id_empresa']]);
  
        if ($saldo <= 0 || !$saldo) {
            return [
                "status" => 5,
                "message" => "Cliente não possuí saldo de horas suficiente."
            ];
        }

        try {
            $fields['id_projeto'] = date("ymdhis");
            $query = fieldsToInsertQuery($fields);

            $sql = "INSERT INTO horastrab({$query[0]}) 
                    VALUES ({$query[1]})";

            //die($sql);
            $stmt = $db->prepare($sql);
        
            $int = ['id_empresa', 'id_usuario', 'id_atividade', 'diff_minutos'];
            foreach ($fields as $key => &$value) {
                $param = array_search($key, $int) ? PDO::PARAM_INT : PDO::PARAM_STR;
                $stmt->bindParam(':' . $key, $value, $param);
            }
            $stmt->execute();

            $minutos_lancar = $fields['diff_minutos'] * -1;
    
             SaldoHoras::lancar(['id_empresa' => $fields['id_empresa'], 'minutos' => $minutos_lancar,'id_projeto'=>$fields['id_projeto']]);

            return [
                "status" => 1,
                "message" => "Horas lançadas com sucesso!!"
            ];

            $db = null;
        } catch (PDOException $e) {
            $error = '{"error":{"text":' . $e->getMessage() . '}}';
            die(json_encode(array(
                "status" => 500,
                "error" => $error,
                "message" => "Erro no servidor"
            )));
        }
    }

    public static function editar_horas(array $fields)
    {
       
       
        $fields['dt_inicio'] = $fields['dt_lancamento'] . " " . $fields['hora_ini'];
        $fields['dt_fim'] = $fields['dt_lancamento'] . " " . $fields['hora_fim'];
        
        $id = $fields['id'];
        $id_projeto = $fields['id_projeto'];

        try {
            $db = getDB();

            //VERIFICA SE JÁ FOI FEITO UM LANÇAMENTO DE HORAS NESSE HORÁRIO
            $message = HorasTrab::verifica_horas($fields);
            if ($message != null && $message['id'] != $id) {
                return [
                    "status" => 2,
                    "message" => $message['message']
                ];
            }
            $fields['diff_minutos'] = diff_horas_em_minutos($fields['hora_ini'], $fields['hora_fim']);
            $fields['diff_horas'] = convert_minutes_in_hhmm($fields['diff_minutos']);

            unset($fields['dt_lancamento']);
            unset($fields['hora_ini']);
            unset($fields['hora_fim']);
            unset($fields['id_projeto']);

            $query = fieldsToEditQuery($fields);

            $sql = "UPDATE horastrab SET {$query} WHERE id = :id";

            $stmt = $db->prepare($sql);
            $int = ['id_empresa', 'id_usuario', 'id_atividade', 'diff_minutos', 'id'];
            foreach ($fields as $key => &$value) {
                $param = array_search($key, $int) ? PDO::PARAM_INT : PDO::PARAM_STR;
                $stmt->bindParam(':' . $key, $value, $param);
            }
            $stmt->bindParam('id', $id, PDO::PARAM_INT);
            $stmt->execute();

            $minutos_lancar = $fields['diff_minutos'] * -1;
          
            SaldoHoras::editar(['id_empresa' => $fields['id_empresa'],'minutos'=>$minutos_lancar,'id_projeto'=>$id_projeto]);

            die(json_encode(array(
                "status" => 1,
                "message" => "Horas lançadas com sucesso"
            )));

            $db = null;
        } catch (PDOException $e) {
            $error = '{"error":{"text":' . $e->getMessage() . '}}';
            die(json_encode(array(
                "status" => 500,
                "error" => $error,
                "message" => "Erro no servidor"
            )));
        }
    }
}
