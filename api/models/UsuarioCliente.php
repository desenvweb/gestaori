<?php


class UsuarioCliente {
    public static function get_usuario(array $fields){
        try {
            $db = getDB();
    
            $sql = "SELECT * FROM usu_cliente WHERE id = :ID";
    
            $stmt = $db->prepare($sql);
            $stmt->bindParam("ID", $fields['id_usucliente'], PDO::PARAM_INT);
            $stmt->execute();
            $data = $stmt->fetch(PDO::FETCH_OBJ);

            return $data;
        } catch (PDOException $e) {
            $error = '{"error":{"text":' . $e->getMessage() . '}}';
            die(json_encode(array(
                "status" => 500,
                "error" => $error,
                "message" => "Erro no servidor"
            )));
        }
    }

    public static function get_usuario_email($email){
        try {
            $db = getDB();
    
            $sql = "SELECT * FROM usu_cliente WHERE email1 = :email";

            $stmt = $db->prepare($sql);
            $stmt->bindParam("email", $email);
            $stmt->execute();
            $data = $stmt->fetchAll(PDO::FETCH_OBJ);
    
            return $data;
        } catch (PDOException $e) {
            $error = '{"error":{"text":' . $e->getMessage() . '}}';
            die(json_encode(array(
                "status" => 500,
                "error" => $error,
                "message" => "Erro no servidor"
            )));
        }
    }

    public static function get_usuarios(array $fields)
    {

        $pagina = $fields['pagina'] ? $fields['pagina']  : 0;
        $limite = $fields['limite'] ? $fields['limite'] : 10;

        $qnt = $pagina * $limite;

        $where = '';

        if (!empty($fields['condicao'])) {
            $condicao = $fields['condicao'];
            $where = "WHERE (uc.email1 LIKE '%{$condicao}%' or uc.nome LIKE '%{$condicao}%')";
        }
        try {
            $db = getDB();

            $sql = "SELECT uc.id, uc.nome, uc.email1, uc.email2,
                    uc.id_empresa, e.nome_fantasia as empresa,  uc.dt_cadastro, uc.whatsapp
                    FROM usu_cliente as uc
                    LEFT JOIN empresas as e ON e.id = uc.id_empresa
                    {$where}
                    LIMIT :limite OFFSET :qtd";

            $stmt = $db->prepare($sql);
            $stmt->bindParam('limite', $limite, PDO::PARAM_INT);
            $stmt->bindParam('qtd', $qnt, PDO::PARAM_INT);
            $stmt->execute();

            $data = $stmt->fetchAll(PDO::FETCH_OBJ);

            return $data;
        } catch (PDOException $e) {
            $error = '{"error":{"text":' . $e->getMessage() . '}}';
            die(json_encode(array(
                "status" => 500,
                "error" => $error,
                "message" => "Erro no servidor"
            )));
        }
    }

}