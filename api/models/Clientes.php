<?php


class Clientes
{
    public static function soma_valor_mensal_contratos()
    {
        try {
            $db = getDB();

            //id 543 e 544 "clientes" fictícios internos
            $sql = "SELECT SUM(valor_contrato) as soma_valores_total FROM cliente 
            WHERE excluido != 'S' and id != 543 and id != 544 and periodicidade_pagamento = 'mensal'";
            $stmt = $db->query($sql);
            $data = $stmt->fetch(PDO::FETCH_ASSOC);

            return ($data);

        } catch (PDOException $e) {
            $error = '{"error":{"text":' . $e->getMessage() . '}}';
            die(json_encode(array(
                "status" => 500,
                "error" => $error,
                "message" => "Erro no servidor"
            )));
        }
    }

    public static function soma_valor_anual_contratos()
    {
        try {
            $db = getDB();

            //id 543 e 544 "clientes" fictícios internos
            $sql = "SELECT SUM(valor_contrato) as soma_valores_total FROM cliente 
            WHERE excluido != 'S' and id != 543 and id != 544 and periodicidade_pagamento = 'anual'";
            $stmt = $db->query($sql);
            $data = $stmt->fetch(PDO::FETCH_ASSOC);

            return ($data);

        } catch (PDOException $e) {
            $error = '{"error":{"text":' . $e->getMessage() . '}}';
            die(json_encode(array(
                "status" => 500,
                "error" => $error,
                "message" => "Erro no servidor"
            )));
        }
    }

    public static function total_clientes(){
        try {
            $db = getDB();

            //id 543 e 544 "clientes" fictícios internos
            $sql = "SELECT COUNT(id) as total_clientes FROM cliente 
            WHERE excluido != 'S' and id != 543 and id != 544";
            $stmt = $db->query($sql);
            $data = $stmt->fetch(PDO::FETCH_ASSOC);

            return ($data);

        } catch (PDOException $e) {
            $error = '{"error":{"text":' . $e->getMessage() . '}}';
            die(json_encode(array(
                "status" => 500,
                "error" => $error,
                "message" => "Erro no servidor"
            )));
        }
    }

    public static function get_empresa($id){
        try {
            $db = getDB();

            $sql = "SELECT * FROm clientes WHERE id = :id";
            $stmt = $db->prepare($sql);
            $stmt->bindParam("ID", $id, PDO::PARAM_INT);
            $stmt->execute();

            $data = $stmt->fetch(PDO::FETCH_ASSOC);

            return ($data);

        } catch (PDOException $e) {
            $error = '{"error":{"text":' . $e->getMessage() . '}}';
            die(json_encode(array(
                "status" => 500,
                "error" => $error,
                "message" => "Erro no servidor"
            )));
        }
    }
}
