<?php


class Adendos {
    
    public static function cadastrar(array $fields)
    {

        // var_dump(Contratos::$status_ativos);
        $dt_proximoReajuste = $fields['dt_reajuste_nova'];
        unset($fields['dt_reajuste_nova']);
        $contrato = Contratos::get_contrato(['id_contrato' => $fields['id_contrato']]);
        if($contrato === false){
            return [
                "status" => 2,
                "message" => 'Contrato não existe'
            ];
        }
        
        //verificando se realmente contrato está ativo
        $vigencia_final = new DateTime($contrato->dt_vigenciafinal);
        $now = (new DateTime())->modify('-3 hours');

        if ($now->getTimestamp() <= $vigencia_final->getTimestamp()) {

            if(in_array($contrato->id_status, Contratos::$status_ativos)){
                return [
                    "status" => 2,
                    "message" => 'Contrato ainda está vigente.'
                ];
            }
        }

        $adendo = self::get_adendo_vigente(["id_contrato" => $fields['id_contrato']]);
        if($adendo !== false){
            return [
                "status" => 2,
                "message" => 'Contrato já possui um adendo vigente'
            ];
        }

        $fields['dt_vigenciafinal_antiga'] = $contrato->dt_vigenciafinal;

        $now = new DateTime();
        $dt_vigenciafinal_nova = new DateTime($fields['dt_vigenciafinal_nova']);

        if($now->getTimestamp() >= $dt_vigenciafinal_nova->getTimestamp()){
            return [
                "status" => 2,
                "message" => 'Nova data da Vigência Final muito antiga.'
            ];
        }

        $meses = diff_meses($now->format('Y-m-d'), $dt_vigenciafinal_nova->format('Y-m-d'));
        if ($meses <= 0) {
            return [
                "status" => 2,
                "message" => 'Adendo com menos de 1 mês de validade',
                "http" => 400
            ];
        }

        if (array_key_exists('horas_totais_nova', $fields)) {
            if($contrato->tipo_contrato === 'manutencao' || $contrato->tipo_contrato === 'manutencao_saas'){
                $meses = diff_meses($contrato->dt_vigenciafinal, $fields['dt_vigenciafinal_nova']);
                $horas_contrato = $fields['horas_totais_nova'];

                if($fields['horas_totais_nova'] <= 0){
                    return [
                        "status" => 2,
                        "message" => 'As horas do contrato não podem ser igual ou menor a 0.'
                    ];
                }

                if ($meses > 0) {
                    if ($meses < 12) {
                        $horas_mes = ceil($horas_contrato / $meses);
                    } else {
                        $horas_mes = ceil($horas_contrato / 12);
                    }
                } else {
                    return [
                        "status" => 2,
                        "message" => 'Contrato com menos de 1 mês de validade',
                        "http" => 400
                    ];
                }

            }
        }

        try {

            $db = getDB();

            $query = fieldsToInsertQuery($fields);
            $sql = "INSERT INTO adendos ({$query[0]}) VALUES ({$query[1]})";
            $stmt = $db->prepare($sql);

            $int = ['id_contrato', 'id_cadastrante'];
            foreach ($fields as $key => &$value) {
                $param = array_search($key, $int) ? PDO::PARAM_INT : PDO::PARAM_STR;
                $stmt->bindParam(':' . $key, $value, $param);
            }

            $db = null;

            $stmt->execute();

        } catch (PDOException $e) {
            $error = '{"error":{"text":' . $e->getMessage() . '}}';
            http_response_code(500);
            die(json_encode(array(
                "status" => 500,
                "error" => $error,
                "message" => "Erro no servidor"
            )));
        }


        $fields_novo['id_status'] = 1;
        $fields_novo['dt_vigenciafinal'] = $dt_vigenciafinal_nova->format('Y-m-d');
        $fields_novo['id_contrato'] = $contrato->id;
        $fields_novo['dt_proximoReajuste'] = $dt_proximoReajuste;
        $fields_novo['valor_total'] = $fields['valor_total_nova'];
        if (array_key_exists('usuarios_saas_nova', $fields)) {
            $fields_novo['usuarios_saas'] = $fields['usuarios_saas_nova'];
        }
        if (array_key_exists('horas_totais_nova', $fields)) {
            $fields_novo['horas_totais'] = $fields['horas_totais_nova'];
            $fields_novo['horas_mes'] = $horas_mes ;
        }
        //die(json_encode($fields_novo));
        Contratos::editar($fields_novo);
        

        if($contrato->tipo_contrato === 'manutencao'){

            $minutos_mes = ceil($contrato->horas_mes * 60);
            SaldoHoras::lancar(["id_empresa" => $contrato->id_empresa, 'minutos' => $minutos_mes]);

            $horas_recebido = [
                "minutos" => $minutos_mes,
                "id_contrato" => $contrato->id
            ];

            HorasAReceber::cadastrar_horas_recebidas_contrato($horas_recebido);

            $horas_a_receber = [
                "dt_inicial" => $fields['dt_vigenciainicial'],
                "dt_final" => $fields['dt_vigenciafinal'],
                "minutos" => $minutos_mes,
                "id_contrato" => $contrato->id
            ];

            HorasAReceber::cadastrar($horas_a_receber);
        }


        return [
            "status" => 1,
            'mesage' => 'Adendo Cadastrado com sucesso.'
        ];
    }


    public static function get_adendo_vigente(array $fields){
        try {

            $db = getDB();

            $sql = "SELECT * FROM adendos WHERE id_contrato = :id_contrato and dt_vigenciafinal_nova > NOW()";

            // die($sql);

            $stmt = $db->prepare($sql);
            $stmt->bindParam('id_contrato', $fields['id_contrato'], PDO::PARAM_INT);
            $stmt->execute();

            $data = $stmt->fetch(PDO::FETCH_OBJ);

            $db = null;

            return $data;
        } catch (PDOException $e) {
            $error = '{"error":{"text":' . $e->getMessage() . '}}';
            http_response_code(500);
            die(json_encode(array(
                "status" => 500,
                "error" => $error,
                "message" => "Erro no servidor"
            )));
        }
    }

    public static function get_adendos_contratos(array $fields){
        try {

            $db = getDB();

            $sql = "SELECT * FROM adendos WHERE id_contrato = :id_contrato
                    ORDER BY id DESC";

            // die($sql);

            $stmt = $db->prepare($sql);
            $stmt->bindParam('id_contrato', $fields['id_contrato'], PDO::PARAM_INT);
            $stmt->execute();

            $data = $stmt->fetchAll(PDO::FETCH_OBJ);

            $db = null;

            return $data ?? [];
            
        } catch (PDOException $e) {
            $error = '{"error":{"text":' . $e->getMessage() . '}}';
            http_response_code(500);
            die(json_encode(array(
                "status" => 500,
                "error" => $error,
                "message" => "Erro no servidor"
            )));
        }
    }
}