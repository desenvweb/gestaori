<?php


class Notificacoes
{
    public static function get_notifcacoes($id)
    {

        try {
            $db = getDB();

            if ($id == 71){
                $sql = "SELECT * FROM notificacao 
                WHERE id_usuario <> :id and resolvida = FALSE and descricao like 'Novo comentário cadastrado%'
                union
                SELECT * FROM notificacao 
                WHERE id_usuario = :id and resolvida = FALSE
                ORDER BY id DESC
                LIMIT 50";
            }else{
                $sql = "SELECT * FROM notificacao 
                WHERE id_usuario = :id and resolvida = FALSE
                ORDER BY id DESC
                LIMIT 50";
            }

            $stmt = $db->prepare($sql);
            $stmt->bindParam('id', $id, PDO::PARAM_INT);
            $stmt->execute();

            $data = $stmt->fetchAll(PDO::FETCH_OBJ);

            $ticket_id = 0;
            $ticket_list = array("0");
            foreach ($data as $d) {
                $ticket_transf = strpos($d->descricao, "transferido para sua responsabilidade");
                if ($ticket_transf > 0){
                    $ticket_id = substr($d->descricao, 7, 5);
                }else{
                    $ticket_coment = strpos($d->descricao, "Novo comentário cadastrado no ticket");
                    if ($ticket_coment >= 0){
                        $ticket_id = substr($d->descricao, 38);
                        if (array_search($ticket_id, $ticket_list)){
                            $d->descricao = $d->descricao;
                            self::update_notificacao_ok($d->id);
                        }else{
                            array_push($ticket_list,$ticket_id);
                            $d->descricao = $d->descricao;
                        }
                        
                    }else{
                        $ticket_id = 0;
                    }
                }
                $d->ticket = $ticket_id;
                if ($ticket_id > 0) {
                    $status_ticket = Ticket::get_statusticket($ticket_id);
                
                    if ($status_ticket[0]->id_status == 3){
                        self::update_notificacao_ok($d->id);
                        $d->ticket_status = $status_ticket[0]->id_status;
                    }else{
                        $d->ticket_status = $status_ticket[0]->status;
                    }
                }
            }

            $data = array_filter($data, function ($var) {
                return (strpos($var->ticket_status, 3) === false);
            });

            return $data;

            $db = null;
        } catch (PDOException $e) {
            $error = '{"error":{"text":' . $e->getMessage() . '}}';
            die(json_encode(array(
                "status" => 500,
                "error" => $error,
                "message" => "Erro no servidor"
            )));
        }
    }

    public static function add_notifcacao($fields = [])
    {

        $fields['dt_cadastro'] = date('Y-m-d H:i:s', time() - 3 * 60 * 60);
        $fields = array_filter($fields);

        try {
            $db = getDB();

            $query = fieldsToInsertQuery($fields);
            $sql = "INSERT INTO notificacao ({$query[0]}) VALUES ({$query[1]})";

            //die($sql);
            $stmt = $db->prepare($sql);
            $int = ['id_usuario'];
            foreach ($fields as $key => &$value) {
                $param = array_search($key, $int) ? PDO::PARAM_INT : PDO::PARAM_STR;
                $stmt->bindParam(':' . $key, $value, $param);
            }
            $stmt->execute();

            return array(
                "status" => 1,
                "message" => "Adicionado com sucesso"
            );

            $db = null;
        } catch (PDOException $e) {
            $error = '{"error":{"text":' . $e->getMessage() . '}}';
            die(json_encode(array(
                "status" => 500,
                "error" => $error,
                "message" => "Erro no servidor"
            )));
        }
    }

    public static function update_notificacao_ok($id)
    {
        try {
            $db = getDB();

            $sql = "UPDATE notificacao SET resolvida = TRUE 
                    WHERE id = :id";

            $stmt = $db->prepare($sql);
            $stmt->bindParam('id', $id, PDO::PARAM_INT);
            $stmt->execute();
            
            //die(json_encode(array(
            //    "status" => 1,
            //    "message" => "Notificação editada com sucesso"
            //)));            
            
            $db = null;
        } catch (PDOException $e) {
            $error = '{"error":{"text":' . $e->getMessage() . '}}';
            die(json_encode(array(
                "status" => 500,
                "error" => $error,
                "message" => "Erro no servidor"
            )));
        }
    }
}
