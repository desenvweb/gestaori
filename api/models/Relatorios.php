<?php

use Dompdf\Dompdf;

include_once dirname(__FILE__) . '/../../vendor/autoload.php';


class Relatorios
{
    public static function relatorio_ticket(array $fields = [])
    {

        $ticket = Ticket::get_ticket(['id_ticket' => $fields['id_ticket']]);

        $html = '';

        $head = '<fieldset>';
        $head .= '<p><b>Ticket:</b> ' . $ticket->id . '</p>';
        $head .= '<p><b>Titulo:</b> ' . $ticket->titulo . '</p>';
        $head .= '<p><b>Data Abertura:</b> ' . formatDMYHHMMSS($ticket->dt_abertura) . '</p>';
        $head .= '<p><b>Empresa:</b> ' . $ticket->nome_fantasia . '</p>';
        $head .= '</fieldset>';

        $body = '';

        foreach ($ticket->comentarios as $key => $value) {
                $dt_cadastro = formatDMYHHMMSS($value->dt_cadastro);
                $body .= "<p> {$dt_cadastro} - {$value->nome} </p>";
                $body .= "<p> {$value->texto} </p>";
                $body .= '<br>';
        }

        $html .= $head;
        $html .= $body;

        $pdf = new Dompdf();

        $pdf->loadHtml($html);
        $pdf->render();
        echo $pdf->output();

        header('Content-Type: application/pdf');
        die();
    }
}
