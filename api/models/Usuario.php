<?php

include_once dirname(__FILE__) . "/HorasTrab.php";


class Usuario
{
    public static function set_status($fields = [])
    {
        $fields['dt_inicio'] =  date("Y-m-d H:i:s", time() - 3 * 60 * 60);
        unset($fields['dt_fim']);

        $fields = array_filter($fields);

        try {
            $db = getDB();

            $usuario = get_usuarioID($fields['id_usuario']);
            if (sizeof($usuario) > 0) {

                //Atualiza o log anterior
                $sql = "UPDATE log_statusUsu SET dt_fim = :dt_fim
                        WHERE id_usuario = :id_usuario and dt_fim IS NULL
                        ORDER BY id DESC LIMIT 1";

                $stmt = $db->prepare($sql);
                $stmt->bindParam("dt_fim", $fields['dt_inicio']);
                $stmt->bindParam("id_usuario", $fields['id_usuario'], PDO::PARAM_INT);
                $stmt->execute();

                //Cria o novo log
                $query = fieldsToInsertQuery($fields);
                $sql = "INSERT INTO log_statusUsu ({$query[0]}) VALUES ({$query[1]})";

                $stmt = $db->prepare($sql);

                $int = ['id_usuario'];
                foreach ($fields as $key => &$value) {
                    $param = array_search($key, $int) ? PDO::PARAM_INT : PDO::PARAM_STR;
                    $stmt->bindParam(':' . $key, $value, $param);
                }

                $stmt->execute();

                return array(
                    "status" => 1,
                    "message" => "Status alterado com sucesso"
                );
            } else {
                return array(
                    "status" => 2,
                    "message" => "Não encontrou o usuário"
                );
            }

            $db = null;
        } catch (PDOException $e) {
            $error = '{"error":{"text":' . $e->getMessage() . '}}';
            die(json_encode(array(
                "status" => 500,
                "error" => $error,
                "message" => "Erro no servidor"
            )));
        }
    }

    public static function get_status($id_usuario)
    {
        try {
            $db = getDB();

            $usuario = get_usuarioID($id_usuario);
            if (sizeof($usuario) > 0) {

                $sql = "SELECT * FROM log_statusUsu
                        WHERE id_usuario = :id_usuario AND dt_fim IS NULL";

                $stmt = $db->prepare($sql);
                $stmt->bindParam("id_usuario", $id_usuario, PDO::PARAM_INT);
                $stmt->execute();

                $data = $stmt->fetch(PDO::FETCH_OBJ);

                return array(
                    "status" => 1,
                    "data" => $data
                );
            } else {
                return array(
                    "status" => 2,
                    "message" => "Não encontrou o usuário"
                );
            }

            $db = null;
        } catch (PDOException $e) {
            $error = '{"error":{"text":' . $e->getMessage() . '}}';
            die(json_encode(array(
                "status" => 500,
                "error" => $error,
                "message" => "Erro no servidor"
            )));
        }
    }


    public static function get_situacao($id_usuario)
    {
        $status = self::get_status($id_usuario);
        $trabalhando = HorasTrab::verifica_trabalho($id_usuario);
        $usuario = get_usuarioID($id_usuario);

        if (!$status['data']) {
            $status['data']->status = 'ausente';
            $status['data']->dt_inicio = $usuario[0]->dt_cadastro ?? date("Y-m-d H:i:s", time());
        }

        if ($trabalhando['status'] == 2) {
            $data = ["situacao" => $status['data']->status, "dt_inicio" => $status['data']->dt_inicio];
        } else {
            $data = ["situacao" => $trabalhando['message'], "dt_inicio" => $trabalhando['data']->dt_inicio];
        }

        return $data;
    }

    public static function get_user(array $fields)
    {
        try {
            $db = getDB();

            $sql = "SELECT * FROM usuario
                    WHERE id = :id";

            $stmt = $db->prepare($sql);
            $stmt->bindParam('id', $fields['id_usuario'], PDO::PARAM_INT);
            $stmt->execute();

            $data = $stmt->fetch(PDO::FETCH_OBJ);

            if($data !== false){
                return $data;
            }else{
               return self::get_usuarios_antigos($fields);
            }
            

            $db = null;
        } catch (PDOException $e) {
            $error = '{"error":{"text":' . $e->getMessage() . '}}';
            die(json_encode(array(
                "status" => 500,
                "error" => $error,
                "message" => "Erro no servidor"
            )));
        }
    }

    public static function get_pj_vencer()
    {
        //Filtros

        try {
            $db = getDB();
            $sql = "SELECT u.nome ,u.funcao ,u.dt_inicio_trabalho ,u.vencimento_contrato FROM usuario u 
                WHERE u.tipo_colaborador LIKE 'PJ' and u.vencimento_contrato <= CURRENT_DATE() + INTERVAL 15 DAY 
                order by u.vencimento_contrato";

            //die($sql);
            $stmt = $db->prepare($sql);
            $stmt->execute();
            $data = $stmt->fetchAll(PDO::FETCH_OBJ);

            $db = null;
            return $data;
        } catch (PDOException $e) {
            $error = '{"error":{"text":' . $e->getMessage() . '}}';
            die(json_encode(array(
                "status" => 500,
                "error" => $error,
                "message" => "Erro no servidor"
            )));
        }
    }
       
    public static function get_usuarios_antigos(array $fields){
        try {
            $db = getDB();
    
            $sql = "SELECT * FROM usuarios WHERE id = :ID";
    
            $stmt = $db->prepare($sql);
            $stmt->bindParam("ID", $fields['id_usuario'], PDO::PARAM_INT);
            $stmt->execute();
            $data = $stmt->fetch(PDO::FETCH_OBJ);
    
            return $data;
        } catch (PDOException $e) {
            $error = '{"error":{"text":' . $e->getMessage() . '}}';
            die(json_encode(array(
                "status" => 500,
                "error" => $error,
                "message" => "Erro no servidor"
            )));
        }
    }

    public static function editar_usuario(array $fields)
    {
        $fields = array_filter($fields);
        $id = $fields['id'];
        $email = $fields['email'];

        if (!empty($fields['cnpj'])) {
            $fields['cpf'] = null;
        } else {
            $fields['cnpj'] = null;
        }

        if (empty($fields['direito_ferias'])) {
            $fields['direito_ferias'] = null;
        }
        
        if (empty($fields['vencimento_ferias'])) {
            $fields['vencimento_ferias'] = null;
        }

        if (empty($fields['salario'])) {
            $fields['salario'] = null;
        }

        if (empty($fields['vencimento_contrato'])) {
            $fields['vencimento_contrato'] = null;
        }

        if (empty($fields['experiencia'])) {
            $fields['experiencia'] = null;
        }

        if (empty($fields['instrucao'])) {
            $fields['instrucao'] = null;
        }

        if (empty($fields['avaliacao'])) {
            $fields['avaliacao'] = null;
        }

        if (!empty($fields['senha'])) {
            $fields['senha'] = strtoupper(md5($fields['senha']));
        }

        if (empty($fields['super_admin'])) {
            $fields['super_admin'] = 0;
        }

        unset($fields['id']);
        $query =fieldsToEditQuery($fields);

        try {
            $db = getDB();
            $usuario = get_usuarioID($id);

            if (sizeof($usuario) > 0) {

                $verifica = get_usuarioEmail($email);

                if ($verifica[0]->id == $id || sizeof($verifica) === 0) {

                    $sql = "UPDATE usuario SET {$query} 
                        WHERE id = :ID";

                    $stmt = $db->prepare($sql);
                    //die($sql);

                    $int = ['tipo_usuario', 'id_setor'];
                    foreach ($fields as $key => &$value) {
                        $param = array_search($key, $int) ? PDO::PARAM_INT : PDO::PARAM_STR;
                        $stmt->bindParam(':' . $key, $value, $param);
                    }
                    $stmt->bindParam("ID", $id, PDO::PARAM_INT);

                    $stmt->execute();

                    Anexos::adicionar_anexo(['id_contratado' => $id], 'anexo_ps');

                    die(json_encode(array(
                        "status" => 1,
                        "message" => "Usuário editado"
                    )));
                } else {
                    die(json_encode(array(
                        "status" => 3,
                        "message" => "E-mail já cadastro"
                    )));
                }
            } else {
                die(json_encode(array(
                    "status" => 2,
                    "message" => "Não foi possível editar usuário"
                )));
            }

            $db = null;
        } catch (PDOException $e) {
            $error = '{"error":{"text":' . $e->getMessage() . '}}';
            die(json_encode(array(
                "status" => 500,
                "error" => $error,
                "message" => "Erro no servidor"
            )));
        }
    }
}
