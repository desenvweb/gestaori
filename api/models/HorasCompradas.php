<?php



class HorasCompradas {

    public static function cadastrar(array $fields){

        $now = new DateTime();
        $now = $now->modify('-3 hours');

        $fields['dt_cadastro'] = $now->format('Y-m-d H:i:s');

        try {

            $db = getDB();

            $query = fieldsToInsertQuery($fields);
            $sql = "INSERT INTO horas_compradas ({$query[0]}) VALUES ({$query[1]})";
            $stmt = $db->prepare($sql);


            $int = ['id_empresa', 'total_horas'];
            foreach ($fields as $key => &$value) {
                $param = array_search($key, $int) ? PDO::PARAM_INT : PDO::PARAM_STR;
                $stmt->bindParam(':' . $key, $value, $param);
            }

            $stmt->execute();

            $minutos_lancar = ceil($fields['total_horas'] * 60);

            SaldoHoras::lancar(['id_empresa' => $fields['id_empresa'], 'minutos' => $minutos_lancar]);

            return array(
                "status" => 1,
                "message" => "Horas cadastradas com sucesso",
                "http" => 200
            );

            $db = null;
        } catch (PDOException $e) {
            $error = '{"error":{"text":' . $e->getMessage() . '}}';
            http_response_code(500);
            die(json_encode(array(
                "status" => 500,
                "error" => $error,
                "message" => "Erro no servidor"
            )));
        }
    }

    public static function get_horas_empresa(array $fields){
        try {

            $db = getDB();

            $sql = "SELECT hc.id, hc.id_empresa, hc.valor_total, hc.total_horas, hc.obs, hc.id_cadastrante, hc.dt_cadastro, u.nome as cadastrante
                    FROM horas_compradas as hc
                    LEFT JOIN usuario as u ON u.id = hc.id_cadastrante
                    WHERE id_empresa = :id_empresa";
            $stmt = $db->prepare($sql);

            $stmt->bindParam(':id_empresa', $fields['id_empresa'], PDO::PARAM_INT);

            $stmt->execute();
            $data = $stmt->fetchAll(PDO::FETCH_OBJ);

            return $data;

            $db = null;
        } catch (PDOException $e) {
            $error = '{"error":{"text":' . $e->getMessage() . '}}';
            http_response_code(500);
            die(json_encode(array(
                "status" => 500,
                "error" => $error,
                "message" => "Erro no servidor"
            )));
        }
    }
}