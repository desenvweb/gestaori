<?php


class Fornecedores
{

    public static function get_fornecedores(array $fields)
    {
        $pagina = isset($fields['pagina']) ? $fields['pagina']  : 0;
        $limite = isset($fields['limite']) ? $fields['limite'] : 10;

        $qnt = $pagina * $limite;

        $where = 'WHERE 1';
        $join = '';

        if (!empty($fields['condicao'])) {
            $condicao = $fields['condicao'];
            $where .= " and (f.cnpj LIKE '%{$condicao}%' or f.nome_fantasia LIKE '%{$condicao}%' 
                            or f.razao_social LIKE '%{$condicao}%' or f.categoria LIKE '%{$condicao}%')";
        }

        try {

            $db = getDB();

            $sql = "SELECT f.* FROM fornecedores f
                    {$join}
                    {$where}
                    ORDER BY f.nome_fantasia
                    LIMIT :LIMITE OFFSET :QTD";
            
            $stmt = $db->prepare($sql);
            $stmt->bindParam('LIMITE', $limite, PDO::PARAM_INT);
            $stmt->bindParam('QTD', $qnt, PDO::PARAM_INT);

            //die($sql);
            $stmt->execute();

            $data = $stmt->fetchAll(PDO::FETCH_OBJ);

            return $data;

            $db = null;
        } catch (PDOException $e) {
            $error = '{"error":{"text":' . $e->getMessage() . '}}';
            http_response_code(500);
            die(json_encode(array(
                "status" => 500,
                "error" => $error,
                "message" => "Erro no servidor"
            )));
        }
    }

    public static function editar_fornecedor($fields = [])
    {

        $id = $fields['id'];

        $query = fieldsToEditQuery($fields);

        try {
            $db = getDB();

            $sql = "UPDATE fornecedores
                SET {$query}
                WHERE id = :id_fornecedor;";

            $stmt = $db->prepare($sql);

            $int = [];

            foreach ($fields as $key => &$value) {
                $param = array_search($key, $int) ? PDO::PARAM_INT : PDO::PARAM_STR;
                $stmt->bindParam(':' . $key, $value, $param);
            }

            $stmt->bindParam('id_fornecedor', $id, PDO::PARAM_INT);

            $stmt->execute();

            return [
                "status" => 1,
                "message" => "Fornecedor editado"
            ];

            $db = null;
            
        } catch (PDOException $e) {
            $error = '{"error":{"text":' . $e->getMessage() . '}}';
            die(json_encode(array(
                "status" => 500,
                "error" => $error,
                "message" => "Erro no servidor"
            )));
        }
    }

    public static function cadastrar_fornecedor($fields = [])
    {

        $now = (new DateTime())->modify('-3 hours');
        $fields['dt_cadastro'] = $now->format('Y-m-d H:i:s');

        try {
            $db = getDB();

            $query = fieldsToInsertQuery($fields);
            $sql = "INSERT INTO fornecedores ({$query[0]}) VALUES ({$query[1]})";

            $stmt = $db->prepare($sql);

            $int = [];
            foreach ($fields as $key => &$value) {
                $param = array_search($key, $int) ? PDO::PARAM_INT : PDO::PARAM_STR;
                $stmt->bindParam(':' . $key, $value, $param);
            }

            $stmt->execute();

            return [
                "status" => 1,
                "message" => "Fornecedor editado"
            ];

            $db = null;
        } catch (PDOException $e) {
            $error = '{"error":{"text":' . $e->getMessage() . '}}';
            die(json_encode(array(
                "status" => 500,
                "error" => $error,
                "message" => "Erro no servidor"
            )));
        }
    }

    public static function excluir_fornecedor(array $fields)
    {

        $id_fornecedor = $fields['id_fornecedor'];

        try {
            $db = getDB();

            $sql = "DELETE FROM fornecedores WHERE id = :id_fornecedor";

            $stmt = $db->prepare($sql);
            $stmt->bindParam('id_fornecedor', $id_fornecedor, PDO::PARAM_INT);
            $stmt->execute();

            return [
                "status" => 1,
                "message" => "Fornecedor excluido com sucesso"
            ];

            $db = null;
        } catch (PDOException $e) {
            $error = '{"error":{"text":' . $e->getMessage() . '}}';
            die(json_encode(array(
                "status" => 500,
                "error" => $error,
                "message" => "Erro no servidor"
            )));
        }      

    }
}
