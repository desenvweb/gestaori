<?php


class HorasAReceber
{

    public static function cadastrar(array $fields)
    {

        $dt_inicial  = (new DateTime($fields['dt_inicial']))->modify('first day of next month');
        $dt_final = (new DateTime($fields['dt_final']))->modify('first day of next month');

        // Define qual será o intervalo a ser calculado
        $interval = DateInterval::createFromDateString('1 month');
        // Cria o período de data entre o inicio, final e o intervalo
        $period = new DatePeriod($dt_inicial, $interval, $dt_final);

        $now = (new DateTime())->modify('-3 hours')->format("Y-m-d H:i:s");

        foreach ($period as $dt) {

            $fields_query = [
                'data_receber' => $dt->format("Y-m-d"),
                'minutos' => $fields['minutos'],
                'id_contrato' => $fields['id_contrato'],
                'data_cadastro' => $now,
                'recebido' => false,
                'adiantado' => false
            ];

            try {

                $db = getDB();

                $query = fieldsToInsertQuery($fields_query);
                $sql = "INSERT INTO horas_a_receber ({$query[0]}) VALUES ({$query[1]})";
                $stmt = $db->prepare($sql);

                $int = ['id_contrato', 'minutos', 'adiantado', 'recebido'];
                foreach ($fields_query as $key => &$value) {
                    $param = array_search($key, $int) ? PDO::PARAM_INT : PDO::PARAM_STR;
                    $stmt->bindParam(':' . $key, $value, $param);
                }

                 $ok = $stmt->execute();

                $db = null;
            } catch (PDOException $e) {
                $error = '{"error":{"text":' . $e->getMessage() . '}}';
                http_response_code(500);
                die(json_encode(array(
                    "status" => 500,
                    "error" => $error,
                    "message" => "Erro no servidor"
                )));
            }
        }

        return array(
            "status" => 1,
            "message" => "Horas a receber cadastradas com sucesso",
            "http" => 200
        );
    }


    public static function verificar()
    {

        try {
            $now = new DateTime();

            $date = $now->format("Y-m-d");
            $datetime = $now->modify('-3 hours')->format("Y-m-d H:i:s");

            $db = getDB();
            $stmt = $db->query("SELECT * FROM horas_a_receber WHERE data_receber = '{$date}' and recebido = false");

            $data = $stmt->fetchAll(PDO::FETCH_OBJ);

            foreach ($data as $d) {

                $minutos = $d->minutos;

                $contrato = Contratos::get_contrato(["id_contrato" => $d->id_contrato]);

                if ($contrato !== false) {
                    $contrato = $contrato['data'];

                    $id_empresa = $contrato->id_empresa;

                    //Credita à empresa as horas dela
                    $sql = "INSERT INTO saldo_horas (id_empresa, minutos, data_movimentacao) VALUES ({$id_empresa}, {$minutos}, '{$datetime}')";
                    $db->query($sql);

                    //Seta como recebido as horas 
                    $sql = "UPDATE horas_a_receber SET recebido = true WHERE id = {$d->id}";
                    $db->query($sql);
                } else {
                    $db->query("DELETE FROM horas_a_receber WHERE id_contrato = $d->id_contrato");
                    continue;
                }
            }


            $db = null;
        } catch (PDOException $e) {
            $error = '{"error":{"text":' . $e->getMessage() . '}}';
            enviar_email('desenvweb@redeindustrial.com.br', 'Erro na rota verificar_horas_a_receber do Sistema de Gestão', 'Erro: ' . $error, 'Desenvolvedor');

            http_response_code(500);
            die(json_encode(array(
                "status" => 500,
                "error" => $error,
                "message" => "Erro no servidor"
            )));
        }
    }


    public static function cadastrar_horas_recebidas_contrato(array $fields)
    {

        $now = new DateTime();

        $now_date = $now->format("Y-m-d");
        $now_datetime = $now->modify('-3 hours')->format("Y-m-d H:i:s");

        $fields_query = [
            'data_receber' => $now_date,
            'minutos' => $fields['minutos'],
            'id_contrato' => $fields['id_contrato'],
            'data_cadastro' => $now_datetime,
            'recebido' => true,
            'adiantado' => false
        ];

        try {

            $db = getDB();

            $query = fieldsToInsertQuery($fields_query);
            $sql = "INSERT INTO horas_a_receber ({$query[0]}) VALUES ({$query[1]})";

            $stmt = $db->prepare($sql);

            $int = ['id_contrato', 'minutos', 'adiantado', 'recebido'];
            foreach ($fields_query as $key => &$value) {
                $param = array_search($key, $int) ? PDO::PARAM_INT : PDO::PARAM_STR;
                $stmt->bindParam(':' . $key, $value, $param);
            }

           $ok = $stmt->execute();

           return $ok;
            $db = null;
        } catch (PDOException $e) {
            $error = '{"error":{"text":' . $e->getMessage() . '}}';
            http_response_code(500);
            die(json_encode(array(
                "status" => 500,
                "error" => $error,
                "message" => "Erro no servidor"
            )));
        }

        return array(
            "status" => 1,
            "message" => "Horas a receber cadastradas com sucesso",
            "http" => 200
        );
    }
}
