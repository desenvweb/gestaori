<?php


class AdendosController {
    public static function cadastrar(object $request, object $requester)
    {
        $fields = $request->post('');

        $fields['id_cadastrante'] = $requester->ID_USUARIO;

        $fields_expected = [
            'id_contrato' => [
                'type' => 'int',
                'required' => true
            ],
            'dt_vigenciafinal_nova' => [
                'type' => 'string',
                'required' => true
            ],
            'dt_reajuste_nova' => [
                'type' => 'string',
                'required' => true
            ],
            'horas_mensais_nova'=> [
                'type' => 'string',
                'required' => false
            ],
            'horas_totais_nova'=> [
                'type' => 'string',
                'required' => false
            ],
            'usuarios_saas_nova'=> [
                'type' => 'string',
                'required' => false
            ],
            'valor_total_nova'=> [
                'type' => 'string',
                'required' => true
            ],
            'id_cadastrante' => [
                'type' => 'int',
                'required' => true
            ],
            'obs' => [
                'type' => 'string',
                'required' => false
            ],

        ];

        $return = valida_campos($fields_expected, $fields);

        if ($return['success'] === true) {
            $response = Adendos::cadastrar($return['data']);
            http_response_code($response['http']);
            die(json_encode($response));
        } else {
            http_response_code($return['status']);
            die(json_encode($return));
        }
    }

}