
<?php
session_start();

use Dompdf\Dompdf;

include_once dirname(__FILE__) . "/../models/Usuario.php";
include_once dirname(__FILE__) . "/../models/HorasTrab.php";
include_once dirname(__FILE__) . '/../../vendor/autoload.php';
include_once dirname(__FILE__) . "/../models/Projeto.php";
include_once dirname(__FILE__) . "/../models/Contratos.php";


class HorasTrabController
{
    public static function lancar_horas(object $request)
    {
        $fields = $request->post('');

        $fields_expected = [
            'dt_lancamento' => [
                'type' => 'string',
                'required' => true
            ],
            'hora_ini' => [
                'type' => 'string',
                'required' => true
            ],
            'hora_fim' => [
                'type' => 'string',
                'required' => true
            ],
            'id_projeto' => [
                'type' => 'int',
                'required' => false
            ],
            'id_empresa' => [
                'type' => 'int',
                'required' => true,
            ],
            'id_usuario' => [
                'type' => 'int',
                'required' => true
            ],
            'descricao' => [
                'type' => 'string',
                'required' => true
            ],
            'id_ticket' => [
                'type' => 'int',
                'required' => false
            ],
            'id_atividade' => [
                'type' => 'int',
                'required' => true
            ]
        ];

        $return = valida_campos($fields_expected, $fields);

        if ($return['success'] === true) {
            $response = HorasTrab::lancar_horas($return['data']);
            die(json_encode($response));
        } else {
            http_response_code($return['status']);
            die(json_encode($return));
        }
    }

    public static function get_horasUsuario($fields)
    {
        $id_usuario = $fields['id_usuario'];
        $pagina = $fields['pagina'] ? $fields['pagina'] : 0;
        $limite = $fields['limite'] ? $fields['limite'] : 10;

        $qnt = $pagina * $limite;

        $where = 'WHERE ht.dt_fim IS NOT NULL and ht.id_usuario = :id_usuario';


        if (!empty($fields['cliente'])) {
            $where .= "and WHERE ht.id_empresa = {$fields['cliente']}";
        }
        if (!empty($fields['colaborador'])) {
            $where .= "and ht.id_usuario = {$fields['colaborador']}";
        }

        try {
            $db = getDB();

            $sql = "SELECT ht.id, ht.dt_inicio, ht.dt_fim, ht.diff_horas, ht.diff_minutos, 
                    ht.id_empresa, ht.id_usuario, ht.descricao, c.razao_social, ht.id_ticket 
                FROM horastrab AS ht
                LEFT JOIN cliente AS c ON c.id = ht.id_empresa
                ${where} ORDER BY ht.dt_inicio DESC
                LIMIT :limite OFFSET :qnt";

            $stmt = $db->prepare($sql);
            $stmt->bindParam('qnt', $qnt, PDO::PARAM_INT);
            $stmt->bindParam('limite', $limite, PDO::PARAM_INT);
            $stmt->bindParam("id_usuario", $id_usuario, PDO::PARAM_INT);
            $stmt->execute();
            $data = $stmt->fetchAll(PDO::FETCH_OBJ);

            if (sizeof($data) > 0) {
                die(json_encode(array(
                    "status" => 1,
                    "data" => $data,
                    "message" => "Horas retornadas com sucesso"
                )));
            } else {
                die(json_encode(array(
                    "status" => 2,
                    "message" => "Usuário não lançou horas"
                )));
            }


            $db = null;
        } catch (PDOException $e) {
            $error = '{"error":{"text":' . $e->getMessage() . '}}';
            die(json_encode(array(
                "status" => 500,
                "error" => $error,
                "message" => "Erro no servidor"
            )));
        }
    }

    public static function get_horas(object $request, object $requester)
    {
        $fields = $request->get('');

        if($requester->TIPO_USUARIO == 2){
            $fields['colaborador'] = $requester->ID_USUARIO;
        }

        $fields_expected = [
            'page' => [
                'type' => 'int',
                'required' => false
            ],
            'limite' => [
                'type' => 'int',
                'required' => false
            ],
            'id_usuario' => [
                'type' => 'int',
                'required' => false
            ],
            'id_empresa' => [
                'type' => 'int',
                'required' => false
            ],
            'dt_inicial' => [
                'type' => 'string',
                'required' => false,
            ],
            'dt_final' => [
                'type' => 'string',
                'required' => false
            ]
        ];

        $return = valida_campos($fields_expected, $fields);

        if ($return['success'] === true) {
            $response = HorasTrab::get_horas($return['data']);
            die(json_encode($response));
        } else {
            http_response_code($return['status']);
            die(json_encode($return));
        }
    }

    public static function editar_horas(object $request)
    {
        $fields = $request->post('');

        $fields_expected = [
            'id_usuario' => [
                'type' => 'int',
                'required' => true
            ],
            'id_empresa' => [
                'type' => 'int',
                'required' => true
            ],
            'hora_ini' => [
                'type' => 'string',
                'required' => true
            ],
            'dt_lancamento' => [
                'type' => 'string',
                'required' => true
            ],
            'id_ticket' => [
                'type' => 'int',
                'required' => false,
            ],
            'hora_fim' => [
                'type' => 'string',
                'required' => true
            ],
            'id' => [
                'type' => 'int',
                'required' => true
            ],
            'descricao' => [
                'type' => 'string',
                'required' => true
            ],
            'id_atividade' => [
                'type' => 'int',
                'required' => true
            ],
            'id_projeto' => [
                'type' => 'string',
                'required' => false
            ],
            
        ];

        $return = valida_campos($fields_expected, $fields);

        if ($return['success'] === true) {
            $response = HorasTrab::editar_horas($return['data']);
            die(json_encode($response));
        } else {
            http_response_code($return['status']);
            die(json_encode($return));
        }

    }

    public static function verifica_trabalho($fields = [])
    {
        $id_usuario = $fields['id_usuario'];

        try {
            $db = getDB();
            $sql = "SELECT ht.id, ht.dt_inicio, ht.dt_fim, ht.diff_horas,
                    ht.diff_minutos, ht.id_empresa, ht.id_usuario, ht.descricao,
                    cli.razao_social, cli.cnpj, usu.nome, ht.id_ticket, atv.nome as atividade,  p.nome as projeto 
                    FROM horastrab AS ht
                    LEFT JOIN usuario AS usu ON usu.id = ht.id_usuario  
                    LEFT JOIN cliente AS cli ON cli.id = ht.id_empresa
                    LEFT JOIN atividade AS atv ON atv.id = ht.id_atividade 
                    LEFT JOIN projeto as p ON p.id = ht.id_projeto
                    WHERE ht.id_usuario = :id_usuario AND ht.dt_fim IS NULL";

            $stmt = $db->prepare($sql);
            $stmt->bindParam("id_usuario", $id_usuario, PDO::PARAM_INT);
            $stmt->execute();
            $data = $stmt->fetch(PDO::FETCH_OBJ);

            if (!empty($data)) {
                die(json_encode(
                    array(
                        "status" => 1,
                        "data" => $data,
                        "message" => "Em trabalho"
                    )
                ));
            } else {
                die(json_encode(
                    array(
                        "status" => 2,
                        "message" => "Não está em trabalho"
                    )
                ));
            }
            $db = null;
        } catch (PDOException $e) {
            $error = '{"error":{"text":' . $e->getMessage() . '}}';
            die(json_encode(array(
                "status" => 500,
                "error" => $error,
                "message" => "Erro no servidor"
            )));
        }
    }

    public static function iniciar_ld($request, $requester)
    {
        $fields = $request->post('');

        $fields['id_usuario'] = $requester->ID_USUARIO;

        $fields_expected = [
            'id_projeto' => [
                'type' => 'int',
                'required' => false
            ],
            'id_atividade' => [
                'type' => 'int',
                'required' => true
            ],
            'id_empresa' => [
                'type' => 'int',
                'required' => false
            ],
            'descricao' => [
                'type' => 'string',
                'required' => true
            ],
            'id_ticket' => [
                'type' => 'int',
                'required' => false
            ],
            'id_usuario' => [
                'type' => 'int',
                'required' => true
            ]
        ];

        $return = valida_campos($fields_expected, $fields);

        if ($return['success'] === true) {
            $response = HorasTrab::iniciar_ld($return['data']);
            die(json_encode($response));
        } else {
            http_response_code($return['status']);
            die(json_encode($return));
        }
    }

    public static function finalizar_ld($request, $requester)
    {
        $fields = $request->post('');

        $fields['id_usuario'] = $requester->ID_USUARIO;

        $fields_expected = [
            'id_empresa' => [
                'type' => 'int',
                'required' => true
            ],
            'id_usuario' => [
                'type' => 'int',
                'required' => true
            ],
            'dt_inicio' => [
                'type' => 'string',
                'required' => true
            ]
        ];

        $return = valida_campos($fields_expected, $fields);

        if ($return['success'] === true) {
            $response = HorasTrab::finalizar_ld($return['data']);
            die(json_encode($response));
        } else {
            http_response_code($return['status']);
            die(json_encode($return));
        }
    }


    public static function fim_expediente()
    {
        try {
            $db = getDB();

            $sql = "SELECT h.* FROM horastrab h LEFT JOIN usuario u on u.id = h.id_usuario WHERE dt_fim IS NULL and u.tipo_colaborador = 'CLT'";

            $stmt = $db->prepare($sql);
            $stmt->execute();
            $data = $stmt->fetchAll(PDO::FETCH_OBJ);

            // Servicor de produção tem 3horas adiantiadas
            $dataAtual = date('Y-m-d H:i:s', time() - 3 * 60 * 60);

            $dtIniAl = date('Y-m-d') . " 12:00:00";
            $dtFimAl = date('Y-m-d') . " 13:00:00";

            $dtFimTar = date('Y-m-d') . " 17:30:00";

            if (strtotime($dataAtual) > strtotime($dtIniAl) && strtotime($dataAtual) < strtotime($dtFimAl)) {
                foreach ($data as $d) {

                    $dt_inicio = $d->dt_inicio;
                    $dt_fim = explode(" ", $d->dt_inicio)[0] . " 12:00:00";

                    $diff_minutos = diff_horas_em_minutos(
                        explode(" ", $dt_inicio)[1],
                        explode(" ", $dt_fim)[1]
                    );
                    $diff_horas = convert_minutes_in_hhmm($diff_minutos);

                    $sql = "UPDATE horastrab 
                        SET dt_fim = :dt_fim,
                            diff_horas = :diff_horas,
                            diff_minutos = :diff_minutos
                        WHERE id = :id";

                    $stmt = $db->prepare($sql);
                    $stmt->bindParam("dt_fim", $dt_fim, PDO::PARAM_STR);
                    $stmt->bindParam("diff_horas", $diff_horas, PDO::PARAM_STR);
                    $stmt->bindParam("diff_minutos", $diff_minutos, PDO::PARAM_STR);
                    $stmt->bindParam("id", $d->id, PDO::PARAM_INT);
                    $stmt->execute();
                }
            } else if (strtotime($dataAtual) > strtotime($dtFimTar)) {
                foreach ($data as $d) {

                    $dt_inicio = $d->dt_inicio;
                    $dt_fim = explode(" ", $d->dt_inicio)[0] . " 17:30:00";

                    $diff_minutos = diff_horas_em_minutos(
                        explode(" ", $dt_inicio)[1],
                        explode(" ", $dt_fim)[1]
                    );
                    $diff_horas = convert_minutes_in_hhmm($diff_minutos);

                    $sql = "UPDATE horastrab 
                        SET dt_fim = :dt_fim,
                            diff_horas = :diff_horas,
                            diff_minutos = :diff_minutos
                        WHERE id = :id";

                    $stmt = $db->prepare($sql);
                    $stmt->bindParam("dt_fim", $dt_fim, PDO::PARAM_STR);
                    $stmt->bindParam("diff_horas", $diff_horas, PDO::PARAM_STR);
                    $stmt->bindParam("diff_minutos", $diff_minutos, PDO::PARAM_STR);
                    $stmt->bindParam("id", $d->id, PDO::PARAM_INT);
                    $stmt->execute();
                }
            }

            $db = null;
        } catch (PDOException $e) {
            $error = '{"error":{"text":' . $e->getMessage() . '}}';
            die(json_encode(array(
                "status" => 500,
                "error" => $error,
                "message" => "Erro no servidor"
            )));
        }
    }

    public static function excluir_lancamento($fields = [])
    {
        $id = $fields['id_lancamento'];

        try {
            $db = getDB();

            $sql = "DELETE FROM horastrab
                WHERE id = :ID;";

            $stmt = $db->prepare($sql);
            $stmt->bindParam("ID", $id, PDO::PARAM_INT);
            $stmt->execute();

            die(json_encode(array(
                "status" => 1,
                "message" => "Lançamento excluido"
            )));

            $db = null;
        } catch (PDOException $e) {
            $error = '{"error":{"text":' . $e->getMessage() . '}}';
            die(json_encode(array(
                "status" => 500,
                "error" => $error,
                "message" => "Erro no servidor"
            )));
        }
    }

    public static function relatorio_horas($fields = [])
    {
        $data = HorasTrab::relatorio_horas($fields);

        die(json_encode($data));
    }

    public static function relatorio_horas_pdf($request)
    {

        $fields = $request->get('');

        $fields_expected = [
            'status_tkt' => [
                'type' => 'string',
                'required' => true
            ]
        ];

        $fields = $request->get('');

        $fields_expected = [
            'colaborador' => [
                'type' => 'int',
                'required' => true
            ],
            'cliente' => [
                'type' => 'string',
                'required' => false
            ],

            'dt_inicial' => [
                'type' => 'string',
                'required' => false
            ],
            'dt_final' => [
                'type' => 'string',
                'required' => false
            ]
        ];
        $return = valida_campos($fields_expected, $fields);

        if ($return['success'] === true) {
            $response = HorasTrab::relatorio_horas_pdf($return['data']);
            die(json_encode($response));
        } else {
            http_response_code($return['status']);
            die(json_encode($return));
        }

    }

    public static function relatorio_horas_e_projetos(object $request)
    {
        $fields = $request->post('');

        $fields_expected = [
            'id_empresa' => [
                'type' => 'int',
                'required' => true
            ],
            'dt_inicial' => [
                'type' => 'string',
                'required' => false
            ],
            'dt_final' => [
                'type' => 'string',
                'required' => false
            ]
        ];

        $return = valida_campos($fields_expected, $fields);

        if ($return['success'] === true) {

            $return = $return['data'];
            $horas = HorasTrab::relatorio_horas($return);
            $projetos = Projeto::get_projetos_descontados_empresa(['id_empresa' => $return['id_empresa']]);


            $infos = $horas['data']['info'];
            
            $infos->horas_totais_projetos = $projetos->total_horas;
            $infos->total_projetos = $projetos->total_projetos;

            unset($horas['data']['info']);

            $data = [
            "status" => 1,
            "data" => ["horas" => $horas['data'], "info" => $infos]
             ];

            die(json_encode($data));
        } else {
            http_response_code($return['status']);
            die(json_encode($return));
        }
    }

    public static function relatorio_horas_empresa_pdf($request)
    {
        $fields = $request->get('');

        $fields_expected = [
            'id_empresa' => [
                'type' => 'int',
                'required' => true
            ],
            'dt_inicial' => [
                'type' => 'string',
                'required' => false
            ],
            'dt_final' => [
                'type' => 'string',
                'required' => false
            ]
        ];

        $return = valida_campos($fields_expected, $fields);

        if ($return['success'] === true) {

            $return = $return['data'];
            $horas = HorasTrab::relatorio_horas_empresa_pdf($return);

            die();
        } else {
            http_response_code($return['status']);
            die(json_encode($return));
        }
    }
}

?>
