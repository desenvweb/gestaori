<?php

require_once dirname(__FILE__) . "/../models/UsuarioCliente.php";

class UsuarioClienteController
{

    public static function cadastrar_usuario($fields = [])
    {

        $fields['senha'] = password_hash('redeindustrial', PASSWORD_BCRYPT);
        $fields['dt_cadastro'] = date('Y-m-d H:i:s', time() - 3 * 60 * 60);

        try {

            $verifica = UsuarioCliente::get_usuario_email($fields['email']);

            if (sizeof($verifica) <= 0) {
                $db = getDB();

                $query = fieldsToInsertQuery($fields);

                $sql = "INSERT INTO usu_cliente ({$query[0]}) VALUES ({$query[1]})";

                $stmt = $db->prepare($sql);

                $int = ['id_empresa', 'id_cadastrante'];
                foreach ($fields as $key => &$value) {
                    $param = array_search($key, $int) ? PDO::PARAM_INT : PDO::PARAM_STR;
                    $stmt->bindParam(':' . $key, $value, $param);
                }

                $stmt->execute();

                die(json_encode(array(
                    "status" => 1,
                    "message" => "Usuário cadastrado com sucesso"
                )));
            } else {
                die(json_encode(array(
                    "status" => 2,
                    "message" => "E-mail já cadastrado"
                )));
            }

            $db = null;
        } catch (PDOException $e) {
            $error = '{"error":{"text":' . $e->getMessage() . '}}';
            die(json_encode(array(
                "status" => 500,
                "error" => $error,
                "message" => "Erro no servidor"
            )));
        }
    }

    public static function editar_usuario($fields = [])
    {

        $id = $fields['id'];
        unset($fields['id']);

        try {
            $db = getDB();

            $query = fieldsToEditQuery($fields);

            $sql = "UPDATE usu_cliente
            SET {$query}
            WHERE id = :id ";

            $stmt = $db->prepare($sql);

            $int = ['id_empresa', 'id_cadastrante'];
            foreach ($fields as $key => &$value) {
                $param = array_search($key, $int) ? PDO::PARAM_INT : PDO::PARAM_STR;
                $stmt->bindParam(':' . $key, $value, $param);
            }

            $stmt->bindParam(':id', $id, PDO::PARAM_INT);

            $stmt->execute();

            die(json_encode(array(
                "status" => 1,
                "message" => "Usuário cadastrado com sucesso"
            )));

            $db = null;
        } catch (PDOException $e) {
            $error = '{"error":{"text":' . $e->getMessage() . '}}';
            die(json_encode(array(
                "status" => 500,
                "error" => $error,
                "message" => "Erro no servidor"
            )));
        }
    }


    public static function excluir_usuario($fields = [])
    {
        $id = $fields['id_usuario'];
        try {
            $db = getDB();

            $verifica = UsuarioCliente::get_usuario(['id_usucliente' => $id]);

            if (sizeof($verifica) > 0) {

                $sql = "DELETE FROM usu_cliente
                            WHERE id = :id";
                $stmt = $db->prepare($sql);
                $stmt->bindParam(":id", $id);
                $stmt->execute();

                die(json_encode(array(
                    "status" => 1,
                    "message" => "Usuário excluido com sucesso"
                )));
            } else {
                die(json_encode(array(
                    "status" => 2,
                    "message" =>  'Nenhuma empresa com esse usuário'
                )));
            }
        } catch (PDOException $e) {
            $error = '{"error":{"text":' . $e->getMessage() . '}}';
            die(json_encode(array(
                "status" => 500,
                "error" => $error,
                "message" => "Erro no servidor"
            )));
        }
    }

    public static function get_usuarios(object $request)
    {
        $fields = $request->get('');

        $fields_expected = [
            'id_contrato' => [
                'pagina' => 'int',
                'required' => false
            ],
            'limite' => [
                'type' => 'string',
                'required' => false
            ],
            'condicao' => [
                'type' => 'string',
                'required' => false
            ]
        ];

        $return = valida_campos($fields_expected, $fields);

        if ($return['success'] === true) {
            $data = UsuarioCliente::get_usuarios($return['data']);

            if (sizeof($data) > 0) {
                die(json_encode([
                    "status" => 1,
                    "data" => $data,
                    "message" => "Usuários encontrados"
                ]));

            } else {
                die(json_encode([
                    "status" => 2,
                    "message" => "Não encontrou Usuários"
                ]));
            }

        } else {
            http_response_code($return['status']);
            die(json_encode($return));
        }
    }




    public static function get_usuario_email($fields = [])
    {
        $data = UsuarioCliente::get_usuario_email(['id_usucliente' => $fields['email']]);

        if (sizeof($data) > 0) {
            die(json_encode(array(
                "status" => 1,
                "data" => $data,
                "message" => "Usuário retornado com sucesso"
            )));
        } else {
            die(json_encode(array(
                "status" => 2,
                "message" => "Nenhum usuário encontrado"
            )));
        }
    }
}
