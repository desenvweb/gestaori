<?php

class EmpresasController
{

    public static function get_empresas($request)
    {

        $fields = $request->get('');

        $fields_expected = [
            'condicao' => [
                'type' => 'string',
                'required' => false
            ],
            'limite' => [
                'type' => 'int',
                'required' => false
            ] ,
            'pagina' => [
                'type' => 'int',
                'required' => false
            ],
            'contrato' => [
                'type' => 'int',
                'required' => false
            ],
            'tpcontrato' => [
                'type' => 'string',
                'required' => false
            ],
            'contratada' => [
                'type' => 'string',
                'required' => false
            ],
            'dtvgfinal_inicio' => [
                'type' => 'string',
                'required' => false
            ],
            'dtvgfinal_fim' => [
                'type' => 'string',
                'required' => false
            ],
            'periodicidade_pagamento_filtro' => [
                'type' => 'string',
                'required' => false
            ]
        ];

        $return =  valida_campos($fields_expected, $fields);

        if ($return['success'] === true) {
            $data = Empresas::get_empresas($return['data']);

            if (sizeof($data) > 0) {
                $response =  [
                    "status" => 1,
                    "message" => "Empresas encontradas com sucesso",
                    "data" => $data
                ];
            } else {
                $response = [
                    "status" => 2,
                    "message" => "Nenhuma empresa encontrado"
                ];
            }

            die(json_encode($response));

        } else {
            http_response_code($return['status']);
            die(json_encode($return));
        }
    }

    public static function get_empresas_com_horas($request)
    {

        $fields = $request->get('');

        $fields_expected = [
            'condicao' => [
                'type' => 'string',
                'required' => false
            ],
            'limite' => [
                'type' => 'int',
                'required' => false
            ] ,
            'pagina' => [
                'type' => 'int',
                'required' => false
            ]
        ];

        $return =  valida_campos($fields_expected, $fields);

        if ($return['success'] === true) {
            $data = Empresas::get_empresas_com_horas($return['data']);

            if (sizeof($data) > 0) {
                $response =  [
                    "status" => 1,
                    "message" => "Empresas encontradas com sucesso",
                    "data" => $data
                ];
            } else {
                $response = [
                    "status" => 2,
                    "message" => "Nenhuma empresa encontrado"
                ];
            }

            die(json_encode($response));

        } else {
            http_response_code($return['status']);
            die(json_encode($return));
        }
    }

    public static function get_todos_clientes($fields = [])
    {
        $where = '';
     
        if(!empty($fields['condicao'])){
            $condicao = $fields['condicao'];
            $where = "WHERE nome_fantasia LIKE '%{$condicao}%'";
        }

        $limite = $fields['limite'] ?? 10;

        try {

            $db = getDB();

            $sql1 = "SELECT * FROM empresas {$where} LIMIT :limite";

            $stmt1 = $db->prepare($sql1);
            $stmt1->bindParam("limite", $limite, PDO::PARAM_INT);
            $stmt1->execute();
            $data = $stmt1->fetchAll(PDO::FETCH_OBJ);

            die(json_encode([
                "status" => 1,
                "data" => $data,
                "message" => 'Empresas retornadas com sucesso' 
            ]));

            

        } catch (PDOException $e) {
            $error = '{"error":{"text":' . $e->getMessage() . '}}';
            die(json_encode(array(
                "status" => 500,
                "error" => $error,
                "message" => "Erro no servidor"
            )));
        }
    }

    public static function get_clientes_saas($fields = [])
    {
        $where = "WHERE (e.id in (select c.id_empresa from contratos c where c.tipo_contrato = 'saas' or c.tipo_contrato = 'manutencao_saas'))";
     
        if(!empty($fields['condicao'])){
            $condicao = $fields['condicao'];
            $where .= $where + " and (e.nome_fantasia LIKE '%{$condicao}%')";
        }

        $limite = $fields['limite'] ?? 10;

        try {

            $db = getDB();

            $sql1 = "SELECT e.* FROM empresas e {$where} order by e.nome_fantasia LIMIT :limite";
            $stmt1 = $db->prepare($sql1);
            $stmt1->bindParam("limite", $limite, PDO::PARAM_INT);
            $stmt1->execute();
            $data = $stmt1->fetchAll(PDO::FETCH_OBJ);

            die(json_encode([
                "status" => 1,
                "data" => $data,
                "message" => 'Empresas retornadas com sucesso' 
            ]));

            

        } catch (PDOException $e) {
            $error = '{"error":{"text":' . $e->getMessage() . '}}';
            die(json_encode(array(
                "status" => 500,
                "error" => $error,
                "message" => "Erro no servidor"
            )));
        }
    }

    public static function get_empresa($request)
    {

        $fields = $request->get('');


        $fields_expected = [
            'id_empresa' => [
                'type' => 'int',
                'required' => true
            ]
        ];

        $return = valida_campos($fields_expected, $fields);

        if ($return['success'] === true) {
            $data = Empresas::get_empresa($return['data']);

            if (sizeof($data) > 0) {
                $response =  [
                    "status" => 1,
                    "message" => "Empresa encontradas com sucesso",
                    "data" => $data
                ];
            } else {
                $response = [
                    "status" => 2,
                    "message" => "Nenhuma empresa encontrado"
                ];
            }

            die(json_encode($response));

        } else {
            http_response_code($return['status']);
            die(json_encode($return));
        }
    }

    public static function excluir_empresa($request)
    {

        $fields = $request->get('');


        $fields_expected = [
            'id_empresa' => [
                'type' => 'int',
                'required' => true
            ]
        ];

        $return = valida_campos($fields_expected, $fields);

        if ($return['success'] === true) {
            $response = Empresas::excluir_empresa($return['data']);
            die(json_encode($response));

        } else {
            http_response_code($return['status']);
            die(json_encode($return));
        }
    }


    public static function editar_empresa($request)
    {

        $fields = $request->post('');

        $fields_expected = [
            'id' =>[
                'type' => 'int',
                'required' => true
            ],
            'razao_social' => [
                'type' => 'string',
                'required' => true
            ],
            'nome_fantasia' => [
                'type' => 'string',
                'required' => true
            ],
            'cnpj' => [
                'type' => 'string',
                'required' => true
            ] ,
            'inscricao_estadual' => [
                'type' => 'string',
                'required' => false
            ],
            'cep' => [
                'type' => 'string',
                'required' => false
            ],
            'logradouro' => [
                'type' => 'string',
                'required' => false
            ],
            'numero' => [
                'type' => 'string',
                'required' => false
            ],
            'bairro' => [
                'type' => 'string',
                'required' => false
            ],
            'cidade' => [
                'type' => 'string',
                'required' => false
            ],
            'uf' => [
                'type' => 'string',
                'required' => false
            ],
            'contato' => [
                'type' => 'string',
                'required' => false
            ],
            'fone' => [
                'type' => 'string',
                'required' => false
            ],
            'email1' => [
                'type' => 'string',
                'required' => false
            ],
            'licenca' => [
                'type' => 'string',
                'required' => false
            ],
            'obs' => [
                'type' => 'string',
                'required' => false
            ],
            
        ];

        $return =  valida_campos($fields_expected, $fields);

        if ($return['success'] === true) {
            $response = Empresas::editar_empresa($return['data']);
            die(json_encode($response));

        } else {
            http_response_code($return['status']);
            die(json_encode($return));
        }
    }

    public static function cadastrar_empresa($request)
    {

        $fields = $request->post('');

        $fields_expected = [
            //'id' =>[
            //    'type' => 'int',
            //    'required' => true
            //],
            'razao_social' => [
                'type' => 'string',
                'required' => true
            ],
            'nome_fantasia' => [
                'type' => 'string',
                'required' => true
            ],
            'cnpj' => [
                'type' => 'string',
                'required' => true
            ] ,
            'inscricao_estadual' => [
                'type' => 'string',
                'required' => false
            ],
            'cep' => [
                'type' => 'string',
                'required' => false
            ],
            'logradouro' => [
                'type' => 'string',
                'required' => false
            ],
            'numero' => [
                'type' => 'string',
                'required' => false
            ],
            'bairro' => [
                'type' => 'string',
                'required' => false
            ],
            'cidade' => [
                'type' => 'string',
                'required' => false
            ],
            'uf' => [
                'type' => 'string',
                'required' => false
            ],
            'contato' => [
                'type' => 'string',
                'required' => false
            ],
            'fone' => [
                'type' => 'string',
                'required' => false
            ],
            'email1' => [
                'type' => 'string',
                'required' => false
            ],
            'licenca' => [
                'type' => 'string',
                'required' => false
            ],
            'obs' => [
                'type' => 'string',
                'required' => false
            ],
            
        ];

        $return =  valida_campos($fields_expected, $fields);

        if ($return['success'] === true) {
            $response = Empresas::cadastrar_empresa($return['data']);
            die(json_encode($response));

        } else {
            http_response_code($return['status']);
            die(json_encode($return));
        }
    }

    public static function dados_gerais()
    {
        try {

            $db = getDB();

            $valor_mensal_total = Empresas::soma_valor_mensal_contratos();
            $valor_anual_total = Empresas::soma_valor_anual_contratos();
            $total_clients = Empresas::total_clientes();

            $sql = "SELECT SUM(usuarios_saas) as usuarios_saas  FROM contratos c WHERE c.dt_vigenciafinal >= NOW()";
            $stmt = $db->query($sql);
            $data = $stmt->fetchAll(PDO::FETCH_OBJ);

            die(json_encode(array(
                "status" => 1,
                "data" => [
                    "valor_mensal_total" => $valor_mensal_total['soma_valores_total'],
                    "valor_anual_total" => $valor_anual_total['soma_valores_total'],
                    "valor_total" => ($valor_mensal_total['soma_valores_total']*100 + $valor_anual_total['soma_valores_total']*100) / 100,
                    "total_clientes" => $total_clients['total_clientes'],
                    "qnt_usuarios_saas" => $data[0]->usuarios_saas
                ]
            )));
        } catch (PDOException $e) {
            $error = '{"error":{"text":' . $e->getMessage() . '}}';
            die(json_encode(array(
                "status" => 500,
                "error" => $error,
                "message" => "Erro no servidor"
            )));
        }
    }
}

