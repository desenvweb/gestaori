<?php
session_start();

include_once dirname(__FILE__) . "/../models/Anexos.php";
include_once dirname(__FILE__) . "/../models/Usuario.php";
include_once dirname(__FILE__) . "/../models/Notificacoes.php";
include_once dirname(__FILE__) . "/../models/HorasTrab.php";


class UsuarioController
{
    public static function login($fields = [])
    {

        $email = $fields['email'];
        $senha = strtoupper(md5($fields['senha']));

        try {
            $db = getDB();

            $sql = "SELECT u.id, u.tipo_usuario, u.nome, u.id_setor, u.super_admin, u.tipo_colaborador FROM usuario as u
                WHERE email = :EMAIL AND senha = :SENHA";

            $stmt = $db->prepare($sql);
            $stmt->bindParam("EMAIL", $email, PDO::PARAM_STR);
            $stmt->bindParam("SENHA", $senha, PDO::PARAM_STR);
            $stmt->execute();
            $login = $stmt->fetch(PDO::FETCH_ASSOC);

            if ($login) {
                $_SESSION['logado'] = true;
                $_SESSION['tipo_usuario'] = $login['tipo_usuario'];
                $_SESSION['tipo_colaborador'] = $login['tipo_colaborador'];
                $_SESSION['super_admin'] = $login['super_admin'];
                $_SESSION['id_user'] = $login['id'];
                $_SESSION['id_setor'] = $login['id_setor'];

                //GERA TOKEN DE ACESSO
                $auth_token = microtime(true);
                $auth_token = password_hash(str_replace(".", "", $auth_token), PASSWORD_BCRYPT);
                $login['AUTH_TOKEN'] = $auth_token;

                registra_log($login['id'], $auth_token, $login['tipo_usuario'], $login['id_setor']);

                unset($login['senha']);

                Usuario::set_status(["id_usuario" => $login['id'], "status" => 'presente']);

                die(json_encode(array(
                    "status" => 1,
                    "data" => $login,
                    "message" => "Usuário logado"
                )));
            } else {
                die(json_encode(array(
                    "status" => 2,
                    "message" => "E-mail ou senha inválido(a)"
                )));
            }

            $db = null;
        } catch (PDOException $e) {
            $error = '{"error":{"text":' . $e->getMessage() . '}}';
            die(json_encode(array(
                "status" => 500,
                "error" => $error,
                "message" => "Erro no servidor"
            )));
        }
    }

    public static function cadastrar_usuario($fields = [])
    {

        $fields = array_filter($fields);
        $fields['senha'] = strtoupper(md5($fields['senha']));
        if ($fields['super_admin'] == on) {
            $fields['super_admin'] = 1;
        }else{
            $fields['super_admin'] = 0;
        }


        try {
            $db = getDB();

            $cadData = get_usuarioEmail($fields['email']);

            if (sizeof($cadData) == 0) {

                $query =fieldsToInsertQuery($fields);
                $sql = "INSERT INTO usuario ({$query[0]}) VALUES ({$query[1]})";

                $stmt = $db->prepare($sql);

                $int = ['tipo_usuario', 'id_setor'];
                foreach ($fields as $key => &$value) {
                    $param = array_search($key, $int) ? PDO::PARAM_INT : PDO::PARAM_STR;
                    $stmt->bindParam(':' . $key, $value, $param);
                }

                $stmt->execute();

                $id_usuario = $db->lastInsertId();

                if (strlen($_FILES['anexo']['name'][0])) {
                   $anexos = Anexos::adicionar_anexo(['id_contratado' => $id_usuario], 'anexo_ps');

                    if ($anexos['status'] === 2) {

                        $sql = "DELETE FROM usuario
                                WHERE id={$id_usuario}";
                        $db->query($sql);

                        die(json_encode(array(
                            "status" => 2,
                            "message" => $anexos['message'],
                            "error" => $anexos['error']
                        )));
                    }
                }

                die(json_encode(array(
                    "status" => 1,
                    "message" => "Usuário cadastrado"
                )));
            } else {

                die(json_encode(array(
                    "status" => 2,
                    "message" => "E-mail já cadastrado"
                )));
            }

            $db = null;
        } catch (PDOException $e) {
            $error = '{"error":{"text":' . $e->getMessage() . '}}';
            die(json_encode(array(
                "status" => 500,
                "error" => $error,
                "message" => "Erro no servidor"
            )));
        }
    }

    public static function excluir_usuario($fields = [])
    {
        $id = $fields['id'];

        try {
            $db = getDB();

            $verifica = get_usuarioID($id);

            if (sizeof($verifica) > 0) {
                $sql = "DELETE FROM horastrab WHERE id_usuario = :ID";

                $stmt = $db->prepare($sql);
                $stmt->bindParam('ID', $id, PDO::PARAM_INT);
                $stmt->execute();

                $sql = "UPDATE anexo_ps SET excluido = 'S' WHERE id_contratado = :ID";

                $stmt = $db->prepare($sql);
                $stmt->bindParam('ID', $id, PDO::PARAM_INT);
                $stmt->execute();

                $sql = "DELETE FROM usuario WHERE id = :ID;";

                $stmt = $db->prepare($sql);
                $stmt->bindParam('ID', $id, PDO::PARAM_INT);
                $stmt->execute();

                die(json_encode(array(
                    "status" => 1,
                    "message" => "Usuário excluido"
                )));
            } else {
                die(json_encode(array(
                    "status" => 2,
                    "message" => "Usuário não existe"
                )));
            }
        } catch (PDOException $e) {
            $error = '{"error":{"text":' . $e->getMessage() . '}}';
            die(json_encode(array(
                "status" => 500,
                "error" => $error,
                "message" => "Erro no servidor"
            )));
        }
    }

    public static function get_usuarios($fields = [])
    {
        $pagina = $fields['pagina'] ? $fields['pagina']  : 0;
        $limite = $fields['limite'] ? $fields['limite'] : 10;

        $qnt = $pagina * $limite;

        //Filtros
        $where = '';

        if (!empty($fields['condicao'])) {
            $condicao = $fields['condicao'];
            $where .= " WHERE (nome LIKE '%{$condicao}%')";
        }

        try {
            $db = getDB();
            if ($_SESSION['super_admin'] == 0) {
                $sql = "select u.id ,u.nome ,u.email ,u.funcao ,u.telefone ,u.skype, u.id_setor, u.dt_inicio_trabalho from usuario u
                    {$where}
                    ORDER BY u.nome ASC 
                    LIMIT :limite OFFSET :qtd";

            }else{
                $sql = "select u.* from usuario u
                    {$where}
                    ORDER BY u.nome ASC 
                    LIMIT :limite OFFSET :qtd";
            }

            //die($sql);

            $stmt = $db->prepare($sql);
            $stmt->bindParam('limite', $limite, PDO::PARAM_INT);
            $stmt->bindParam('qtd', $qnt, PDO::PARAM_INT);
            $stmt->execute();
            $data = $stmt->fetchAll(PDO::FETCH_OBJ);

            foreach ($data as $d) {
                $d->situacao = Usuario::get_situacao($d->id);
                unset($d->senha);
            }

            if (sizeof($data) > 0) {
                die(json_encode(array(
                    "status" => 1,
                    "data" => $data,
                    "message" => "Usuários encontrados"
                )));
            } else {
                die(json_encode(array(
                    "status" => 2,
                    "message" => "Não encontrou Usuários"
                )));
            }

            $db = null;
        } catch (PDOException $e) {
            $error = '{"error":{"text":' . $e->getMessage() . '}}';
            die(json_encode(array(
                "status" => 500,
                "error" => $error,
                "message" => "Erro no servidor"
            )));
        }
    }

    public static function get_usuario($fields = [])
    {
        $id = $fields['id_usuario'];
        try {

            $data = get_usuarioID($id);
            unset($data[0]->senha);

            if (sizeof($data) > 0) {
                die(json_encode(array(
                    "status" => 1,
                    "data" => $data,
                    "message" => "Usuário encontrado"
                )));
            } else {
                die(json_encode(array(
                    "status" => 2,
                    "message" => "Não encontrou o usuário"
                )));
            }
        } catch (PDOException $e) {
            $error = '{"error":{"text":' . $e->getMessage() . '}}';
            die(json_encode(array(
                "status" => 500,
                "error" => $error,
                "message" => "Erro no servidor"
            )));
        }
    }

    public static function nova_senha_app($fields = [])
    {
        $id = $fields['id'];
        $senha = strtoupper(md5($fields['senha']));

        try {
            $db = getDB();

            $sql = "UPDATE usuario 
                    SET senha = :senha 
                    WHERE id = :id";

            $stmt = $db->prepare($sql);
            $stmt->bindParam("senha", $senha, PDO::PARAM_STR);
            $stmt->bindParam("id", $id, PDO::PARAM_INT);

            if ($stmt->execute()) {
                die(json_encode(array(
                    "status" => 1,
                    "message" => "Senha redefinida com sucesso"
                )));
            } else {
                die(json_encode(array(
                    "status" => 2,
                    "message" => "Não foi possível redefinir a senha"
                )));
            }

            $db = null;
        } catch (PDOException $e) {
            $error = '{"error":{"text":' . $e->getMessage() . '}}';
            die(json_encode(array(
                "status" => 500,
                "error" => $error,
                "message" => "Erro no servidor"
            )));
        }
    }

    public static function set_status($fields = [])
    {
        die(json_encode(Usuario::set_status($fields)));
    }

    public static function get_infos($fields = []){
        

        $trabalhando = HorasTrab::verifica_trabalho($fields['id_usuario']);
        $situacao = Usuario::get_situacao($fields['id_usuario']);
        $notificacao = Notificacoes::get_notifcacoes($fields['id_usuario']);
        $contratos_pj = Usuario::get_pj_vencer();
        $contratos_vencer = Contratos::get_contratos_vencer();

        die(json_encode(array(
            "status" => 1,
            "data" => [ "status" => $situacao,
                        "notificacao" => $notificacao,
                        "trabalhando" => $trabalhando,
                        "contratos_pj" => $contratos_pj,
                        "contratos_vencer" => $contratos_vencer
            ]
        )));
    }

    public static function get_trabalhos($fields = []){

        $trabalhando = HorasTrab::verifica_trabalhos($fields['id_usuario']);
        die(json_encode(array(
            "status" => 1,
            "data" => $trabalhando,
            "message" => "Usuário encontrado"
        )));
        

        
    }


    public static function set_visualizado($fields = []){
        $now = date('Y-m-d H:i:s', time() - 3 * 60 * 60);
        
        try {
            $db = getDB();

            $sql = "UPDATE notificacao 
                    SET dt_leiturausuario = '{$now}' 
                    WHERE id_usuario = :id and dt_leiturausuario IS NULL";

            $stmt = $db->prepare($sql);
            $stmt->bindParam("id", $fields['id_usuario'], PDO::PARAM_INT);
            $stmt->execute();

                die(json_encode(array(
                    "status" => 1,
                    "message" => "Leitura cadastrada com sucesso"
                )));

            $db = null;
        } catch (PDOException $e) {
            $error = '{"error":{"text":' . $e->getMessage() . '}}';
            die(json_encode(array(
                "status" => 500,
                "error" => $error,
                "message" => "Erro no servidor"
            )));
        }
    }


    public static function editar(object $request)
    {
        $fields = $request->post('');


        $fields_expected = [
            'id' => [
                'type' => 'integer',
                'required' => true
            ],
            'nome' => [
                'type' => 'string',
                'required' => true
            ],
            'cpf' => [
                'type' => 'string',
                'required' => false
            ],
            'cnpj' => [
                'type' => 'string',
                'required' => false
            ],
            'email' => [
                'type' => 'string',
                'required' => true
            ],
            'tipo_usuario' => [
                'type' => 'int',
                'required' => true
            ],
            'funcao' => [
                'type' => 'string',
                'required' => false
            ],
            'telefone' => [
                'type' => 'string',
                'required' => false
            ],
            'tipo_colaborador' => [
                'type' => 'string',
                'required' => false
            ],
            'logradouro' => [
                'type' => 'string',
                'required' => false
            ],
            'numero' => [
                'type' => 'string',
                'required' => false
            ],
            'bairro' => [
                'type' => 'string',
                'required' => false
            ],
            'cidade' => [
                'type' => 'string',
                'required' => false
            ],
            'estado' => [
                'type' => 'string',
                'required' => false
            ],
            'cep' => [
                'type' => 'string',
                'required' => false
            ],
            'skype' => [
                'type' => 'string',
                'required' => false
            ],
            'id_setor' => [
                'type' => 'int',
                'required' => false
            ],
            'vencimento_ferias' => [
                'type' => 'string',
                'required' => false
            ],
            'direito_ferias' => [
                'type' => 'string',
                'required' => false
            ],
            'dt_inicio_trabalho' => [
                'type' => 'string',
                'required' => true
            ],
            'vencimento_contrato' => [
                'type' => 'string',
                'required' => false
            ],
            'senha' => [
                'type' => 'string',
                'required' => false
            ],
            'super_admin' => [
                'type' => 'int',
                'required' => false
            ],
            'instrucao' => [
                'type' => 'float',
                'required' => false
            ],
            'salario' => [
                'type' => 'float',
                'required' => false
            ],
            'experiencia' => [
                'type' => 'string',
                'required' => false
            ],
            'avaliacao' => [
                'type' => 'string',
                'required' => false
            ]
        ];

        $return = valida_campos($fields_expected, $fields);

        if ((!fields['cnpj']) && (!fields['cpf'])){
            die(json_encode(array(
                "status" => 500,
                "error" => 'CPF ou CNPJ não informado!',
                "message" => "Erro no servidor"
            )));
        }
        

        if ($return['success'] === true) {
            $response = Usuario::editar_usuario($return['data']);
            http_response_code($response['http']);
            die(json_encode($response));
        } else {
            http_response_code($return['status']);
            die(json_encode($return));
        }
    }

}
