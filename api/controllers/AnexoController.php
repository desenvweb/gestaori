<?php

class AnexoController
{
    public static function adicionar_anexo($fields = [])
    {
        $arquivos = $_FILES['anexo'];

        for ($i = 0; $i < count($arquivos['name']); $i++) {
            $path_file = pathinfo($arquivos['name'][$i]);

            $id_contratado = $fields['id_contratado'];
            $nome_arquivo = $path_file['filename'];
            $nome_path = uniqid();
            $extensao = $path_file['extension'];
            $url = $nome_path . '.' . $extensao;
            $data_atual = date('Y-m-d H:i:s');

            $tabela = $fields['tipo_contratado'] === 'PS' ? 'anexo_ps' : $fields['tipo_contratado'];

            $path = dirname(__FILE__) . "/../../anexos/" . $url;

            try {
                $db = getDB();

                if (move_uploaded_file($arquivos['tmp_name'][$i], $path)) {
                    $sql = "INSERT INTO {$tabela} (url_arquivo, id_contratado, nome, data_cadastro, excluido)
                            VALUES (:URL_ARQUIVO, :ID_CONTRATADO, :NOME, :DATA_CADASTRO, 'N')";
                    $stmt = $db->prepare($sql);

                    $stmt->bindParam("URL_ARQUIVO", $path, PDO::PARAM_STR);
                    $stmt->bindParam("ID_CONTRATADO", $id_contratado, PDO::PARAM_INT);
                    $stmt->bindParam("NOME", $nome_arquivo, PDO::PARAM_STR);
                    $stmt->bindParam("DATA_CADASTRO", $data_atual, PDO::PARAM_STR);

                    $stmt->execute();
                    
                } else {
                    die(json_encode(array(
                        "status" => 2,
                        "message" => 'Não foi possível fazer upload do anexo ' . $i,
                        'error' => $_FILES['map']['error'][$i]
                    )));
                }

                die (json_encode(array(
                    "status" => 1,
                    "message" => 'Anexo salvo'
                )));

                $db = null;
            } catch (PDOException $e) {
                $error = '{"error":{"text":' . $e->getMessage() . '}}';
                die(json_encode(array(
                    "status" => 500,
                    "error" => $error,
                    "message" => "Erro no servidor"
                )));
            }
        }
    }

    public static function excluir_anexo($fields = [])
    {
        $id = $fields['id_anexo'];

        $tabela = $fields['tipo_contratado'] === 'PS' ? 'anexo_ps' : $fields['tipo_contratado'];

        try {
            $db = getDB();

            $sql = "UPDATE {$tabela} 
                    SET excluido = 'S'
                    WHERE id = :ID";

            //die($sql);
            $stmt = $db->prepare($sql);
            $stmt->bindParam("ID", $id, PDO::PARAM_INT);
            $stmt->execute();

            die(json_encode(array(
                "status" => 1,
                "message" => "Excluido com sucesso"
            )));
        } catch (PDOException $e) {
            $error = '{"error":{"text":' . $e->getMessage() . '}}';
            die(json_encode(array(
                "status" => 500,
                "error" => $error,
                "message" => "Erro no servidor"
            )));
        }
    }

    public static function get_anexos($fields = [])
    {
        $id = $fields['id_contratado'];

        $tabela = $fields['tipo_contratado'] === 'PS' ? 'anexo_ps' : $fields['tipo_contratado'];

        try {
            $db = getDB();

            $sql = "SELECT * FROM {$tabela} 
                    WHERE id_contratado = :ID and excluido = 'N'";

            $stmt = $db->prepare($sql);
            $stmt->bindParam("ID", $id, PDO::PARAM_INT);
            $stmt->execute();
            $data = $stmt->fetchAll(PDO::FETCH_OBJ);

            if (sizeof($data) > 0) {
                die(json_encode(array(
                    "status" => 1,
                    "data" => $data,
                    "message" => "Anexos encontrados com sucesso"
                )));
            } else {
                die(json_encode(array(
                    "status" => 1,
                    "message" => "Nenhum anexo encontrado"
                )));
            }
        } catch (PDOException $e) {
            $error = '{"error":{"text":' . $e->getMessage() . '}}';
            die(json_encode(array(
                "status" => 500,
                "error" => $error,
                "message" => "Erro no servidor"
            )));
        }
    }

}
