<?php


class CategoriaController
{
    public static function get_categorias()
    {
        try {
            $db = getDB();

            $sql = "SELECT * FROM categoria_ticket";

            $stmt = $db->prepare($sql);
            $stmt->execute();
            $data = $stmt->fetchAll(PDO::FETCH_OBJ);

            if (sizeof($data) > 0) {
                die(json_encode(array(
                    "status" => 1,
                    "data" => $data,
                    "message" => "Categorias retornados com sucesso"
                )));
            } else {
                die(json_encode(array(
                    "status" => 2,
                    "message" => "Nenhuma categoria encontrada"
                )));
            }

            $db = null;
        } catch (PDOException $e) {
            $error = '{"error":{"text":' . $e->getMessage() . '}}';
            die(json_encode(array(
                "status" => 500,
                "error" => $error,
                "message" => "Erro no servidor"
            )));
        }
    }
}
