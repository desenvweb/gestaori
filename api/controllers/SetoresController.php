<?php


class SetoresController {
     public static function get_setores(){
        try {
            $db = getDB();

            $sql = "SELECT * FROM setor";
                    
            $stmt = $db->prepare($sql);
            $stmt->execute();
            $data = $stmt->fetchAll(PDO::FETCH_OBJ);

            if (sizeof($data) > 0) {
                die(json_encode(array(
                    "status" => 1,
                    "message" => "Setores encontrados",
                    "data" => $data
                )));
            } else {
                die(json_encode(array(
                    "status" => 2,
                    "message" => "Nenhum setor encontrado"
                )));
            }

            $db = null;
        } catch (PDOException $e) {
            $error = '{"error":{"text":' . $e->getMessage() . '}}';
            die(json_encode(array(
                "status" => 500,
                "error" => $error,
                "message" => "Erro no servidor"
            )));
        }
     }
}