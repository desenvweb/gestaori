<?php

include_once dirname(__FILE__) . "/../models/Anexos.php";
include_once dirname(__FILE__) . "/../models/HorasTrab.php";
include_once dirname(__FILE__) . "/../models/Clientes.php";


class ClientesController
{
    public static function cadastrar_cliente($fields = [])
    {
        $meses = diff_meses($fields['vigencia_inicial_adendo'], $fields['vigencia_final_adendo']);

        $fields['horas_mes'] = ($fields['validade_horas'] == "anos")
            ? (intval($fields['horas_contrato'] / $meses))
            : $fields['horas_contrato'];

        $fields['horas_contrato'] = $fields['horas_contrato'] ? $fields['horas_contrato'] : 0;
        $fields['email'] = implode(', ', $fields['email']);
        $fields['excluido'] = 'N';
        $fields = array_filter($fields);

        $query = fieldsToInsertQuery($fields);

        try {
            $db = getDB();

            $sql = "INSERT INTO cliente ({$query[0]}) VALUES({$query[1]})";
            $stmt = $db->prepare($sql);

            $int = ['numero', 'horas_mes', 'usuarios_saas'];

            foreach ($fields as $key => &$value) {
                $param = array_search($key, $int) ? PDO::PARAM_INT : PDO::PARAM_STR;
                $stmt->bindParam(':' . $key, $value, $param);
            }

            $stmt->execute();

            $id_cliente = $db->lastInsertId();

            if (strlen($_FILES['anexo']['name'][0])) {

                $anexos = Anexos::adicionar_anexo([
                    "id_contratado" => $id_cliente,
                ], 'anexo_cliente');

                if ($anexos['status'] === 2) {

                    $sql = "DELETE FROM cliente
                            WHERE id={$id_cliente}";
                    $db->query($sql);

                    die(json_encode(array(
                        "status" => 2,
                        "message" => $anexos['message'],
                        "error" => $anexos['error']
                    )));
                }
            }

            die(json_encode(array(
                "status" => 1,
                "message" => 'Cliente cadastrado com sucesso.'
            )));

            $db = null;
        } catch (PDOException $e) {
            $error = '{"error":{"text":' . $e->getMessage() . '}}';
            die(json_encode(array(
                "status" => 500,
                "error" => $error,
                "message" => "Erro no servidor"
            )));
        }
    }

    public static function get_clientes($fields = [])
    {

        $pagina = isset($fields['pagina']) ? $fields['pagina']  : 0;
        $limite = isset($fields['limite']) ? $fields['limite'] : 10;

        $qnt = $pagina * $limite;

        //Filtros
        $where = '';

        if (!empty($fields['condicao'])) {
            $condicao = $fields['condicao'];
            $where .= "AND (cnpj LIKE '%{$condicao}%' or nome_fantasia LIKE '%{$condicao}%' 
                            or razao_social LIKE '%{$condicao}%')";
        }

        if (!empty($fields['dtvgfinal_inicio'])) {
            $condicao = $fields['dtvgfinal_inicio'];
            $where .= "AND vigencia_inicial_adendo > '{$condicao}'";
        }

        if (!empty($fields['dtvgfinal_fim'])) {
            $condicao = $fields['dtvgfinal_fim'];
            $where .= "AND vigencia_final_adendo < '{$condicao}'";
        }

        if (!empty($fields['contratada'])) {
            $condicao = $fields['contratada'];
            $where .= "AND empresa_contratada = '{$condicao}'";
        }

        if (!empty($fields['periodicidade_pagamento_filtro'])) {
            $condicao = $fields['periodicidade_pagamento_filtro'];
            $where .= "AND periodicidade_pagamento = '{$condicao}'";
        }

        try {
            $db = getDB();

            $sql = "SELECT * FROM cliente
                    WHERE excluido = 'N' {$where}
                    ORDER BY nome_fantasia ASC
                    LIMIT :LIMITE OFFSET :QTD ";

            //die($sql);
            $stmt = $db->prepare($sql);
            $stmt->bindParam('LIMITE', $limite, PDO::PARAM_INT);
            $stmt->bindParam('QTD', $qnt, PDO::PARAM_INT);
            $stmt->execute();
            $data = $stmt->fetchAll(PDO::FETCH_OBJ);

            // foreach ($data as $d) {
            //     $d->horas_restantes = $d->validade_horas == 'anos' ? HorasTrab::horas_restantes_contrato($d->id) :
            //         HorasTrab::horas_restantes_mes($d->id);
            //     $d->email = explode(',', $d->email);
            // }

            if (sizeof($data) > 0) {
                die(json_encode(array(
                    "status" => 1,
                    "data" => $data,
                    "message" => "Clientes encontrados"
                )));
            } else {
                die(json_encode(array(
                    "status" => 2,
                    "message" => "Não encontrou Clientes"
                )));
            }

            $db = null;
        } catch (PDOException $e) {
            $error = '{"error":{"text":' . $e->getMessage() . '}}';
            die(json_encode(array(
                "status" => 500,
                "error" => $error,
                "message" => "Erro no servidor"
            )));
        }
    }

    public static function get_cliente($fields)
    {
        $id = $fields['id'];

        try {
            $db = getDB();

            $sql = "SELECT * FROM cliente WHERE id = :ID and excluido = 'N'";

            $stmt = $db->prepare($sql);
            $stmt->bindParam('ID', $id, PDO::PARAM_INT);
            $stmt->execute();
            $data = $stmt->fetchAll(PDO::FETCH_OBJ);

            foreach ($data as $d) {
                $d->horas_restantes = HorasTrab::horas_restantes_contrato($d->id);
                $d->email = explode(',', $d->email);
            }

            if (sizeof($data) > 0) {
                die(json_encode(array(
                    "status" => 1,
                    "data" => $data,
                    "message" => "Cliente encontrado"
                )));
            } else {
                die(json_encode(array(
                    "status" => 2,
                    "message" => "Cliente não encontrado"
                )));
            }

            $db = null;
        } catch (PDOException $e) {
            $error = '{"error":{"text":' . $e->getMessage() . '}}';
            die(json_encode(array(
                "status" => 500,
                "error" => $error,
                "message" => "Erro no servidor"
            )));
        }
    }

    public static function excluir_cliente($fields = [])
    {

        $id = $fields['id'];

        try {
            $verifica = get_cliente($id);
            if (sizeof($verifica) > 0) {
                $db = getDB();

                $sql = "UPDATE cliente
                    SET excluido = 'S'
                    WHERE id = :ID";

                $stmt = $db->prepare($sql);
                $stmt->bindParam("ID", $id, PDO::PARAM_INT);
                $stmt->execute();


                $sql = "UPDATE anexo_cliente 
                SET excluido = 'S'
                WHERE id_contratado = :ID";

                $stmt = $db->prepare($sql);
                $stmt->bindParam("ID", $id, PDO::PARAM_INT);
                $stmt->execute();

                die(json_encode(array(
                    "status" => 1,
                    "message" => "Cliente excluido"
                )));

                $db = null;
            } else {
                die(json_encode(array(
                    "status" => 2,
                    "message" => "Cliente não encontrado"
                )));
            }
        } catch (PDOException $e) {
            $error = '{"error":{"text":' . $e->getMessage() . '}}';
            die(json_encode(array(
                "status" => 500,
                "error" => $error,
                "message" => "Erro no servidor"
            )));
        }
    }

    public static function editar_cliente($fields = [])
    {

        $id = $fields['id'];
        unset($fields['id']);

        $meses = diff_meses($fields['vigencia_inicial_adendo'], $fields['vigencia_final_adendo']);

        $fields['horas_mes'] = ($fields['validade_horas'] == "anos")
            ? (intval($fields['horas_contrato'] / $meses))
            : $fields['horas_contrato'];

        $fields['horas_contrato'] = $fields['horas_contrato'] ? $fields['horas_contrato'] : 0;
        $fields['email'] = implode(', ', $fields['email']);

        $fields = array_filter($fields);


        $query = fieldsToEditQuery($fields);


        try {
            $verifica = get_cliente($id);

            if (sizeof($verifica) > 0) {
                $db = getDB();

                $sql = "UPDATE cliente
                    SET {$query}
                    WHERE id = :ID;";

                $stmt = $db->prepare($sql);

                $int = ['numero', 'horas_mes', 'usuarios_saas'];

                foreach ($fields as $key => &$value) {
                    $param = array_search($key, $int) ? PDO::PARAM_INT : PDO::PARAM_STR;
                    $stmt->bindParam(':' . $key, $value, $param);
                }

                $stmt->bindParam(':ID', $id, PDO::PARAM_INT);

                $stmt->execute();

                if (strlen($_FILES['anexo']['name'][0])) {

                    $anexos = Anexos::adicionar_anexo([
                        "id_contratado" => $id,
                    ], 'anexo_cliente');

                    if ($anexos['status'] === 2) {
                        die(json_encode(array(
                            "status" => 2,
                            "message" => $anexos['message'],
                            "error" => $anexos['error']
                        )));
                    }
                }

                die(json_encode(array(
                    "status" => 1,
                    "message" => "Cliente editado"
                )));

                $db = null;
            } else {
                die(json_encode(array(
                    "status" => 2,
                    "message" => "Cliente não encontrado"
                )));
            }
        } catch (PDOException $e) {
            $error = '{"error":{"text":' . $e->getMessage() . '}}';
            die(json_encode(array(
                "status" => 500,
                "error" => $error,
                "message" => "Erro no servidor"
            )));
        }
    }

    public static function get_horas_restantes($fields)
    {

        $id_cliente = $fields['id_cliente'];

        try {
            $horas_restantes = HorasTrab::horas_restantes_contrato(["id_cliente" => $id_cliente]);
            die(json_encode($horas_restantes));
        } catch (PDOException $e) {
            $error = '{"error":{"text":' . $e->getMessage() . '}}';
            die(json_encode(array(
                "status" => 500,
                "error" => $error,
                "message" => "Erro no servidor"
            )));
        }
    }

    public static function verifica_contratos()
    {
        try {
            $db = getDB();

            $sql = "SELECT * FROM cliente
                    WHERE excluido = 'N'";

            $stmt = $db->prepare($sql);
            $stmt->execute();
            $data = $stmt->fetchAll(PDO::FETCH_OBJ);

            $todayLess15 = date('Y-m-d', strtotime("-15 days", strtotime(date('Y-m-d'))));
            $today = date('Y-m-d');

            foreach ($data as $d) {
                $dt_fimContrato = date($d->vigencia_final_adendo);

                //strtotime buga com datas acima de 2038, caso necessário verificar outra função para fazer isso
                if (strtotime($dt_fimContrato) < strtotime($today) && $d->status != 'CONTRATO VENCIDO') {
                    $txt = "Venceu o contrato do cliente"
                        . "<br>Cliente: " . $d->razao_social
                        . "<br>CNPJ: " . $d->cnpj
                        . "<br>Data de Vencimento: " . formatDMY($d->vigencia_final_adendo);
                    envia_email("Contrato Vencido", $txt);

                    $sql = "UPDATE cliente
                        SET status = 'CONTRATO VENCIDO'
                        WHERE id={$d->id}";
                    $db->query($sql);
                } else if (strtotime($dt_fimContrato) < strtotime($todayLess15)) {

                    $txt = "Faltam menos de 15 dias para vencimento do contrato do cliente"
                        . "<br>Cliente: " . $d->razao_social
                        . "<br>CNPJ: " . $d->cnpj
                        . "<br>Data de Vencimento: " . formatDMY($d->vigencia_final_adendo);

                    envia_email("Vencimento de Contrato", $txt);
                } else if (strtotime($dt_fimContrato) > strtotime($today) && $d->status != 'ATIVO') {
                    $sql = "UPDATE cliente
                    SET status = 'ATIVO'
                    WHERE id={$d->id}";

                    $db->query($sql);
                }
            }

            $db = null;
        } catch (PDOException $e) {
            $error = '{"error":{"text":' . $e->getMessage() . '}}';
            die(json_encode(array(
                "status" => 500,
                "error" => $error,
                "message" => "Erro no servidor"
            )));
        }
    }

    public static function dados_gerais()
    {
        try {

            $db = getDB();

            $valor_mensal_total = Clientes::soma_valor_mensal_contratos();
            $valor_anual_total = Clientes::soma_valor_anual_contratos();
            $total_clients = Clientes::total_clientes();

            $sql = "SELECT SUM(usuarios_saas) as usuarios_saas  FROM cliente WHERE excluido = 'N'";
            $stmt = $db->query($sql);
            $data = $stmt->fetchAll(PDO::FETCH_OBJ);

            die(json_encode(array(
                "status" => 1,
                "data" => [
                    "valor_mensal_total" => $valor_mensal_total['soma_valores_total'],
                    "valor_anual_total" => $valor_anual_total['soma_valores_total'],
                    "valor_total" =>
                    intval($valor_mensal_total['soma_valores_total']) + intval($valor_anual_total['soma_valores_total']),
                    "total_clientes" => $total_clients['total_clientes'],
                    "qnt_usuarios_saas" => $data[0]->usuarios_saas
                ]
            )));
        } catch (PDOException $e) {
            $error = '{"error":{"text":' . $e->getMessage() . '}}';
            die(json_encode(array(
                "status" => 500,
                "error" => $error,
                "message" => "Erro no servidor"
            )));
        }
    }

    public static function get_todos_clientes($fields = [])
    {
        $where = '';
     
        if(!empty($fields['condicao'])){
            $condicao = $fields['condicao'];
            $where = "WHERE nome_fantasia LIKE '%{$condicao}%'";
        }

        $limite = $fields['limite'] ?? 10;

        try {

            $db = getDB();

            $sql1 = "SELECT * FROM empresas {$where} LIMIT :limite";

            $stmt1 = $db->prepare($sql1);
            $stmt1->bindParam("limite", $limite, PDO::PARAM_INT);
            $stmt1->execute();
            $data = $stmt1->fetchAll(PDO::FETCH_OBJ);

            die(json_encode([
                "status" => 1,
                "data" => $data,
                "message" => 'Empresas retornadas com sucesso' 
            ]));

        } catch (PDOException $e) {
            $error = '{"error":{"text":' . $e->getMessage() . '}}';
            die(json_encode(array(
                "status" => 500,
                "error" => $error,
                "message" => "Erro no servidor"
            )));
        }
    }
}
