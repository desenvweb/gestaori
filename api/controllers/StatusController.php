<?php


class StatusController
{
    public static function get_status_projeto($fields = [])
    {
        $pagina = $fields['page'] ? $fields['page'] : 0;
        $limite = $fields['limite'] ? $fields['limite'] : 10;

        $qnt = $pagina * $limite;

        try {
            $db = getDB();

            $sql = "SELECT * FROM status
                    LIMIT :limite OFFSET :qnt";

            // die($sql);

            $stmt = $db->prepare($sql);
            $stmt->bindParam('qnt', $qnt, PDO::PARAM_INT);
            $stmt->bindParam('limite', $limite, PDO::PARAM_INT);
            $stmt->execute();
            $data = $stmt->fetchAll(PDO::FETCH_OBJ);

            if (sizeof($data) > 0) {
                die(json_encode(array(
                    "status" => 1,
                    "data" => $data,
                    "message" => "Status retornadas com sucesso"
                )));
            } else {
                die(json_encode(array(
                    "status" => 2,
                    "message" => "Não existem Status cadastrados"
                )));
            }


            $db = null;
        } catch (PDOException $e) {
            $error = '{"error":{"text":' . $e->getMessage() . '}}';
            die(json_encode(array(
                "status" => 500,
                "error" => $error,
                "message" => "Erro no servidor"
            )));
        }
    }

    public static function get_status_ticket($fields = [])
    {
        $pagina = $fields['page'] ? $fields['page'] : 0;
        $limite = $fields['limite'] ? $fields['limite'] : 50;

        $qnt = $pagina * $limite;

        try {
            $db = getDB();

            $sql = "SELECT * FROM status_ticket
                    LIMIT :limite OFFSET :qnt";

            // die($sql);

            $stmt = $db->prepare($sql);
            $stmt->bindParam('qnt', $qnt, PDO::PARAM_INT);
            $stmt->bindParam('limite', $limite, PDO::PARAM_INT);
            $stmt->execute();
            $data = $stmt->fetchAll(PDO::FETCH_OBJ);

            if (sizeof($data) > 0) {
                die(json_encode(array(
                    "status" => 1,
                    "data" => $data,
                    "message" => "Status retornadas com sucesso"
                )));
            } else {
                die(json_encode(array(
                    "status" => 2,
                    "message" => "Não existem Status cadastrados"
                )));
            }


            $db = null;
        } catch (PDOException $e) {
            $error = '{"error":{"text":' . $e->getMessage() . '}}';
            die(json_encode(array(
                "status" => 500,
                "error" => $error,
                "message" => "Erro no servidor"
            )));
        }
    }

    public static function cadastrar_status($fields = [])
    {
        try {
            $db = getDB();

            $sql = "INSERT INTO status (nome)
                    VALUES(:nome);";

            $stmt = $db->prepare($sql);
            $stmt->bindParam('nome', $fields['nome']);
            $stmt->execute();
            $data = $stmt->fetchAll(PDO::FETCH_OBJ);

            die(json_encode(array(
                "status" => 1,
                "data" => $data,
                "message" => "Status cadastrado com sucesso"
            )));


            $db = null;
        } catch (PDOException $e) {
            $error = '{"error":{"text":' . $e->getMessage() . '}}';
            die(json_encode(array(
                "status" => 500,
                "error" => $error,
                "message" => "Erro no servidor"
            )));
        }
    }

    public static function editar_status($fields = [])
    {
        try {
            $db = getDB();

            $sql = "UPDATE status SET nome = :nome WHERE id = :id";

            $stmt = $db->prepare($sql);
            $stmt->bindParam('nome', $fields['nome']);
            $stmt->bindParam('id', $fields['id']);
            $stmt->execute();
            $data = $stmt->fetchAll(PDO::FETCH_OBJ);

            die(json_encode(array(
                "status" => 1,
                "data" => $data,
                "message" => "Status editado com sucesso"
            )));


            $db = null;
        } catch (PDOException $e) {
            $error = '{"error":{"text":' . $e->getMessage() . '}}';
            die(json_encode(array(
                "status" => 500,
                "error" => $error,
                "message" => "Erro no servidor"
            )));
        }
    }

    public static function excluir_status($fields = [])
    {
        try {
            $db = getDB();

            $sql = "DELETE from status WHERE id = :id";

            $stmt = $db->prepare($sql);
            $stmt->bindParam('id', $fields['id']);
            $stmt->execute();
            $data = $stmt->fetchAll(PDO::FETCH_OBJ);

            die(json_encode(array(
                "status" => 1,
                "data" => $data,
                "message" => "Status excluido com sucesso"
            )));


            $db = null;
        } catch (PDOException $e) {
            $error = '{"error":{"text":' . $e->getMessage() . '}}';
            die(json_encode(array(
                "status" => 500,
                "error" => $error,
                "message" => "Erro no servidor"
            )));
        }
    }
}
