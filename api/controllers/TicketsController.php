<?php

include_once dirname(__FILE__) . "/../models/Usuario.php";
include_once dirname(__FILE__) . "/../models/UsuarioCliente.php";
include_once dirname(__FILE__) . "/../models/Ticket.php";
include_once dirname(__FILE__) . "/../models/Notificacoes.php";
include_once dirname(__FILE__) . "/../models/Anexos.php";

class TicketsController
{

    public static function cadastrar_ticket(object $request, object $requester)
    {

        $fields = $request->post('');
        $fields['id_responsavel'] = $requester->ID_USUARIO;

        $fields_expected = [
            'id_responsavel' => [
                'type' => 'int',
                'required' => true
            ],
            'id_categoria' => [
                'type' => 'int',
                'required' => true
            ],
            'id_cliente' => [
                'type' => 'int',
                'required' => true
            ],
            'id_usucliente' => [
                'type' => 'int',
                'required' => true
            ],
            'titulo' => [
                'type' => 'string',
                'required' => true
            ],
            'dt_prevista' => [
                'type' => 'string',
                'required' => true
            ],
            'gut' => [
                'type' => 'int',
                'required' => true
            ],
            'gravidade' => [
                'type' => 'int',
                'required' => true
            ],
            'urgencia' => [
                'type' => 'int',
                'required' => true
            ],
            'tendencia' => [
                'type' => 'int',
                'required' => true
            ],
            'informacoes_gerais' => [
                'type' => 'string',
                'required' => true
            ],
            'dt_fechamento' => [
                'type' => 'string',
                'required' => false
            ],


        ];

        $return = valida_campos($fields_expected, $fields);

        if ($return['success'] === true) {
            $response = Ticket::cadastrar_ticket($return['data']);
            die(json_encode($response));
        } else {
            http_response_code($return['status']);
            die(json_encode($return));
        }
    }
    public static function marcar_lido($fields = [])
    {
        Notificacoes::update_notificacao_ok($fields['id']);
    }

    public static function editar_ticket($fields = [])
    {

        $id = $fields['id_ticket'];
        unset($fields['id_ticket']);

        if ($fields['id_notificacao']){

            Notificacoes::update_notificacao_ok($fields['id_notificacao']);
            unset($fields['id_notificacao']);
        }
        if ($fields['id_status'] == 3) {
            $fields['dt_fechamento'] = (new DateTime())->modify('-3 hours')->format('Y-m-d H:i:s');
        }else{
            $fields['dt_fechamento'] = null;
        }
        $fields = array_filter($fields);

        try {
            $db = getDB();

            $ticket = Ticket::get_tickets($id);

            if (sizeof($ticket) > 0) {

                if ($fields['id_status'] == 3) {
                    $fields['dt_fechamento'] = (new DateTime())->modify('-3 hours')->format('Y-m-d H:i:s');

                    //solicitar nota do ticket
                        $token = md5(uniqid(""));
                        //Cadastrar anexo do comentário
                        Anexos::adicionar_anexo(['id_comentario' => $id], 'anexo_comentariotk');
                                        
                        //Enviar e-mail de comentário cadastrado
                        $usu_cliente = UsuarioCliente::get_usuario(['id_usucliente' => $ticket[0]->id_usucliente]);
        
                        $assunto = "Ticket {$ticket[0]->id}: Qual a sua avaliação sobre o suporte recebido ?";
        
                        $body = '<b>E-mail automático, por favor não responder.</b>';
                        $body .= '<br><br>';
                        $body .= "Olá, {$usu_cliente->nome},";
                        $body .= '<br>';
                        $body .= 'Queremos saber sua opinião sobre nosso atendimento ao cliente. Dedique alguns instantes para responder a seguinte pergunta:';
                        $body .= '<br><br>';
                        $body .= '<b>Qual a sua avaliação sobre o suporte recebido?</b>';
                        $body .= '<br>';
                        $host = 'https://gestaori.redeindustrial.com.br';//;$_SERVER['HTTP_HOST'];
                        $body .= "<a href='{$host}/paginas/tickets/avaliacao.php?token={$token}&avaliacao=810'>Ótimo, estou muito contente</a><br>";
                        $body .= "<a href='{$host}/paginas/tickets/avaliacao.php?token={$token}&avaliacao=57'>Bom, estou contente</a><br>";
                        $body .= "<a href='{$host}/paginas/tickets/avaliacao.php?token={$token}&avaliacao=14'>Ruim, estou descontente</a><br>";
                        $body .= '<br>';
                        $body .= '<hr>';
                        $body .= 'Rede Industrial.<br>Todos direitos reservados.';
        
                        enviar_email($usu_cliente->email1, $assunto, $body, $usu_cliente->nome);
                        ticket::cadastrar_avaliacao(['id_ticket' => $ticket[0]->id, 'token' => $token]);
        
                }else{
                    $fields['dt_fechamento'] = null;
                }

                $query = fieldsToEditQuery($fields);
                $sql = "UPDATE ticket SET {$query} WHERE id = :id";
                $stmt = $db->prepare($sql);

                $int = ['id_setor', 'id_categoria', 'id_responsavel'];
                foreach ($fields as $key => &$value) {
                    $param = array_search($key, $int) ? PDO::PARAM_INT : PDO::PARAM_STR;
                    $stmt->bindParam(':' . $key, $value, $param);
                }

                $stmt->bindParam('id', $id, PDO::PARAM_INT);
                $stmt->execute();

                if ($fields['id_responsavel'] != $ticket[0]->id_responsavel) {
                    Notificacoes::add_notifcacao([
                        "descricao" => "Ticket {$id} transferido para sua responsabilidade",
                        "id_usuario" => $fields['id_responsavel'],
                        "id_usuario_ant" => $ticket[0]->id_responsavel
                    ]);
                }

                die(json_encode(array(
                    "status" => 1,
                    "message" => "Ticket editado com sucesso"
                )));
            } else {
                die(json_encode(array(
                    "status" => 2,
                    "message" => "Nenhum ticket com esse ID"
                )));
            }

            $db = null;
        } catch (PDOException $e) {
            $error = '{"error":{"text":' . $e->getMessage() . '}}';
            die(json_encode(array(
                "status" => 500,
                "error" => $error,
                "message" => "Erro no servidor"
            )));
        }
    }

    public static function excluir_ticket($request)
    {

        $fields = $request->get('');

        $fields_expected = [
            'id_ticket' => [
                'type' => 'int',
                'required' => true
            ]
        ];

        $return = valida_campos($fields_expected, $fields);

        if ($return['success'] === true) {
            $response = Ticket::excluir_ticket($return['data']);
            die(json_encode($response));
        } else {
            http_response_code($return['status']);
            die(json_encode($return));
        }
    }

    public static function relatorio(object $request)
    {

        $fields = $request->get('');

        $fields_expected = [
            'id_ticket' => [
                'type' => 'int',
                'required' => true
            ]
        ];

        $return = valida_campos($fields_expected, $fields);

        if ($return['success'] === true) {
            $response = Relatorios::relatorio_ticket($return['data']);
            die(json_encode($response));
        } else {
            http_response_code($return['status']);
            die(json_encode($return));
        }
    }

    public static function get_tickets($fields)
    {

        $pagina = isset($fields['pagina']) ? $fields['pagina']  : 0;
        $limite = isset($fields['limite']) ? $fields['limite'] : 10;
        $groupby = isset($fields['groupby']) ? $fields['groupby'] : '';

        $qnt = $pagina * $limite;

        $where = 'WHERE 1 ';

        if (!empty($fields['condicao'])) {
            $condicao = $fields['condicao'];
            $where .= " AND CONVERT(t.id, CHAR) LIKE '{$condicao}%'";
        }

        //if (!empty($fields['id_responsavel']  ) {
        if (!empty($fields['id_responsavel'] and $_SESSION['tipo_usuario'] != 1)) {
            $id_responsavel = $fields['id_responsavel'];
            $where .= " AND t.id_responsavel = {$id_responsavel}";
        }

        if(!empty($fields['status_nao'])) {
            $status_nao = $fields['status_nao'];
            $where .= " AND t.id_status != {$status_nao}";
        }

        if(!empty($fields['status_tkt'])) {
            $status_tkt = $fields['status_tkt'];
            switch ($status_tkt) {
                case 'finalizados_tkt':
                    $where .= " AND t.id_status = 3";
                    break;
                case 'pendentes_tkt':
                    $where .= " AND t.id_status != 3";
                    break;
            }
        }
        if(!empty($fields['empresa'])) {
            $empresa = '%'.$fields['empresa'].'%';
            //$where .= " AND (e.nome_fantasia LIKE '{$empresa}' or e.cnpj like '{$empresa}' or e.razao_social LIKE '{$empresa}')";
            $where .= " AND c.id in (select uc.id from usu_cliente uc where uc.id_empresa in (select e2.id FROM empresas e2 where (e2.razao_social LIKE '{$empresa}' or e2.cnpj like '{$empresa}' or e2.nome_fantasia LIKE '{$empresa}')))";
        }

        if(!empty($fields['usuario_cli'])) {
            $usuario_cli = '%'.$fields['usuario_cli'].'%';
            $where .= " AND (c.nome LIKE '{$usuario_cli}' or c.email1 LIKE '{$usuario_cli}' or c.email2 LIKE '{$usuario_cli}')";
        }

        if(!empty($fields['responsavel'])) {
            $responsavel = '%'.$fields['responsavel'].'%';
            $where .= " AND u.nome LIKE '{$responsavel}'";
        }

        if ($groupby == 'gutnomefantasia'){
            $orderby = 'ORDER BY responsavel, nome_fantasia, gut DESC, id DESC';
        } else if ($groupby == 'nomefantasia'){
            $orderby = 'ORDER BY nome_fantasia, id DESC';
        } else if ($groupby == 'gut'){
            $orderby = 'ORDER BY responsavel ASC, gut DESC, id DESC';
        }else{
            $orderby = 'ORDER BY id DESC';
        }

        // die($where);

        try {
            $db = getDB();

            $sql = "SELECT t.id, t.titulo, t.dt_abertura, t.dt_fechamento, t.id_categoria, t.id_cliente, 
            t.id_usucliente, t.id_status, st.descricao as status, c.nome as usuario, t.id_responsavel,
            e.nome_fantasia, e.cnpj, ct.descricao as categoria, t.id_setor, t.dt_prevista, t.informacoes_gerais,
            t.gut, t.gravidade, t.urgencia, t.tendencia, u.nome as responsavel
            FROM ticket as t          
            LEFT JOIN status_ticket as st ON st.id = t.id_status
            LEFT JOIN usu_cliente as c ON c.id = t.id_usucliente
            LEFT JOIN empresas as e ON e.id = c.id_empresa
            LEFT JOIN categoria_ticket as ct ON ct.id = t.id_categoria
            LEFT JOIN usuario as u ON u.id = t.id_responsavel
            {$where}
            AND (t.id_cliente is null or t.id_cliente = 0)

            UNION 

            SELECT t.id, t.titulo, t.dt_abertura, t.dt_fechamento, t.id_categoria, t.id_cliente, 
            t.id_usucliente, t.id_status, st.descricao as status, c.nome as usuario, t.id_responsavel,
            e.nome_fantasia, e.cnpj, ct.descricao as categoria, t.id_setor, t.dt_prevista, t.informacoes_gerais,
            t.gut, t.gravidade, t.urgencia, t.tendencia, u.nome as responsavel
            FROM ticket as t          
            LEFT JOIN status_ticket as st ON st.id = t.id_status
            LEFT JOIN usu_cliente as c ON c.id = t.id_usucliente
            LEFT JOIN empresas as e ON e.id = t.id_cliente
            LEFT JOIN categoria_ticket as ct ON ct.id = t.id_categoria
            LEFT JOIN usuario as u ON u.id = t.id_responsavel
            {$where}
            AND t.id_cliente > 0

            {$orderby}

            LIMIT :limite OFFSET :qnt";

            //die($sql);

            $stmt = $db->prepare($sql);
            $stmt->bindParam('qnt', $qnt, PDO::PARAM_INT);
            $stmt->bindParam('limite', $limite, PDO::PARAM_INT);
            $stmt->execute();
            $data = $stmt->fetchAll(PDO::FETCH_OBJ);

            if (sizeof($data) > 0) {
                die(json_encode(array(
                    "status" => 1,
                    "data" => $data,
                    "message" => "Tickets retornados com sucesso"
                )));
            } else {
                die(json_encode(array(
                    "status" => 2,
                    "message" => "Nenhum ticket encontrado"
                )));
            }

            $db = null;
        } catch (PDOException $e) {
            $error = '{"error":{"text":' . $e->getMessage() . '}}';
            die(json_encode(array(
                "status" => 500,
                "error" => $error,
                "message" => "Erro no servidor"
            )));
        }
    }

    public static function add_comentario($fields = [])
    {

        $fields = array_filter($fields);

        $now = (new DateTime())->modify('-6 hours');

        $fields['dt_cadastro'] = $now->format('Y-m-d H:i:s');//date("Y-m-d H:i:s", time() -3 * 60 * 60);
        $fields['privado'] = 0;
        $texto = nl2br($fields['texto']);

        unset($fields['cliente']);
        unset($fields['texto']);

        try {
            $ticket = Ticket::get_tickets($fields['id_ticket']);

            if (sizeof($ticket) > 0) {
                $db = getDB();

                $query = fieldsToInsertQuery($fields);
                $sql = "INSERT INTO ticketitem ({$query[0]}) VALUES ({$query[1]})";
                // die($sql);
                $stmt = $db->prepare($sql);

                $int = ['id_usuario', 'id_ticket'];
                foreach ($fields as $key => &$value) {
                    $param = array_search($key, $int) ? PDO::PARAM_INT : PDO::PARAM_STR;
                    $stmt->bindParam(':' . $key, $value, $param);
                }

                $stmt->execute();

                $id = $db->lastInsertId();

                $sql = "INSERT INTO dialogo_ticket (codticketitem, texto) VALUES ({$id}, :texto)";

                $stmt = $db->prepare($sql);
                $stmt->bindParam('texto', $texto);
                $stmt->execute();

                //Cadastrar anexo do comentário
                Anexos::adicionar_anexo(['id_comentario' => $id], 'anexo_comentariotk');
                
                //Enviar e-mail de comentário cadastrado
                $usu_cliente = UsuarioCliente::get_usuario(['id_usucliente' => $ticket[0]->id_usucliente]);
                $usuario = Usuario::get_user(['id_usuario' => $fields['id_usuario']]);

                $assunto = utf8_decode("Novo comentário cadastrado no ticket {$fields['id_ticket']}");
               
                $body = '<b>E-mail automático, por favor não responder.</b>';
                $body .= '<br><br>';
                $body .= "Novo comentário cadastrado no ticket <b>{$fields['id_ticket']}</b> pelo agente
                         <b>{$usuario->nome}</b>";
                $body .= '<br><br>';
                $body .= $texto;

                enviar_email($usu_cliente->email1, $assunto, $body, $usu_cliente->nome);

                die(json_encode(array(
                    "status" => 1,
                    "message" => "Comentário cadastrado com sucesso"
                )));
            } else {
                die(json_encode(array(
                    "status" => 2,
                    "message" => "Nenhum ticket encontrado"
                )));
            }

            $db = null;
        } catch (PDOException $e) {
            $error = '{"error":{"text":' . $e->getMessage() . '}}';
            die(json_encode(array(
                "status" => 500,
                "error" => $error,
                "message" => "Erro no servidor"
            )));
        }
    }

    public static function get_comentarios($request)
    {
        $fields = $request->get('');

        $fields_expected = [
            'id_ticket' => [
                'type' => 'int',
                'required' => true
            ]
        ];

        $return = valida_campos($fields_expected, $fields);

        if ($return['success'] === true) {
            $response = Ticket::get_comentarios($return['data']);

            if(sizeof($response) > 0 ){
                die(json_encode([
                    "status" => 1,
                    "message" => 'Comentários encontrados com sucesso.',
                    "data" => $response 
                ]));
            }else{
                die(json_encode([
                    "status" => 2,
                    "message" => 'Nenhum comentário encontrado com sucesso.'
                ]));
            }
            die(json_encode($response));
        } else {
            http_response_code($return['status']);
            die(json_encode($return));
        }
    }
    
    public static function get_avaliacao_ticket($request)
    {
        $fields = $request->get('');

        $fields_expected = [
            'token' => [
                'type' => 'string',
                'required' => true
            
            ]
        ];

        $return = valida_campos($fields_expected, $fields);

        if ($return['success'] === true) {
            $response = Ticket::get_avaliacao_ticket($return['data']);

            if(sizeof($response) > 0 ){
                die(json_encode([
                    "status" => 1,
                    "message" => 'Comentários encontrados com sucesso.',
                    "data" => $response 
                ]));
            }else{
                die(json_encode([
                    "status" => 2,
                    "message" => 'Nenhum comentário encontrado com sucesso.'
                ]));
            }
            die(json_encode($response));
        } else {
            http_response_code($return['status']);
            die(json_encode($return));
        }
    }

    public static function relatorio_tickets_pdf(object $request)
    {

        $fields = $request->get('');

        $fields_expected = [
            'status_tkt' => [
                'type' => 'string',
                'required' => true
            ]
        ];

        $return = valida_campos($fields_expected, $fields);

        if ($return['success'] === true) {
            $response = Ticket::relatorio_tickets_pdf($return['data']);
            die(json_encode($response));
        } else {
            http_response_code($return['status']);
            die(json_encode($return));
        }
    }

    public static function get_gut_gravidade()
    {
        try {
            $db = getDB();

            $sql = "SELECT * FROM gut_gravidade";

            $stmt = $db->prepare($sql);
            $stmt->execute();
            $data = $stmt->fetchAll(PDO::FETCH_OBJ);

            if (sizeof($data) > 0) {
                die(json_encode(array(
                    "status" => 1,
                    "data" => $data,
                    "message" => "gut_gravidade retornados com sucesso"
                )));
            } else {
                die(json_encode(array(
                    "status" => 2,
                    "message" => "Nenhuma gut_gravidade encontrada"
                )));
            }

            $db = null;
        } catch (PDOException $e) {
            $error = '{"error":{"text":' . $e->getMessage() . '}}';
            die(json_encode(array(
                "status" => 500,
                "error" => $error,
                "message" => "Erro no servidor"
            )));
        }
    }

    public static function get_gut_urgencia()
    {
        try {
            $db = getDB();

            $sql = "SELECT * FROM gut_urgencia";

            $stmt = $db->prepare($sql);
            $stmt->execute();
            $data = $stmt->fetchAll(PDO::FETCH_OBJ);

            if (sizeof($data) > 0) {
                die(json_encode(array(
                    "status" => 1,
                    "data" => $data,
                    "message" => "gut_urgencia retornados com sucesso"
                )));
            } else {
                die(json_encode(array(
                    "status" => 2,
                    "message" => "Nenhuma gut_urgencia encontrada"
                )));
            }

            $db = null;
        } catch (PDOException $e) {
            $error = '{"error":{"text":' . $e->getMessage() . '}}';
            die(json_encode(array(
                "status" => 500,
                "error" => $error,
                "message" => "Erro no servidor"
            )));
        }
    }
    
    public static function get_gut_tendencia()
    {
        try {
            $db = getDB();

            $sql = "SELECT * FROM gut_tendencia";

            $stmt = $db->prepare($sql);
            $stmt->execute();
            $data = $stmt->fetchAll(PDO::FETCH_OBJ);

            if (sizeof($data) > 0) {
                die(json_encode(array(
                    "status" => 1,
                    "data" => $data,
                    "message" => "gut_tendencia retornados com sucesso"
                )));
            } else {
                die(json_encode(array(
                    "status" => 2,
                    "message" => "Nenhuma gut_tendencia encontrada"
                )));
            }

            $db = null;
        } catch (PDOException $e) {
            $error = '{"error":{"text":' . $e->getMessage() . '}}';
            die(json_encode(array(
                "status" => 500,
                "error" => $error,
                "message" => "Erro no servidor"
            )));
        }
    }

    public static function add_avaliacao_ticket($fields = [])
    {

        $token = $fields['token'];
        $id_ticket = $fields['id_ticket'];
        

        switch ($fields['avaliacao']) {
            case 810:
                $avaliacao = 'Ótimo, estou muito contente';
                break;
            case 57:
                $avaliacao = 'Bom, estou contente';
                break;
            case 14:
                $avaliacao = 'Ruim, estou descontente';
                break;
        }
        
        $fields['avaliacao'] = $avaliacao;
        $fields['dt_avaliacao'] = (new DateTime())->modify('-3 hours')->format('Y-m-d H:i:s');

        $fields = array_filter($fields);

        try {
            $db = getDB();

            $ticket = Ticket::get_tickets($id_ticket);

            if (sizeof($ticket) > 0) {

                $query = fieldsToEditQuery($fields);
                $sql = "UPDATE avaliacao_ticket SET {$query} WHERE token = :token";
                $stmt = $db->prepare($sql);

                $int = ['id_ticket'];
                foreach ($fields as $key => &$value) {
                    $param = array_search($key, $int) ? PDO::PARAM_INT : PDO::PARAM_STR;
                    $stmt->bindParam(':' . $key, $value, $param);
                }
                $stmt->execute();

                Notificacoes::add_notifcacao([
                    "descricao" => "Cliente registrou a avaliação sobre o suporte recebido no atendimento do Ticket {$id_ticket} como: {$avaliacao}",
                    "id_usuario" => $ticket[0]->id_responsavel
                ]);

                Notificacoes::add_notifcacao([
                    "descricao" => "Cliente registrou a avaliação sobre o suporte recebido no atendimento do Ticket {$id_ticket} como {$avaliacao}",
                    "id_usuario" => 71
                ]);

                die(json_encode(array(
                    "status" => 1,
                    "message" => "Avaliação cadastrada com sucesso"
                )));
            } else {
                die(json_encode(array(
                    "status" => 2,
                    "message" => "Nenhum ticket com esse ID"
                )));
            }

            $db = null;
        } catch (PDOException $e) {
            $error = '{"error":{"text":' . $e->getMessage() . '}}';
            die(json_encode(array(
                "status" => 500,
                "error" => $error,
                "message" => "Erro no servidor"
            )));
        }
    }
}
