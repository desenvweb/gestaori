<?php


class HorasCompradasController
{

    public static function cadastrar(object $request, object $requester)
    { 
            $fields = $request->post('');

            $fields['id_cadastrante'] = $requester->ID_USUARIO;

            $fields_expected = [
                'id_empresa' => [
                    'type' => 'int',
                    'required' => true
                ],
                'total_horas' => [
                    'type' => 'int',
                    'required' => true,
                ],
                'valor_total' => [
                    'type' => 'float',
                    'required' => true
                ],
                'id_cadastrante' => [
                    'type' => 'int',
                    'required' => true
                ],
                'obs' => [
                    'type' => 'string',
                    'required' => false
                ]
            ];

            $return = valida_campos($fields_expected, $fields);

            if ($return['success'] === true) {
                $response = HorasCompradas::cadastrar($return['data']);
                
                http_response_code($response['http']);
                die(json_encode($response));
            } else {
                http_response_code($return['status']);
                die(json_encode($return));
            }
        
    }

    public static function excluir($fields = [])
    {
        $id = $fields['id_lancamento'];
        $id_empresa = $fields['id_empresa'];
        $horas = $fields['total_horas'];

         try {

            $minutos_lancar = ceil($horas * -60);

            SaldoHoras::lancar(['id_empresa' => $id_empresa, 'minutos' => $minutos_lancar]);

            $db = getDB();

            $sql = "DELETE FROM horas_compradas
                WHERE id = :ID;";

            $stmt = $db->prepare($sql);
            $stmt->bindParam("ID", $id, PDO::PARAM_INT);
            $stmt->execute();

            die(json_encode(array(
                "status" => 1,
                "message" => "Lançamento excluido"
            )));

            $db = null;
        } catch (PDOException $e) {
            $error = '{"error":{"text":' . $e->getMessage() . '}}';
            die(json_encode(array(
                "status" => 500,
                "error" => $error,
                "message" => "Erro no servidor"
            )));
        }

        try {
            $db = getDB();

            $sql = "DELETE FROM horas_compradas
                WHERE id = :ID;";

            $stmt = $db->prepare($sql);
            $stmt->bindParam("ID", $id, PDO::PARAM_INT);
            $stmt->execute();

            die(json_encode(array(
                "status" => 1,
                "message" => "Lançamento excluido"
            )));

            $db = null;
        } catch (PDOException $e) {
            $error = '{"error":{"text":' . $e->getMessage() . '}}';
            die(json_encode(array(
                "status" => 500,
                "error" => $error,
                "message" => "Erro no servidor"
            )));
        }
    }
    
}
