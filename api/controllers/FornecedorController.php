<?php

include_once dirname(__FILE__) . "/../models/Fornecedores.php";
class FornecedorController
{

    public static function get_fornecedores($request)
    {

        $fields = $request->get('');

        $fields_expected = [
            'condicao' => [
                'type' => 'string',
                'required' => false
            ],
            'limite' => [
                'type' => 'int',
                'required' => false
            ] ,
            'pagina' => [
                'type' => 'int',
                'required' => false
            ],
        ];

        $return =  valida_campos($fields_expected, $fields);

        if ($return['success'] === true) {
            $data = Fornecedores::get_fornecedores($return['data']);

            if (sizeof($data) > 0) {
                $response =  [
                    "status" => 1,
                    "message" => "Fornecedor encontradas com sucesso",
                    "data" => $data
                ];
            } else {
                $response = [
                    "status" => 2,
                    "message" => "Nenhuma fornecedor encontrado"
                ];
            }

            die(json_encode($response));

        } else {
            http_response_code($return['status']);
            die(json_encode($return));
        }
    }

    

    public static function excluir_fornecedor($request)
    {

        $fields = $request->get('');


        $fields_expected = [
            'id_fornecedor' => [
                'type' => 'int',
                'required' => true
            ]
        ];

        $return = valida_campos($fields_expected, $fields);

        if ($return['success'] === true) {
            $response = Fornecedores::excluir_fornecedor($return['data']);
            die(json_encode($response));

        } else {
            http_response_code($return['status']);
            die(json_encode($return));
        }
    }


    public static function editar_fornecedor($request)
    {

        $fields = $request->post('');

        $fields_expected = [
            'id' =>[
                'type' => 'int',
                'required' => true
            ],
            'razao_social' => [
                'type' => 'string',
                'required' => true
            ],
            'nome_fantasia' => [
                'type' => 'string',
                'required' => true
            ],
            'cnpj' => [
                'type' => 'string',
                'required' => true
            ] ,
            'inscricao_estadual' => [
                'type' => 'string',
                'required' => false
            ],
            'cep' => [
                'type' => 'string',
                'required' => true
            ],
            'logradouro' => [
                'type' => 'string',
                'required' => false
            ],
            'numero' => [
                'type' => 'string',
                'required' => false
            ],
            'bairro' => [
                'type' => 'string',
                'required' => false
            ],
            'cidade' => [
                'type' => 'string',
                'required' => false
            ],
            'uf' => [
                'type' => 'string',
                'required' => false
            ],
            'contato' => [
                'type' => 'string',
                'required' => false
            ],
            'fone' => [
                'type' => 'string',
                'required' => false
            ],
            'emails' => [
                'type' => 'string',
                'required' => false
            ],
            'categoria' => [
                'type' => 'string',
                'required' => true
            ],
            'obs' => [
                'type' => 'string',
                'required' => false
            ],
            
        ];

        $return =  valida_campos($fields_expected, $fields);

        if ($return['success'] === true) {
            $response = Fornecedores::editar_fornecedor($return['data']);
            die(json_encode($response));

        } else {
            http_response_code($return['status']);
            die(json_encode($return));
        }
    }

    public static function cadastrar_fornecedor($request)
    {

        $fields = $request->post('');

        //die(json_encode($fields));

        $fields_expected = [
            'razao_social' => [
                'type' => 'string',
                'required' => true
            ],
            'nome_fantasia' => [
                'type' => 'string',
                'required' => true
            ],
            'cnpj' => [
                'type' => 'string',
                'required' => true
            ] ,
            'inscricao_estadual' => [
                'type' => 'string',
                'required' => false
            ],
            'cep' => [
                'type' => 'string',
                'required' => true
            ],
            'logradouro' => [
                'type' => 'string',
                'required' => false
            ],
            'numero' => [
                'type' => 'string',
                'required' => false
            ],
            'bairro' => [
                'type' => 'string',
                'required' => false
            ],
            'cidade' => [
                'type' => 'string',
                'required' => false
            ],
            'uf' => [
                'type' => 'string',
                'required' => false
            ],
            'contato' => [
                'type' => 'string',
                'required' => false
            ],
            'fone' => [
                'type' => 'string',
                'required' => false
            ],
            'emails' => [
                'type' => 'string',
                'required' => false
            ],
            'categoria' => [
                'type' => 'string',
                'required' => false
            ],
            'obs' => [
                'type' => 'string',
                'required' => false
            ],
            
        ];

        $return =  valida_campos($fields_expected, $fields);

        if ($return['success'] === true) {
            $response = Fornecedores::cadastrar_fornecedor($return['data']);
            die(json_encode($response));

        } else {
            http_response_code($return['status']);
            die(json_encode($return));
        }
    }

    
}

