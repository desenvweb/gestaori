<?php

include_once dirname(__FILE__) . "/../models/Projeto.php";

class ProjetosController
{

    public static function cadastrar_projeto($fields = [])
    {
        $fields['id_status'] = 4;
        $fields['desconta_horas'] = 'N';
        $fields = array_filter($fields);
        
        try {
            $db = getDB();

            $query = fieldsToInsertQuery($fields);
            $sql = "INSERT INTO projeto ({$query[0]}) VALUES ({$query[1]})";
            $stmt = $db->prepare($sql);

            $int = ['id_empresa', 'id_responsavel', 'id_executante', 'id_comercial'];
            foreach ($fields as $key => &$value) {
                $param = array_search($key, $int) ? PDO::PARAM_INT : PDO::PARAM_STR;
                $stmt->bindParam(':' . $key, $value, $param);
            }

            $stmt->execute();

            die(json_encode(array(
                "status" => 1,
                "message" => "Projeto cadastrado com sucesso"
            )));

            $db = null;
        } catch (PDOException $e) {
            $error = '{"error":{"text":' . $e->getMessage() . '}}';
            die(json_encode(array(
                "status" => 500,
                "error" => $error,
                "message" => "Erro no servidor"
            )));
        }
    }

    public static function editar_projeto($fields = [])
    {

        $id = $fields['id'];
        unset($fields['id']);

        $fields['desconta_horas'] = $fields['desconta_horas'] ? 'S' : 'N';

        $query =fieldsToEditQuery($fields);

        try {
            $db = getDB();

            $projeto = Projeto::get_projeto($id);


            if($projeto){
             //Verifica se o cliente tem horas suficientes para o projeto
            if($projeto[0]->desconta_horas === 'N'){
                $tempo_restante_contrato = HorasTrab::horas_restantes_contrato($fields['id_cliente']);
                $min_restante = convert_hhmm_in_minutes($tempo_restante_contrato['hhmm_restantes']);

                if ($fields['desconta_horas'] == 'S' && $min_restante < ($fields['total_horas'] * 60)) {
                    die(json_encode(array(
                        "status" => 4,
                        "message" => "O Cliente não tem horas suficiente para esse projeto."
                    )));
                }
            }

            $sql = "UPDATE projeto SET {$query} WHERE id = :id";

            $stmt = $db->prepare($sql);

            $int = ['id_responsavel', 'id_responsavel', 'id_executante', 'id', 'id_cliente'];

            foreach ($fields as $key => &$value) {
                $param = array_search($key, $int) ? PDO::PARAM_INT : PDO::PARAM_STR;
                $stmt->bindParam(':' . $key, $value, $param);
            }

            $stmt->bindParam(':id', $id);

            $stmt->execute();

            die(json_encode(array(
                "status" => 1,
                "message" => "Projeto editado com sucesso"
            )));
        }else {
            die(json_encode(array(
                "status" => 2,
                "message" => "Nenhum projeto encontrado"
            )));
        }

            $db = null;
        } catch (PDOException $e) {
            $error = '{"error":{"text":' . $e->getMessage() . '}}';
            die(json_encode(array(
                "status" => 500,
                "error" => $error,
                "message" => "Erro no servidor"
            )));
        }
    }

    public static function excluir_projeto($fields = [])
    {
        try {
            $db = getDB();

            $verifica = Projeto::get_projeto($fields['id']);

            if ($verifica) {

                $sql = "DELETE FROM projeto
                        WHERE id=:id;";
                $stmt = $db->prepare($sql);

                foreach ($fields as $key => &$value) {
                    $stmt->bindParam(':' . $key, $value);
                }

                $stmt->execute();

                die(json_encode(array(
                    "status" => 1,
                    "message" => "Projeto excluido com sucesso"
                )));
            } else {
                die(json_encode(array(
                    "status" => 2,
                    "message" => "Nenhum projeto encontrado"
                )));
            }

            $db = null;
        } catch (PDOException $e) {
            $error = '{"error":{"text":' . $e->getMessage() . '}}';
            die(json_encode(array(
                "status" => 500,
                "error" => $error,
                "message" => "Erro no servidor"
            )));
        }
    }

    public static function get_projetos(object $request)
    {
        $fields = $request->get('');

        $fields_expected = [
            'condicao' => [
                'type' => 'string',
                'required' => false
            ],
            'pagina' => [
                'type' => 'int',
                'required' => false
            ],
            'limite' => [
                'type' => 'int',
                'required' => false
            ]
        ];

        $return = valida_campos($fields_expected, $fields);

        if ($return['success'] === true) {
            $data = Projeto::get_projetos($return['data']);
            
            if (sizeof($data) > 0) {
                die(json_encode(array(
                    "status" => 1,
                    "message" => "Projeto(s) encontrados",
                    "data" => $data
                )));
            } else {
                die(json_encode(array(
                    "status" => 2,
                    "message" => "Nenhum projeto encontrado"
                )));
            }

        } else {
            http_response_code($return['status']);
            die(json_encode($return));
        }
    }


    public static function get_projetos_empresa($fields = [])
    {
        $id_cliente = $fields['id_cliente'];

        try {
            $db = getDB();

            $sql = "SELECT p.id, p.total_horas, p.prev_termino, p.descricao, e.nome_fantasia as cliente, 
                    u.nome as responsavel, ua.nome as comercial, ub.nome as executante, p.nome FROM projeto as p
                    LEFT JOIN empresas as e ON e.id = p.id_empresa
                    LEFT JOIN usuario as u ON u.id = p.id_responsavel
                    LEFT JOIN usuario as ua ON ua.id = p.id_comercial
                    LEFT JOIN usuario as ub ON ub.id = p.id_executante
                    WHERE id_cliente = :id";

            $stmt = $db->prepare($sql);
            $stmt->bindParam('id', $id_cliente, PDO::PARAM_INT);
            $stmt->execute();
            $data = $stmt->fetchAll(PDO::FETCH_OBJ);

            if (sizeof($data) > 0) {
                die(json_encode(array(
                    "status" => 1,
                    "message" => "Projeto(s) encontrados",
                    "data" => $data
                )));
            } else {
                die(json_encode(array(
                    "status" => 2,
                    "message" => "Nenhum projeto encontrado"
                )));
            }

            $db = null;
        } catch (PDOException $e) {
            $error = '{"error":{"text":' . $e->getMessage() . '}}';
            die(json_encode(array(
                "status" => 500,
                "error" => $error,
                "message" => "Erro no servidor"
            )));
        }
    }

    public static function lancamentos_projeto($fields = [])
    {
        $id_projeto = $fields['id_projeto'];

        try {
            $db = getDB();

            $sql = "SELECT h.id, h.dt_inicio, h.dt_fim, h.diff_horas, h.diff_minutos, h.descricao,
                    h.ticket, h.id_projeto, a.nome as atividade, u.nome as colaborador FROM horastrab as h
                    LEFT JOIN usuario as u ON u.id = h.id_usuario
                    LEFT JOIN atividade as a ON a.id = h.id_atividade
                    WHERE id_projeto = :id";

            $stmt = $db->prepare($sql);
            $stmt->bindParam('id', $id_projeto, PDO::PARAM_INT);
            $stmt->execute();
            $data = $stmt->fetchAll(PDO::FETCH_OBJ);

            if (sizeof($data) > 0) {
                die(json_encode(array(
                    "status" => 1,
                    "message" => "Lançamentos encontrados",
                    "data" => $data
                )));
            } else {
                die(json_encode(array(
                    "status" => 2,
                    "message" => "Nenhum lançamento nesse projeto"
                )));
            }

            $db = null;
        } catch (PDOException $e) {
            $error = '{"error":{"text":' . $e->getMessage() . '}}';
            die(json_encode(array(
                "status" => 500,
                "error" => $error,
                "message" => "Erro no servidor"
            )));
        }
    }

    public static function get_projeto($fields = [])
    {
        try {
            $db = getDB();

            $sql = "SELECT p.id, p.total_horas, p.prev_termino, p.descricao, e.nome_fantasia as cliente,
                    u.nome as responsavel, ua.nome as comercial, ub.nome as executante, p.nome, s.nome as status,
                    p.id_responsavel, p.id_comercial, p.id_executante, p.id_empresa FROM projeto as p
                    LEFT JOIN empresas as e ON e.id = p.id_empresa
                    LEFT JOIN usuario as u ON u.id = p.id_responsavel
                    LEFT JOIN usuario as ua ON ua.id = p.id_comercial
                    LEFT JOIN usuario as ub ON ub.id = p.id_executante
                    LEFT JOIN status as s ON s.id = p.id_status
                    WHERE p.id = :ID";

            $stmt = $db->prepare($sql);
            $stmt->bindParam("ID", $fields['id_projeto']);
            $stmt->execute();
            $data = $stmt->fetchAll(PDO::FETCH_OBJ);

            if (sizeof($data) > 0) {
                die(json_encode(array(
                    "status" => 1,
                    "message" => "Projeto encontrados",
                    "data" => $data
                )));
            } else {
                die(json_encode(array(
                    "status" => 2,
                    "message" => "Nenhum projeto encontrado"
                )));
            }

            $db = null;
        } catch (PDOException $e) {
            $error = '{"error":{"text":' . $e->getMessage() . '}}';
            die(json_encode(array(
                "status" => 500,
                "error" => $error,
                "message" => "Erro no servidor"
            )));
        }
    }
}
