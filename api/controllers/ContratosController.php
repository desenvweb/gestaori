<?php



class ContratosController
{

    public static function cadastrar(object $request, object $requester)
    {
        $fields = $request->post('');

        $fields['id_cadastrante'] = $requester->ID_USUARIO;

        $fields_expected = [
            'dt_vigenciafinal' => [
                'type' => 'string',
                'required' => true
            ],
            'dt_vigenciainicial' => [
                'type' => 'string',
                'required' => true
            ],
            'horas_totais' => [
                'type' => 'int',
                'required' => false
            ],
            'id_cadastrante' => [
                'type' => 'int',
                'required' => true
            ],
            'id_empresa' => [
                'type' => 'int',
                'required' => true
            ],
            'indice' => [
                'type' => 'string',
                'required' => true
            ],
            'multa' => [
                'type' => 'string',
                'required' => true
            ],
            'obs' => [
                'type' => 'string',
                'required' => false
            ],
            'tipo_contrato' => [
                'type' => 'string',
                'required' => true
            ],
            'valor_total' => [
                'type' => 'float',
                'required' => true
            ],
            'empresa_contratada' => [
                'type' => 'string',
                'required' => true
            ],
            'periodicidade_pagamento' => [
                'type' => 'string',
                'required' => true
            ],
            'usuarios_saas' => [
                'type' => 'int',
                'required' => false
            ],
            'dt_proximoReajuste' => [
                'type' => 'string',
                'required' => false
            ]
        ];

        $return = valida_campos($fields_expected, $fields);

        if ($return['success'] === true) {
            $response = Contratos::cadastrar($return['data']);
            http_response_code($response['http']);
            die(json_encode($response));
        } else {
            http_response_code($return['status']);
            die(json_encode($return));
        }
    }

    public static function cadastrar_contrato_faturar(object $request, object $requester)
    {
        $fields = $request->post('');


        $fields_expected = [
            'id_cadastrante' => [
                'type' => 'integer',
                'required' => true
            ],
            'razao_social' => [
                'type' => 'string',
                'required' => true
            ],
            'nome_fantasia' => [
                'type' => 'string',
                'required' => true
            ],
            'cnpj' => [
                'type' => 'string',
                'required' => true
            ] ,
            'inscricao_estadual' => [
                'type' => 'string',
                'required' => false
            ],
            'cep' => [
                'type' => 'string',
                'required' => false
            ],
            'logradouro' => [
                'type' => 'string',
                'required' => false
            ],
            'numero' => [
                'type' => 'string',
                'required' => false
            ],
            'bairro' => [
                'type' => 'string',
                'required' => false
            ],
            'cidade' => [
                'type' => 'string',
                'required' => false
            ],
            'uf' => [
                'type' => 'string',
                'required' => false
            ],
            'contato' => [
                'type' => 'string',
                'required' => false
            ],
            'fone' => [
                'type' => 'string',
                'required' => false
            ],
            'email' => [
                'type' => 'string',
                'required' => false
            ],
            'tipo_contrato' => [
                'type' => 'string',
                'required' => true
            ],
            'dt_faturamento' => [
                'type' => 'string',
                'required' => true
            ],
            'horas_totais' => [
                'type' => 'int',
                'required' => false
            ],
            'usuarios_saas' => [
                'type' => 'int',
                'required' => false
            ],
            'valor_total' => [
                'type' => 'float',
                'required' => true
            ],
            'multa' => [
                'type' => 'string',
                'required' => true
            ],
            'empresa_contratada' => [
                'type' => 'string',
                'required' => true
            ],
            'indice' => [
                'type' => 'string',
                'required' => true
            ],
            'periodicidade_pagamento' => [
                'type' => 'string',
                'required' => true
            ],
            'obs' => [
                'type' => 'string',
                'required' => false
            ]
        ];

        $return = valida_campos($fields_expected, $fields);

        if ($return['success'] === true) {
            $response = Contratos::cadastrar_contrato_faturar($return['data']);
            http_response_code($response['http']);
            die(json_encode($response));
        } else {
            http_response_code($return['status']);
            die(json_encode($return));
        }
    }
    
    public static function editar(object $request)
    {
        $fields = $request->post('');

        $fields_expected = [
            'id_contrato' => [
                'type' => 'int',
                'required' => true
            ],
            'dt_vigenciafinal' => [
                'type' => 'string',
                'required' => true
            ],
            'dt_vigenciainicial' => [
                'type' => 'string',
                'required' => true
            ],
            'horas_totais' => [
                'type' => 'int',
                'required' => false
            ],
            'horas_mes' => [
                'type' => 'int',
                'required' => false
            ],
            'indice' => [
                'type' => 'string',
                'required' => true
            ],
            'multa' => [
                'type' => 'string',
                'required' => true
            ],
            'obs' => [
                'type' => 'string',
                'required' => false
            ],
            'valor_total' => [
                'type' => 'float',
                'required' => true
            ],
            'empresa_contratada' => [
                'type' => 'string',
                'required' => true
            ],
            'periodicidade_pagamento' => [
                'type' => 'string',
                'required' => true
            ],
            'usuarios_saas' => [
                'type' => 'int',
                'required' => false
            ],
            'tipo_contrato' => [
                'type' => 'string',
                'required' => true
            ],
            'dt_proximoReajuste' => [
                'type' => 'string',
                'required' => false
            ]
        ];

        $return = valida_campos($fields_expected, $fields);

        if ($return['success'] === true) {
            $response = Contratos::editar($return['data']);
            http_response_code($response['http']);
            die(json_encode($response));
        } else {
            http_response_code($return['status']);
            die(json_encode($return));
        }
    }

    public static function editar_contrato_faturar(object $request)
    {
        $fields = $request->post('');


        $fields_expected = [
            'id' => [
                'type' => 'integer',
                'required' => true
            ],
            'id_cadastrante' => [
                'type' => 'integer',
                'required' => true
            ],
            'status' => [
                'type' => 'integer',
                'required' => true
            ],
            'razao_social' => [
                'type' => 'string',
                'required' => false
            ],
            'nome_fantasia' => [
                'type' => 'string',
                'required' => false
            ],
            'cnpj' => [
                'type' => 'string',
                'required' => false
            ],
            'inscricao_estadual' => [
                'type' => 'string',
                'required' => false
            ],
            'cep' => [
                'type' => 'string',
                'required' => false
            ],
            'logradouro' => [
                'type' => 'string',
                'required' => false
            ],
            'numero' => [
                'type' => 'string',
                'required' => false
            ],
            'bairro' => [
                'type' => 'string',
                'required' => false
            ],
            'cidade' => [
                'type' => 'string',
                'required' => false
            ],
            'uf' => [
                'type' => 'string',
                'required' => false
            ],
            'contato' => [
                'type' => 'string',
                'required' => false
            ],
            'fone' => [
                'type' => 'string',
                'required' => false
            ],
            'email' => [
                'type' => 'string',
                'required' => false
            ],
            'tipo_contrato' => [
                'type' => 'string',
                'required' => false
            ],
            'dt_faturamento' => [
                'type' => 'string',
                'required' => false
            ],
            'horas_totais' => [
                'type' => 'int',
                'required' => false
            ],
            'usuarios_saas' => [
                'type' => 'int',
                'required' => false
            ],
            'valor_total' => [
                'type' => 'float',
                'required' => false
            ],
            'multa' => [
                'type' => 'string',
                'required' => false
            ],
            'empresa_contratada' => [
                'type' => 'string',
                'required' => false
            ],
            'indice' => [
                'type' => 'string',
                'required' => false
            ],
            'periodicidade_pagamento' => [
                'type' => 'string',
                'required' => false
            ],
            'obs' => [
                'type' => 'string',
                'required' => false
            ],
        ];

        $return = valida_campos($fields_expected, $fields);

        if ($return['success'] === true) {
            $response = Contratos::editar_contrato_faturar($return['data']);
            http_response_code($response['http']);
            die(json_encode($response));
        } else {
            http_response_code($return['status']);
            die(json_encode($return));
        }
    }

    public static function get_contrato_ativo($request)
    {
        $fields = $request->get('');

        $fields_expected = [
            'id_empresa' => [
                'type' => 'int',
                'required' => true
            ]
        ];

        $return = valida_campos($fields_expected, $fields);

        if ($return['success'] === true) {
            $response = Contratos::get_contrato_ativo($return['data']);
            die(json_encode($response));
        } else {
            http_response_code($return['status']);
            die(json_encode($return));
        }
    }


    public static function get_contratos_faturar()
    {
        $response = Contratos::get_contratos_faturar();
        die(json_encode($response));
    }
    

    public static function verifica_contrato(){

       Contratos::verificar_contratos();
    }


    public static function excluir($request)
    {
        $fields = $request->get('');

        $fields_expected = [
            'id_contrato' => [
                'type' => 'int',
                'required' => true
            ]
        ];

        $return = valida_campos($fields_expected, $fields);

        if ($return['success'] === true) {
            $response = Contratos::excluir($return['data']);
            die(json_encode($response));
        } else {
            http_response_code($return['status']);
            die(json_encode($return));
        }
    }

    public static function excluir_contrato_faturar($request)
    {
        $fields = $request->get('');

        $fields_expected = [
            'id' => [
                'type' => 'int',
                'required' => true
            ]
        ];

        $return = valida_campos($fields_expected, $fields);

        if ($return['success'] === true) {
            $response = Contratos::excluir_contrato_faturar($return['data']);
            die(json_encode($response));
        } else {
            http_response_code($return['status']);
            die(json_encode($return));
        }
    }
}
