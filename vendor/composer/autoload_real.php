<?php

// autoload_real.php @generated by Composer

class ComposerAutoloaderInit0b380323c8bb5fb5c00d9fa849d8212c
{
    private static $loader;

    public static function loadClassLoader($class)
    {
        if ('Composer\Autoload\ClassLoader' === $class) {
            require __DIR__ . '/ClassLoader.php';
        }
    }

    /**
     * @return \Composer\Autoload\ClassLoader
     */
    public static function getLoader()
    {
        if (null !== self::$loader) {
            return self::$loader;
        }

        require __DIR__ . '/platform_check.php';

        spl_autoload_register(array('ComposerAutoloaderInit0b380323c8bb5fb5c00d9fa849d8212c', 'loadClassLoader'), true, true);
        self::$loader = $loader = new \Composer\Autoload\ClassLoader(\dirname(__DIR__));
        spl_autoload_unregister(array('ComposerAutoloaderInit0b380323c8bb5fb5c00d9fa849d8212c', 'loadClassLoader'));

        require __DIR__ . '/autoload_static.php';
        \Composer\Autoload\ComposerStaticInit0b380323c8bb5fb5c00d9fa849d8212c::getInitializer($loader)();

        $loader->register(true);

        return $loader;
    }
}
